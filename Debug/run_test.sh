DIRECTORY="/home/igor/Documents/git/srcCCO3D_Newtonian-V03-13-11/Res/"

FILES=("TreeNterm5_S1" "TreeNterm10_S1" "TreeNterm20_S1" "TreeNterm50_S1" "TreeNterm100_S1")

for IT in {0,1,2,3,4}

do

##
#PARTE 1 CARREGAR ELEMENTO
##
./INGU element load vtk ${DIRECTORY}${FILES[$IT]}.vtk ARTERY_TREE el_t5
##
#PARTE 2 - ESCALAR ELEMENTO
##
./INGU element scale el_t5 POINT points_coordinates 0.1
./INGU element scale el_t5 CELL RADIUS 0.1
##
#PARTE 2 - SETAR DENSIDADE E VISCOSIDADE
##
./INGU element set_all_field el_t5 CELL VISCOSITY 0.0449
./INGU element set_all_field el_t5 CELL DENSITY 1.134
./INGU element set_all_field el_t5 CELL YOUNG_MODULUS 10000000
##
#PARTE 3 - CRIAR WISEOBJECT
##
./INGU object create obj_t5 el_t5
##
#PARTE 4 - ADICIONAR FÁBRICA DE ITERAÇÃO
##
./INGU object iteration_factories set obj_t5 DUAN_AND_ZAMIR
##
#PARTE 5 - ITERAR WISEOBJECT
##
./INGU object set obj_t5
./INGU object go obj_t5
##
#PARTE 6 - SALVAR OBJETO
##
./INGU object save_vtk obj_t5 ${DIRECTORY}${FILES[$IT]}_ITERATED.vtk
##
#PARTE 7 - LIMPAR AMBIENTE
##
./INGU object delete obj_t5
./INGU element delete el_t5

done
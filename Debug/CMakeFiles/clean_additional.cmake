# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "CMakeFiles/IGU_autogen.dir/AutogenUsed.txt"
  "CMakeFiles/IGU_autogen.dir/ParseCache.txt"
  "CMakeFiles/INGU_autogen.dir/AutogenUsed.txt"
  "CMakeFiles/INGU_autogen.dir/ParseCache.txt"
  "CMakeFiles/PERIGU_autogen.dir/AutogenUsed.txt"
  "CMakeFiles/PERIGU_autogen.dir/ParseCache.txt"
  "IGU_autogen"
  "INGU_autogen"
  "PERIGU_autogen"
  )
endif()

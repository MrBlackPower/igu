IGU

/*************************************
		HOW TO COMPILE IN UBUNTU
*************************************/

1. Install QT:

	a) Run :
		wget http://download.qt.io/official_releases/qt/5.12/5.12.2/qt-opensource-linux-x64-5.12.2.run

		chmod +x qt-opensource-linux-x64-5.12.2.run
	
		./qt-opensource-linux-x64-5.12.2.run
		
	OR
	
		sudo apt-get install qtbase5-dev
		sudo apt-get install qtdeclarative5-dev

	b)Install qt following the wizard (QT Creator Optional).

2.Install Secondary Liraries

	c) Run :
		sudo apt-get install build-essential
		sudo apt-get install libfontconfig1
		sudo apt-get install mesa-common-dev
		sudo apt-get install libglu1-mesa-dev -y
		sudo apt-get install freeglut3-dev
		sudo apt-get install libflann-dev
3.Clone Repository

	git clone https://MrBlackPower@bitbucket.org/MrBlackPower/igu.git

/**** Using QT Creator
*************************************/

4. Open Project
	
	a) select file "igu/Projects/Pressure_Peaking.pro" (or "igu/Projects/CMakeLists.txt" for CMAKE project)

	b) Configure Project according to libraries found on compile. (AUTOMATED BY Qt Configuration)

5.Configure Project to Compile CCO Core (OPTIONAL)

	a) Copy CCO_Stub content to the location chosen to compile the project.

	b) Add Make steps to your project:
		-f ArterialTreeCore_2D.mak
		-f ArterialTreeCore_3D.mak

	  Or Run the folloqing in the same directory:
		make -f ArterialTreeCore_2D.mak
		make -f ArterialTreeCore_3D.mak

	The first will compile CCO libraris every time the project is compiled, the latter only once.
	
/**** Using Pure QMAKE
*************************************/		

5. Choose directory to compile into, then run:

	qmake -makefile /path-to-project/igu/Projects/Pressure_Peaking.pro

6.  Compile the project:

	cmake --build . --taget all

7. Compile CCO libraries (OPTIONAL)

	a) Copy CCO_Stub content to the location chosen to compile the project. 

	b) Run:

		make -f ArterialTreeCore_2D.mak
		make -f ArterialTreeCore_3D.mak

/**** Using Pure CMAKE
*************************************/		

5. Choose directory to compile into, then run:

	cmake /path-to-project/igu/Projects/

6.  Compile the project:

	cmake --build . --taget all

7. Compile CCO libraries (OPTIONAL)

	a) Copy CCO_Stub content to the location chosen to compile the project. 

	b) Run:

		make -f ArterialTreeCore_2D.mak
		make -f ArterialTreeCore_3D.mak

/*************************OTHER*****/

Caso o erro "cannot find -lGL" permaneça, rode os seguintes comandos:

		sudo rm /usr/lib/x86_64-linux-gnu/libGL.so
		sudo ln -s /usr/lib/libGL.so.1 /usr/lib/x86_64-linux-gnu/libGL.so


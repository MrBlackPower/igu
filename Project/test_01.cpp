#include "model/wise/point.h"
#include "model/wise/colorbar.h"
#include "model/wise/wisestructure.h"
#include "model/wise/graphicelement.h"
#include "model/wise/graphicobject.h"

#include "helper/igumath.h"
#include "helper/igufile.h"

#include "model/wiseobject/wisearterytree.h"
#include "model/wiseobject/wiseartery.h"
#include "model/wiseobject/wisepoly.h"
#include "model/wiseobject/wisemesh.h"
#include "model/wiseobject/wisemesh.h"

#include <QString>

#include <iostream>
#include <vector>

using namespace std;

using namespace wisepoint;

int args_size;
vector<QString> args;

int main(int argc, char *argv[])
{
    args_size = argc;

    cout << "[TEST_01] TES #01 TESTING POINTS" << endl;
    cout << "[TEST_01] TES #01 READ " << args_size << endl;

    for(int i = 0; i < args_size; i++){

        args.push_back(argv[i]);

        cout << i << ":" << argv[i] << endl;
    }

    point p;

    igumath igu;

    return 0;
}

#ifndef LFGRAPHIC_H
#define LFGRAPHIC_H

#include "../graphic2d.h"
#include "staticgraphic.h"

struct LFGraphicStartParams{
    double a = 0.0;
    double b = 2 * PI;
    int n = 200;
    int example = 0;
    double cfl = 0.25;
    double K = 1.0;
    double dx = (double)(2 * PI) / 199;
    vector<double> last_data;
};

class LFGraphic : public Graphic2D
{
public:
    LFGraphic(QString name, int example = 0, SmartObject* parent = 0);
    LFGraphic(int example = 0, SmartObject* parent = 0);

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    bool setContextRange(QString field);

private:
    LFGraphicStartParams startParams;
    Error error;

    virtual double f(double u);

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    double iteration();
    bool initialize();
    bool updateContextVisualParameter(QString field, QString value);
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    DataType getLiveParameterDataType(QString name);
    void mouse_press_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool);
};

#endif // LFGRAPHIC_H

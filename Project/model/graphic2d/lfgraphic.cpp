#include "lfgraphic.h"

LFGraphic::LFGraphic(QString name, int example, SmartObject* parent) : Graphic2D(name,parent){
    error.absolute = 0.0;
    error.numel = N;
    startParams.example = example;
    setContextRange("Y-AXIS");
}

LFGraphic::LFGraphic(int example, SmartObject* parent) : Graphic2D(("KT GRAPHIC " + example),parent)
{
    error.absolute = 0.0;
    error.numel = N;
    startParams.example = example;
    setContextRange("Y-AXIS");
}

bool LFGraphic::initialize(){
    if(raw_data.size() < 2){
        switch (startParams.example) {
        case 0:
        {
            vector<point*> exact;
            Graphic2D* g;
            clearData();
            for(int i = 0; i < startParams.n; i++){
                double x = startParams.a + (startParams.dx * i);
                point* p = new point(0, x, MathHelper::sine(x * (180 / PI)));
                addData(p);
            }

            //SETS EXACT GRAPHIC
            for(int i = 0; i < EXACT_N; i++){
                double x = (double)startParams.a + (((double)(startParams.b - startParams.a) / (double)EXACT_N) * (double)i);
                point* p = new point(0, x, MathHelper::sine((x - time.modelTime + time.deltaT) * (180 / PI)));
                exact.push_back(p);
            }

            break;
        }
        case 1:
        {
            vector<point*> exact;
            Graphic2D* g;
            clearData();
            for(int i = 0; i < startParams.n; i++){
                double x = startParams.a + (startParams.dx * i);
                point* p = new point(0, x, 0.5 + MathHelper::sine(x * (180 / PI)));
                addData(p);
            }

            //SETS EXACT GRAPHIC
            if(N < EXACT_N){
                for(int i = 0; i < EXACT_N; i++){
                    double x = (double)startParams.a + (((double)(startParams.b - startParams.a) / (double)EXACT_N) * (double)i);
                    point* p = new point(0, x,  0.5 + MathHelper::sine(x * (180 / PI)));
                    exact.push_back(p);
                }

            }

            break;
        }
        case 2:
        {
            vector<point*> exact;
            Graphic2D* g;
            clearData();
            for(int i = 0; i < startParams.n; i++){
                double x = startParams.a + (startParams.dx * i);
                if(x > 0){
                    point* p = new point(0, x, 2.0);
                    addData(p);
                } else {
                    point* p = new point(0, x, -2.0);
                    addData(p);
                }
            }

            //SETS EXACT GRAPHIC
            if(N < EXACT_N){
                for(int i = 0; i < EXACT_N; i++){
                    double x = (double)startParams.a + (((double)(startParams.b - startParams.a) / (double)EXACT_N) * (double)i);
                    point* p;
                    if(x > 0){
                        p = new point(0, x, 2.0);
                    } else {
                        p = new point(0, x, -2.0);
                    }
                    exact.push_back(p);
                }
            }

            break;
        }
        default:
        {
            clearData();
            for(int i = 0; i < startParams.n; i++){
                double x = startParams.a + (startParams.dx * i);
                point* p = new point(0, x, MathHelper::sine(x * (180 / PI)));
                addData(p);
            }
            break;
        }
        }
    }

    point aux = *raw_data[0];
    double max_val = aux.Y();

    for(int i = 1; i < N; i++){
        aux = *raw_data[i];
        startParams.last_data.push_back(aux.Y());

        if(max_val < aux.Y())
            max_val = aux.Y();
    }

    time.deltaT = startParams.cfl * startParams.dx / max_val;

    return true;
}

double LFGraphic::iteration(){
    QElapsedTimer timer;
    timer.start();

    vector<double> u;
    for(int i = 0; i < N; i++)
        u.push_back(raw_data[i]->Y());

    vector<double> u_new;

    u_new.push_back(u[0]);
    for(int i = 1; i < N-1; i++){
        u_new.push_back((0.5 * (u[i+1] + u[i-1])) - ((startParams.K * time.deltaT /(2.0 * startParams.dx)) * (f(u[i+1]) - f(u[i-1]))));
    }
    u_new.push_back(u[(N-1)]);

    time.iterations ++;
    time.modelTime += time.deltaT;

    for(int i = 0; i < N; i++)
        raw_data[i]->Y(u_new[i]);

    startParams.last_data = u;

    error.absolute = 0.0;
    error.numel = N;

    for(int i = 0; i < N; i++){
        if(abs(u[i] - MathHelper::sine(raw_data[i]->X() - time.modelTime + time.deltaT)) > error.absolute)
            error.absolute = abs(u[i] - MathHelper::sine(raw_data[i]->X() - time.modelTime + time.deltaT));
    }

    return ((double)timer.elapsed()/1000);
}

void LFGraphic::mouse_press_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool){

}

void LFGraphic::mouse_move_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool){

}

vector<Field> LFGraphic::getStartParameters(){
    vector<Field> params;

   Field aux;
   aux.names = "EXAMPLE";
   aux.required = false;
   aux.stream = FIELD_IN;
   aux.type = integer;

    params.push_back(aux);

    return params;
}

vector<Field> LFGraphic::getContextVisualParameters(){
    vector<Field> params;

    return params;
}

vector<QString> LFGraphic::getRanges(){
    vector<QString> params;

    params.push_back("Y-AXIS");

    return params;
}

bool LFGraphic::updateStartParameter(QString field, QString value){
    if(field == "EXAMPLE"){
        startParams.example = value.toInt();
        return true;
    }

    return false;
}

vector<Field> getStartParameters(){
    vector<Field> params;

   Field aux;
   aux.names = "EXAMPLE";
   aux.required = false;
   aux.stream = FIELD_IN;
   aux.type = integer;

    params.push_back(aux);

    return params;
}

vector<QString> getContextVisualParameters(){
    vector<QString> params;

    return params;
}

vector<QString> getRanges(){
    vector<QString> params;

    params.push_back("Y-AXIS");

    return params;
}

double LFGraphic::f(double u){
    switch (startParams.example) {
    case 0:
        return u;
    case 1:
        return pow(u,2)/2;
        break;
    case 2:
        return ((pow(u,2) - 1)*(pow(u,2) - 4)/4.0);
    default:
        return u;
    }
}

bool LFGraphic::updateContextVisualParameter(QString field, QString value){
    return false;
}

QString LFGraphic::getLiveParameterList(){
    return QString("");
}

QString LFGraphic::getLiveParameter(QString name){
    return QString("");
}

DataType LFGraphic::getLiveParameterDataType(QString name){
    return undefined;
}

bool LFGraphic::setContextRange(QString field){
    if(field == "Y-AXIS"){
        range.max = limit.top;
        range.min = limit.bottom;
        range.parameter = "Y-AXIS";
        return true;
    }
    return false;
}

#ifndef LIVEPARAMGRAPHIC_H
#define LIVEPARAMGRAPHIC_H


#include "../graphic2d.h"
#include "../liveparameter.h"
#include "../../window/dialog/selectfromstringlistdialog.h"
#include <QEventLoop>
#include <QTimer>

#define TIMEOUT 12000

class LiveParamGraphic: public Graphic2D
{
public:
    LiveParamGraphic(GraphicObject* gObj, QString field ,QString name = "LIVE PARAM GRAPHIC", SmartObject* parent = NULL);
    LiveParamGraphic(int GID, QString field ,QString name = "LIVE PARAM GRAPHIC", SmartObject* parent = NULL);

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    bool setContextRange(QString field);

private:
    GraphicObject* master;
    QString field;
    LiveParameter* parameter;

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool initialize();
    double iteration();
    bool updateContextVisualParameter(QString field, QString value);
    DataType getLiveParameterDataType(QString name);
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    void mouse_press_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool);
};

#endif // LIVEPARAMGRAPHIC_H

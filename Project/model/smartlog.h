#ifndef SMARTLOG_H
#define SMARTLOG_H

#include <vector>
#include <QTextBrowser>
#include <QFile>
#include <QObject>
#include <QDateTime>
#include "smartobject.h"
#include "smartlogmessage.h"
#include "../helper/vtkfile.h"
#include "../helper/fileHelper.h"

#define MAX_BUFFER 100
#define MAX_SIZE 1000
#define LOG_PRINT_SIZE 20

#define DEFAULT_LOG "log.igu"

struct SmartLogBuffer{
    int buffer_size = 0;

    vector<QString> logs_filenames;
    vector<SmartLogMessage> logs;


    vector<QString> raws_filenames;
    vector<vector<QString>> raws;

    vector<QString> vtks_filenames;
    vector<VTKFile> vtks;
};

class SmartLog : public SmartObject
{
Q_OBJECT
public:
    SmartLog();
    ~SmartLog();

    void clean();
    void updateLogWindow(int lines = LOG_PRINT_SIZE);

    /********************************************************
    *  SMART OBJECT VIRTUAL METHODS                       *
    ********************************************************/
    vector<QString> print();
    vector<QString> raw();
    vector<QString> data();

public slots:
    void process();
    void addToBuffer(SmartLogMessage data, QString filename = "log.igu");
    void addToBuffer(VTKFile data, QString filename = "");
    void addToBuffer(vector<QString> data, QString filename = "log.igu");

    void finish();

signals:
    void updateLog(vector<QString> log);

    void finished();
    void error(QString err);

private:

    vector<SmartLogMessage> emit_log;

    int N;

    SmartLogBuffer buffer;

    vector<QFile*> files_open;

    vector<QString> files_open_name;

    QFile* record_file;

    bool toFinish;
};

#endif // SMARTLOG_H

#include "wisemeshfactory.h"

int WiseMeshFactory::last_id = 0;

WiseMeshFactory::WiseMeshFactory() : WiseElementFactory(DEFAULT_TYPE_NAME,DEFAULT_NAME)
{

}

WiseMeshFactory::~WiseMeshFactory()
{

}

unique_ptr<WiseElementFactory> WiseMeshFactory::clone_factory_aux(){
    return unique_ptr<WiseElementFactory>{new WiseMeshFactory()};
}

unique_ptr<WiseElement>  WiseMeshFactory::make(string name, string dna){
    last_id++;

    unique_ptr<WiseMesh> obj = unique_ptr<WiseMesh>{new WiseMesh{name,dna}};

    return move(obj);
}

unique_ptr<WiseElement> WiseMeshFactory::recreate(unique_ptr<WiseElement> ptr){
    vector<point> points;
    vector<double> value;
    vector<string> value_raw;

    //SETS PARAMETERS
    points = ptr->get_points();

    string v_name = "valor VALOR value VALUE";


    //VALUE - SET
    stringstream s1(v_name);

    while(value_raw.empty() && s1)
    {
        string aux;
        s1 >> aux;
        value_raw = ptr->get_point_data(aux);
    }

    if(points.size() < 4){
        return 0;
    }

    if(value_raw.size() < 4){
        return 0;
    }

    for(string s : value_raw){
        stringstream ss(s);
        double v = 0.0;

        ss >> v;

        value.push_back(v);
    }

    //ADDS POINTS
    WiseMesh* o = dynamic_cast<WiseMesh*>(ptr.release());

    pair<double,double> min = {0,0};
    pair<double,double> max = {0,0};
    for(int i = 0; i < points.size(); i++){
        if(i == 0){
            min.first = points.operator[](i).X();
            max.first = points.operator[](i).X();
            min.second = points.operator[](i).Y();
            max.second = points.operator[](i).Y();
        } else {
            if(points.operator[](i).X() < min.first)
                min.first = points.operator[](i).X();

            if(points.operator[](i).X() > max.first)
                max.first = points.operator[](i).X();

            if(points.operator[](i).Y() < min.second)
                min.second = points.operator[](i).Y();

            if(points.operator[](i).Y() > max.second)
                max.second = points.operator[](i).Y();
        }
    }

    //ONLY WORKS IF N = M
    int s = sqrt(points.size());
    o->set_grid(min,max,s);

    for(int k = 0; k  < points.size(); k++){
        int i = k/s;
        int j = k - (i*s);
        o->set_point(i,j,value.operator[](k));
    }

    o->set_status(HOT);

    o->update_structure();

    unique_ptr<WiseMesh> obj(o);

    return move(obj);
}

vector<string> WiseMeshFactory::examples_list(){
    vector<string> r;

    r.push_back("COSINOIDAL_SATURATION");

    r.push_back("COSINOIDAL_X_VELOCITY");

    r.push_back("COSINOIDAL_Y_VELOCITY");

    r.push_back("COSINOIDAL_FONT");

    return r;
}

unique_ptr<WiseElement> WiseMeshFactory::make_example(string example, string name){
    //SATURATION FOR THE COSINOIDAL EXAMPLE
    if(example == "COSINOIDAL_SATURATION"){
        last_id++;
        unique_ptr<WiseMesh> mesh = unique_ptr<WiseMesh>{new WiseMesh{name}};

        pair<double,double> min{-1,-1};
        pair<double,double> max{1,1};
        int n = 128;
        int m = 128;
        mesh->set_grid(min,max,n,m);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                double h1 = (max.first - min.first)/(n-1);
                double h2 = (max.second - min.second)/(m-1);
                double x = min.first + (i*h1);
                double y = min.second + (j*h2);
                double z = 2 * PI * igumath::cosine(PI * x) * igumath::cosine(PI * y);

                mesh->set_point(i,j,z);
            }
        }

        mesh->update_structure();

        return std::move(mesh);
    }

    //X-VELOCITY FOR THE COSINOIDAL EXAMPLE
    if(example == "COSINOIDAL_X_VELOCITY"){
        last_id++;
        unique_ptr<WiseMesh> mesh = unique_ptr<WiseMesh>{new WiseMesh{name}};

        pair<double,double> min{-1,-1};
        pair<double,double> max{1,1};
        int n = 128;
        int m = 128;
        mesh->set_grid(min,max,n,m);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                double h1 = (max.first - min.first)/(n-1);
                double h2 = (max.second - min.second)/(m-1);
                double x = min.first + (i*h1);
                double y = min.second + (j*h2);
                double z = -igumath::cosine(PI * x) * igumath::sine(PI * y);

                mesh->set_point(i,j,z);
            }
        }

        mesh->update_structure();

        return std::move(mesh);
    }

    //Y-VELOCITY FOR THE COSINOIDAL EXAMPLE
    if(example == "COSINOIDAL_Y_VELOCITY"){
        last_id++;
        unique_ptr<WiseMesh> mesh = unique_ptr<WiseMesh>{new WiseMesh{name}};

        pair<double,double> min{-1,-1};
        pair<double,double> max{1,1};
        int n = 128;
        int m = 128;
        mesh->set_grid(min,max,n,m);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                double h1 = (max.first - min.first)/(n-1);
                double h2 = (max.second - min.second)/(m-1);
                double x = min.first + (i*h1);
                double y = min.second + (j*h2);
                double z = igumath::sine(PI * x) * igumath::cosine(PI * y);

                mesh->set_point(i,j,z);
            }
        }

        mesh->update_structure();

        return std::move(mesh);
    }

    //EXACT FOR THE COSINOIDAL EXAMPLE
    if(example == "COSINOIDAL_FONT"){
        last_id++;
        unique_ptr<WiseMesh> mesh = unique_ptr<WiseMesh>{new WiseMesh{name}};

        pair<double,double> min{-1,-1};
        pair<double,double> max{1,1};
        int n = 128;
        int m = 128;
        mesh->set_grid(min,max,n,m);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                double z = 0;

                mesh->set_point(i,j,z);
            }
        }

        mesh->update_structure();

        return std::move(mesh);
    }

    return nullptr;
}

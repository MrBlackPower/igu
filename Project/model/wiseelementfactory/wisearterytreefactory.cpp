#include "wisearterytreefactory.h"

int WiseArteryTreeFactory::last_id = 0;

WiseArteryTreeFactory::WiseArteryTreeFactory() : WiseElementFactory(DEFAULT_TYPE_NAME,DEFAULT_NAME)
{

}

WiseArteryTreeFactory::~WiseArteryTreeFactory()
{

}

unique_ptr<WiseElementFactory> WiseArteryTreeFactory::clone_factory_aux(){
    return unique_ptr<WiseElementFactory>{new WiseArteryTreeFactory()};
}

unique_ptr<WiseElement>  WiseArteryTreeFactory::make(string name, string dna){
    last_id++;
    
    unique_ptr<WiseArteryTree> tree = unique_ptr<WiseArteryTree>{new WiseArteryTree{name,dna}};

    return std::move(tree);
}

unique_ptr<WiseElement> WiseArteryTreeFactory::recreate(unique_ptr<WiseElement> ptr){
    vector<point> points;
    vector<WiseLine> lines;
    vector<double> radius;
    vector<double> youngModulus;
    vector<double> density;
    vector<double> viscosity;
    vector<string> radius_raw;
    vector<string> youngModulus_raw;
    vector<string> density_raw;
    vector<string> viscosity_raw;

    //SETS PARAMETERS
    points = ptr->get_points();
    lines = ptr->get_lines();

    string r_name = "raio RAIO radius RADIUS";
    string E_name = "modulo_young MODULO_YOUNG young_modulus YOUNG_MODULUS ";
    string density_name = "densidade DENSIDADE density DENSITY";
    string viscosity_name = "viscosidade VISCOSIDADE viscosity VISCOSITY";


    //RADIUS - SET
    stringstream s1(r_name);

    while(radius_raw.empty() && s1)
    {
        string aux;
        s1 >> aux;
        radius_raw = ptr->get_cell_data(aux);
    }

    //YOUNG MODULUS - SET
    stringstream s2(E_name);

    while(youngModulus_raw.empty() && s2)
    {
        string aux;
        s2 >> aux;
        youngModulus_raw = ptr->get_cell_data(aux);
    }

    //DENSITY - SET
    stringstream s3(density_name);

    while(density_raw.empty() && s3)
    {
        string aux;
        s3 >> aux;
        density_raw = ptr->get_cell_data(aux);
    }

    //VISCOSITY - SET
    stringstream s4(viscosity_name);

    while(viscosity_raw.empty() && s4)
    {
        string aux;
        s4 >> aux;
        viscosity_raw = ptr->get_cell_data(aux);
    }

    //RADIUS
    if(radius_raw.empty()){
        return 0;
    }

    for(int i = 0; i < radius_raw.size(); i++){
        string aux = radius_raw[i];
        radius.push_back(QString(aux.data()).toDouble());
    }

    //YOUNG MODULUS
    if(!youngModulus_raw.empty()){
        for(int i = 0; i < youngModulus_raw.size(); i++){
            string aux = youngModulus_raw[i];
            youngModulus.push_back(QString(aux.data()).toDouble());
        }
    } else {
        for(int i = 0; i < lines.size(); i++){
            double aux = 10000000;
            youngModulus.push_back(aux);
        }
    }

    //DENSITY
    if(!density_raw.empty()){
        for(int i = 0; i < density_raw.size(); i++){
            string aux = density_raw[i];
            density.push_back(QString(aux.data()).toDouble());
        }
    } else {
        for(int i = 0; i < lines.size(); i++){
            double aux = 1.134;
            density.push_back(aux);
        }
    }

    //VISCOSITY
    if(!viscosity_raw.empty()){
        for(int i = 0; i < viscosity_raw.size(); i++){
            string aux = viscosity_raw[i];
            viscosity.push_back(QString(aux.data()).toDouble());
        }
    } else {
        for(int i = 0; i < lines.size(); i++){
            double aux = 0.96;
            viscosity.push_back(aux);
        }
    }

    if(points.size() < 2 || lines.empty()){
        return 0;
    }

    //OTHER DATA
    vector<string> alpha_raw         = ptr->get_cell_data("ALPHA");
    vector<string> wavespeed_raw     = ptr->get_cell_data("WAVE_SPEED");
    vector<string> h_raw             = ptr->get_cell_data("WALL_THICKNESS");
    vector<string> length_raw        = ptr->get_cell_data("LENGTH");
    vector<string> phi_raw           = ptr->get_cell_data("PHI");
    vector<string> w_raw             = ptr->get_cell_data("ANGULAR_FREQUENCY");

    //COMPLEX DATA
    vector<string> beta_abs_raw      = ptr->get_cell_data("BETA_ABS");
    vector<string> beta_real_raw     = ptr->get_cell_data("BETA_REAL");
    vector<string> beta_image_raw    = ptr->get_cell_data("BETA_IMAGE");

    vector<string> cv_abs_raw        = ptr->get_cell_data("VISCOUS_WAVE_SPEED_ABS");
    vector<string> cv_real_raw       = ptr->get_cell_data("VISCOUS_WAVE_SPEED_REAL");
    vector<string> cv_image_raw      = ptr->get_cell_data("VISCOUS_WAVE_SPEED_IMAGE");

    vector<string> Ec_abs_raw        = ptr->get_cell_data("VISCOUS_YOUNG_MODULUS_ABS");
    vector<string> Ec_real_raw       = ptr->get_cell_data("VISCOUS_YOUNG_MODULUS_REAL");
    vector<string> Ec_image_raw      = ptr->get_cell_data("VISCOUS_YOUNG_MODULUS_IMAGE");

    vector<string> flow_abs_raw      = ptr->get_cell_data("FLOW_ABS");
    vector<string> flow_real_raw     = ptr->get_cell_data("FLOW_REAL");
    vector<string> flow_image_raw    = ptr->get_cell_data("FLOW_IMAGE");

    vector<string> pressure_abs_raw        = ptr->get_cell_data("PRESSURE_ABS");
    vector<string> pressure_real_raw       = ptr->get_cell_data("PRESSURE_REAL");
    vector<string> pressure_image_raw      = ptr->get_cell_data("PRESSURE_IMAGE");

    vector<string> _pressure_abs_raw        = ptr->get_cell_data("MEDIUM_PRESSURE_ABS");
    vector<string> _pressure_real_raw       = ptr->get_cell_data("MEDIUM_PRESSURE_REAL");
    vector<string> _pressure_image_raw      = ptr->get_cell_data("MEDIUM_PRESSURE_IMAGE");

    vector<string> R_abs_raw        = ptr->get_cell_data("REFLECTION_COEFFICIENT_ABS");
    vector<string> R_real_raw       = ptr->get_cell_data("REFLECTION_COEFFICIENT_REAL");
    vector<string> R_image_raw      = ptr->get_cell_data("REFLECTION_COEFFICIENT_IMAGE");

    vector<string> Y_abs_raw        = ptr->get_cell_data("ADMITTANCE_ABS");
    vector<string> Y_real_raw       = ptr->get_cell_data("ADMITTANCE_REAL");
    vector<string> Y_image_raw      = ptr->get_cell_data("ADMITTANCE_IMAGE");

    vector<string> Ye_abs_raw        = ptr->get_cell_data("EFFECTIVE_ADMITTANCE_ABS");
    vector<string> Ye_real_raw       = ptr->get_cell_data("EFFECTIVE_ADMITTANCE_REAL");
    vector<string> Ye_image_raw      = ptr->get_cell_data("EFFECTIVE_ADMITTANCE_IMAGE");

    bool complete = !wavespeed_raw.empty() && (wavespeed_raw.size() == alpha_raw.size());
    complete = complete && (wavespeed_raw.size() == h_raw.size());
    complete = complete && (wavespeed_raw.size() == length_raw.size());
    complete = complete && (wavespeed_raw.size() == phi_raw.size());
    complete = complete && (wavespeed_raw.size() == w_raw.size());
    complete = complete && (wavespeed_raw.size() == beta_abs_raw.size());
    complete = complete && (wavespeed_raw.size() == beta_real_raw.size());
    complete = complete && (wavespeed_raw.size() == beta_image_raw.size());
    complete = complete && (wavespeed_raw.size() == Ec_abs_raw.size());
    complete = complete && (wavespeed_raw.size() == Ec_real_raw.size());
    complete = complete && (wavespeed_raw.size() == Ec_image_raw.size());
    complete = complete && (wavespeed_raw.size() == Ec_abs_raw.size());
    complete = complete && (wavespeed_raw.size() == Ec_real_raw.size());
    complete = complete && (wavespeed_raw.size() == Ec_image_raw.size());
    complete = complete && (wavespeed_raw.size() == flow_abs_raw.size());
    complete = complete && (wavespeed_raw.size() == flow_real_raw.size());
    complete = complete && (wavespeed_raw.size() == flow_image_raw.size());
    complete = complete && (wavespeed_raw.size() == pressure_abs_raw.size());
    complete = complete && (wavespeed_raw.size() == pressure_real_raw.size());
    complete = complete && (wavespeed_raw.size() == pressure_image_raw.size());
    complete = complete && (wavespeed_raw.size() == _pressure_abs_raw.size());
    complete = complete && (wavespeed_raw.size() == _pressure_real_raw.size());
    complete = complete && (wavespeed_raw.size() == _pressure_image_raw.size());
    complete = complete && (wavespeed_raw.size() == R_abs_raw.size());
    complete = complete && (wavespeed_raw.size() == R_real_raw.size());
    complete = complete && (wavespeed_raw.size() == R_image_raw.size());
    complete = complete && (wavespeed_raw.size() == Y_abs_raw.size());
    complete = complete && (wavespeed_raw.size() == Y_real_raw.size());
    complete = complete && (wavespeed_raw.size() == Y_image_raw.size());
    complete = complete && (wavespeed_raw.size() == Ye_abs_raw.size());
    complete = complete && (wavespeed_raw.size() == Ye_real_raw.size());
    complete = complete && (wavespeed_raw.size() == Ye_image_raw.size());

    //ADDS ARTERIES
    WiseArteryTree* tree = dynamic_cast<WiseArteryTree*>(ptr.release());
    for(int i = 0; i < lines.size(); i++){
        WiseLine l = lines[i];

        point a = points[(l.a)];
        point b = points[(l.b)];

        if(!complete){
            tree->add_node(a,b,radius[i],youngModulus[i],density[i],viscosity[i]);

        }else{
            WiseArtery::WiseArteryParams p;

            p.p              = b;

            p.alpha          = QString(alpha_raw[i].data()).toDouble();
            p.c              = QString(wavespeed_raw[i].data()).toDouble();
            p.h              = QString(alpha_raw[i].data()).toDouble();
            p.length         = igumath::euclidean_distance(&a,&b);
            p.phi            = QString(phi_raw[i].data()).toDouble();
            p.w              = QString(w_raw[i].data()).toDouble();

            p.beta_abs       = QString(beta_abs_raw[i].data()).toDouble();
            p.beta           = {QString(beta_real_raw[i].data()).toDouble(),QString(beta_image_raw[i].data()).toDouble()};

            p.cv_abs         = QString(cv_abs_raw[i].data()).toDouble();
            p.cv             = {QString(cv_real_raw[i].data()).toDouble(),QString(cv_image_raw[i].data()).toDouble()};

            p.Ec_abs         = QString(beta_abs_raw[i].data()).toDouble();
            p.Ec             = {QString(beta_real_raw[i].data()).toDouble(),QString(beta_image_raw[i].data()).toDouble()};

            p.flow_abs       = QString(cv_abs_raw[i].data()).toDouble();
            p.flow           = {QString(cv_real_raw[i].data()).toDouble(),QString(cv_image_raw[i].data()).toDouble()};

            p.pressure_abs   = QString(pressure_abs_raw[i].data()).toDouble();
            p.pressure       = {QString(pressure_real_raw[i].data()).toDouble(),QString(pressure_image_raw[i].data()).toDouble()};

            p._pressure_abs  = QString(_pressure_abs_raw[i].data()).toDouble();
            p._pressure      = {QString(_pressure_real_raw[i].data()).toDouble(),QString(_pressure_image_raw[i].data()).toDouble()};

            p.R_abs          = QString(cv_abs_raw[i].data()).toDouble();
            p.R              = {QString(cv_real_raw[i].data()).toDouble(),QString(cv_image_raw[i].data()).toDouble()};

            p._Y_abs         = QString(pressure_abs_raw[i].data()).toDouble();
            p._Y             = {QString(pressure_real_raw[i].data()).toDouble(),QString(pressure_image_raw[i].data()).toDouble()};

            p.Ye_abs         = QString(_pressure_abs_raw[i].data()).toDouble();
            p.Ye             = {QString(_pressure_real_raw[i].data()).toDouble(),QString(_pressure_image_raw[i].data()).toDouble()};

            //NECESSARY DATA
            p.r         = radius[i];
            p.E         = youngModulus[i];
            p.density   = density[i];
            p.viscosity = viscosity[i];

            tree->add_node(a,p);
        }
    }

    tree->set_status(HOT);

    //WAVESPEED             ADMITTANCE         REFLECTION_COEF
    //EFFECTIVE_ADMITTANCE  MEDIUM_PRESSURE    BETA
//    for(string s : wavespeed_raw)
//        tree->add_cell_data("WAVESPEED",s);
//    for(string s : beta_raw)
//        tree->add_cell_data("BETA",s);
//    for(string s : admittance_raw)
//        tree->add_cell_data("ADMITTANCE",s);
//    for(string s : ref_coef_raw)
//        tree->add_cell_data("REFLECTION_COEFFICIENT",s);
//    for(string s : Ye_raw)
//        tree->add_cell_data("EFFECTIVE_ADMITTANCE",s);
//    for(string s : _p_raw)
//        tree->add_cell_data("MEDIUM_PRESSURE",s);

    unique_ptr<WiseArteryTree> obj(tree);

    return move(obj);
}

vector<string> WiseArteryTreeFactory::examples_list(){
    vector<string> r;

    r.push_back("DUAN_AND_ZAMIR");

    r.push_back("SIMPLE_ONE");

    r.push_back("SIMPLE_TWO");

    return r;
}

unique_ptr<WiseElement> WiseArteryTreeFactory::make_example(string example, string name){
    //DUAN AND ZAMIR (1995) ARTERIAL TREE EXAMPLE
    if(example == "DUAN_AND_ZAMIR"){
        last_id++;

        unique_ptr<WiseArteryTree> tree = unique_ptr<WiseArteryTree>{new WiseArteryTree{name}};

        point p1(0,2,28);
        point p2(0,2,3);
        point p3(0,2,-8);
        point p4(0,-5,-17.746794);
        point p5(0,-8,-27.286186);
        point p6(0,9,-17.746794);
        point p7(0,12,-27.286186);

        tree->add_node(p1,p2,0.65,4800000,0.96,0.0385);

        tree->add_node(p2,p3,0.45,10000000,1.134,0.0449);

        tree->add_node(p3,p4,0.3,10000000,1.172,0.0472);

        tree->add_node(p4,p5,0.2,10000000,1.235,0.0494);

        tree->add_node(p3,p6,0.3,10000000,1.172,0.0472);

        tree->add_node(p6,p7,0.2,10000000,1.235,0.0494);

        tree->update_structure();

        return std::move(tree);
    }

    //SAMPLE TREE ONE
    if(example == "SIMPLE_ONE"){
        last_id++;

        unique_ptr<WiseArteryTree> tree = unique_ptr<WiseArteryTree>{new WiseArteryTree{name}};

        point p1(0,0,30);
        point p2(0,0,10);
        point p3(0,-5,0);
        point p4(0,5,0);

        tree->add_node(p1,p2,0.4,4800000,0.96,0.0385);

        tree->add_node(p2,p3,0.2828427125,10000000,1.134,0.0449);

        tree->add_node(p2,p4,0.2828427125,10000000,1.134,0.0449);

        tree->update_structure();

        return std::move(tree);
    }


    //SAMPLE TREE ONE
    if(example == "SIMPLE_TWO"){
        last_id++;

        unique_ptr<WiseArteryTree> tree = unique_ptr<WiseArteryTree>{new WiseArteryTree{name}};

        point p1(0,0,35);
        point p2(0,0,15);
        point p3(0,-5,5);
        point p4(0,5,5);
        point p5(0,3,0);
        point p6(0,7,0);

        tree->add_node(p1,p2,0.4,4800000,0.96,0.0385);

        tree->add_node(p2,p3,0.2828427125,10000000,1.134,0.0449);

        tree->add_node(p2,p4,0.2828427125,10000000,1.134,0.0449);

        tree->add_node(p4,p5,0.2,10000000,1.172,0.0472);

        tree->add_node(p4,p5,0.2,10000000,1.172,0.0472);

        tree->update_structure();

        return std::move(tree);
    }

    return 0;
}

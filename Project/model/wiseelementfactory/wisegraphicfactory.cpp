#include "wisegraphicfactory.h"

int WiseGraphicFactory::last_id = 0;

WiseGraphicFactory::WiseGraphicFactory() : WiseElementFactory(DEFAULT_TYPE_NAME,DEFAULT_NAME)
{

}

WiseGraphicFactory::~WiseGraphicFactory()
{

}

unique_ptr<WiseElementFactory> WiseGraphicFactory::clone_factory_aux(){
    return unique_ptr<WiseElementFactory>{new WiseGraphicFactory()};
}

unique_ptr<WiseElement>  WiseGraphicFactory::make(string name, string dna){
    last_id++;

    unique_ptr<WiseGraphic> obj = unique_ptr<WiseGraphic>{new WiseGraphic{name,dna}};

    return move(obj);
}

unique_ptr<WiseElement> WiseGraphicFactory::recreate(unique_ptr<WiseElement> ptr){
    vector<point> points;
    vector<double> value;
    vector<string> value_raw;

    //SETS PARAMETERS
    points = ptr->get_points();

    string v_name = "valor VALOR value VALUE";


    //VALUE - SET
    stringstream s1(v_name);

    while(value_raw.empty() && s1)
    {
        string aux;
        s1 >> aux;
        value_raw = ptr->get_point_data(aux);
    }

    if(points.size() < 2){
        return 0;
    }

    if(value_raw.size() < 2){
        return 0;
    }

    for(string s : value_raw){
        stringstream ss(s);
        double v = 0.0;

        ss >> v;

        value.push_back(v);
    }

    //ADDS POINTS
    WiseGraphic* o = dynamic_cast<WiseGraphic*>(ptr.release());

    double min = 0;
    double max = 0;
    for(int i = 0; i < points.size(); i++){
        if(i == 0){
            min = points.operator[](i).X();
            max = points.operator[](i).X();
        } else {
            if(points.operator[](i).X() < min)
                min = points.operator[](i).X();

            if(points.operator[](i).X() > max)
                max = points.operator[](i).X();
        }
    }

    o->set_grid(min,max,(int)points.size() - ONE);

    for(int i = 0; i  < points.size(); i++){
        o->set_point(i,value.operator[](i));
    }

    o->update_structure();

    unique_ptr<WiseGraphic> obj(o);

    return move(obj);
}

vector<string> WiseGraphicFactory::examples_list(){
    vector<string> r;

    r.push_back("SIN");

    r.push_back("COS");

    return r;
}

unique_ptr<WiseElement> WiseGraphicFactory::make_example(string example, string name){
    //SINE
    if(example == "SIN"){
        last_id++;

        unique_ptr<WiseGraphic> graphic = unique_ptr<WiseGraphic>{new WiseGraphic{name}};

        double a = 0;
        double b = 360;
        int N = 100;
        double h = (b - a)/(N);
        graphic->set_grid(a,b,N);
        for(int i = 0; i <= N; i++){
            double x = (i * h) + a;
            double y = igumath::sine(x);

            graphic->set_point(i,y);
        }

        graphic->update_structure();

        return move(graphic);
    }

    //COSINE
    if(example == "COS"){
        last_id++;

        unique_ptr<WiseGraphic> graphic = unique_ptr<WiseGraphic>{new WiseGraphic{name}};

        double a = 0;
        double b = 360;
        int N = 100;
        double h = (b - a)/(N);
        graphic->set_grid(a,b,N);
        for(int i = 0; i <= N; i++){
            double x = (i * h) + a;
            double y = igumath::cosine(x);

            graphic->set_point(i,y);
        }

        graphic->update_structure();

        return move(graphic);
    }

    return 0;
}

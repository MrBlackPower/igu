#include "wisepolyfactory.h"

int WisePolyFactory::last_id = 0;

WisePolyFactory::WisePolyFactory() : WiseElementFactory(DEFAULT_POLY_TYPE,DEFAULT_NAME)
{

}

WisePolyFactory::~WisePolyFactory(){

}

/*********************
 * VIRTUAL METHODS
 *********************/

unique_ptr<WiseElement> WisePolyFactory::recreate(unique_ptr<WiseElement> ptr){
    vector<point> points;
    vector<double> value;
    vector<string> value_raw;

    //SETS PARAMETERS
    points = ptr->get_points();

    string v_name = "valor VALOR value VALUE";


    //VALUE - SET
    stringstream s1(v_name);

    while(value_raw.empty() && s1)
    {
        string aux;
        s1 >> aux;
        value_raw = ptr->get_point_data(aux);
    }

    if(points.size() < 2){
        return 0;
    }

    if(value_raw.size() < 2){
        return 0;
    }

    for(string s : value_raw){
        stringstream ss(s);
        double v = 0.0;

        ss >> v;

        value.push_back(v);
    }

    //ADDS POINTS
    WisePoly* o = dynamic_cast<WisePoly*>(ptr.release());

    point min = {0,0,0};
    point max = {0,0,0};
    for(int i = 0; i < points.size(); i++){
        if(i == 0){
            min.X(points.operator[](i).X());
            max.X(points.operator[](i).X());
            min.Y(points.operator[](i).Y());
            max.Y(points.operator[](i).Y());
            min.Z(points.operator[](i).Z());
            max.Z(points.operator[](i).Z());
        } else {
            if(points.operator[](i).X() < min.X())
                min.X(points.operator[](i).X());

            if(points.operator[](i).X() > max.X())
                max.X(points.operator[](i).X());

            if(points.operator[](i).Y() < min.Y())
                min.Y(points.operator[](i).Y());

            if(points.operator[](i).Y() > max.Y())
                max.Y(points.operator[](i).Y());

            if(points.operator[](i).Z() < min.Z())
                min.Z(points.operator[](i).Z());

            if(points.operator[](i).Z() > max.Z())
                max.Z(points.operator[](i).Z());
        }
    }

    //ONLY WORKS IF N = M = L
    int s = cbrt(points.size());
    o->set_grid(min,max,s);

    for(int i = 0; i  < s; i++){
         for(int j = 0; j  < s; j++){
             for(int k = 0; k  < s; k++){
                int v = o->get_point_id(i,j,k);
                o->set_point(i,j,k,value.operator[](v));
            }
        }
    }

    o->set_status(HOT);

    o->update_structure();

    unique_ptr<WisePoly> obj(o);

    return std::move(obj);
}

unique_ptr<WiseElementFactory> WisePolyFactory::clone_factory_aux(){
    return unique_ptr<WiseElementFactory>{new WisePolyFactory()};
}

unique_ptr<WiseElement>  WisePolyFactory::make(string name, string dna){
    last_id++;

    unique_ptr<WisePoly> obj = unique_ptr<WisePoly>{new WisePoly{name,dna}};

    return std::move(obj);
}

vector<string> WisePolyFactory::examples_list(){
    vector<string> r;

    /*********************
     * ADI 3D EXAMPLES - START
     *********************/

    r.push_back("HYPERBOLICAL_SATURATION");

    r.push_back("HYPERBOLICAL_EXACT");

    r.push_back("HYPERBOLICAL_X_VELOCITY");

    r.push_back("HYPERBOLICAL_Y_VELOCITY");

    r.push_back("HYPERBOLICAL_Z_VELOCITY");

    r.push_back("SINOIDAL_SATURATION");

    r.push_back("SINOIDAL_EXACT");

    r.push_back("SINOIDAL_X_VELOCITY");

    r.push_back("SINOIDAL_Y_VELOCITY");

    r.push_back("SINOIDAL_Z_VELOCITY");

    r.push_back("ALPHA_SATURATION");

    r.push_back("ALPHA_EXACT");

    r.push_back("ALPHA_X_VELOCITY");

    r.push_back("ALPHA_Y_VELOCITY");

    r.push_back("ALPHA_Z_VELOCITY");

    r.push_back("ALPHA_CONVECTION_SATURATION");

    r.push_back("ALPHA_CONVECTION_EXACT");

    r.push_back("ALPHA_CONVECTION_X_VELOCITY");

    r.push_back("ALPHA_CONVECTION_Y_VELOCITY");

    r.push_back("ALPHA_CONVECTION_Z_VELOCITY");

    /*********************
     * ADI 3D EXAMPLES - END
     *********************/
    return r;
}

unique_ptr<WiseElement> WisePolyFactory::make_example(string example, string name){
    /*********************
     * ADI 3D EXAMPLES - START
     *********************/
    //SATURATION FOR THE HYPERBOLICAL EXAMPLE
    if(example == "HYPERBOLICAL_SATURATION"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
        point max(0,5,5,5);
        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double h1 = (max.X() - min.X())/(n-1);
                    double h2 = (max.Y() - min.Y())/(m-1);
                    double h3 = (max.Z() - min.Z())/(l-1);
                    double x = min.X() + (i*h1);
                    double y = min.Y() + (j*h2);
                    double z = min.Z() + (k*h3);
                    double v = (1 - pow(x,2)) * (1 - pow(y,2)) * (1 - pow(z,2)) * exp(0);

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //EXACT FOR THE HYPERBOLICAL EXAMPLE
    if(example == "HYPERBOLICAL_EXACT"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
        point max(0,5,5,5);
        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double h1 = (max.X() - min.X())/(n-1);
                    double h2 = (max.Y() - min.Y())/(m-1);
                    double h3 = (max.Z() - min.Z())/(l-1);
                    double x = min.X() + (i*h1);
                    double y = min.Y() + (j*h2);
                    double z = min.Z() + (k*h3);
                    double v = (-3 * exp(0) * (1 - pow(x,2)) * (1 - pow(y,2)) * (1 - pow(z,2))) +
                               (2 * exp(0) * (1 - pow(y,2)) * (1 - pow(z,2))) +
                               (2 * exp(0) * (1 - pow(x,2)) * (1 - pow(z,2))) +
                               (2 * exp(0) * (1 - pow(x,2)) * (1 - pow(y,2)));

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //X VELOCITY FOR THE HYPERBOLICAL EXAMPLE
    if(example == "HYPERBOLICAL_X_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //Y VELOCITY FOR THE HYPERBOLICAL EXAMPLE
    if(example == "HYPERBOLICAL_Y_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //Z VELOCITY FOR THE HYPERBOLICAL EXAMPLE
    if(example == "HYPERBOLICAL_Z_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }
    //SATURATION FOR THE SINOIDAL EXAMPLE
    if(example == "SINOIDAL_SATURATION"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double h1 = (max.X() - min.X())/(n-1);
                    double h2 = (max.Y() - min.Y())/(m-1);
                    double h3 = (max.Z() - min.Z())/(l-1);
                    double x = min.X() + (i*h1);
                    double y = min.Y() + (j*h2);
                    double z = min.Z() + (k*h3);
                    double v = igumath::sine(PI * x) * igumath::sine(PI * y) * igumath::sine(PI * z) * exp(0);

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //EXACT FOR THE SINOIDAL EXAMPLE
    if(example == "SINOIDAL_EXACT"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //X VELOCITY FOR THE SINOIDAL EXAMPLE
    if(example == "SINOIDAL_X_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //Y VELOCITY FOR THE SINOIDAL EXAMPLE
    if(example == "SINOIDAL_Y_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //Z VELOCITY FOR THE SINOIDAL EXAMPLE
    if(example == "SINOIDAL_Z_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }
    //SATURATION FOR THE ALPHA EXAMPLE
    if(example == "ALPHA_SATURATION"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double h1 = (max.X() - min.X())/(n-1);
                    double h2 = (max.Y() - min.Y())/(m-1);
                    double h3 = (max.Z() - min.Z())/(l-1);
                    double x = min.X() + (i*h1);
                    double y = min.Y() + (j*h2);
                    double z = min.Z() + (k*h3);
                    double v = (1/pow(1,3/2))*exp(-(pow(x - 0.5,2)/0.01)-(pow(y - 0.5,2)/0.01)-(pow(z - 0.5,2)/0.01));

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //EXACT FOR THE ALPHA EXAMPLE
    if(example == "ALPHA_EXACT"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double h1 = (max.X() - min.X())/(n-1);
                    double h2 = (max.Y() - min.Y())/(m-1);
                    double h3 = (max.Z() - min.Z())/(l-1);
                    double x = min.X() + (i*h1);
                    double y = min.Y() + (j*h2);
                    double z = min.Z() + (k*h3);
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //X VELOCITY FOR THE ALPHA EXAMPLE
    if(example == "ALPHA_X_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //Y VELOCITY FOR THE ALPHA EXAMPLE
    if(example == "ALPHA_Y_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //Z VELOCITY FOR THE ALPHA EXAMPLE
    if(example == "ALPHA_Z_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }
    //SATURATION FOR THE ALPHA_CONVECTION EXAMPLE
    if(example == "ALPHA_CONVECTION_SATURATION"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double h1 = (max.X() - min.X())/(n-1);
                    double h2 = (max.Y() - min.Y())/(m-1);
                    double h3 = (max.Z() - min.Z())/(l-1);
                    double x = min.X() + (i*h1);
                    double y = min.Y() + (j*h2);
                    double z = min.Z() + (k*h3);
                    double v = (1/pow(1,3/2))*exp(-(pow(x - 0.5,2)/0.01)-(pow(y - 0.5,2)/0.01)-(pow(z - 0.5,2)/0.01));

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //EXACT FOR THE ALPHA_CONVECTION EXAMPLE
    if(example == "ALPHA_CONVECTION_EXACT"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //X VELOCITY FOR THE ALPHA_CONVECTION EXAMPLE
    if(example == "ALPHA_CONVECTION_X_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0.8;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //Y VELOCITY FOR THE ALPHA_CONVECTION EXAMPLE
    if(example == "ALPHA_CONVECTION_Y_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0.8;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }

    //Z VELOCITY FOR THE ALPHA_CONVECTION EXAMPLE
    if(example == "ALPHA_CONVECTION_Z_VELOCITY"){
        last_id++;
        unique_ptr<WisePoly> poly = unique_ptr<WisePoly>{new WisePoly{name}};

        point min(0,0,0,0);
         point max(0,5,5,5);

        int m = 32;
        int n = 32;
        int l = 32;

        poly->set_grid(min,max,m,n,l);

        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int k = 0; k < l; k++){
                    double v = 0.8;

                    poly->set_point(i,j,k,v);
                }
            }
        }

        poly->update_structure();

        return move(poly);

    }


    /*********************
     * ADI 3D EXAMPLES - END
     *********************/
    return 0;
}


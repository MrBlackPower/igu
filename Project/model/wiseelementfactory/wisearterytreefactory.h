#ifndef WISEARTERYTREEFACTORY_H
#define WISEARTERYTREEFACTORY_H

#include "model/wise/wiseelementfactory.h"
#include "model/wiseobject/wisearterytree.h"

#include <memory>
#include <iostream>

#define DEFAULT_TYPE_NAME "ARTERY_TREE"
#define DEFAULT_NAME "ARTERY_TREE_ELEMENT_FACTORY"

using namespace std;

class WiseArteryTreeFactory : public WiseElementFactory
{
public:
    WiseArteryTreeFactory();
    ~WiseArteryTreeFactory();

    /*********************
     * VIRTUAL METHODS
     *********************/

    unique_ptr<WiseElementFactory> clone_factory_aux();

    unique_ptr<WiseElement>  make(string name, string dna = "NONE");

    unique_ptr<WiseElement> recreate(unique_ptr<WiseElement> ptr);

    vector<string> examples_list();

    unique_ptr<WiseElement> make_example(string example, string name);

private:

    static int last_id;

};

#endif // WISEARTERYTREEFACTORY_H

#ifndef WISEPOLYFACTORY_H
#define WISEPOLYFACTORY_H

#include "model/wise/wiseelementfactory.h"
#include "model/wiseobject/wisepoly.h"

#define DEFAULT_POLY_TYPE "POLY"
#define DEFAULT_NAME "POLY_ELEMENT_FACTORY"

class WisePolyFactory : public WiseElementFactory
{
public:
    WisePolyFactory();
    ~WisePolyFactory();

    /*********************
     * VIRTUAL METHODS
     *********************/

    unique_ptr<WiseElementFactory> clone_factory_aux();

    unique_ptr<WiseElement>  make(string name, string dna);

    unique_ptr<WiseElement> recreate(unique_ptr<WiseElement> ptr);

    vector<string> examples_list();

    unique_ptr<WiseElement> make_example(string example, string name);


private:

    static int last_id;

};

#endif // WISEPOLYFACTORY_H

#ifndef WISEMESHFACTORY_H
#define WISEMESHFACTORY_H

#include "model/wise/wiseelementfactory.h"

#include "model/wiseobject/wisemesh.h"

#include <memory>
#include <iostream>

#define DEFAULT_TYPE_NAME "MESH"
#define DEFAULT_NAME "MESH_ELEMENT_FACTORY"

using namespace std;

class WiseMeshFactory : public WiseElementFactory
{
public:
    WiseMeshFactory();
    ~WiseMeshFactory();

    /*********************
     * VIRTUAL METHODS
     *********************/

    unique_ptr<WiseElementFactory> clone_factory_aux();

    unique_ptr<WiseElement>  make(string name, string dna);

    unique_ptr<WiseElement> recreate(unique_ptr<WiseElement> ptr);

    vector<string> examples_list();

    unique_ptr<WiseElement> make_example(string example, string name);

private:

    static int last_id;
};

#endif // WISEMESHFACTORY_H

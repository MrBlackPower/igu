#ifndef WISEGRAPHICFRACTORY_H
#define WISEGRAPHICFRACTORY_H

#include "model/wise/wiseelementfactory.h"

#include "model/wiseobject/wisegraphic.h"

#include <memory>
#include <iostream>

#define DEFAULT_TYPE_NAME "GRAPHIC"
#define DEFAULT_NAME "GRAPHIC_ELEMENT_FACTORY"

using namespace std;


class WiseGraphicFactory : public WiseElementFactory
{
public:
    WiseGraphicFactory();
    ~WiseGraphicFactory();

    /*********************
     * VIRTUAL METHODS
     *********************/

    unique_ptr<WiseElementFactory> clone_factory_aux();

    unique_ptr<WiseElement>  make(string name, string dna = "NONE");

    unique_ptr<WiseElement> recreate(unique_ptr<WiseElement> ptr);

    vector<string> examples_list();

    unique_ptr<WiseElement> make_example(string example, string name);

private:

    static int last_id;
};

#endif // WISEGRAPHICFRACTORY_H

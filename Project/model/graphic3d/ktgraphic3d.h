#ifndef KTGRAPHIC3D_H
#define KTGRAPHIC3D_H

#include "../graphic3d.h"
#include "lines3d.h"

#define DEFAULT_M 60
#define DEFAULT_N 60
#define DEFAULT2_M 64
#define DEFAULT2_N 64

struct KTGraphic3DStartParams{
    //LIMITS (a,b) (c,d)
    double a = -1.5;
    double b = 1.5;
    double c = -1.5;
    double d = 1.5;
    int m = DEFAULT_M;
    int n = DEFAULT_N;
    double cfl = 0.25;
    int example = 0;
    int coutour_type = 1;
    double dx;
    double dy;
};

struct KTVelocity{
    double a = 0.0;
    double b = 0.0;
};

class KTGraphic3D : public Graphic3D{
public:
    KTGraphic3D(int m, int n, SmartObject* parent = NULL);
    KTGraphic3D(int example, SmartObject* parent = NULL);
    KTGraphic3D(QString name, SmartObject* parent = NULL);
    KTGraphic3D(SmartObject* parent = NULL);

    bool setUV(vector<KTVelocity> vels);
    bool setVV(vector<KTVelocity> vels);

    Lines3D* contour(double f);

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    bool setContextRange(QString field);

private:
    KTGraphic3DStartParams startParams;
    Error error;
    vector<KTVelocity> vv;
    vector<KTVelocity> uv;

    vector<double> KT();
    virtual double f(double u);
    virtual double df(double u);
    virtual double g(double u);
    virtual double dg(double u);
    vector<double> doUx();
    vector<double> doUy();
    double minmod(double a, double b, double c);

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool initialize();
    double iteration();
    bool updateContextVisualParameter(QString field, QString value);
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    DataType getLiveParameterDataType(QString name);
    void mouse_press_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool);

};

#endif // KTGRAPHIC3D_H

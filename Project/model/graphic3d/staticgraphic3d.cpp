#include "staticgraphic3d.h"

StaticGraphic3D::StaticGraphic3D(int m, int n, SmartObject* parent) : Graphic3D("STATIC GRAPHIC 3D",m,n,parent){
    setContextRange("Z-AXIS");
}

StaticGraphic3D::StaticGraphic3D(QString name, int m, int n, SmartObject* parent) : Graphic3D(name,m,n,parent){
    setContextRange("Z-AXIS");
}

StaticGraphic3D::StaticGraphic3D(QString name,int m, SmartObject* parent) : Graphic3D(name,m,parent){
    setContextRange("Z-AXIS");
}


bool StaticGraphic3D::initialize(){
    ColorBar cb = Brasil();
    setMax(limit._far);
    setMin(limit._near);
    this->setColorBar(cb);

    return true;
}

double StaticGraphic3D::iteration(){
    return 0.0;
}

Lines3D* StaticGraphic3D::contour(double f){
    Lines3D* line = new Lines3D();
    for(int i = 0; i < getM()-1; i++){
        for(int j = 0; j < getN()-1; j++){
            if(N > ID(i+1,j+1)){
                point a = *raw_data[ID(i,j)];
                point b = *raw_data[ID(i+1,j)];
                point c = *raw_data[ID(i+1,j+1)];
                point d = *raw_data[ID(i,j+1)];

                bool vInside[] = {(a.Z() > f),(b.Z() > f),(c.Z() > f),(d.Z() > f)};

                vector<int> ids;
                if((!vInside[0]&&vInside[1])||(vInside[0]&&!vInside[1])){
                   point* p = new point();
                   p->operator=(MathHelper::interpolate(a,b,a.Z(),b.Z(),f));
                   ids.push_back(line->getN());
                   line->addData(p);
                }
                if((!vInside[1]&&vInside[2])||(vInside[1]&&!vInside[2])){
                   point* p = new point();
                   p->operator=(MathHelper::interpolate(b,c,b.Z(),c.Z(),f));
                   ids.push_back(line->getN());
                   line->addData(p);
                }
                if((!vInside[2]&&vInside[3])||(vInside[2]&&!vInside[3])){
                   point* p = new point();
                   p->operator=(MathHelper::interpolate(c,d,c.Z(),d.Z(),f));
                   ids.push_back(line->getN());
                   line->addData(p);
                }
                if((!vInside[3]&&vInside[0])||(vInside[3]&&!vInside[0])){
                   point* p = new point();
                   p->operator=(MathHelper::interpolate(d,a,d.Z(),a.Z(),f));
                   ids.push_back(line->getN());
                   line->addData(p);
                }

                for(int i = 1; i < ids.size(); i++){
                    line->addLine(ids[i-1],ids[i]);
                }
            }
        }
    }

    return line;
}

void StaticGraphic3D::mouse_press_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool){

}

void StaticGraphic3D::mouse_move_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool){

}

vector<Field> StaticGraphic3D::getStartParameters(){
    vector<Field> params;

    return params;
}

vector<Field> StaticGraphic3D::getContextVisualParameters(){
    vector<Field> params;

    return params;
}

vector<QString> StaticGraphic3D::getRanges(){
    vector<QString> params;

    params.push_back("Z-AXIS");

    return params;
}

bool StaticGraphic3D::updateStartParameter(QString field, QString value){
    return false;
}

bool StaticGraphic3D::updateContextVisualParameter(QString field, QString value){
    return false;
}

QString StaticGraphic3D::getLiveParameterList(){
    return QString("");
}

QString StaticGraphic3D::getLiveParameter(QString name){
    return QString("");
}

DataType StaticGraphic3D::getLiveParameterDataType(QString name){
    return undefined;
}

bool StaticGraphic3D::setContextRange(QString field){
    if(field == "Z-AXIS"){
        range.max = limit._far;
        range.min = limit._near;
        range.parameter = "Z-AXIS";
        return true;
    }

    return false;
}

#include "graphic3d.h"

Graphic3D::Graphic3D(QString name, int M, int N, SmartObject* parent) : GraphicObject(name,parent)
{
    this->M = M;
    this->N = N;
    NODES = 0;
}

Graphic3D::Graphic3D(QString name, int M, SmartObject* parent) : GraphicObject(name,parent)
{
    this->M = M;
    this->N = 0;
    NODES = 0;
}

void Graphic3D::resizeTransform(){
    vector<point> points;

    for(int i = 0; i < raw_data.size(); i++)
        points.push_back(*raw_data[i]);

    resetLimit();
    updateLimit(points);
}

vector<QString> Graphic3D::print(){
    vector<QString> ans;
    QString buffer;
    ans.push_back(buffer.asprintf("%d",N));
    for(int i = 0; i < raw_data.size(); i++){
        point* p = raw_data[i];
        buffer = buffer.asprintf("%f %f %f",p->X(),p->Y(),p->Z());
        ans.push_back(buffer);
    }

    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 3D PRINTED"));
    return ans;
}

vector<QString> Graphic3D::data(){
    vector<QString> ans;
    QString buffer;

    buffer = buffer.asprintf("3-DIMENSIONAL GRAPHIC");
    ans.push_back(buffer);

    buffer = buffer.asprintf("(X,Y)");
    ans.push_back(buffer);

    for(int i = 0; i < raw_data.size(); i++){
        point* p = raw_data[i];
        buffer = buffer.asprintf("(%.4f,%.4f,%.4f)",p->X(),p->Y(),p->Z());
        ans.push_back(buffer);
    }



    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 3D DATA SAVED"));
    return ans;
}

vector<QString> Graphic3D::raw(){
    vector<QString> raw;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    raw.push_back(line);
    line = line.asprintf("PRESSURE PEAKING 3D GRAPHIC OUTPUT SmartID = %d\n",getID());
    raw.push_back(line);
    line = line.asprintf("ASCII\n");
    raw.push_back(line);
    line = line.asprintf("DATASET POLYDATA\n");
    raw.push_back(line);
    line = line.asprintf("POINTS  %d  double\n", N);
    raw.push_back(line);

    for(int i = 0; i < raw_data.size(); i++){
        point* p = raw_data[i];
        line = line.asprintf("%f  %f  %f\n", p->X(), p->Y(), p->Z());
        raw.push_back(line);
    }

    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 3D RAW DATA SAVED"));
    return raw;
}

void Graphic3D::draw(){
    glBegin(GL_QUADS);
    for(int i = 0; i < M - 1; i++){
        for(int j = 0; j < N - 1; j++){
            if(N > ID(i+1,j+1)){
                point a = *raw_data[ID(i,j)];
                point b = *raw_data[ID(i+1,j)];
                point c = *raw_data[ID(i+1,j+1)];
                point d = *raw_data[ID(i,j+1)];

                setMaterialColor(interpolateColor(a.Z()));
                glVertex3f(a.X(), a.Y(), a.Z());

                setMaterialColor(interpolateColor(b.Z()));
                glVertex3f(b.X(), b.Y(), b.Z());

                setMaterialColor(interpolateColor(c.Z()));
                glVertex3f(c.X(), c.Y(), c.Z());

                setMaterialColor(interpolateColor(d.Z()));
                glVertex3f(d.X(), d.Y(), d.Z());


                setMin(a.Z());
                setMax(a.Z());
                setMin(b.Z());
                setMax(b.Z());
                setMin(c.Z());
                setMax(c.Z());
                setMin(d.Z());
                setMax(d.Z());
            }
        }
    }
    glEnd();
}

int Graphic3D::getM(){
    return M;
}

int Graphic3D::getN(){
    return N;
}

void Graphic3D::setM(int M){
    this->M = M;
}

void Graphic3D::setN(int N){
    this->N = N;
}

int Graphic3D::ID(int i, int j){
    return i + (j * M);
}

bool Graphic3D::load(VTKFile* file){
    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 3D LOAD RUN",SmartLogType::LOG_LOAD));

    clearData();

    vector<point> points = file->getPoints();

    for(int i = 0; i < points.size(); i++)
        addData(points[i]);

    return true;
}

bool Graphic3D::addData(point p){
    point* aux;
    aux->operator=(p);

    raw_data.push_back(aux);

    updateLimit(&p);
    NODES++;

    setMin(p.Z());
    setMax(p.Z());

    return true;
}

bool Graphic3D::addData(point* p){
    raw_data.push_back(p);
    updateLimit(p);
    NODES++;

    setMin(p->Z());
    setMax(p->Z());

    return true;
}

bool Graphic3D::clearData(){
    raw_data.clear();
    NODES = 0;
    resetLimit();

    range.max = -1;
    range.min = 1;

    return true;
}

vector<Field> Graphic3D::getPointDataList(){
    vector<Field> list;
    return list;
}

vector<Field> Graphic3D::getCellDataList(){
    vector<Field> list;
    return list;
}

vector<QString> Graphic3D::getExtrapolations(){
    vector<QString> aux;

    return aux;
}

GraphicObject* Graphic3D::extrapolateContext(QString extrapolation){
    return NULL;
}

bool Graphic3D::compatibility(VTKFile *file){
    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 3D COMPATIBILITY RUN",SmartLogType::LOG_COMPATIBILITY));

//    if(file->getN() < 4)
//        return false;

//    //CHECKS IF POINTS ARE EQUALLY DISTED AND ALL CELLS COMPLETE
//    //number of points has to be a multiple of 4
//    if(file->getN() % 4 != 0.0)
//        return false;

//    //they have to be all equally disted
//    vector<point> points = file->getPoints();
//    point d1 = points[ID(i,j)] - points[ID(i+1,j)];
//    point d2 = points[ID(i,j)] - points[ID(i,j+1)];
//    point d3 = points[ID(i,j)] - points[ID(i+1,j+1)];

//    if(d1.X() != 0.0)
//        return false;

//    if(d2.Y() != 0.0)
//        return false;

//    if(d3.X() == 0.0 || d3.Y() == 0.0)
//        return false;

//    for(int i = ONE; i < points.size(); i++){
//        point d1 = points[ID(i,j)] - points[ID(i+1,j)];
//        point d2 = points[ID(i,j)] - points[ID(i,j+1)];
//        point d3 = points[ID(i,j)] - points[ID(i+1,j+1)];
//    }

    return false;
}

void Graphic3D::finish(){

}

point Graphic3D::operator[](int i){
    if(i < 0 || i >= raw_data.size())
        return point();

    return *raw_data[i];
}

point Graphic3D::operator ()(int i, int j){
    int id = ID(i,j);
    return operator [](id);
}

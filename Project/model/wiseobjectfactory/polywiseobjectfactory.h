#ifndef POLYMODELFACTORY_H
#define POLYMODELFACTORY_H

#include "model/wise/wiseobjectfactory.h"

class PolyWiseObjectFactory : public WiseObjectFactory
{
public:
    PolyWiseObjectFactory();

    unique_ptr<WiseObject> make(string name, unique_ptr<WiseElement> element);

    unique_ptr<WiseObject> make(string name, unique_ptr<WiseElement> hot, vector<unique_ptr<WiseElement>> freezer);
};

#endif // POLYMODELFACTORY_H

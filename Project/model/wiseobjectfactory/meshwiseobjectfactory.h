#ifndef MESHMODELFACTORY_H
#define MESHMODELFACTORY_H

#include "model/wise/wiseobjectfactory.h"
#include "model/wiseelementfactory/wisemeshfactory.h"

class MeshWiseObjectFactory : public WiseObjectFactory
{
public:
    MeshWiseObjectFactory();

    unique_ptr<WiseObject> make(string name, unique_ptr<WiseElement> element);

    unique_ptr<WiseObject> make(string name, unique_ptr<WiseElement> hot, vector<unique_ptr<WiseElement>> freezer);
};

#endif // MESHMODELFACTORY_H

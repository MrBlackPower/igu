#include "polywiseobjectfactory.h"

PolyWiseObjectFactory::PolyWiseObjectFactory() : WiseObjectFactory(unique_ptr<WiseElementFactory>{new WisePolyFactory()})
{

}

unique_ptr<WiseObject> PolyWiseObjectFactory::make(string name, unique_ptr<WiseElement> element){
    if(!element.get())
        return NULL;

    if(element->get_params()->type != "POLY")
        return NULL;


    WisePolyFactory f;
    unique_ptr<WiseElement> cold = f.clone(element->get_name(),element.get(),WiseElementStatus::COLD);


    unique_ptr<WiseObject> r {new WiseObject(name,move(element),move(cold),f.clone_factory())};

    return move(r);
}

unique_ptr<WiseObject> PolyWiseObjectFactory::make(string name, unique_ptr<WiseElement> hot, vector<unique_ptr<WiseElement>> freezer){
    if(!hot.get() || freezer.empty())
        return NULL;

    if(hot->get_params()->type != "POLY")
        return NULL;

    WisePolyFactory f;

    unique_ptr<WiseObject> r {new WiseObject(name,move(hot),move(freezer),f.clone_factory())};

    return move(r);
}



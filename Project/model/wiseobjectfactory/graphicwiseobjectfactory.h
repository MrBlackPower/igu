#ifndef GRAPHICMODELFACTORY_H
#define GRAPHICMODELFACTORY_H

#include "model/wise/wiseobjectfactory.h"
#include "model/wiseelementfactory/wisegraphicfactory.h"

class GraphicWiseObjectFactory : public WiseObjectFactory
{
public:
    GraphicWiseObjectFactory();

    unique_ptr<WiseObject> make(string name, unique_ptr<WiseElement> element);

    unique_ptr<WiseObject> make(string name, unique_ptr<WiseElement> hot, vector<unique_ptr<WiseElement>> freezer);
};

#endif // GRAPHICMODELFACTORY_H

#ifndef ARTERYTREEMODELFACTORY_H
#define ARTERYTREEMODELFACTORY_H

#include "model/wise/wiseobjectfactory.h"
#include "model/wise/wiseobjectfactories.h"
#include "model/wiseelementfactory/wisearterytreefactory.h"

class ArteryTreeWiseObjectFactory : public WiseObjectFactory
{
public:
    ArteryTreeWiseObjectFactory();

    unique_ptr<WiseObject> make(string name, unique_ptr<WiseElement> element);

    unique_ptr<WiseObject> make(string name, unique_ptr<WiseElement> hot, vector<unique_ptr<WiseElement>> freezer);
};

#endif // ARTERYTREEMODELFACTORY_H

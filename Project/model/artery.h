#ifndef ARTERY_H
#define ARTERY_H

/************************************************************************************
  Name:        artery.h
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Header class of an artery structure.
************************************************************************************/

#include <math.h>
#include <iostream>
#include <fstream>
#include <complex>
#include "helper/mathHelper.h"
#include "model/wise/point.h"
#include "graphic2d.h"
#include "graphic2d/staticgraphic.h"

#define PI 3.14159265359
#define SIZE 1.0
#define DEFAULT_GRAPHIC_SIZE 100

using namespace std;

class  Artery : public SmartObject
{

public:

    enum ArteryParameter{
        POINTS,
        LINES,
        PRESSURE,
        FLOW,
        RADIUS,
        YOUNG_MOD,
        DENSITY,
        VISCOSITY,
        LENGTH,
        WAVESPEED,
        BETA,
        ADMITTANCE,
        REFLECTION_COEF,
        EFFECTIVE_ADMITTANCE,
        MEDIUM_PRESSURE,
        PRESSURE_WAVE,
        FLOW_WAVE
    };
    /************************************************************************************
        CONSTRUCTOR

        @param id identificator of artery.
        @param p point armazenated in the artery, in this case, the distal node.
        @param length the length of the artery (in centimeters)
        @param youngModulus the Young`s Modulus of this artery
        @param density the Density in this artery
        @param viscosity the fluid Viscosity of this artery
        @param radius the Radius of this artery (in centimeters)
        @param isLeaf if this artery is a leaf in the tree representation
************************************************************************************/
    Artery(int id, point* p, double length, double youngModulus, double density, double viscosity,
           double radius, bool isLeaf, GraphicObject* arteryTree, int arteryTreeGID);

/************************************************************************************
        DESTRUCTOR
************************************************************************************/
    virtual ~Artery();

    bool equals(Artery* a);

    Artery* findArtery(point* point);

    bool hasFather();

    bool hasLeft();

    bool hasRight();

    bool hasSon();

    void hover();

    void unhover();

    bool isHovered();

    bool isLeaf();

    bool isSelected();

    void select();

    void unselect();

        /********************************************************
        *                  PRINTS                               *
        * Recursive prints walking first to the left side.      *
        ********************************************************/
    void printField(ArteryParameter fieldCode, vector<QString>* raw);

/********************************************************
*                  GETS & SETS                          *
********************************************************/

    complex<double> getAdmittance();

    bool setAdmittance(bool viscous, double viscousMultiplier, double phi0);

    bool setAdmittance();

    double getAlpha();

    bool setAlpha(double viscousMultiplier);

    double getAngularFrequency();

    bool setAngularFrequency(double frequency);

    double getArea();

    complex<double> getBeta();

    bool setBeta();

    double getDensity();

    void setDensity(double val);

    double getDiameter();

    complex<double> getEffectiveAdmittance();

    bool setEffectiveAdmittance();

    Artery* getFather();

    void setFather(Artery* a);

    complex<double> getFlow();

    vector<point*> getFlowWave();

    vector<point*> getRealFlowWave();

    complex<double> getFlow(double x, complex<double> rootAdmittance);

    complex<double> getRealFlow(double x, complex<double> rootAdmittance);

    bool setFlow(complex<double> rootAdmittance);

    bool setRealFlow(complex<double> rootAdmittance);

    bool setFlow0(complex<double> rootAdmittance);

    double getFrequency();

    int getID();

    Artery* getLeft();

    void setLeft(Artery* a);

    void setLeaf(bool val);

    double getLength();

    void setLength(double val);

    complex<double> getMediumPressure();

    bool setMediumPressure();

    bool setMediumPressure(double _p0);

    point getPoint();

    double getModelTime();

    void setPoint(point p);

    double getPhaseAngle();

    bool setPhaseAngle(double phi0);

/************************************************************************************
        Recursive method to set the following attributes using recursion.

        Wave Speed                                      [c]  in  [cm/s]
        Beta                                            [^b] in  []
        Admittance                                      [Y]  in  []
        Reflection Coefficient                          [R]  in  []
        Effective Admittance                            [Ye] in  []

        Because of the way these equations are solved this recursion is done from
        bottom to top. Check the full report for further information.

        @return if all steps were successfull.
************************************************************************************/
    bool setPhaseOne(double frequency);

/************************************************************************************
        Recursive method to set the following attributes using recursion.

        Wave Speed                                      [c]  in  [cm/s]
        Beta                                            [^b] in  []
        Admittance                                      [Y]  in  []
        Reflection Coefficient                          [R]  in  []
        Effective Admittance                            [Ye] in  []

        Because of the way these equations are solved this recursion is done from
        bottom to top. Check the full report for further information.

        @return if all steps were successfull.
************************************************************************************/
    bool setPhaseOne(double frequency, bool viscous, double viscousMultiplier, double phi0);

/************************************************************************************
        Recursive method to set the following attributes using recursion.

        Medium Pressure                                 [_p] in  [cm/s]
        Pressure at the distal node                     [P]  in  []
        Flow at the distal node                         [Q]  in  []

        Because of the way these equations are solved this recursion is done from
        top to bottom. Check the full report for further information.

        @return if all steps were successfull.
************************************************************************************/
    bool setPhaseTwo(double p0, complex<double> rootAdmittance);

    complex<double> getPressure();

    vector<point*> getPressureWave();

    vector<point*> getRealPressureWave();

    complex<double> getPressure(double x);

    bool setPressure();

    bool setPressure0(double _p0);

    complex<double> getRealPressure(double x);

    bool setRealPressure(double p0);

    double getRadius();

    void setRadius(double val);

    complex<double> getReflectionCoef();

    bool setReflectionCoef();

    Artery* getRight();

    void setRight(Artery* a);

    double getThickness();

    void setThickness(double val);

    double getViscosity();

    void setViscosity(double val);

    bool setViscousBeta();

    complex<double> getViscousFactor();

    bool setViscousFactor();

    complex<double> getViscousWaveSpeed();

/************************************************************************************
        Setter for this artery's wave speed.

        @return if the algorithm was able to set the artery's wave speed.
************************************************************************************/
    bool setViscousWaveSpeed();

/************************************************************************************
        Getter Young's Modulus for viscoelastic walls in this artery.

        -1 flag for Young's Modulus for viscoelastic walls not set yet.

        @return the Young's Modulus for viscoelastic walls value of this artery.
************************************************************************************/
    complex<double> getViscousYoungsModulus();

/************************************************************************************
        Setter for this artery's Young's Modulus for viscoelastic walls.

        @return if the algorithm was able to set the artery's Young's Modulus for viscoelastic walls.
************************************************************************************/
    bool setViscousYoungsModulus();

/************************************************************************************
        Getter for Young's modulus value given at the start.

        @return the Young's modulus value of this artery.
************************************************************************************/
    double getYoungModulus();

/************************************************************************************
        Setter for the Young's modulus value of this artery.

        @param val value to substitute the Young's modulus value.
************************************************************************************/
    void setYoungModulus(double val);

/************************************************************************************
        Getter for wave speed value in this artery.

        -1 flag for wave speed not set yet.

        @return the wave speed value of this artery.
************************************************************************************/
    double getWaveSpeed();

/************************************************************************************
        Setter for this artery's wave speed.

        @return if the algorithm was able to set the artery's wave speed.
************************************************************************************/
    bool setWaveSpeed();

    double getPressureMax();

    double getPressureMin();

    double getFlowMax();

    double getFlowMin();

    void clearMinMax();

    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();
    void solveLog(SmartLogMessage msg);

protected:

private:
/************************************************************************************
        LIST OF ATTRIBUTES

        father..............pointer to father artery.

        left................pointer to left sided son artery.

        right...............pointer to right sided son artery.

        p...................pointer to this artery distal node (x,y) point
                            representation.

        pressure_wave.......LineGraphic element of this artery pressure wave.

        flow_wave...........LineGraphic element of this artery flow wave.

        beta................beta value of this artery.

        cv..................wave speed for viscous fluid value of this artery.

        Ec..................young's modulus for viscoelastic walls value of
                            this artery.

        flow................flow value at this artery's distal node.

        pressure............pressure value at this artery's distal node.

        _pressure...........medium pressure value of this artery.

        R...................reflection coefficiente of this artery.

        viscousFactor.......viscous factor value of this artery.

        Y...................admittance value of this artery.

        Ye..................effective admittance of this artery.

        alpha...............womersley's non-dimensional number value for this artery.

        c...................wave speed value of this artery

        density.............density value of this artery.

        E...................young's modulus value of this artery.

        h...................wall thickness value of this artery.

        length..............length value of this artery.

        phi.................phase angle between pressure and wall displacement.

        r...................radius value of this artery.

        viscosity...........viscosity value of this artery.

        w...................angular frequency value of this artery.

        id..................identity number of this artery.

        hovered.............if this artery is being hovered or not

        leaf................if this artery is a leaf or not.

        selected............if this artery is selected or not.
************************************************************************************/

    int ArteryTreeSID;

    int ArteryTreeGID;

    Artery* father;

    Artery* left;

    Artery* right;

    point p;

    /**
     * @brief flow_wave Equivalente à P em todo o domínio da artéria.
     */
    Graphic2D* flow_wave;

    static double flow_max;

    static double flow_min;
    /**
     * @brief pressure_wave Equivalente à _P em todo o domínio da artéria.
     */
    Graphic2D* pressure_wave;

    static double pressure_max;

    static double pressure_min;

    /**
     * @brief flow_wave Equivalente à p em todo o domínio da artéria.
     */
    Graphic2D* real_pressure_wave;

    /**
     * @brief pressure_wave Equivalente à q em todo o domínio da artéria.
     */
    Graphic2D* real_flow_wave;

    complex<double> beta;

    complex<double> cv;

    complex<double> Ec;

    complex<double> flow;

    /**
     * @brief pressure_0 Equivalente à q0.
     */
    complex<double> flow_0;

    /**
     * @brief pressure Equivalente à P.
     */
    complex<double> pressure;

    /**
     * @brief pressure_0 Equivalente à p0.
     */
    complex<double> pressure_0;

    /**
     * @brief _pressure Equivalente à _P.
     */
    complex<double> _pressure;

    /**
     * @brief pressure Equivalente à p.
     */
    complex<double> real_pressure;

    /**
     * @brief _pressure Equivalente à q.
     */
    complex<double> real_flow;

    complex<double> R;

    complex<double> viscousFactor;

    complex<double> _Y;

    complex<double> Ye;

    double alpha;

    double c;

    double density;

    double E;

    double h;

    double length;

    double phi;

    double r;

    double viscosity;

    double w;

    int graphicSize;

    int id;

    bool hovered;

    bool leaf;

    bool selected;

/************************************************************************************
        PRIVATE METHODS
************************************************************************************/
/************************************************************************************
        Plots a (x,y) representation of a graphic in the file opened. X here has a
        SIZE of 1.0 for every artery. And every artery has n points distributed
        equally inside this space. Y is calculated finding the pressure in this
         x point.

        After ploting this artery the iteration will be sent to it's left sided son.

        @param file the file opened to save the plot.
        @param n number of points each artery represents
        @param x0 the x this artery starts on.
        @param rootAdmittance admittance in root's artery
************************************************************************************/
    void f_plotAux(FILE* file, int n, int x0, complex<double> rootAdmittance);

/************************************************************************************
        Auxiliar recursive method to set the following attributes using recursion.

        Medium Pressure                                 [_p] in  [cm/s]
        Pressure at the distal node                     [P]  in  []
        Flow at the distal node                         [Q]  in  []

        Because of the way these equations are solved this recursion is done from
        top to bottom. Check the full report for further information.

        @return if all steps were successfull.
************************************************************************************/
    bool setPhaseTwoAux(double _p0, complex<double> rootAdmittance);
};

#endif // ARTERY_H

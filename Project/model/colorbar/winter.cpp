#include "winter.h"

Winter::Winter() : ColorBar("WINTER"){
    QColor c;
    addColor(c.fromRgb(48,48,241));
    addColor(c.fromRgb(48,241,48));
}

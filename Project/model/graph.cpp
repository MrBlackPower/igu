#include "graph.h"

template <class n_type, class e_type>
Graph<n_type,e_type>::Graph(QString name, SmartObject* parent) : GraphicObject(name, parent)
{
    sphere_radius = DEFAULT_RADIUS;
    draw_node = true;
    draw_edge = true;
    N = 0;
    M = 0;
    start = NULL;
    end = NULL;
    selected = NULL;
    hovered = NULL;
    lastSolution = NULL;
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::resizeTransform(){
    vector<point> points;

    for (int i = 0; i < nodes.size(); i++) {
        NodeGraph<n_type,e_type>* n = nodes[i];

        points.push_back(n->getPoint());
    }

    resetLimit();
    updateLimit(points);
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::hover(NodeGraph<n_type,e_type>* node){
    bool found = false;

    for (int i = 0;i < nodes.size(); i++) {
        NodeGraph<n_type,e_type>* n = nodes[i];
        if(n->equals(node))
            found = true;
    }

    if(!found)
        return;

    node->hover();
    hovered = node;
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::unhover(){
    if(hovered != NULL){
        hovered->unhover();
        hovered = NULL;
    }
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::select(NodeGraph<n_type,e_type>* node){
    bool found = false;

    for (int i = 0;i < nodes.size(); i++) {
        NodeGraph<n_type,e_type>* n = nodes[i];
        if(n->equals(node))
            found = true;
    }

    if(!found)
        return;

    node->select();
    selected = node;
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::unselect(){
    if(selected != NULL){
        selected->unselect();
        selected = NULL;
    }
}

//METHODS

template <class n_type, class e_type>
bool Graph<n_type,e_type>::addNode(n_type data, double x, double y, double z){
    QString label = label.asprintf("NODE_GRAPH_%d",N);

    NodeGraph<n_type,e_type>* n = new NodeGraph<n_type,e_type>(N,label,data,x,y,z,this);

    //ADJUSTS SIZE OF GRAPHIC OBJECT
    if(N == 0){
        resetLimit();
        updateLimit(point(ZERO,x,y,z));
    } else {
        updateLimit(point(ZERO,x,y,z));
    }

    nodes.push_back(n);
    N++;

    return true;
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::addEdge(bool directional, NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end, e_type weight){
    if(start == NULL || end == NULL)
        return false;

    if(start->neighboorOf(end))
        return false;

    if(!directional)
        if(end->neighboorOf(start))
            return false;

    if(directional){
        start->addEdge(end,weight);
    } else {
        start->addEdge(end,weight);
        end->addEdge(start,weight);
    }

    M++;

    return true;
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::updateEdge(bool directional, NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end, e_type weight){
    int nFound = 0; //counter of nodes found
    int startID = 0;
    int endID = 0;

    if(start->equals(end))
        return false;

    for(int i = 0; i < nodes.size() || nFound < 2; i++){
        if(start->equals(nodes[i])){
            startID = i;
            nFound ++;
        }

        if(end->equals(nodes[i])){
            endID = i;
            nFound++;
        }
    }

    if(nFound != 2)
        return false;

    if(!nodes[startID]->neighboorOf(nodes[endID]))
        return false;

    if(directional){
        start->updateCost(end,weight);
    } else {
        start->updateCost(end,weight);
        end->updateCost(start,weight);
    }

    return true;
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::clear(){
    N = 0;
    M = 0;
    start = NULL;
    end = NULL;
    selected = NULL;
    hovered = NULL;
    nodes.clear();
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::deleteNode(double x, double y){
    return deleteNode(selectNode(x,y,0.0));
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::deleteNode(NodeGraph<n_type,e_type>* n){
    //Deletes Node from Nodes List
    for(int i = 0; i < nodes.size(); i++){
        int id_a = n->getId();
        int id_b = nodes[i]->getId();
        if(id_a == id_b){
            //Deletes all Edges that references n
            for(int j = 0; j < nodes.size(); j++){
                nodes[j]->removeEdge(nodes[i]);
            }

            nodes.erase(nodes.begin() + i);

            //ADJUSTS SIZE OF THE GRAPHIC OBJECT
            if(N == 0){
                resetLimit();
                updateLimit(point(ZERO,ZERO,ZERO));
            } else {
                vector<point> aux;

                for(int j = 0; j < nodes.size(); j++){
                    NodeGraph<n_type,e_type>* n = nodes[i];
                    aux.push_back(n->getPoint());
                }

                updateLimit(aux);
            }

            N--;
            delete n;
            return true;
        }
    }

    return false;
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::deleteEdge(NodeGraph<n_type,e_type>* a,NodeGraph<n_type,e_type>* b){
    if(!hasEdge(a,b))
        return false;

    a->removeEdge(b);

    return true;
}

template <class n_type, class e_type>
int Graph<n_type,e_type>::edges(){
    return M;
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::hasNode(NodeGraph<n_type,e_type>* n){
    for (int i = 0; i < nodes.size(); i++) {
        if(n->equals(nodes[i]))
            return true;
    }
    return false;
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::populate(){
    populate(MathHelper::random(DEFAULT_N),MathHelper::random(DEFAULT_M));
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::populate(int N, int M, double x, double y, double z){
    MathHelper::randomSeed();
    //POPULATES NODES
    populateNodes(N,x,y,z);

    //POPULATES EDGES
    populateEdges(M,x,y,z);
}

template <>
void Graph<QString,int>::populateNodes(int N, double x, double y, double z){
    for(int i = 0; i < N; i ++){
        double x1 = MathHelper::random(x,-x);
        double y1 = MathHelper::random(y,-y);
        double z1 = MathHelper::random(z,-z);

        addNode(QString::asprintf("s%d",this->N),x1,y1,z1);
    }
}

template <>
void Graph<QString,float>::populateNodes(int N, double x, double y, double z){
    for(int i = 0; i < N; i ++){
        double x1 = MathHelper::random(x,-x);
        double y1 = MathHelper::random(y,-y);
        double z1 = MathHelper::random(z,-z);

        addNode(QString::asprintf("s%d",this->N),x1,y1,z1);
    }
}

template <>
void Graph<QString,double>::populateNodes(int N, double x, double y, double z){
    for(int i = 0; i < N; i ++){
        double x1 = MathHelper::random(x,-x);
        double y1 = MathHelper::random(y,-y);
        double z1 = MathHelper::random(z,-z);

        addNode(QString::asprintf("s%d",this->N),x1,y1,z1);
    }
}

template <>
void Graph<QString,QString>::populateNodes(int N, double x, double y, double z){
    for(int i = 0; i < N; i ++){
        double x1 = MathHelper::random(x,-x);
        double y1 = MathHelper::random(y,-y);
        double z1 = MathHelper::random(z,-z);

        addNode(QString::asprintf("s%d",this->N),x1,y1,z1);
    }
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::populateNodes(int N, double x, double y, double z){
    for(int i = 0; i < N; i ++){
        double x1 = MathHelper::random(x,-x);
        double y1 = MathHelper::random(y,-y);
        double z1 = MathHelper::random(z,-z);

        addNode(ONE,x1,y1,z1);
    }
}

template <>
void Graph<int,QString>::populateEdges(int M, double x, double y, double z){
    for(int i = 0; i < M; i++){
        int rand_i = MathHelper::random(N);
        int rand_j = MathHelper::random(N);

        NodeGraph<int,QString>* a = this->operator [](rand_i);
        NodeGraph<int,QString>* b = this->operator [](rand_j);

        if(a == NULL || b == NULL){
            i--;
        } else {
            if(!addEdge(false,a,b,QString::asprintf("t%d",this->M)))
                i--;
        }
    }
}

template <>
void Graph<float,QString>::populateEdges(int M, double x, double y, double z){
    for(int i = 0; i < M; i++){
        int rand_i = MathHelper::random(N);
        int rand_j = MathHelper::random(N);

        NodeGraph<float,QString>* a = this->operator [](rand_i);
        NodeGraph<float,QString>* b = this->operator [](rand_j);

        if(a == NULL || b == NULL){
            i--;
        } else {
            if(!addEdge(false,a,b,QString::asprintf("t%d",this->M)))
                i--;
        }
    }
}

template <>
void Graph<double,QString>::populateEdges(int M, double x, double y, double z){
    for(int i = 0; i < M; i++){
        int rand_i = MathHelper::random(N);
        int rand_j = MathHelper::random(N);

        NodeGraph<double,QString>* a = this->operator [](rand_i);
        NodeGraph<double,QString>* b = this->operator [](rand_j);

        if(a == NULL || b == NULL){
            i--;
        } else {
            if(!addEdge(false,a,b,QString::asprintf("t%d",this->M)))
                i--;
        }
    }
}

template <>
void Graph<QString,QString>::populateEdges(int M, double x, double y, double z){
    for(int i = 0; i < M; i++){
        int rand_i = MathHelper::random(N);
        int rand_j = MathHelper::random(N);

        NodeGraph<QString,QString>* a = this->operator [](rand_i);
        NodeGraph<QString,QString>* b = this->operator [](rand_j);

        if(a == NULL || b == NULL){
            i--;
        } else {
            if(!addEdge(false,a,b,QString::asprintf("t%d",this->M)))
                i--;
        }
    }
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::populateEdges(int M, double x, double y, double z){
    for(int i = 0; i < M; i++){
        int rand_i = MathHelper::random(N);
        int rand_j = MathHelper::random(N);

        NodeGraph<n_type,e_type>* a = this->operator [](rand_i);
        NodeGraph<n_type,e_type>* b = this->operator [](rand_j);

        if(a == NULL || b == NULL){
            i--;
        } else {
            if(!addEdge(false,a,b,ONE))
                i--;
        }
    }
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::populate(vector<n_type> data){
    for(int i = 0; i < data.size(); i ++){
        double x = MathHelper::random(DEFAULT_x,-DEFAULT_x);
        double y = MathHelper::random(DEFAULT_y,-DEFAULT_y);
        double z = MathHelper::random(DEFAULT_z,-DEFAULT_z);
        addNode(data[i],x,y,z);
    }
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::populate(vector<point> pos, vector<n_type> data){
    if(pos.size() != data.size())
        return;

    for(int i = 0; i < data.size(); i ++){
        point p = pos[i];
        addNode(data[i],p.X(),p.Y());
    }
}

template <class n_type, class e_type>
int Graph<n_type,e_type>::size(){
    return N;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* Graph<n_type,e_type>::operator[] (int n){
    if(n < 0)
        return NULL;

    if(n < N)
        return nodes[n];

    return NULL;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* Graph<n_type,e_type>::operator() (int n,int e){
    if(n < N){
        NodeGraph<n_type,e_type>* node = nodes[n];
        vector<NodeGraph<n_type,e_type>*> edges = node->getEdges();
        if(e < edges.size()){
            return edges[e];
        }
    }

    return NULL;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* Graph<n_type,e_type>::selectNode(double x, double y, double radius){
    point p = point(0,x,y);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<n_type,e_type>* n = nodes[i];
        point aux = n->getPoint();
        if(MathHelper::euclideanDistance(&p,&aux) <= radius)
            return n;
    }

    return NULL;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* Graph<n_type,e_type>::getStart(){
    return start;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>* Graph<n_type,e_type>::getEnd(){
    return end;
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::dominant(Solution* s){
    return dominant(s->getVector());
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::dominant(Solution* s, vector<int>* dmntd, vector<int>* non_dmntd){
    if(dmntd == NULL || non_dmntd == NULL)
        return false;

    dmntd->clear();
    non_dmntd->clear();

    int dominant = 0;
    vector<int> nodes = s->getVector();
    for(int i = 0; i < N; i++){
        if (MathHelper::isIn(&nodes,this->operator [](i)->getId())){
            dominant ++;
        } else {
            //NOT DOMINANT, MUT BE DOMINATED
            bool found = false;
            NodeGraph<n_type,e_type>* n = operator[](i);

            vector<NodeGraph<n_type,e_type>*> neighboors = n->getEdges();
            for(int j = 0; j < neighboors.size(); j++){
                NodeGraph<n_type,e_type>* n_aux = neighboors[j];
                if(MathHelper::isIn(&nodes,n_aux->getId())){
                    found = true;
                    break;
                }
            }

            if(found){//DOMINATED
                dmntd->push_back(i);
            } else {//NON=DOMINATED
                non_dmntd->push_back(i);
            }
        }
    }

    return non_dmntd->empty();
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::dominant(Solution* s, vector<pair<int,int>>* dmntd, vector<int>* non_dmntd){
    if(dmntd == NULL || non_dmntd == NULL)
        return false;

    dmntd->clear();
    non_dmntd->clear();

    int dominant = 0;
    vector<int> nodes = s->getVector();
    for(int i = 0; i < N; i++){
        if (MathHelper::isIn(&nodes,this->operator [](i)->getId())){
            dominant ++;
        } else {
            //NOT DOMINANT, MUT BE DOMINATED
            int found = 0;
            NodeGraph<n_type,e_type>* n = operator[](i);

            vector<NodeGraph<n_type,e_type>*> neighboors = n->getEdges();
            for(int j = 0; j < neighboors.size(); j++){
                NodeGraph<n_type,e_type>* n_aux = neighboors[j];
                if(MathHelper::isIn(&nodes,n_aux->getId())){
                    found ++;
                }
            }

            if(found != 0){//DOMINATED
                pair<int,int> r;

                r.first = i;
                r.second = found;

                dmntd->push_back(r);
            } else {//NON=DOMINATED
                non_dmntd->push_back(i);
            }
        }
    }

    return non_dmntd->empty();
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::dominant(vector<int> nodes){
    int dominant = 0;
    vector<int> dominated;
    int dominated_n = 0;

    for(int i = 0; i < N; i++){
        if (MathHelper::isIn(&nodes,this->operator [](i)->getId())){
            dominant ++;
        } else {
            //NOT DOMINANT, MUT BE DOMINATED
            bool found = false;
            NodeGraph<n_type,e_type>* n = operator[](i);

            vector<NodeGraph<n_type,e_type>*> neighboors = n->getEdges();
            for(int j = 0; j < neighboors.size(); j++){
                NodeGraph<n_type,e_type>* n_aux = neighboors[j];
                if(MathHelper::isIn(&nodes,n_aux->getId())){
                    found = true;
                    break;
                }
            }

            if(found){//DOMINATED
                dominated_n++;
            } else {//NON=DOMINATED
                return false;
            }
        }
    }

    return true;
}

template <class n_type, class e_type>
int Graph<n_type,e_type>::countNonDominated(Solution* s){
    return countNonDominated(s->getVector());
}

template <class n_type, class e_type>
int Graph<n_type,e_type>::countNonDominated(vector<int> nodes){
    vector<NodeGraph<n_type,e_type>*> n = getSolutionNodes(nodes);
    vector<int> non_dominated;
    vector<int> dominated;

    for(int i = 0; i < n.size(); i++){
        NodeGraph<n_type,e_type>* aux = n[i];

        vector<NodeGraph<n_type,e_type>*> neighboors;
        aux->getEdges(&neighboors);
        for(int j = 0; j < neighboors.size(); j++){
            NodeGraph<n_type,e_type>* neighboor = neighboors[j];
            if(!MathHelper::isIn(&nodes,neighboor->getId()) && !MathHelper::isIn(&dominated,neighboor->getId()))
                dominated.push_back(neighboor->getId());
        }
    }

    for(int i = 0; i < N; i++){
        if(!MathHelper::isIn(&nodes,i) && !MathHelper::isIn(&dominated,i))
            non_dominated.push_back(i);
    }

    return non_dominated.size();
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::connex(){
    vector<int> aux;
    for(int i = 0; i < N; i++)
        aux.push_back(i);

    return connex(aux);
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::connex(Solution* s){
    return connex(s->getVector());
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::connex(vector<int> nodes){
    vector<int> visited;
    vector<int> non_visited;

    for(int i = 0; i < nodes.size(); i++){
        if(i == ZERO){
            visited.push_back(nodes[ZERO]);
        } else {
            non_visited.push_back(nodes[i]);
        }
    }

    NodeGraph<n_type,e_type>* it = this->operator [](nodes[ZERO]);
    vector<NodeGraph<n_type,e_type>*> neighboors = it->getEdges();

    //CONNEX GRANDCHILDREN
    for(int k = 0; k < neighboors.size() && visited.size() < nodes.size(); k++){
        NodeGraph<n_type,e_type>* aux = neighboors[k];

        if(MathHelper::isIn(&non_visited,aux->getId())){
            bool found = false;
            int m = 0;
            for (m = 0; m < non_visited.size() && !found; m++) {
                if(non_visited[m] == aux->getId()){
                    found = true;
                }
            }

            if(!found)
                return false;

            visited.push_back(non_visited[(m - 1)]);
            non_visited.erase(non_visited.begin() + (m - 1));
            connex(&non_visited,&visited,aux);

        }
    }

    return (visited.size() == nodes.size());
}


template <class n_type, class e_type>
void Graph<n_type,e_type>::connex(vector<int>* non_visited, vector<int>* visited, NodeGraph<n_type,e_type>* it){
    if(visited == NULL || non_visited == NULL)
        return;

    vector<NodeGraph<n_type,e_type>*> neighboors = it->getEdges();

    //CONNEX GRANDCHILDREN
    for(int k = 0; k < neighboors.size() && visited->size() < nodes.size(); k++){
        NodeGraph<n_type,e_type>* aux = neighboors[k];

        if(MathHelper::isIn(non_visited,aux->getId())){
            bool found = false;
            int m = 0;
            for (m = 0; m < (*non_visited).size() && !found; m++) {
                if((*non_visited)[m] == aux->getId()){
                    found = true;
                }
            }

            if(!found)
                return;

            visited->push_back(non_visited->operator[](m - 1));
            non_visited->erase(non_visited->begin() + (m - 1));
            connex(non_visited,visited,aux);

        }
    }
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::connex(vector<NodeGraph<n_type,e_type>*> nodes){
    vector<int> n;

    for(int i = 0; i < nodes.size(); i++)
        n.push_back(nodes[i]->getId());

    return connex(n);
}

template <class n_type, class e_type>
int Graph<n_type,e_type>::depthTree(vector<int>* visited,vector<int>* parent, NodeGraph<n_type,e_type>* root){
    visited->push_back(root->getId());
    parent->push_back(ZERO);
    bool answer = true;
    int r = 0;

    NodeGraph<n_type,e_type>* it = root;
    while(visited->size() != nodes.size() && answer){
        vector<NodeGraph<n_type,e_type>*> neighboors = it->getEdges();

        //ADDS ALL NEIGHBOORS
        for(int i = 0; i < neighboors.size(); i++){
            int x = neighboors[i]->getId();

            if(!MathHelper::isIn(visited,x)){
                r++;
                depthTreeAux(visited,parent,neighboors[i],it->getId());
            }
        }
    }

    return r;
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::depthTreeAux(vector<int>* visited, vector<int>* parent, NodeGraph<n_type,e_type>* it, int father){
    visited->push_back(it->getId());
    parent->push_back(father);
    vector<NodeGraph<n_type,e_type>*> neighboors = it->getEdges();

    //ADDS ALL NEIGHBOORS
    for(int i = 0; i < neighboors.size(); i++){
        int x = neighboors[i]->getId();

        if(!MathHelper::isIn(visited,x))
            depthTreeAux(visited,parent,neighboors[i],it->getId());
    }
}

//SMART OBJECT METHODS

template <>
vector<QString> Graph<int,QString>::print(){
    vector<QString> txt;
    QString line;
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* GRAPH                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = line.asprintf("N= %d - M = %d", N, M);
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<int,QString>* currentNode = nodes[i];
        line = "******************************************************************************";
        txt.push_back(line);
        line = line.asprintf("* NODE (%d)                                                                  *", currentNode->getID());
        txt.push_back(line);
        line = line.asprintf(" * AT (%.3f,.3f)                                                 *",currentNode->getPoint().X(),currentNode->getPoint().Y());
        line = "********************************EDGES*****************************************";
        txt.push_back(line);

        vector<NodeGraph<int,QString>*> edges = currentNode->getEdges();
        vector<QString> costs = currentNode->getCosts();
        int start = currentNode->getId();
        QString lblStart = line.asprintf("(%.3f,%.3f,%.3f)",currentNode->getPoint().X(),currentNode->getPoint().Y(),currentNode->getPoint().Z());

        for(int j = 0; j < edges.size(); j++){
            int end = edges[j]->getId();
            QString lblEnd = line.asprintf("(%.3f,%.3f,%.3f)",edges[j]->getPoint().X(),edges[j]->getPoint().Y(),edges[j]->getPoint().Z());
            QString cost = costs[j];
            line = line.asprintf("( %d ) %s#%d -> %s#%d, %s",j,lblStart.toLatin1().data(),start,lblEnd.toLatin1().data(),end,cost.toStdString().data());
            txt.push_back(line);
        }

        line = "******************************************************************************";
        txt.push_back(line);
    }

    line = "******************************************************************************";
    txt.push_back(line);
    line = "* END OF GRAPH                                                               *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    return txt;
}

template <>
vector<QString> Graph<float,QString>::print(){
    vector<QString> txt;
    QString line;
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* GRAPH                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = line.asprintf("N= %d - M = %d", N, M);
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<float,QString>* currentNode = nodes[i];
        line = "******************************************************************************";
        txt.push_back(line);
        line = line.asprintf("* NODE (%d)                                                                  *", currentNode->getID());
        txt.push_back(line);
        line = line.asprintf(" * AT (%.3f,.3f)                                                 *",currentNode->getPoint().X(),currentNode->getPoint().Y());
        line = "********************************EDGES*****************************************";
        txt.push_back(line);

        vector<NodeGraph<float,QString>*> edges = currentNode->getEdges();
        vector<QString> costs = currentNode->getCosts();
        int start = currentNode->getId();
        QString lblStart = line.asprintf("(%.3f,%.3f,%.3f)",currentNode->getPoint().X(),currentNode->getPoint().Y(),currentNode->getPoint().Z());

        for(int j = 0; j < edges.size(); j++){
            int end = edges[j]->getId();
            QString lblEnd = line.asprintf("(%.3f,%.3f,%.3f)",edges[j]->getPoint().X(),edges[j]->getPoint().Y(),edges[j]->getPoint().Z());
            QString cost = costs[j];
            line = line.asprintf("( %d ) %s#%d -> %s#%d, %s",j,lblStart.toLatin1().data(),start,lblEnd.toLatin1().data(),end,cost.toStdString().data());
            txt.push_back(line);
        }

        line = "******************************************************************************";
        txt.push_back(line);
    }

    line = "******************************************************************************";
    txt.push_back(line);
    line = "* END OF GRAPH                                                               *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    return txt;
}

template <>
vector<QString> Graph<double,QString>::print(){
    vector<QString> txt;
    QString line;
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* GRAPH                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = line.asprintf("N= %d - M = %d", N, M);
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<double,QString>* currentNode = nodes[i];
        line = "******************************************************************************";
        txt.push_back(line);
        line = line.asprintf("* NODE (%d)                                                                  *", currentNode->getID());
        txt.push_back(line);
        line = line.asprintf(" * AT (%.3f,.3f)                                                 *",currentNode->getPoint().X(),currentNode->getPoint().Y());
        line = "********************************EDGES*****************************************";
        txt.push_back(line);

        vector<NodeGraph<double,QString>*> edges = currentNode->getEdges();
        vector<QString> costs = currentNode->getCosts();
        int start = currentNode->getId();
        QString lblStart = line.asprintf("(%.3f,%.3f,%.3f)",currentNode->getPoint().X(),currentNode->getPoint().Y(),currentNode->getPoint().Z());

        for(int j = 0; j < edges.size(); j++){
            int end = edges[j]->getId();
            QString lblEnd = line.asprintf("(%.3f,%.3f,%.3f)",edges[j]->getPoint().X(),edges[j]->getPoint().Y(),edges[j]->getPoint().Z());
            QString cost = costs[j];
            line = line.asprintf("( %d ) %s#%d -> %s#%d, %s",j,lblStart.toLatin1().data(),start,lblEnd.toLatin1().data(),end,cost.toStdString().data());
            txt.push_back(line);
        }

        line = "******************************************************************************";
        txt.push_back(line);
    }

    line = "******************************************************************************";
    txt.push_back(line);
    line = "* END OF GRAPH                                                               *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    return txt;
}

template <>
vector<QString> Graph<QString,QString>::print(){
    vector<QString> txt;
    QString line;
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* GRAPH                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = line.asprintf("N= %d - M = %d", N, M);
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<QString,QString>* currentNode = nodes[i];
        line = "******************************************************************************";
        txt.push_back(line);
        line = line.asprintf("* NODE (%d)                                                                  *", currentNode->getID());
        txt.push_back(line);
        line = line.asprintf(" * AT (%.3f,.3f)                                                 *",currentNode->getPoint().X(),currentNode->getPoint().Y());
        line = "********************************EDGES*****************************************";
        txt.push_back(line);

        vector<NodeGraph<QString,QString>*> edges = currentNode->getEdges();
        vector<QString> costs = currentNode->getCosts();
        int start = currentNode->getId();
        QString lblStart = line.asprintf("(%.3f,%.3f,%.3f)",currentNode->getPoint().X(),currentNode->getPoint().Y(),currentNode->getPoint().Z());

        for(int j = 0; j < edges.size(); j++){
            int end = edges[j]->getId();
            QString lblEnd = line.asprintf("(%.3f,%.3f,%.3f)",edges[j]->getPoint().X(),edges[j]->getPoint().Y(),edges[j]->getPoint().Z());
            QString cost = costs[j];
            line = line.asprintf("( %d ) %s#%d -> %s#%d, %s",j,lblStart.toLatin1().data(),start,lblEnd.toLatin1().data(),end,cost.toStdString().data());
            txt.push_back(line);
        }

        line = "******************************************************************************";
        txt.push_back(line);
    }

    line = "******************************************************************************";
    txt.push_back(line);
    line = "* END OF GRAPH                                                               *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    return txt;
}

template <class n_type, class e_type>
vector<QString> Graph<n_type,e_type>::print(){
    vector<QString> txt;
    QString line;
    line = "******************************************************************************";
    txt.push_back(line);
    line = "* GRAPH                                                                      *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);
    line = line.asprintf("N= %d - M = %d", N, M);
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<n_type,e_type>* currentNode = nodes[i];
        line = "******************************************************************************";
        txt.push_back(line);
        line = line.asprintf("* NODE (%d)                                                                  *", currentNode->getID());
        txt.push_back(line);
        line = line.asprintf(" * AT (%.3f,.3f)                                                 *",currentNode->getPoint().X(),currentNode->getPoint().Y());
        line = "********************************EDGES*****************************************";
        txt.push_back(line);

        vector<NodeGraph<n_type,e_type>*> edges = currentNode->getEdges();
        vector<e_type> costs = currentNode->getCosts();
        int start = currentNode->getId();
        QString lblStart = line.asprintf("(%.3f,%.3f,%.3f)",currentNode->getPoint().X(),currentNode->getPoint().Y(),currentNode->getPoint().Z());

        for(int j = 0; j < edges.size(); j++){
            int end = edges[j]->getId();
            QString lblEnd = line.asprintf("(%.3f,%.3f,%.3f)",edges[j]->getPoint().X(),edges[j]->getPoint().Y(),edges[j]->getPoint().Z());
            e_type cost = costs[j];
            line = line.asprintf("( %d ) %s#%d -> %s#%d, %d",j,lblStart.toLatin1().data(),start,lblEnd.toLatin1().data(),end,cost);
            txt.push_back(line);
        }

        line = "******************************************************************************";
        txt.push_back(line);
    }

    line = "******************************************************************************";
    txt.push_back(line);
    line = "* END OF GRAPH                                                               *";
    txt.push_back(line);
    line = "******************************************************************************";
    txt.push_back(line);

    return txt;
}

template <class n_type, class e_type>
vector<QString> Graph<n_type,e_type>::data(){
    return print();
}

template <>
vector<QString> Graph<QString,int>::raw(){
    vector<QString> txt;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    txt.push_back(line);

    line = line.asprintf("GRAPH OUTPUT SmartID = %d\n",getID());
    txt.push_back(line);

    line = line.asprintf("ASCII\n");
    txt.push_back(line);

    line = line.asprintf("DATASET POLYDATA\n");
    txt.push_back(line);

    line = line.asprintf("POINTS  %d  double", N );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < nodes.size(); i++){
        point p = nodes[i]->getPoint();
        line = line.asprintf("%f  %f  %f", p.X(), p.Y(), p.Z());
        txt.push_back(line);
    }

    //PRINTS LINES (FOLLOWED BY WEIGHT)
    line = line.asprintf("");
    txt.push_back(line);
    line = line.asprintf("LINES %d %d", M, M * 3);
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        vector<NodeGraph<QString,int>*> nAux = nodes[i]->getEdges();
        for(int j = 0; j < nAux.size(); j++){
            line = line.asprintf("2  %d %d", i, nAux[j]->getId());
            txt.push_back(line);
        }
    }

    line = line.asprintf("POINT_DATA  %d", N);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<QString,int>* nAux = nodes[i];
        line = line.asprintf("%d", nAux->buffer.toStdString().data());
        txt.push_back(line);
    }
    line = line.asprintf("CELL_DATA  %d",M);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<QString,int>* nAux = nodes[i];
        vector<int> costs = nAux->getCosts();
        for(int j = 0; j < costs.size(); j++){
            line = line.asprintf("%d", costs[j]);
            txt.push_back(line);
        }
    }

    txt.push_back(line);
    return txt;
}

template <>
vector<QString> Graph<QString,float>::raw(){
    vector<QString> txt;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    txt.push_back(line);

    line = line.asprintf("GRAPH OUTPUT SmartID = %d\n",getID());
    txt.push_back(line);

    line = line.asprintf("ASCII\n");
    txt.push_back(line);

    line = line.asprintf("DATASET POLYDATA\n");
    txt.push_back(line);

    line = line.asprintf("POINTS  %d  double", N );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < nodes.size(); i++){
        point p = nodes[i]->getPoint();
        line = line.asprintf("%f  %f  %f", p.X(), p.Y(), p.Z());
        txt.push_back(line);
    }

    //PRINTS LINES (FOLLOWED BY WEIGHT)
    line = line.asprintf("");
    txt.push_back(line);
    line = line.asprintf("LINES %d %d", M, M * 3);
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        vector<NodeGraph<QString,float>*> nAux = nodes[i]->getEdges();
        for(int j = 0; j < nAux.size(); j++){
            line = line.asprintf("2  %d %d", i, nAux[j]->getId());
            txt.push_back(line);
        }
    }

    line = line.asprintf("POINT_DATA  %d", N);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<QString,float>* nAux = nodes[i];
        line = line.asprintf("%d", nAux->buffer.toStdString().data());
        txt.push_back(line);
    }
    line = line.asprintf("CELL_DATA  %d",M);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<QString,float>* nAux = nodes[i];
        vector<float> costs = nAux->getCosts();
        for(int j = 0; j < costs.size(); j++){
            line = line.asprintf("%d", costs[j]);
            txt.push_back(line);
        }
    }

    txt.push_back(line);
    return txt;
}

template <>
vector<QString> Graph<QString,double>::raw(){
    vector<QString> txt;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    txt.push_back(line);

    line = line.asprintf("GRAPH OUTPUT SmartID = %d\n",getID());
    txt.push_back(line);

    line = line.asprintf("ASCII\n");
    txt.push_back(line);

    line = line.asprintf("DATASET POLYDATA\n");
    txt.push_back(line);

    line = line.asprintf("POINTS  %d  double", N );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < nodes.size(); i++){
        point p = nodes[i]->getPoint();
        line = line.asprintf("%f  %f  %f", p.X(), p.Y(), p.Z());
        txt.push_back(line);
    }

    //PRINTS LINES (FOLLOWED BY WEIGHT)
    line = line.asprintf("");
    txt.push_back(line);
    line = line.asprintf("LINES %d %d", M, M * 3);
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        vector<NodeGraph<QString,double>*> nAux = nodes[i]->getEdges();
        for(int j = 0; j < nAux.size(); j++){
            line = line.asprintf("2  %d %d", i, nAux[j]->getId());
            txt.push_back(line);
        }
    }

    line = line.asprintf("POINT_DATA  %d", N);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<QString,double>* nAux = nodes[i];
        line = line.asprintf("%d", nAux->buffer.toStdString().data());
        txt.push_back(line);
    }
    line = line.asprintf("CELL_DATA  %d",M);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<QString,double>* nAux = nodes[i];
        vector<double> costs = nAux->getCosts();
        for(int j = 0; j < costs.size(); j++){
            line = line.asprintf("%d", costs[j]);
            txt.push_back(line);
        }
    }

    txt.push_back(line);
    return txt;
}

template <>
vector<QString> Graph<int,QString>::raw(){
    vector<QString> txt;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    txt.push_back(line);

    line = line.asprintf("GRAPH OUTPUT SmartID = %d\n",getID());
    txt.push_back(line);

    line = line.asprintf("ASCII\n");
    txt.push_back(line);

    line = line.asprintf("DATASET POLYDATA\n");
    txt.push_back(line);

    line = line.asprintf("POINTS  %d  double", N );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < nodes.size(); i++){
        point p = nodes[i]->getPoint();
        line = line.asprintf("%f  %f  %f", p.X(), p.Y(), p.Z());
        txt.push_back(line);
    }

    //PRINTS LINES (FOLLOWED BY WEIGHT)
    line = line.asprintf("");
    txt.push_back(line);
    line = line.asprintf("LINES %d %d", M, M * 3);
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        vector<NodeGraph<int,QString>*> nAux = nodes[i]->getEdges();
        for(int j = 0; j < nAux.size(); j++){
            line = line.asprintf("2  %d %d", i, nAux[j]->getId());
            txt.push_back(line);
        }
    }

    line = line.asprintf("POINT_DATA  %d", N);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<int,QString>* nAux = nodes[i];
        line = line.asprintf("%d", nAux->buffer);
        txt.push_back(line);
    }
    line = line.asprintf("CELL_DATA  %d",M);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<int,QString>* nAux = nodes[i];
        vector<QString> costs = nAux->getCosts();
        for(int j = 0; j < costs.size(); j++){
            line = line.asprintf("%d", costs[j].toStdString().data());
            txt.push_back(line);
        }
    }

    txt.push_back(line);
    return txt;
}

template <>
vector<QString> Graph<float,QString>::raw(){
    vector<QString> txt;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    txt.push_back(line);

    line = line.asprintf("GRAPH OUTPUT SmartID = %d\n",getID());
    txt.push_back(line);

    line = line.asprintf("ASCII\n");
    txt.push_back(line);

    line = line.asprintf("DATASET POLYDATA\n");
    txt.push_back(line);

    line = line.asprintf("POINTS  %d  double", N );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < nodes.size(); i++){
        point p = nodes[i]->getPoint();
        line = line.asprintf("%f  %f  %f", p.X(), p.Y(), p.Z());
        txt.push_back(line);
    }

    //PRINTS LINES (FOLLOWED BY WEIGHT)
    line = line.asprintf("");
    txt.push_back(line);
    line = line.asprintf("LINES %d %d", M, M * 3);
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        vector<NodeGraph<float,QString>*> nAux = nodes[i]->getEdges();
        for(int j = 0; j < nAux.size(); j++){
            line = line.asprintf("2  %d %d", i, nAux[j]->getId());
            txt.push_back(line);
        }
    }

    line = line.asprintf("POINT_DATA  %d", N);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<float,QString>* nAux = nodes[i];
        line = line.asprintf("%d", nAux->buffer);
        txt.push_back(line);
    }
    line = line.asprintf("CELL_DATA  %d",M);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<float,QString>* nAux = nodes[i];
        vector<QString> costs = nAux->getCosts();
        for(int j = 0; j < costs.size(); j++){
            line = line.asprintf("%d", costs[j].toStdString().data());
            txt.push_back(line);
        }
    }

    txt.push_back(line);
    return txt;
}

template <>
vector<QString> Graph<double,QString>::raw(){
    vector<QString> txt;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    txt.push_back(line);

    line = line.asprintf("GRAPH OUTPUT SmartID = %d\n",getID());
    txt.push_back(line);

    line = line.asprintf("ASCII\n");
    txt.push_back(line);

    line = line.asprintf("DATASET POLYDATA\n");
    txt.push_back(line);

    line = line.asprintf("POINTS  %d  double", N );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < nodes.size(); i++){
        point p = nodes[i]->getPoint();
        line = line.asprintf("%f  %f  %f", p.X(), p.Y(), p.Z());
        txt.push_back(line);
    }

    //PRINTS LINES (FOLLOWED BY WEIGHT)
    line = line.asprintf("");
    txt.push_back(line);
    line = line.asprintf("LINES %d %d", M, M * 3);
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        vector<NodeGraph<double,QString>*> nAux = nodes[i]->getEdges();
        for(int j = 0; j < nAux.size(); j++){
            line = line.asprintf("2  %d %d", i, nAux[j]->getId());
            txt.push_back(line);
        }
    }

    line = line.asprintf("POINT_DATA  %d", N);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<double,QString>* nAux = nodes[i];
        line = line.asprintf("%d", nAux->buffer);
        txt.push_back(line);
    }
    line = line.asprintf("CELL_DATA  %d",M);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<double,QString>* nAux = nodes[i];
        vector<QString> costs = nAux->getCosts();
        for(int j = 0; j < costs.size(); j++){
            line = line.asprintf("%d", costs[j].toStdString().data());
            txt.push_back(line);
        }
    }

    txt.push_back(line);
    return txt;
}

template <>
vector<QString> Graph<QString,QString>::raw(){
    vector<QString> txt;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    txt.push_back(line);

    line = line.asprintf("GRAPH OUTPUT SmartID = %d\n",getID());
    txt.push_back(line);

    line = line.asprintf("ASCII\n");
    txt.push_back(line);

    line = line.asprintf("DATASET POLYDATA\n");
    txt.push_back(line);

    line = line.asprintf("POINTS  %d  double", N );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < nodes.size(); i++){
        point p = nodes[i]->getPoint();
        line = line.asprintf("%f  %f  %f", p.X(), p.Y(), p.Z());
        txt.push_back(line);
    }

    //PRINTS LINES (FOLLOWED BY WEIGHT)
    line = line.asprintf("");
    txt.push_back(line);
    line = line.asprintf("LINES %d %d", M, M * 3);
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        vector<NodeGraph<QString,QString>*> nAux = nodes[i]->getEdges();
        for(int j = 0; j < nAux.size(); j++){
            line = line.asprintf("2  %d %d", i, nAux[j]->getId());
            txt.push_back(line);
        }
    }

    line = line.asprintf("POINT_DATA  %d", N);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<QString,QString>* nAux = nodes[i];
        line = line.asprintf("%d", nAux->buffer.toStdString().data());
        txt.push_back(line);
    }
    line = line.asprintf("CELL_DATA  %d",M);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<QString,QString>* nAux = nodes[i];
        vector<QString> costs = nAux->getCosts();
        for(int j = 0; j < costs.size(); j++){
            line = line.asprintf("%d", costs[j].toStdString().data());
            txt.push_back(line);
        }
    }

    txt.push_back(line);
    return txt;
}

template <class n_type, class e_type>
vector<QString> Graph<n_type,e_type>::raw(){
    vector<QString> txt;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    txt.push_back(line);

    line = line.asprintf("GRAPH OUTPUT SmartID = %d\n",getID());
    txt.push_back(line);

    line = line.asprintf("ASCII\n");
    txt.push_back(line);

    line = line.asprintf("DATASET POLYDATA\n");
    txt.push_back(line);

    line = line.asprintf("POINTS  %d  double", N );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < nodes.size(); i++){
        point p = nodes[i]->getPoint();
        line = line.asprintf("%f  %f  %f", p.X(), p.Y(), p.Z());
        txt.push_back(line);
    }

    //PRINTS LINES (FOLLOWED BY WEIGHT)
    line = line.asprintf("");
    txt.push_back(line);
    line = line.asprintf("LINES %d %d", M, M * 3);
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        vector<NodeGraph<n_type,e_type>*> nAux = nodes[i]->getEdges();
        for(int j = 0; j < nAux.size(); j++){
            line = line.asprintf("2  %d %d", i, nAux[j]->getId());
            txt.push_back(line);
        }
    }

    line = line.asprintf("POINT_DATA  %d", N);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<n_type,e_type>* nAux = nodes[i];
        line = line.asprintf("%d", nAux->buffer);
        txt.push_back(line);
    }

    line = line.asprintf("CELL_DATA  %d",M);
    txt.push_back(line);
    line = line.asprintf("scalars WEIGHT double");
    txt.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    txt.push_back(line);

    for(int i = 0; i < nodes.size(); i++){
        NodeGraph<n_type,e_type>* nAux = nodes[i];
        vector<e_type> costs = nAux->getCosts();
        for(int j = 0; j < costs.size(); j++){
            line = line.asprintf("%d", costs[j]);
            txt.push_back(line);
        }
    }

    txt.push_back(line);
    return txt;
}

template <class n_type, class e_type>
void Graph<n_type,e_type>::draw(){
    QColor selected_color = QColor("red");
    QColor hovered_color = QColor("orange");

    if(draw_edge){
        for(int i = 0; i < nodes.size(); i++){
            NodeGraph<n_type,e_type>* n = nodes[i];
            vector<NodeGraph<n_type,e_type>*> nEdges = n->getEdges();
            point start = n->getPoint();

            //DRAWS LINES
            for(int j = 0; j < nEdges.size(); j++){
                NodeGraph<n_type,e_type>* s = nEdges[j];
                point end = s->getPoint();
                drawArrow(&start,&end,sphere_radius,0.5);
            }
        }
    }
    if(draw_node){
        for(int i = 0; i < nodes.size(); i++){
            NodeGraph<n_type,e_type>* n = nodes[i];
            vector<NodeGraph<n_type,e_type>*> nEdges = n->getEdges();
            point start = n->getPoint();

            if(selected != NULL)
                if(selected->equals(n))
                    drawSphere(&start,sphere_radius * 1.5,selected_color);

            if(hovered != NULL)
                if(hovered->equals(n))
                    drawSphere(&start,sphere_radius * 1.5,hovered_color);

            drawSphere(&start,sphere_radius,n->c);
        }
    }

    drawContext();
}

template <>
GraphicObjectComposer Graph<QString,int>::new_node(point p){
    GraphicObjectComposer component;

    component.name = "NODE";

    component.pt = p;

    vector<Field> fields;
    Field f;

    f.names = "NODE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = " ";

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
GraphicObjectComposer Graph<QString,float>::new_node(point p){
    GraphicObjectComposer component;

    component.name = "NODE";

    component.pt = p;

    vector<Field> fields;
    Field f;

    f.names = "NODE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = " ";

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
GraphicObjectComposer Graph<QString,double>::new_node(point p){
    GraphicObjectComposer component;

    component.name = "NODE";

    component.pt = p;

    vector<Field> fields;
    Field f;

    f.names = "NODE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = " ";

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
GraphicObjectComposer Graph<QString,QString>::new_node(point p){
    GraphicObjectComposer component;

    component.name = "NODE";

    component.pt = p;

    vector<Field> fields;
    Field f;

    f.names = "NODE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = " ";

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <class n_type, class e_type>
GraphicObjectComposer Graph<n_type,e_type>::new_node(point p){
    GraphicObjectComposer component;

    component.name = "NODE";

    component.pt = p;

    vector<Field> fields;
    Field f;

    f.names = "NODE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    f.current_value = " ";

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
GraphicObjectComposer Graph<QString,int>::edit_node(NodeGraph<QString,int>* n){
    GraphicObjectComposer component;

    component.name = QString::asprintf("NODE ID=%d",n->getId());

    component.pt = n->getPoint();

    vector<Field> fields;
    Field f;

    f.names = "NODE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = n->buffer;

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
GraphicObjectComposer Graph<QString,float>::edit_node(NodeGraph<QString,float>* n){
    GraphicObjectComposer component;

    component.name = QString::asprintf("NODE ID=%d",n->getId());

    component.pt = n->getPoint();

    vector<Field> fields;
    Field f;

    f.names = "NODE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = n->buffer;

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
GraphicObjectComposer Graph<QString,double>::edit_node(NodeGraph<QString,double>* n){
    GraphicObjectComposer component;

    component.name = QString::asprintf("NODE ID=%d",n->getId());

    component.pt = n->getPoint();

    vector<Field> fields;
    Field f;

    f.names = "NODE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = n->buffer;

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
GraphicObjectComposer Graph<QString,QString>::edit_node(NodeGraph<QString,QString>* n){
    GraphicObjectComposer component;

    component.name = QString::asprintf("NODE ID=%d",n->getId());

    component.pt = n->getPoint();

    vector<Field> fields;
    Field f;

    f.names = "NODE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = n->buffer;

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <class n_type, class e_type>
GraphicObjectComposer Graph<n_type,e_type>::edit_node(NodeGraph<n_type,e_type>* n){
    GraphicObjectComposer component;

    component.name = QString::asprintf("NODE ID=%d",n->getId());

    component.pt = n->getPoint();

    vector<Field> fields;
    Field f;

    f.names = "NODE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    f.current_value = n->buffer;

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
bool Graph<QString,int>::new_node(GraphicObjectComposer node){
    QString buffer;
    point p = node.pt;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "NODE_COST"){
            buffer = f.current_value;
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    addNode(buffer,p.X(),p.Y(),p.Z());
}

template <>
bool Graph<QString,float>::new_node(GraphicObjectComposer node){
    QString buffer;
    point p = node.pt;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "NODE_COST"){
            buffer = f.current_value;
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    addNode(buffer,p.X(),p.Y(),p.Z());
}

template <>
bool Graph<QString,double>::new_node(GraphicObjectComposer node){
    QString buffer;
    point p = node.pt;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "NODE_COST"){
            buffer = f.current_value;
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    addNode(buffer,p.X(),p.Y(),p.Z());
}

template <>
bool Graph<QString,QString>::new_node(GraphicObjectComposer node){
    QString buffer;
    point p = node.pt;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "NODE_COST"){
            buffer = f.current_value;
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    addNode(buffer,p.X(),p.Y(),p.Z());
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::new_node(GraphicObjectComposer node){
    n_type buffer;
    point p = node.pt;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "NODE_COST"){
            buffer = f.current_value.toDouble();
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    addNode(buffer,p.X(),p.Y(),p.Z());
}

template <>
bool Graph<QString,int>::edit_node(NodeGraph<QString,int>* n, GraphicObjectComposer node){
    QString buffer;
    point p = node.pt;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "NODE_COST"){
            buffer = f.current_value;
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    n->setPoint(p);
    n->buffer = buffer;
}

template <>
bool Graph<QString,float>::edit_node(NodeGraph<QString,float>* n, GraphicObjectComposer node){
    QString buffer;
    point p = node.pt;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "NODE_COST"){
            buffer = f.current_value;
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    n->setPoint(p);
    n->buffer = buffer;
}

template <>
bool Graph<QString,double>::edit_node(NodeGraph<QString,double>* n, GraphicObjectComposer node){
    QString buffer;
    point p = node.pt;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "NODE_COST"){
            buffer = f.current_value;
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    n->setPoint(p);
    n->buffer = buffer;
}

template <>
bool Graph<QString,QString>::edit_node(NodeGraph<QString,QString>* n, GraphicObjectComposer node){
    QString buffer;
    point p = node.pt;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "NODE_COST"){
            buffer = f.current_value;
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    n->setPoint(p);
    n->buffer = buffer;
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::edit_node(NodeGraph<n_type,e_type>* n, GraphicObjectComposer node){
    n_type buffer;
    point p = node.pt;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "NODE_COST"){
            buffer = f.current_value.toDouble();
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    n->setPoint(p);
    n->buffer = buffer;
}

template <>
GraphicObjectComposer Graph<int,QString>::new_edge(NodeGraph<int,QString>* start, NodeGraph<int,QString>* end){
    GraphicObjectComposer component;

    component.name = "EDGE";

    component.xyz[0] = false;
    component.xyz[1] = false;
    component.xyz[2] = false;

    vector<Field> fields;
    Field f;

    f.names = "EDGE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = " ";

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
GraphicObjectComposer Graph<float,QString>::new_edge(NodeGraph<float,QString>* start, NodeGraph<float,QString>* end){
    GraphicObjectComposer component;

    component.name = "EDGE";

    component.xyz[0] = false;
    component.xyz[1] = false;
    component.xyz[2] = false;

    vector<Field> fields;
    Field f;

    f.names = "EDGE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = " ";

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
GraphicObjectComposer Graph<double,QString>::new_edge(NodeGraph<double,QString>* start, NodeGraph<double,QString>* end){
    GraphicObjectComposer component;

    component.name = "EDGE";

    component.xyz[0] = false;
    component.xyz[1] = false;
    component.xyz[2] = false;

    vector<Field> fields;
    Field f;

    f.names = "EDGE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = " ";

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
GraphicObjectComposer Graph<QString,QString>::new_edge(NodeGraph<QString,QString>* start, NodeGraph<QString,QString>* end){
    GraphicObjectComposer component;

    component.name = "EDGE";

    component.xyz[0] = false;
    component.xyz[1] = false;
    component.xyz[2] = false;

    vector<Field> fields;
    Field f;

    f.names = "EDGE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = " ";

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <class n_type, class e_type>
GraphicObjectComposer Graph<n_type,e_type>::new_edge(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end){
    GraphicObjectComposer component;

    component.name = "EDGE";

    component.xyz[0] = false;
    component.xyz[1] = false;
    component.xyz[2] = false;

    vector<Field> fields;
    Field f;

    f.names = "EDGE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    f.current_value = " ";

    fields.push_back(f);

    component.fields = fields;

    return component;
}


template <class n_type, class e_type>
GraphicObjectComposer Graph<n_type,e_type>::edit_edge(NodeGraph<n_type,e_type>* start,NodeGraph<n_type,e_type>* end){
    bool found_node = hasNode(start);
    bool found_edge = false;
    e_type cost;
    GraphicObjectComposer component;

    if(!found_node)
        return component;

    vector<NodeGraph<n_type,e_type>*> edges = start->getEdges();
    vector<e_type> costs = start->getCosts();
    for(int i = 0; i < edges.size(); i++){
        NodeGraph<n_type,e_type>* e = edges[i];
        if(e->equals(end)){
            found_edge = true;
            cost = costs[i];
        }
    }

    if(!found_edge)
        return component;

    component.name = "EDGE";

    component.xyz[0] = false;
    component.xyz[1] = false;
    component.xyz[2] = false;

    vector<Field> fields;
    Field f;

    f.names = "EDGE_COST";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = cost;

    fields.push_back(f);

    component.fields = fields;

    return component;
}

template <>
bool Graph<int,QString>::new_edge(NodeGraph<int,QString>* start, NodeGraph<int,QString>* end, GraphicObjectComposer node){
    if(!hasNode(start) || !hasNode(end))
        return false;

    QString cost;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "EDGE_COST"){
            cost = QString(f.current_value.remove(' ').left(1));
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    addEdge(true,start,end,cost);

    return true;

}

template <>
bool Graph<float,QString>::new_edge(NodeGraph<float,QString>* start, NodeGraph<float,QString>* end, GraphicObjectComposer node){
    if(!hasNode(start) || !hasNode(end))
        return false;

    QString cost;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "EDGE_COST"){
            cost = QString(f.current_value.remove(' ').left(1));
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    addEdge(true,start,end,cost);

    return true;

}

template <>
bool Graph<double,QString>::new_edge(NodeGraph<double,QString>* start, NodeGraph<double,QString>* end, GraphicObjectComposer node){
    if(!hasNode(start) || !hasNode(end))
        return false;

    QString cost;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "EDGE_COST"){
            cost = QString(f.current_value.remove(' ').left(1));
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    addEdge(true,start,end,cost);

    return true;

}

template <>
bool Graph<QString,QString>::new_edge(NodeGraph<QString,QString>* start, NodeGraph<QString,QString>* end, GraphicObjectComposer node){
    QString cost;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "EDGE_COST"){
            cost = QString(f.current_value.remove(' ').left(1));
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    addEdge(true,start,end,cost);

    return true;

}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::new_edge(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end, GraphicObjectComposer node){
    if(!hasNode(start) || !hasNode(end))
        return false;

    e_type cost;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "EDGE_COST"){
            cost = f.current_value.toDouble();
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    addEdge(true,start,end,cost);

    return true;

}

template <>
bool Graph<int,QString>::edit_edge(NodeGraph<int,QString>* start, NodeGraph<int,QString>* end, GraphicObjectComposer node){
    if(!hasNode(start) || !hasNode(end))
        return false;

    QString cost;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "EDGE_COST"){
            cost = f.current_value;
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    return start->updateCost(end,cost);
}

template <>
bool Graph<float,QString>::edit_edge(NodeGraph<float,QString>* start, NodeGraph<float,QString>* end, GraphicObjectComposer node){
    if(!hasNode(start) || !hasNode(end))
        return false;

    QString cost;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "EDGE_COST"){
            cost = QString(f.current_value.remove(' ').left(1));
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    return start->updateCost(end,cost);
}

template <>
bool Graph<double,QString>::edit_edge(NodeGraph<double,QString>* start, NodeGraph<double,QString>* end, GraphicObjectComposer node){
    if(!hasNode(start) || !hasNode(end))
        return false;

    QString cost;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "EDGE_COST"){
            cost = QString(f.current_value.remove(' ').left(1));
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    return start->updateCost(end,cost);
}

template <>
bool Graph<QString,QString>::edit_edge(NodeGraph<QString,QString>* start, NodeGraph<QString,QString>* end, GraphicObjectComposer node){
    if(!hasNode(start) || !hasNode(end))
        return false;

    QString cost;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "EDGE_COST"){
            cost = QString(f.current_value.remove(' ').left(1));
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    return start->updateCost(end,cost);
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::edit_edge(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end, GraphicObjectComposer node){
    if(!hasNode(start) || !hasNode(end))
        return false;

    e_type cost;
    int read = 0;

    for(int i = 0; i < node.fields.size(); i++){
        Field f = node.fields[i];

        if(f.names == "EDGE_COST"){
            cost = f.current_value.toDouble();
            read++;
        } else {
            return false;
        }
    }

    if(read != 1){
        return false;
    }

    return start->updateCost(end,cost);
}

template <class n_type, class e_type>
bool Graph<n_type,e_type>::hasEdge(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end){
    bool found_node = hasNode(start);
    bool found_edge = false;
    e_type cost;

    if(!found_node)
        return false;

    vector<NodeGraph<n_type,e_type>*> edges = start->getEdges();
    vector<e_type> costs = start->getCosts();
    for(int i = 0; i < edges.size(); i++){
        NodeGraph<n_type,e_type>* e = edges[i];
        if(e->equals(end)){
            found_edge = true;
            cost = costs[i];
        }
    }

    return found_edge;
}


template <class n_type, class e_type>
vector<NodeGraph<n_type,e_type>*> Graph<n_type,e_type>::getSolutionNodes(Solution* s){
    return getSolutionNodes(s->getVector());
}

template <class n_type, class e_type>
vector<NodeGraph<n_type,e_type>*> Graph<n_type,e_type>::getSolutionNodes(vector<int> s){
    vector<NodeGraph<n_type,e_type>*> r;

    for(int i = 0; i < s.size(); i++){
        r.push_back(this->operator[](s[i]));
    }

    return r;
}


template class Graph<int,int>;
template class Graph<int,float>;
template class Graph<int,double>;
template class Graph<int,QString>;
template class Graph<float,int>;
template class Graph<float,float>;
template class Graph<float,double>;
template class Graph<float,QString>;
template class Graph<double,int>;
template class Graph<double,float>;
template class Graph<double,double>;
template class Graph<double,QString>;
template class Graph<QString,QString>;
template class Graph<QString,int>;
template class Graph<QString,float>;
template class Graph<QString,double>;

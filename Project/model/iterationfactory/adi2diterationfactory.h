#ifndef ADI2DITERATIONFACTORY_H
#define ADI2DITERATIONFACTORY_H

#include "model/wise/wiseiterationfactory.h"
#include "model/wiseobject/wisemesh.h"

#include <iostream>
#include <sstream>
#include <string>

#define DEFAULT_TYPE "MESH"

#define DEFAULT_NAME "ADI_2D"

using namespace std;

class ADI2DIterationFactory : public WiseIterationFactory
{
public:
    ADI2DIterationFactory();

    unique_ptr<WiseIterationFactory> clone();

    void set_fields(WiseCollection* object);

    bool iterate(WiseCollection* object);
};

#endif // ADI2DITERATIONFACTORY_H

#ifndef STATICITERATIONFACTORY_H
#define STATICITERATIONFACTORY_H

#include "model/wise/wiseiterationfactory.h"

#include "model/wise/wiseelement.h"

#include <iostream>
#include <sstream>
#include <string>

#define DEFAULT_TYPE "ANY"

#define DEFAULT_NAME "STATIC"

using namespace std;

class StaticIterationFactory : WiseIterationFactory
{
public:
    StaticIterationFactory();

    unique_ptr<WiseIterationFactory> clone();

    void set_fields(WiseCollection* object);

    bool iterate(WiseCollection* object);
};

#endif // STATICITERATIONFACTORY_H

#include "duanandzamiriterationfactory.h"

DuanAndZamirIterationFactory::DuanAndZamirIterationFactory() : WiseIterationFactory(DEFAULT_TYPE,DEFAULT_NAME)
{

}

unique_ptr<WiseIterationFactory> DuanAndZamirIterationFactory::clone(){
    return unique_ptr<WiseIterationFactory>{ new DuanAndZamirIterationFactory()};
}

void DuanAndZamirIterationFactory::set_fields(WiseCollection* object){
    WiseElement* obj = object->pop();
    stringstream s1,s2;

    s1 << (object->id() +1);
    s2 << (object->id() +2);


    obj->add_field("FREQUENCY","3.65",ITERATION,DOUBLE,LIVE);
    obj->add_field("VISCOUS","0",ITERATION,INT,LIVE);
    obj->add_field("VISCOUS_MULTIPLIER","1.0",ITERATION,DOUBLE,LIVE);
    obj->add_field("PHI_0","0",ITERATION,DOUBLE,LIVE);
    obj->add_field("PRESSURE_GRAPHIC",s1.str(),ITERATION,INT,INITIAL);
    obj->add_field("FLOW_GRAPHIC",s1.str(),ITERATION,INT,INITIAL);
}

bool DuanAndZamirIterationFactory::iterate(WiseCollection* object){
    if(!object)
        return false;

    WiseArteryTree* tree = static_cast<WiseArteryTree*>(object->pop());

    if(!tree)
        return false;

    if(!tree->field_set("FREQUENCY") || !tree->field_set("VISCOUS") || !tree->field_set("VISCOUS_MULTIPLIER") ||
            !tree->field_set("PRESSURE_GRAPHIC") || !tree->field_set("FLOW_GRAPHIC"))
        return false;

    stringstream aux;
    int N = tree->get_n() - 1;


    /**
     *PRESSURE PEAK
     *
     * ARGS
     *      0 - DOUBLE : FREQUENCY
     *      1 - BOOL   : VISCOUS
     *      2 - DOUBLE : VISCOUS_FACTOR
     *      3 - DOUBLE : PHI0
     **/
    double frequency = 3.65;
    bool viscous = false;
    double viscous_factor = 1.0;
    double phi0 = 0.0;

    aux.clear();
    aux << tree->get_field("FREQUENCY");
    aux >> frequency;

    aux.clear();
    aux << tree->get_field("VISCOUS");
    aux >> viscous;

    aux.clear();
    aux << tree->get_field("VISCOUS_FACTOR");
    aux >> viscous_factor;

    aux.clear();
    aux << tree->get_field("PHI_0");
    aux >> phi0;

    if(!phase_one(tree->get_root(),frequency,viscous,viscous_factor,phi0))
        return false;

    if(!phase_two(tree->get_root(),1.0,tree->get_root()->params.Ye))
        return false;

    tree->t(tree->t() + tree->dt());

    object->pop()->update_structure();
    object->push_frame();

    return true;
}



bool DuanAndZamirIterationFactory::phase_one(WiseArtery* artery, double frequency, bool viscous, double viscousMultiplier, double phi0){
    bool isLeaf = (!artery->has_right() && !artery->has_left());

    if(!isLeaf){
        if(artery->has_left())
            phase_one(artery->left,frequency,viscous,viscousMultiplier,phi0);

        if(artery->has_right())
            phase_one(artery->right,frequency,viscous,viscousMultiplier,phi0);
    }

    //WALL_THICKNESS
    if(artery->params.h == -1){
        artery->params.h = artery->params.r * 0.1;
    }

    //WAVESPEED
    artery->params.c = sqrt((artery->params.E * artery->params.h)/(artery->params.density * artery->params.r * 2));

    //ANG
    artery->params.w = 2 * PI * frequency;

    //BETA
    artery->params.beta = complex<double> ((artery->params.w * artery->params.length )/( artery->params.c ), 0.0);

    //ADMITTANCE
    if(!viscous){
        artery->params._Y = (PI * artery->params.r * artery->params.r  / (artery->params.density * artery->params.c ));
    } else {
        //Sets parameters for viscous flow
        if(viscousMultiplier > 0.0)
            artery->params.alpha = artery->params.r * sqrt((artery->params.w * artery->params.density)/ (artery->params.viscosity * viscousMultiplier));
        else
            artery->params.alpha = 0;

        complex<double> one (1.0,0.0);

        if(artery->params.alpha != 0){
            complex<double> i (0.0,1.0);

            artery->params.viscousFactor = one - ((2.0)/(artery->params.alpha * sqrt(i)));
        } else {
            artery->params.viscousFactor = one;
        }

        artery->params.phi = phi0 * (1 - exp(-artery->params.w));

        complex<double> aux (0.0,igumath::degree_to_radian(artery->params.phi));

        artery->params.Ec = abs(artery->params.E) * exp(aux);

        artery->params.cv = sqrt((artery->params.Ec * artery->params.h)/(artery->params.density * 2 * artery->params.r)) * sqrt(artery->params.viscousFactor);

        artery->params.beta = artery->params.w * artery->params.length / artery->params.cv;

        artery->params._Y = (PI * artery->params.r * artery->params.r  / (artery->params.density * artery->params.cv )) *  sqrt(artery->params.viscousFactor);
    }


    //REFLECTION COEFFICIENT
    if(isLeaf)
    {
        artery->params.R = 0.0;
    }
    else
    {
        complex<double> Ye1 = artery->has_left()? artery->left->params.Ye : 0.0;
        complex<double> Ye2 = artery->has_right()? artery->right->params.Ye : 0.0;

        if(Ye1 == complex<double> (-1.0 , 0.0) || Ye2 == complex<double> (-1.0 , 0.0))
            return false;


        artery->params.R = (artery->params._Y - (Ye2 + Ye1))/(artery->params._Y + (Ye2 + Ye1));

    }

    //EFFECTIVE ADMITTANCE
    if(isLeaf)
    {
        artery->params.Ye = artery->params._Y;
    }
    else
    {
        if (artery->params.R == complex<double> (-1.0, 0.0))
            return false;

        if (artery->params.beta == complex<double> (-1.0 , 0.0))
            return false;

        complex<double> aux (2 * imag(artery->params.beta) , -2 * real(artery->params.beta));
        complex<double> one (1.0 , 0.0);

        artery->params.Ye = artery->params._Y * ((one - (artery->params.R *  exp(aux)))/(one + (artery->params.R *  exp(aux))));
    }

    return true;
}

bool DuanAndZamirIterationFactory::phase_two(WiseArtery* artery, double p0, complex<double> root_admittance){
    complex<double> q0 = root_admittance * p0;

    //MEDIUM PRESSURE
    if(artery->has_father()){
        if(artery->params.R == complex<double>(-1.0,0.0))
            return false;

        if(artery->params.beta == complex<double> (-1.0 , 0.0))
            return false;

        if(artery->father->params._pressure == complex<double>(-1.0,0.0))
            return false;

        complex<double> _pf = artery->father->params._pressure;
        complex<double> Rf = artery->father->params.R;
        complex<double> betaf = artery->father->params.beta;

        complex<double> aux1 (imag(betaf), real(-betaf));
        complex<double> aux2 (2 * imag(artery->params.beta), -2 * real(artery->params.beta));
        complex<double> one (1.0,0.0);

        artery->params._pressure = (_pf * (one + Rf) * exp(aux1))/(one + artery->params.R *exp(aux2));

    } else {
        artery->params._pressure = p0;
    }

    //PRESSURE
    if(artery->params._pressure == complex<double> (-1.0,0.0))
        return false;

    if(artery->params.beta == complex<double> (-1.0 , 0.0))
        return false;

    artery->params.pressure_wave.clear();

    //SETS PRESSURE WAVE
    for(int i = 0; i < WAVE_PLOT_SIZE; i++){
       complex<double> press = artery->get_pressure((i)/(WAVE_PLOT_SIZE - 1));
       artery->params.pressure_wave.push_back(abs(press));
    }

    //FLOW
    if(artery->params._pressure == complex<double> (-1.0,0.0))
        return false;

    if(artery->params.beta == complex<double> (-1.0 , 0.0))
        return false;

    if(artery->params.Ye == complex<double> (-1.0 , 0.0))
        return false;

    artery->params.pressure_wave.clear();

    //SETS FLOW WAVE
    for(int i = 0; i < WAVE_PLOT_SIZE; i++){
       complex<double> press = artery->get_flow(((i)/(WAVE_PLOT_SIZE - 1)),root_admittance);
       artery->params.pressure_wave.push_back(abs(press * q0));
    }

    //SENDS ITERATION BELOW

    bool isLeaf = (!artery->has_right() && !artery->has_left());

    //UPDATES ABSOLUTE VALUES

    if(!isLeaf){
        bool l = true;
        bool r = true;
        if(artery->has_left())
            l = phase_two(artery->left,p0,root_admittance);

        if(artery->has_right())
            r = phase_two(artery->right,p0,root_admittance);

        if(!r or !l)
            return false;
    }

    return true;
}

#include "adi2diterationfactory.h"

ADI2DIterationFactory::ADI2DIterationFactory() : WiseIterationFactory(DEFAULT_TYPE,DEFAULT_NAME)
{

}

unique_ptr<WiseIterationFactory> ADI2DIterationFactory::clone(){
    return unique_ptr<WiseIterationFactory>{ new ADI2DIterationFactory()};
}

void ADI2DIterationFactory::set_fields(WiseCollection* obj){
    WiseElement* object = obj->pop();
    stringstream s1,s2,s3;

    s1 << (obj->id() +1);
    s2 << (obj->id() +2);
    s3 << (obj->id() +3);

    object->add_field("CASE","COSINOIDAL",ITERATION,STRING,LIVE);
    object->add_field("EPSILON","1",ITERATION,DOUBLE,LIVE);
    object->add_field("EXACT",s1.str(),ITERATION,INT,INITIAL);
    object->add_field("VELOCITY_X",s2.str(),ITERATION,INT,INITIAL);
    object->add_field("VELOCITY_Y",s3.str(),ITERATION,INT,INITIAL);
}

bool ADI2DIterationFactory::iterate(WiseCollection* object){
    if(!object)
        return false;

    WiseMesh* mesh = static_cast<WiseMesh*>(object->pop());

    if(!mesh)
        return false;

    if(!mesh->field_set("CASE") || !mesh->field_set("EXACT") || !mesh->field_set("VELOCITY_X") || !mesh->field_set("VELOCITY_Y") || !mesh->field_set("EPSILON"))
        return false;

    stringstream vx,vy,ve;
    int id_x, id_y, id_e;
    double epsilon;
    double dt = mesh->dt();
    int N = mesh->get_n() - 1;

    vx << mesh->get_field("VELOCITY_X");
    vy << mesh->get_field("VELOCITY_Y");
    ve << mesh->get_field("EXACT");

    vx >> id_x;
    vy >> id_y;
    ve >> id_e;

    if(mesh->has_field("EPSILON")){
        vx.clear();
        vx << mesh->get_field("EPSILON");
        vx >> epsilon;
    } else {
        return false;
    }



    //Condições Iniciais
    WiseMesh* m_e = static_cast<WiseMesh*>(WiseCollection::obj(id_e)->pop());
    WiseMesh* m_x = static_cast<WiseMesh*>(WiseCollection::obj(id_x)->pop());
    WiseMesh* m_y = static_cast<WiseMesh*>(WiseCollection::obj(id_y)->pop());

    if(!m_e or !m_x or !m_y)
        return false;

    vector<vector<double>>* Ue = m_e->get_vector();
    vector<vector<double>>* Ux = m_x->get_vector();
    vector<vector<double>>* Uy = m_y->get_vector();


    /***************************
     * Primeiro Passo - Implícito em X
    ***************************/
    vector<vector<double>>* U = mesh->get_vector();
    vector<vector<double>> UL(N-1,vector<double>(N-1,0));
    double dtt = (dt/2);
    double dx = mesh->dX();
    double omega = dt*epsilon/pow(dx,2);

    //Condições de Contorno - Iniciais


    for(int i = 0; i < Ue->size(); i++){
        for(int j = 0; j < Ue->operator[](i).size(); j++){
            double h1 = mesh->dX();
            double h2 = mesh->dY();
            double x = mesh->limits_x().first + (i*h1);
            double y = mesh->limits_y().first + (j*h2);
            double z = 2 * PI * igumath::cosine(PI * x) * igumath::cosine(PI * y) * exp(-2 * pow(PI,2) * (mesh->t() + dtt) * epsilon);
            double z_x = -igumath::cosine(PI * x) * igumath::sine(PI * y) * exp(-2 * pow(PI,2) * (mesh->t() + dtt) * epsilon);

            Ux->operator[](i).operator[](j) = z_x;
            Ue->operator[](i).operator[](j) = z;
        }
    }

    for(int i = 0 ; i < N+1; i++){
       U[i][0] = Ue[i][0];
       U[i][N] = Ue[i][N];
    }

    //Passo x
    for(int j = 1; j < N; j++){
        //B
        vector<double> b(N-1,0);
        //A
        vector<double> n1(N-2,0);
        vector<double> n2(N-1,0);
        vector<double> n3(N-2,0);

        for(int i = 1; i < N; i++){
            double rho1 = Ux->operator[](i).operator[](j) * (dt/dx);
            double rho2 = Uy->operator[](i).operator[](j) * (dt/dx);

            //B
            b[i-1] = U->operator[](i)[j] * (1-omega) + (omega/2 + rho1/4) * (U->operator[](i)[j-1]) + ( omega/2 - rho1/4 ) * (U->operator[](i)[j+1]);

            //A
            n2[i-1] = 1 + omega;

            if ( i > 1){
                n3[i-2] = - omega / 2 - rho1 /4;
            }else{
                b[i-1] += (omega / 2 + rho1 / 4) * (U->operator[](0)[j]);
            }

            if ( i < N-1){
                n1[i-1] = - omega / 2 + rho1 /4;
            }else{
                b[i-1] += (omega / 2 - rho1 / 4) * (U->operator[](N)[j]);
            }
        }

        vector<double> ul;

        igumath::thomas_algorithm(n3,n2,n1,b,ul);

        for(int l = 0; l < N - 1; l++){
            UL[l][j-1] = ul[l];
        }
    }

    //UL to U
    for(int i = 0; i < N-1; i++)
        for(int j = 0; j < N-1; j++)
            U->operator[](i+1)[j+1] = UL[i][j];


    //Condições de Contorno - Finais

    for(int i = 0 ; i < N+1; i++){
        U[0][i] = Ue[0][i];
        U[N][i] = Ue[N][i];
    }


    /***************************
     * Primeiro Passo - Implícito em Y
    ***************************/
    //Condições de Contorno - Iniciais

    for(int i = 0; i < Ue->size(); i++){
        for(int j = 0; j < Ue->operator[](i).size(); j++){
            double h1 = mesh->dX();
            double h2 = mesh->dY();
            double x = mesh->limits_x().first + (i*h1);
            double y = mesh->limits_y().first + (j*h2);
            double z = 2 * PI * igumath::cosine(PI * x) * igumath::cosine(PI * y) * exp(-2 * pow(PI,2) * (mesh->t() + dt) * epsilon);
            double z_y = igumath::sine(PI * x) * igumath::cosine(PI * y) * exp(-2 * pow(PI,2) * (mesh->t() + dt) * epsilon);

            Ue->operator[](i).operator[](j) = z;
            Uy->operator[](i).operator[](j) = z_y;
        }
    }

    for(int i = 0 ; i < N+1; i++){
       U[0][i] = Ue[0][i];
       U[N][i] = Ue[N][i];
    }

    //Passo y
    for(int i = 1; i < N; i++){
        //B
        vector<double> b(N-1,0);
        //A
        vector<double> n1(N-2,0);
        vector<double> n2(N-1,0);
        vector<double> n3(N-2,0);

        for(int j = 1; j < N; j++){
            double rho1 = Ux->operator[](i)[j] * (dt/dx);
            double rho2 = Uy->operator[](i)[j] * (dt/dx);

            //B
            b[j-1] = U->operator[](i)[j] * (1-omega) + (omega/2 + rho1/4) * (U->operator[](i-1)[j]) + ( omega/2 - rho1/4 ) * (U->operator[](i+1)[j]);

            //A
            n2[j-1] = 1 + omega;

            if ( j > 1){
                n3[j-2] = - omega / 2 - rho1 /4;
            }else{
                b[j-1] += (omega / 2 + rho1 / 4) * (U->operator[](i)[0]);
            }

            if ( j < N-1){
                n1[j-1] = - omega / 2 + rho1 /4;
            }else{
                b[j-1] += (omega / 2 - rho1 / 4) * (U->operator[](i)[N]);
            }
        }

        vector<double> ul;

        igumath::thomas_algorithm(n3,n2,n1,b,ul);

        for(int l = 0; l < N - 1; l++){
            UL[i-1][l] = ul[l];
        }
    }

    //UL to U
    for(int i = 0; i < N-1; i++)
        for(int j = 0; j < N-1; j++)
            U->operator[](i+1)[j+1] = UL[i][j];


    //Condições de Contorno - Finais

    for(int i = 0 ; i < N+1; i++){
        U[i][0] = Ue[i][0];
        U[i][N] = Ue[i][N];
    }

    mesh->t(mesh->t() + mesh->dt());

    object->pop()->update_structure();
    object->push_frame();

    WiseCollection::obj(id_e)->pop()->update_structure();
    WiseCollection::obj(id_x)->pop()->update_structure();
    WiseCollection::obj(id_y)->pop()->update_structure();

    WiseCollection::obj(id_e)->push_frame();
    WiseCollection::obj(id_x)->push_frame();
    WiseCollection::obj(id_y)->push_frame();

    return true;
}

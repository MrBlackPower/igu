#ifndef GRAPHICWALKITERATIONFACTORY_H
#define GRAPHICWALKITERATIONFACTORY_H

#include "model/wise/wiseiterationfactory.h"

#include "model/wiseobject/wisegraphic.h"

#include <iostream>
#include <sstream>
#include <string>

#define DEFAULT_TYPE "GRAPHIC"

#define DEFAULT_NAME "GRAPHIC_WALK"

using namespace std;

class GraphicWalkIterationFactory : public WiseIterationFactory
{
public:
    GraphicWalkIterationFactory();

    unique_ptr<WiseIterationFactory> clone();

    void set_fields(WiseCollection* object);

    bool iterate(WiseCollection* object);
};

#endif // GRAPHICWALKITERATIONFACTORY_H

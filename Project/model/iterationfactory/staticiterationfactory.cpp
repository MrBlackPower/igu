#include "staticiterationfactory.h"

StaticIterationFactory::StaticIterationFactory() : WiseIterationFactory(DEFAULT_TYPE,DEFAULT_NAME)
{

}

unique_ptr<WiseIterationFactory> StaticIterationFactory::clone(){
    return unique_ptr<WiseIterationFactory>{new StaticIterationFactory};
}

void StaticIterationFactory::set_fields(WiseCollection* object){

}

bool StaticIterationFactory::iterate(WiseCollection* object){
    //ADICIONA PASSAGEM DE TEMPO
    object->pop()->t(object->pop()->t() + object->pop()->dt());

    return true;

}

#ifndef DUANANDZAMIRITERATIONFACTORY_H
#define DUANANDZAMIRITERATIONFACTORY_H

#include "model/wise/wiseiterationfactory.h"
#include "model/wiseobject/wisearterytree.h"

#include <iostream>
#include <sstream>
#include <string>

#define DEFAULT_TYPE "ARTERY_TREE"

#define DEFAULT_NAME "DUAN_AND_ZAMIR"

using namespace std;

class DuanAndZamirIterationFactory : public WiseIterationFactory
{
public:
    DuanAndZamirIterationFactory();

    unique_ptr<WiseIterationFactory> clone();

    void set_fields(WiseCollection* object);

    bool iterate(WiseCollection* object);

private:

    bool phase_one(WiseArtery* artery, double frequency, bool viscous, double viscousMultiplier, double phi0);

    bool phase_two(WiseArtery* artery, double p0, complex<double> root_admittance);
};

#endif // DUANANDZAMIRITERATIONFACTORY_H

#include "adi3diterationfactory.h"

ADI3DIterationFactory::ADI3DIterationFactory() : WiseIterationFactory(DEFAULT_TYPE,DEFAULT_NAME)
{

}

ADI3DIterationFactory::ADI3DCases ADI3DIterationFactory::case_string(string cas){
    if("SINOIDAL" == cas){
        return SINOIDAL;
    }
    if("HYPERBOLICAL" == cas){
        return HYPERBOLICAL;
    }
    if("ALPHA" == cas){
        return ALPHA;
    }
    if("ALPHA_CONVECTION" == cas){
        return ALPHA_CONVECTION;
    }

    return SINOIDAL;
}

string ADI3DIterationFactory::case_string(ADI3DCases cas){
    switch (cas) {
    case SINOIDAL:
        return "SINOIDAL";
    break;
    case HYPERBOLICAL:
        return "HYPERBOLICAL";
    break;
    case ALPHA:
        return "ALPHA";
    break;
    case ALPHA_CONVECTION:
        return "ALPHA_CONVECTION";
    break;
    }
}

void ADI3DIterationFactory::set_fields(WiseCollection* object){
    WiseElement* aux = object->pop();
    stringstream s1,s2,s3,s4;

    s1 << (object->id() +1);
    s2 << (object->id() +2);
    s3 << (object->id() +3);
    s3 << (object->id() +4);

    aux->add_field("CASE","SINOIDAL",ITERATION,STRING,INITIAL);
    aux->add_field("EXACT",s1.str(),ITERATION,INT,INITIAL);
    aux->add_field("VELOCITY_X",s2.str(),ITERATION,INT,INITIAL);
    aux->add_field("VELOCITY_Y",s3.str(),ITERATION,INT,INITIAL);
    aux->add_field("VELOCITY_Z",s3.str(),ITERATION,INT,INITIAL);
}

point ADI3DIterationFactory::velocity(ADI3DCases cas, point xyz, double t){
    switch (cas) {
    case SINOIDAL:
        return point{};
    break;
    case HYPERBOLICAL:
        return point{};
    break;
    case ALPHA:
        return point{};
    break;
    case ALPHA_CONVECTION:
        return point{0,0.8,0.8,0.8};
    break;
    }
    return point{};
}

double ADI3DIterationFactory::velocity_x(ADI3DCases cas, point xyz, double t){
    return velocity(cas,xyz,t).X();
}

double ADI3DIterationFactory::velocity_y(ADI3DCases cas, point xyz, double t){
    return velocity(cas,xyz,t).Y();
}

double ADI3DIterationFactory::velocity_z(ADI3DCases cas, point xyz, double t){
    return velocity(cas,xyz,t).Z();
}

unique_ptr<WiseIterationFactory> ADI3DIterationFactory::clone(){
    return unique_ptr<WiseIterationFactory>{ new ADI3DIterationFactory()};
}

bool ADI3DIterationFactory::iterate(WiseCollection* object){
    if(!object)
        return false;

    WisePoly* mesh = static_cast<WisePoly*>(object->pop());

    if(!mesh)
        return false;

    if(!mesh->field_set("CASE") || !mesh->field_set("EXACT") || !mesh->field_set("VELOCITY_X") || !mesh->field_set("VELOCITY_Y") || !mesh->field_set("VELOCITY_Z"))
        return false;

    stringstream vx,vy,vz,ve;
    int id_x, id_y, id_z, id_e;
    double t = mesh->t();
    double dt = mesh->dt();
    double dtt = dt / 3;
    int N = mesh->get_n() - 1;
    ADI3DCases this_case;

    vx << mesh->get_field("VELOCITY_X");
    vy << mesh->get_field("VELOCITY_Y");
    vz << mesh->get_field("VELOCITY_Z");
    ve << mesh->get_field("EXACT");

    vx >> id_x;
    vy >> id_y;
    vz >> id_z;
    ve >> id_e;



    //Condições Iniciais
    WisePoly* m_e = static_cast<WisePoly*>(WiseCollection::obj(id_e)->pop());
    WisePoly* m_x = static_cast<WisePoly*>(WiseCollection::obj(id_x)->pop());
    WisePoly* m_y = static_cast<WisePoly*>(WiseCollection::obj(id_y)->pop());
    WisePoly* m_z = static_cast<WisePoly*>(WiseCollection::obj(id_y)->pop());

    if(!m_e or !m_x or !m_y or !m_z)
        return false;

    vx.clear();
    vx << mesh->get_field("CASE");
    this_case = case_string(vx.str());

    vector<vector<vector<double>>>* U = mesh->get_vector();
    vector<vector<vector<point>>>* U_p = mesh->get_grid();
    vector<vector<vector<double>>>* Ue = m_e->get_vector();
    vector<vector<vector<double>>>* Ux = m_x->get_vector();
    vector<vector<vector<double>>>* Uy = m_y->get_vector();
    vector<vector<vector<double>>>* Uz = m_z->get_vector();
    
    int LAST_M = mesh->get_m() - 1;
    int LAST_N = mesh->get_n() - 1;
    int LAST_L = mesh->get_l() - 1;

    double dx,dy,dz;
    dx = mesh->get_dx();
    dy = mesh->get_dy();
    dz = mesh->get_dz();

    double omega = dt / ( pow(dx,2) * 3);
    
    /***************************
     * Primeiro Passo - Implícito em X
    ***************************/
    for(int i = 0; i < mesh->get_n(); i++){
        for (int j = 0; j < mesh->get_m(); j++) {
            for(int k = 0; k < mesh->get_l(); k++){
                Ue->operator[](i)[j][k] = exact(this_case,U_p->operator[](i)[j][k],t+dtt);
            }
        }
    }

    for(int j = 0; j < mesh->get_m(); j++){
        for(int k = 0; k < mesh->get_l(); k++){
            U->operator[](0)[j][k] = Ue->operator[](0)[j][k];
            U->operator[](LAST_N)[j][k] = Ue->operator[](LAST_N)[j][k];
        }
    }

    vector<vector<vector<double>>> UL(N-1,vector<vector<double>>(N-1,vector<double>(N-1,0)));

    for(int j = 1; j < N - 1; j++){
        for(int k = 1; k < N - 1; k++){
            //B
            vector<double> b(N-1,0);

            //A
            vector<double> n1(N-2,0);
            vector<double> n2(N-1,0);
            vector<double> n3(N-2,0);

            //ul
            vector<double> ul(N-1,0);

            for(int i = 1; i < N - 1; i++){
                double rho1 = Ux->operator[](i)[j][k] * (dt/(6 * dx));
                double rho2 = Uy->operator[](i)[j][k] * (dt/(6 * dx));
                double rho3 = Uz->operator[](i)[j][k] * (dt/(6 * dx));

                //B
                if(rho2 > 0){
                    if(rho3 > 0){
                        b[i-1] = U->operator[](i)[j][k] * (1 - (4*omega) - rho2 - rho3) + (omega + rho2) * (U->operator[](i)[j-1][k]) + ( omega ) * (U->operator[](i)[j+1][k])
                                + (omega + rho3) * (U->operator[](i)[j][k-1]) + (omega) * (U->operator[](i)[j][k+1]);
                    } else {
                        b[i-1] = U->operator[](i)[j][k] * (1 - (4*omega) - rho2 + rho3) + (omega + rho2) * (U->operator[](i)[j-1][k]) + ( omega ) * (U->operator[](i)[j+1][k])
                                + (omega) * (U->operator[](i)[j][k-1]) + (omega - rho3) * (U->operator[](i)[j][k+1]);
                    }
                } else {
                    if(rho3 > 0){
                        b[i-1] = U->operator[](i)[j][k] * (1 - (4*omega) + rho2 - rho3) + (omega) * (U->operator[](i)[j-1][k]) + ( omega - rho2 ) * (U->operator[](i)[j+1][k])
                                + (omega + rho3) * (U->operator[](i)[j][k-1]) + (omega) * (U->operator[](i)[j][k+1]);
                    } else {
                        b[i-1] = U->operator[](i)[j][k] * (1 - (4*omega) + rho2 + rho3) + (omega) * (U->operator[](i)[j-1][k]) + ( omega - rho2 ) * (U->operator[](i)[j+1][k])
                                + (omega) * (U->operator[](i)[j][k-1]) + (omega - rho3) * (U->operator[](i)[j][k+1]);
                    }
                }

                //A
                if(rho1 > 0){
                    n2[i-1] = 1 + (2 * omega) + rho1;

                    if ( i > 1){
                        n3[i-2] = - omega - rho1;
                    }else{
                        b[i-1] += (omega + rho1) * (U->operator[](0)[j][k]);
                    }

                    if ( i < N-1){
                        n1[i-1] = - omega;
                    }else{
                        b[i-1] += (omega) * (U->operator[](N)[j][k]);
                    }
                }else {
                    n2[i-1] = 1 + (2 * omega) - rho1;

                    if ( i > 1){
                        n3[i-2] = - omega;
                    }else{
                        b[i-1] += (omega) * (U->operator[](0)[j][k]);
                    }

                    if ( i < N-1){
                        n1[i-1] = - omega + rho1;
                    }else{
                        b[i-1] += (omega - rho1) * (U->operator[](N)[j][k]);
                    }
                }
            }

            igumath::thomas_algorithm(n3,n2,n1,b,ul);

            for(int l = 0; l < N - 1; l++){
                UL[l][j-1][k-1] = ul[l];
            }
        }
    }

    //UL to U

    for (int i = 0; i < N-1; i++){
        for (int j = 0; j < N-1; j++){
            for (int k = 0; i < N-1; k++){
                U->operator[](i+1)[j+1][k+1] = UL[i][j][k];
            }
        }
    }
    
    
    for(int j = 0; j < mesh->get_m(); j++){
        for(int k = 0; k < mesh->get_l(); k++){
            U->operator[](j)[0][k] = Ue->operator[](j)[0][k];
            U->operator[](j)[LAST_M][k] = Ue->operator[](j)[LAST_M][k];
            U->operator[](j)[k][0] = Ue->operator[](j)[k][0];
            U->operator[](j)[k][LAST_L] = Ue->operator[](j)[k][LAST_L];
        }
    }

    /***************************
     * Primeiro Passo - Implícito em Y
    ***************************/
    for(int i = 0; i < mesh->get_m(); i++){
        for (int j = 0; j < mesh->get_n(); j++) {
            for(int k = 0; k < mesh->get_l(); k++){
                Ue->operator[](i)[j][k] = exact(this_case,U_p->operator[](i)[j][k],t+(2*dtt));
            }
        }
    }

    for(int i = 0; i < mesh->get_m(); i++){
        for(int k = 0; k < mesh->get_l(); k++){
            U->operator[](i)[0][k] = Ue->operator[](i)[0][k];
            U->operator[](i)[LAST_M][k] = Ue->operator[](i)[LAST_M][k];
        }
    }

    for(int i = 1; i < N - 1; i++){
        for(int k = 1; k < N - 1; k++){
            //B
            vector<double> b(N-1,0);

            //A
            vector<double> n1(N-2,0);
            vector<double> n2(N-1,0);
            vector<double> n3(N-2,0);

            //ul
            vector<double> ul(N-1,0);

            for(int j = 1; j < N - 1; j++){
                double rho1 = Ux->operator[](i)[j][k] * (dt/(6 * dx));
                double rho2 = Uy->operator[](i)[j][k] * (dt/(6 * dx));
                double rho3 = Uz->operator[](i)[j][k] * (dt/(6 * dx));

                //B
                if(rho1 > 0){
                    if(rho3 > 0){
                        b[j-1] = U->operator[](i)[j][k] * (1 - (4*omega) - rho1 - rho3) + (omega + rho1) * (U->operator[](i-1)[j][k]) + ( omega ) * (U->operator[](i+1)[j][k])
                                + (omega + rho3) * (U->operator[](i)[j][k-1]) + (omega) * (U->operator[](i)[j][k+1]);
                    } else {
                        b[j-1] = U->operator[](i)[j][k] * (1 - (4*omega) - rho1 + rho3) + (omega + rho1) * (U->operator[](i-1)[j][k]) + ( omega ) * (U->operator[](i+1)[j][k])
                                + (omega) * (U->operator[](i)[j][k-1]) + (omega - rho3) * (U->operator[](i)[j][k+1]);
                    }
                } else {
                    if(rho3 > 0){
                        b[j-1] = U->operator[](i)[j][k] * (1 - (4*omega) + rho1 - rho3) + (omega) * (U->operator[](i-1)[j][k]) + ( omega - rho1 ) * (U->operator[](i+1)[j][k])
                                + (omega + rho3) * (U->operator[](i)[j][k-1]) + (omega) * (U->operator[](i)[j][k+1]);
                    } else {
                        b[j-1] = U->operator[](i)[j][k] * (1 - (4*omega) + rho1 + rho3) + (omega) * (U->operator[](i-1)[j][k]) + ( omega - rho1 ) * (U->operator[](i+1)[j][k])
                                + (omega) * (U->operator[](i)[j][k-1]) + (omega - rho3) * (U->operator[](i)[j][k+1]);
                    }
                }

                //A
                if(rho2 > 0){
                    n2[j-1] = 1 + (2 * omega) + rho2;

                    if ( i > 1){
                        n3[j-2] = - omega - rho2;
                    }else{
                        b[j-1] += (omega + rho2) * (U->operator[](i)[0][k]);
                    }

                    if ( j < N-1){
                        n1[j-1] = - omega;
                    }else{
                        b[j-1] += (omega) * (U->operator[](i)[N][k]);
                    }
                }else {
                    n2[j-1] = 1 + (2 * omega) - rho2;

                    if ( j > 1){
                        n3[j-2] = - omega;
                    }else{
                        b[j-1] += (omega) * (U->operator[](i)[0][k]);
                    }

                    if ( j < N-1){
                        n1[j-1] = - omega + rho2;
                    }else{
                        b[j-1] += (omega - rho2) * (U->operator[](i)[N][k]);
                    }
                }
            }

            igumath::thomas_algorithm(n3,n2,n1,b,ul);

            for(int l = 0; l < mesh->get_n() - 1; l++){
                UL[i-1][l][k-1] = ul[l];
            }
        }
    }


    //UL to U

    for (int i = 0; i < N-1; i++){
        for (int j = 0; j < N-1; j++){
            for (int k = 0; i < N-1; k++){
                U->operator[](i+1)[j+1][k+1] = UL[i][j][k];
            }
        }
    }

    for(int i = 0; i < mesh->get_m(); i++){
        for(int k = 0; k < mesh->get_l(); k++){
            U->operator[](0)[i][k] = Ue->operator[](0)[i][k];
            U->operator[](LAST_N)[i][k] = Ue->operator[](LAST_N)[i][k];
            U->operator[](i)[k][0] = Ue->operator[](i)[k][0];
            U->operator[](i)[k][LAST_L] = Ue->operator[](i)[k][LAST_L];
        }
    }


    /***************************
     * Primeiro Passo - Implícito em Z
    ***************************/
    for(int i = 0; i < mesh->get_m(); i++){
        for (int j = 0; j < mesh->get_n(); j++) {
            for(int k = 0; k < mesh->get_l(); k++){
                Ue->operator[](i)[j][k] = exact(this_case,U_p->operator[](i)[j][k],t+dt);
            }
        }
    }

    for(int i = 0; i < mesh->get_m(); i++){
        for(int k = 0; k < mesh->get_l(); k++){
            U->operator[](i)[0][k] = Ue->operator[](i)[0][k];
            U->operator[](i)[LAST_M][k] = Ue->operator[](i)[LAST_M][k];
        }
    }

    for(int i = 1; i < N - 1; i++){
        for(int j = 1; j < N - 1; j++){
            //B
            vector<double> b(N-1,0);

            //A
            vector<double> n1(N-2,0);
            vector<double> n2(N-1,0);
            vector<double> n3(N-2,0);

            //ul
            vector<double> ul(N-1,0);

            for(int k = 1; k < N - 1; k++){
                double rho1 = Ux->operator[](i)[j][k] * (dt/(6 * dx));
                double rho2 = Uy->operator[](i)[j][k] * (dt/(6 * dx));
                double rho3 = Uz->operator[](i)[j][k] * (dt/(6 * dx));

                //B
                if(rho1 > 0){
                    if(rho2 > 0){
                        b[k-1] = U->operator[](i)[j][k] * (1 - (4*omega) - rho1 - rho2) + (omega + rho1) * (U->operator[](i-1)[j][k]) + ( omega ) * (U->operator[](i+1)[j][k])
                                + (omega + rho2) * (U->operator[](i)[j-1][k]) + (omega) * (U->operator[](i)[j+1][k]);
                    } else {
                        b[k-1] = U->operator[](i)[j][k] * (1 - (4*omega) - rho1 + rho2) + (omega + rho1) * (U->operator[](i-1)[j][k]) + ( omega ) * (U->operator[](i+1)[j][k])
                                + (omega) * (U->operator[](i)[j-1][k]) + (omega - rho2) * (U->operator[](i)[j+1][k]);
                    }
                } else {
                    if(rho2 > 0){
                        b[k-1] = U->operator[](i)[j][k] * (1 - (4*omega) + rho1 - rho2) + (omega) * (U->operator[](i-1)[j][k]) + ( omega - rho1 ) * (U->operator[](i+1)[j][k])
                                + (omega + rho2) * (U->operator[](i)[j-1][k]) + (omega) * (U->operator[](i)[j+1][k]);
                    } else {
                        b[k-1] = U->operator[](i)[j][k] * (1 - (4*omega) + rho1 + rho2) + (omega) * (U->operator[](i-1)[j][k]) + ( omega - rho1 ) * (U->operator[](i+1)[j][k])
                                + (omega) * (U->operator[](i)[j-1][k]) + (omega - rho2) * (U->operator[](i)[j+1][k]);
                    }
                }

                //A
                if(rho3 > 0){
                    n2[k-1] = 1 + (2 * omega) + rho3;

                    if ( i > 1){
                        n3[k-2] = - omega - rho3;
                    }else{
                        b[k-1] += (omega + rho3) * (U->operator[](i)[j][0]);
                    }

                    if ( j < N-1){
                        n1[j-1] = - omega;
                    }else{
                        b[j-1] += (omega) * (U->operator[](i)[j][N]);
                    }
                }else {
                    n2[j-1] = 1 + (2 * omega) - rho3;

                    if ( j > 1){
                        n3[j-2] = - omega;
                    }else{
                        b[j-1] += (omega) * (U->operator[](i)[j][0]);
                    }

                    if ( j < N-1){
                        n1[j-1] = - omega + rho3;
                    }else{
                        b[j-1] += (omega - rho3) * (U->operator[](i)[j][N]);
                    }
                }
            }

            igumath::thomas_algorithm(n3,n2,n1,b,ul);

            for(int l = 0; l < mesh->get_l() - 1; l++){
                UL[i-1][j-1][l] = ul[l];
            }
        }
    }


    //UL to U

    for (int i = 0; i < N-1; i++){
        for (int j = 0; j < N-1; j++){
            for (int k = 0; i < N-1; k++){
                U->operator[](i+1)[j+1][k+1] = UL[i][j][k];
            }
        }
    }

    for(int i = 0; i < mesh->get_n(); i++){
        for(int j = 0; j < mesh->get_m(); j++){
            U->operator[](0)[i][j] = Ue->operator[](0)[i][j];
            U->operator[](LAST_N)[i][j] = Ue->operator[](LAST_N)[i][j];
            U->operator[](i)[0][j] = Ue->operator[](i)[0][j];
            U->operator[](i)[LAST_M][LAST_L] = Ue->operator[](i)[LAST_M][j];
        }
    }

    mesh->t(mesh->t() + mesh->dt());

    object->pop()->update_structure();
    object->push_frame();

    m_e->update_structure();
    m_x->update_structure();
    m_y->update_structure();
    m_z->update_structure();

    WiseCollection::obj(id_e)->push_frame();
    WiseCollection::obj(id_x)->push_frame();
    WiseCollection::obj(id_y)->push_frame();
    WiseCollection::obj(id_z)->push_frame();


    return true;
}


double ADI3DIterationFactory::exact(ADI3DCases cas, point xyz, double t){
    switch (cas) {
    case SINOIDAL:
        return igumath::sine(xyz.X() * PI) * igumath::sine(xyz.Y() * PI) * igumath::sine(xyz.Z() * PI) * exp(-3 * pow(PI,2) * t);
    break;
    case HYPERBOLICAL:
        return (1 - pow(xyz.X(),2)) * (1 - pow(xyz.Y(),2)) * (1 - pow(xyz.Z(),2))  * exp(-3 * t);
    break;
    }
    return 0;
}

#ifndef ADI3DITERATIONFACTORY_H
#define ADI3DITERATIONFACTORY_H

#include "model/wise/wiseiterationfactory.h"
#include "model/wise/wisecollection.h"

#include "model/wiseobject/wisepoly.h"

#include <iostream>
#include <sstream>
#include <string>

#define DEFAULT_TYPE "POLY"

#define DEFAULT_NAME "ADI_3D"

class ADI3DIterationFactory : public WiseIterationFactory
{
public:
    ADI3DIterationFactory();

    enum ADI3DCases{
        SINOIDAL,
        HYPERBOLICAL,
        ALPHA,
        ALPHA_CONVECTION
    };

    ADI3DCases case_string(string cas);

    string case_string(ADI3DCases cas);

private:

    double exact(ADI3DCases cas, point xyz, double t);

    point velocity(ADI3DCases cas, point xyz, double t);

    double velocity_x(ADI3DCases cas, point xyz, double t);

    double velocity_y(ADI3DCases cas, point xyz, double t);

    double velocity_z(ADI3DCases cas, point xyz, double t);

    unique_ptr<WiseIterationFactory> clone();

    void set_fields(WiseCollection* object);

    bool iterate(WiseCollection* object);
};

#endif // ADI3DITERATIONFACTORY_H

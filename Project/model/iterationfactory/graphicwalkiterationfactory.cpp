#include "graphicwalkiterationfactory.h"

GraphicWalkIterationFactory::GraphicWalkIterationFactory(): WiseIterationFactory(DEFAULT_TYPE,DEFAULT_NAME)
{

}

unique_ptr<WiseIterationFactory> GraphicWalkIterationFactory::clone(){
    return unique_ptr<WiseIterationFactory>{ new GraphicWalkIterationFactory()};
}

void GraphicWalkIterationFactory::set_fields(WiseCollection* object){

}

bool GraphicWalkIterationFactory::iterate(WiseCollection* object){


    WiseGraphic* mesh = static_cast<WiseGraphic*>(object->pop());

    if(!mesh)
        return false;

    double t, dt;
    stringstream vx;
    vector<double>* points = mesh->get_vector();
    double aux = points->operator[](points->size() - 1);

    for(int i = 0; i < points->size() - 2; i ++){
        double it = points->operator[](i);
        points->operator[](i) = aux;
        aux = it;
    }

    mesh->t(mesh->t() + mesh->dt());

    mesh->update_structure();
    object->push_frame();

    return true;
}

#include "smartlogmessage.h"

SmartLogMessage::SmartLogMessage(int SID, QString msg, SmartLogType type)
{
    this->SID = SID;
    this->msg = msg;
    this->type = type;
    time = QDateTime::currentDateTime();
}

QString SmartLogMessage::getMessage(){
    return msg;
}

void SmartLogMessage::operator=(QString msg){
    setMessage(msg);
}

void SmartLogMessage::operator=(SmartLogMessage msg){
    SID = msg.getSID();
    setMessage(msg.getMessage());
}

int SmartLogMessage::getSID(){
    return SID;
}

void SmartLogMessage::setMessage(QString msg){
    this->msg = msg;
    time = QDateTime::currentDateTime();
}

void SmartLogMessage::setType(SmartLogType type){
    this->type = type;
}

QString SmartLogMessage::toString(){
    QString aux;
    aux = aux.asprintf("-- [%s] #SID = %d >> %s",time.toString("dd/MM/yyyy hh:mm:ss.z").toStdString().data(),SID,msg.toStdString().data());

    return aux;
}

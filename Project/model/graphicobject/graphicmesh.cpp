#include "graphicmesh.h"

GraphicMesh::GraphicMesh(WiseStructure* structure) : GraphicObject(structure,"NONE",CELL)
{
    square_size = 0.001;
    draw_line = true;
    draw_square = true;
    draw_quad = true;
}

bool GraphicMesh::hover_node(WiseCellType cell_type, int id){

}

bool GraphicMesh::select_node(WiseCellType cell_type, int id){

}

vector<pair<string,string>> GraphicMesh::edit_node(WiseCellType cell_type, int id){

}

bool GraphicMesh::rebuild(){
    if(!params.wise_structure->consistent())
        return false;

    clear();

    //ZERO VALUE
    if(params.parameter == "NONE" or params.cell_type == FIELD){
        //SQUARES (POINTS)
        vector<point>* pts = &params.wise_structure->points;

        vector<double> values = vector<double>(pts->size(),{0});

        if(draw_square){
            for(int i = 0; i < pts->size(); i++){
                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new GraphicPoint(&pts->operator[](i),values[i],square_size * params.wise_structure->get_height(),POINT)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }

        //LINES
        vector<WiseLine>* lns = &params.wise_structure->lines;
        vector<vector<double>> m_values = vector<vector<double>>(lns->size(),{ONE,{ZERO}});
        if(draw_line){
            for(int i = 0; i < lns->size(); i++){
                int A = lns->operator[](i).a;
                int B = lns->operator[](i).b;
                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Line(&pts->operator[](A),&pts->operator[](B),m_values[B],params.cell_type)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }

        //SQUARES (QUADS)
        vector<WiseCell>* cells = &params.wise_structure->cells;
        vector<vector<double>> cm_values = vector<vector<double>>(cells->size(),{FOUR,{ZERO}});
        if(draw_quad){
            for(int i = 0; i < cells->size(); i++){
                vector<int> pts_id = cells->operator[](i).points;

                if (pts_id.size() == FOUR){
                    point* points [4] = {&pts->operator[](pts_id[0]),&pts->operator[](pts_id[1]),&pts->operator[](pts_id[2]),&pts->operator[](pts_id[3])};
                    double values[4] = {cm_values[i][0],cm_values[i][1],cm_values[i][2],cm_values[i][3]};
                    unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Quad(points,values,params.cell_type)};
                    params.elements.operator[](sp->get_type()).push_back(move(sp));
                }
            }
        }


        return true;
    }

    if(params.cell_type == POINT){
        //SQUARES (POINTS)
        vector<point>* pts = &params.wise_structure->points;

        if(!params.wise_structure->has_parameter(params.parameter.data(),params.cell_type) )
            return false;

        vector<double> values = string_to_value(params.wise_structure->get_data(params.cell_type,params.parameter),params.wise_structure->get_point_type(params.parameter));

        if(values.size() != pts->size())
            return false;
        if(draw_square){
            for(int i = 0; i < pts->size(); i++){
                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new GraphicPoint(&pts->operator[](i),values[i],square_size * params.wise_structure->get_height(),POINT)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }


        //LINES
        //IF DATATYPE IS SINGULAR, INTERPOLATE
        //IF ITS NOT, USE VALUES AS INTERMEDIARY
        vector<WiseLine>* lns = &params.wise_structure->lines;
        vector<vector<double>> m_values = string_to_matrix(params.wise_structure->get_data(params.cell_type,params.parameter),params.wise_structure->get_point_type(params.parameter));

        if(m_values.size() != pts->size())
            return false;

        if(draw_line){
            for(int i = 0; i < lns->size(); i++){
                int A = lns->operator[](i).a;
                int B = lns->operator[](i).b;

                vector<double> value(m_values[A]);
                for (auto d : m_values[B])
                    value.push_back(d);

                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Line(&pts->operator[](A),&pts->operator[](B),value,params.cell_type)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }

        //SQUARES (QUADS)
        vector<WiseCell>* cells = &params.wise_structure->cells;

        if(draw_quad){
            for(int i = 0; i < cells->size(); i++){
                vector<int> pts_id = cells->operator[](i).points;

                if (pts_id.size() == FOUR){
                    point* points [4] = {&pts->operator[](pts_id[0]),&pts->operator[](pts_id[1]),&pts->operator[](pts_id[2]),&pts->operator[](pts_id[3])};
                    double values[4] = {values[pts_id[0]],values[pts_id[1]],values[pts_id[2]],values[pts_id[3]]};
                    unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Quad(points,values,params.cell_type)};
                    params.elements.operator[](sp->get_type()).push_back(move(sp));
                }
            }
        }

        return true;
    }

    if(params.cell_type == LINE){
        //SPHERES
        vector<point>* pts = &params.wise_structure->points;
        vector<WiseLine>* lns = &params.wise_structure->lines;

        if(!params.wise_structure->has_parameter(params.parameter.data(),params.cell_type))
            return false;

        unsigned int pts_size = pts->size();
        vector<bool> done(pts_size,false);

        //CYLINDERS & SPHERES
        //IF DATATYPE IS SINGULAR, INTERPOLATE
        //IF ITS NOT, USE VALUES AS INTERMEDIARY
        vector<vector<double>> m_values = string_to_matrix(params.wise_structure->get_data(params.cell_type,params.parameter),params.wise_structure->get_point_type(params.parameter));

        if(m_values.size() != lns->size())
            return false;

        for(int i = 0; i < lns->size(); i++){
            int A = lns->operator[](i).a;
            int B = lns->operator[](i).b;
            if(draw_square){
                if(!done[A]){
                    unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new GraphicPoint(&pts->operator[](i),ZERO,square_size * params.wise_structure->get_height(),params.cell_type,true)};

                    params.elements.operator[](sp->get_type()).push_back(move(sp));
                }
                if(!done[B]){
                    unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new GraphicPoint(&pts->operator[](i),ZERO,square_size * params.wise_structure->get_height(),params.cell_type,true)};

                    params.elements.operator[](sp->get_type()).push_back(move(sp));
                }
            }
            if(draw_line){
                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Line(&pts->operator[](A),&pts->operator[](B),m_values[B],params.cell_type)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }

        return true;
    }
    if(params.cell_type == CELL){
        //SQUARES (POINTS)
        vector<point>* pts = &params.wise_structure->points;
        vector<WiseLine>* lns = &params.wise_structure->lines;

        if(!params.wise_structure->has_parameter(params.parameter.data(),params.cell_type))
            return false;

        unsigned int pts_size = pts->size();
        vector<bool> done(pts_size,false);

        //SQUARES (POINTS) & LINES
        //IF DATATYPE IS SINGULAR, INTERPOLATE
        //IF ITS NOT, USE VALUES AS INTERMEDIARY
        vector<vector<double>> m_values = string_to_matrix(params.wise_structure->get_data(params.cell_type,params.parameter),params.wise_structure->get_point_type(params.parameter));

        for(int i = 0; i < lns->size(); i++){
            int A = lns->operator[](i).a;
            int B = lns->operator[](i).b;
            if(draw_square){
                if(!done[A]){
                    unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new GraphicPoint(&pts->operator[](i),ZERO,square_size * params.wise_structure->get_height(),params.cell_type,true)};

                    params.elements.operator[](sp->get_type()).push_back(move(sp));
                }
                if(!done[B]){
                    unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new GraphicPoint(&pts->operator[](i),ZERO,square_size * params.wise_structure->get_height(),params.cell_type,true)};

                    params.elements.operator[](sp->get_type()).push_back(move(sp));
                }
            }
            if(draw_line){
                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Line(&pts->operator[](A),&pts->operator[](B),ZERO,params.cell_type,true)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }

        //SQUARES (QUADS)
        vector<WiseCell>* cells = &params.wise_structure->cells;

        if(m_values.size() != cells->size())
            return false;

        if(draw_quad){
            for(int i = 0; i < cells->size(); i++){
                vector<int> pts_id = cells->operator[](i).points;

                if (pts_id.size() == FOUR){
                    point* points [4] = {&pts->operator[](pts_id[0]),&pts->operator[](pts_id[1]),&pts->operator[](pts_id[2]),&pts->operator[](pts_id[3])};
                    double values[4] = {m_values[i].front()};

                    if(m_values[i].size() == 4)
                        for(int j = 1; j < 4; j++)
                            values[i] = m_values[i][j];

                    unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Quad(points,values,params.cell_type)};
                    params.elements.operator[](sp->get_type()).push_back(move(sp));
                }
            }
        }

        return true;
    }

}

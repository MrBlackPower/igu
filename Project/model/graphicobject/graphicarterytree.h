#ifndef GRAPHICARTERYTREE_H
#define GRAPHICARTERYTREE_H

#include "model/wise/graphicobject.h"

#include "model/graphicelement/cylinder.h"
#include "model/graphicelement/sphere.h"

using namespace std;

class GraphicArteryTree : public GraphicObject
{
public:
    GraphicArteryTree(WiseStructure* structure);

    bool                        hover_node(WiseCellType cell_type, int id);
    bool                        select_node(WiseCellType cell_type, int id);
    vector<pair<string,string>> edit_node(WiseCellType cell_type, int id);

private:

    bool rebuild();
};

#endif // GRAPHICARTERYTREE_H

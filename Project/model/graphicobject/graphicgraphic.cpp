#include "graphicgraphic.h"

GraphicGraphic::GraphicGraphic(WiseStructure* structure) : GraphicObject(structure,"NONE",CELL)
{
    square_size = 0.001;
    draw_line = true;
    draw_square = true;
}

bool GraphicGraphic::hover_node(WiseCellType cell_type, int id){

}

bool GraphicGraphic::select_node(WiseCellType cell_type, int id){

}

vector<pair<string,string>> GraphicGraphic::edit_node(WiseCellType cell_type, int id){

}

bool GraphicGraphic::rebuild(){
    if(!params.wise_structure->consistent())
        return false;

    clear();

    //ZERO VALUE
    if(params.parameter == "NONE" or params.cell_type == FIELD){
        //SQUARES
        vector<point>* pts = &params.wise_structure->points;

        vector<double> values = vector<double>(pts->size(),{0});

        if(draw_square){
            for(int i = 0; i < pts->size(); i++){
                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new GraphicPoint(&pts->operator[](i),values[i],square_size * params.wise_structure->get_height(),POINT)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }

        //LINES
        vector<WiseLine>* lns = &params.wise_structure->lines;
        vector<vector<double>> m_values = vector<vector<double>>(lns->size(),{ONE,{ZERO}});
        if(draw_line){
            for(int i = 0; i < lns->size(); i++){
                int A = lns->operator[](i).a;
                int B = lns->operator[](i).b;
                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Line(&pts->operator[](A),&pts->operator[](B),m_values[B],params.cell_type)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }

        return true;
    }

    if(params.cell_type == POINT){
        //SPHERES
        vector<point>* pts = &params.wise_structure->points;

        if(!params.wise_structure->has_parameter(params.parameter.data(),params.cell_type))
            return false;

        vector<double> values = string_to_value(params.wise_structure->get_data(params.cell_type,params.parameter),params.wise_structure->get_point_type(params.parameter));

        if(values.size() != pts->size())
            return false;

        if(draw_square){
            for(int i = 0; i < pts->size(); i++){
                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new GraphicPoint(&pts->operator[](i),values[i],square_size * params.wise_structure->get_height(),POINT)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }


        //CYLINDERS
        //IF DATATYPE IS SINGULAR, INTERPOLATE
        //IF ITS NOT, USE VALUES AS INTERMEDIARY
        vector<WiseLine>* lns = &params.wise_structure->lines;
        vector<vector<double>> m_values = string_to_matrix(params.wise_structure->get_data(params.cell_type,params.parameter),params.wise_structure->get_point_type(params.parameter));

        if(m_values.size() != pts->size())
            return false;
        if(draw_line){
            for(int i = 0; i < lns->size(); i++){
                int A = lns->operator[](i).a;
                int B = lns->operator[](i).b;

                vector<double> value(m_values[A]);
                for (auto d : m_values[B])
                    value.push_back(d);

                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Line(&pts->operator[](A),&pts->operator[](B),value,params.cell_type)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }

        return true;
    }

    if(params.cell_type == CELL or params.cell_type == LINE){
        //SQUARES
        vector<point>* pts = &params.wise_structure->points;
        vector<WiseLine>* lns = &params.wise_structure->lines;

        if(!params.wise_structure->has_parameter(params.parameter.data(),params.cell_type))
            return false;

        unsigned int pts_size = pts->size();
        vector<bool> done(pts_size,false);

        //SQUARES & LINES
        //IF DATATYPE IS SINGULAR, INTERPOLATE
        //IF ITS NOT, USE VALUES AS INTERMEDIARY
        vector<vector<double>> m_values = string_to_matrix(params.wise_structure->get_data(params.cell_type,params.parameter),params.wise_structure->get_point_type(params.parameter));

        if(m_values.size() != lns->size())
            return false;

        for(int i = 0; i < lns->size(); i++){
            int A = lns->operator[](i).a;
            int B = lns->operator[](i).b;
            if(draw_square){
                if(!done[A]){
                    unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new GraphicPoint(&pts->operator[](i),m_values[i].front(),square_size * params.wise_structure->get_height(),params.cell_type)};

                    params.elements.operator[](sp->get_type()).push_back(move(sp));
                }
                if(!done[B]){
                    unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new GraphicPoint(&pts->operator[](i),m_values[i].back(),square_size * params.wise_structure->get_height(),params.cell_type)};

                    params.elements.operator[](sp->get_type()).push_back(move(sp));
                }
            }
            if(draw_line){

                vector<double> value(m_values[A]);
                for (auto d : m_values[B])
                    value.push_back(d);

                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Line(&pts->operator[](A),&pts->operator[](B),m_values[B],params.cell_type)};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
        }

        return true;
    }
}

#include "graphicarterytree.h"

GraphicArteryTree::GraphicArteryTree(WiseStructure* structure) : GraphicObject(structure,"NONE",CELL)
{

}


bool GraphicArteryTree::hover_node(WiseCellType cell_type, int id){

}

bool GraphicArteryTree::select_node(WiseCellType cell_type, int id){

}

vector<pair<string,string>> GraphicArteryTree::edit_node(WiseCellType cell_type, int id){

}

bool GraphicArteryTree::rebuild(){
    if(!params.wise_structure->consistent())
        return false;

    clear();

    //ZERO VALUE
    if(params.parameter == "NONE" or params.cell_type == FIELD){
        //SPHERES
        vector<point>* pts = &params.wise_structure->points;
        vector<double> radius = igumath::string_to_double(params.wise_structure->get_point_data("RADIUS"));

        if(radius.size() != pts->size())
            return false;

        vector<double> values = vector<double>(pts->size(),{0});

        for(int i = 0; i < pts->size(); i++){
            unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Sphere(&pts->operator[](i),radius[i]*1.2,params.cell_type,values[i])};

            params.elements.operator[](sp->get_type()).push_back(move(sp));
        }

        //CYLINDERS
        vector<WiseLine>* lns = &params.wise_structure->lines;
        vector<vector<double>> m_values = vector<vector<double>>(lns->size(),{ONE,{ZERO}});
        for(int i = 0; i < lns->size(); i++){
            int A = lns->operator[](i).a;
            int B = lns->operator[](i).b;
            unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Cylinder(&pts->operator[](A),&pts->operator[](B),params.cell_type,{radius[A],radius[B]},m_values[i])};

            params.elements.operator[](sp->get_type()).push_back(move(sp));
        }

        return true;
    }

    if(params.cell_type == POINT){
        //SPHERES
        vector<point>* pts = &params.wise_structure->points;
        vector<double> radius = igumath::string_to_double(params.wise_structure->get_point_data("RADIUS"));

        if(!params.wise_structure->has_parameter(params.parameter.data(),params.cell_type) or radius.size() != pts->size())
            return false;

        vector<double> values = string_to_value(params.wise_structure->get_data(params.cell_type,params.parameter.data()),params.wise_structure->get_point_type(params.parameter.data()));

        if(values.size() != pts->size())
            return false;

        for(int i = 0; i < pts->size(); i++){
            unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Sphere(&pts->operator[](i),radius[i]*1.2,params.cell_type,values[i])};

            params.elements.operator[](sp->get_type()).push_back(move(sp));
        }


        //CYLINDERS
        //IF DATATYPE IS SINGULAR, INTERPOLATE
        //IF ITS NOT, USE VALUES AS INTERMEDIARY
        vector<WiseLine>* lns = &params.wise_structure->lines;
        vector<vector<double>> m_values = string_to_matrix(params.wise_structure->get_data(params.cell_type,params.parameter.data()),params.wise_structure->get_point_type(params.parameter.data()));

        if(m_values.size() != pts->size())
            return false;

        for(int i = 0; i < lns->size(); i++){
            int A = lns->operator[](i).a;
            int B = lns->operator[](i).b;
            unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Cylinder(&pts->operator[](A),&pts->operator[](B),params.cell_type,{radius[A],radius[B]},m_values[B])};

            params.elements.operator[](sp->get_type()).push_back(move(sp));
        }

        return true;
    }

    if(params.cell_type == CELL or params.cell_type == LINE){
        //SPHERES
        vector<point>* pts = &params.wise_structure->points;
        vector<double> radius = igumath::string_to_double(params.wise_structure->get_point_data("RADIUS"));
        vector<WiseLine>* lns = &params.wise_structure->lines;

        if(!params.wise_structure->has_parameter(params.parameter.data(),params.cell_type) or radius.size() != pts->size())
            return false;

        unsigned int pts_size = pts->size();
        vector<bool> done(pts_size,false);

        //CYLINDERS & SPHERES
        //IF DATATYPE IS SINGULAR, INTERPOLATE
        //IF ITS NOT, USE VALUES AS INTERMEDIARY
        vector<vector<double>> m_values = string_to_matrix(params.wise_structure->get_data(params.cell_type,params.parameter.data()),params.wise_structure->get_point_type(params.parameter.data()));

        if(m_values.size() != lns->size())
            return false;

        for(int i = 0; i < lns->size(); i++){
            int A = lns->operator[](i).a;
            int B = lns->operator[](i).b;

            if(!done[A]){
                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Sphere(&pts->operator[](A),radius[A]*1.2,params.cell_type,m_values[i].front())};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }
            if(!done[B]){
                unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Sphere(&pts->operator[](B),radius[B]*1.2,POINT,m_values[i].front())};

                params.elements.operator[](sp->get_type()).push_back(move(sp));
            }

            unique_ptr<GraphicElement> sp = unique_ptr<GraphicElement>{new Cylinder(&pts->operator[](A),&pts->operator[](B),params.cell_type,{radius[A],radius[B]},m_values[i])};

            params.elements.operator[](sp->get_type()).push_back(move(sp));
        }

        return true;
    }
}

#ifndef GRAPHICPOLY_H
#define GRAPHICPOLY_H

#include "model/wise/graphicobject.h"

#include "model/graphicelement/graphicpoint.h"
#include "model/graphicelement/line.h"
#include "model/graphicelement/cube.h"

#define EIGHT 8

using namespace std;

class GraphicPoly : public GraphicObject
{
public:
    GraphicPoly(WiseStructure* structure);

    bool                        hover_node(WiseCellType cell_type, int id);
    bool                        select_node(WiseCellType cell_type, int id);
    vector<pair<string,string>> edit_node(WiseCellType cell_type, int id);

private:

    bool rebuild();

    double square_size;
    bool draw_square;
    bool draw_line;
    bool draw_cube;
};

#endif // GRAPHICPOLY_H

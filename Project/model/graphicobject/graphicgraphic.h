#ifndef GRAPHICGRAPHIC_H
#define GRAPHICGRAPHIC_H


#include "model/wise/graphicobject.h"

#include "model/graphicelement/graphicpoint.h"
#include "model/graphicelement/line.h"

using namespace std;

class GraphicGraphic : public GraphicObject
{
public:
    GraphicGraphic(WiseStructure* structure);

    bool                        hover_node(WiseCellType cell_type, int id);
    bool                        select_node(WiseCellType cell_type, int id);
    vector<pair<string,string>> edit_node(WiseCellType cell_type, int id);

private:

    bool rebuild();

    double square_size;
    bool draw_square;
    bool draw_line;
};

#endif // GRAPHICGRAPHIC_H

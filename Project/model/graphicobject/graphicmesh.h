#ifndef GRAPHICMESH_H
#define GRAPHICMESH_H

#include "model/wise/graphicobject.h"

#include "model/graphicelement/graphicpoint.h"
#include "model/graphicelement/line.h"
#include "model/graphicelement/quad.h"

#define FOUR 4

using namespace std;

class GraphicMesh : public GraphicObject
{
public:
    GraphicMesh(WiseStructure* structure);

    bool                        hover_node(WiseCellType cell_type, int id);
    bool                        select_node(WiseCellType cell_type, int id);
    vector<pair<string,string>> edit_node(WiseCellType cell_type, int id);

private:

    bool rebuild();

    double square_size;
    bool draw_square;
    bool draw_line;
    bool draw_quad;
};

#endif // GRAPHICMESH_H

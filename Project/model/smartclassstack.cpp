#include "smartclassstack.h"

SmartClassStack::SmartClassStack(int SID)
{
    N = 1;

    SmartClass smart;
    smart.className = "SMART OBJECT";
    smart.classID = SID;

    stack.push_back(smart);
}


SmartClassStack::~SmartClassStack()
{
    stack.clear();
    N = 0;
}

void SmartClassStack::addClass(QString className, int classID){
    SmartClass aux;
    aux.level = N;
    aux.classID = classID;
    aux.className = className;

    stack.push_back(aux);

    N++;
}

int SmartClassStack::size(){
    return N;
}

vector<QString> SmartClassStack::getStartHeader(){
    vector<QString> r;

    for(int i = 0; i < stack.size(); i++){
        SmartClass it = stack[i];
        QString aux = tab(it.level);
        aux += aux.asprintf("<%s ID = %d>",it.className.toStdString().data(),it.classID);

        r.push_back(aux);
    }

    return r;
}
vector<QString> SmartClassStack::getFinishHeader(){
    vector<QString> r;

    for(int i = stack.size() - 1; i >= 0; i++){
        SmartClass it = stack[i];
        QString aux = tab(it.level);
        aux += aux.asprintf("</%s>",it.className.toStdString().data());

        r.push_back(aux);
    }

    return r;
}

QString SmartClassStack::tab(int n){
    QString aux = "";

    for(int i = 0; i < n; i++)
        aux += "  ";

    return aux;
}

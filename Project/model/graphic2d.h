#ifndef LINEGRAPHIC_H
#define LINEGRAPHIC_H

#include <QOpenGLWidget>
#include <QColor>
#include <vector>
#include "model/wise/point.h"
#include "graphicobject.h"
#include "colorbar/red.h"

#define TWO 2

#define EXACT_N 10000

/************************************************************************************
  Name:        lineGraphic.h
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Header class of an linear graphic structure.
************************************************************************************/
using namespace std;
using namespace wisepoint;

class Graphic2D : public GraphicObject
{
public:
/************************************************************************************
        CONSTRUCTOR
************************************************************************************/
    Graphic2D(QString name, SmartObject* parent = NULL);
    Graphic2D(QString name, ColorBar cb, SmartObject* parent = NULL);

    void resizeTransform();

/************************************************************************************
        DESTRUCTOR
************************************************************************************/
    ~Graphic2D();
    vector<point*> getData();

    double getY(double x);

    virtual bool addData(Graphic2D* graphic);
    virtual bool addData(vector<point*> data);
    virtual bool addData(vector<point> data);
    virtual bool addData(point p);
    virtual bool addData(point* p);
    bool clearData();

    /********************************************************
    *  SMART OBJECT VIRTUAL METHODS                       *
    ********************************************************/
    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool compatibility(VTKFile *file);
    bool load(VTKFile* file);
    void draw();
    vector<Field> getPointDataList();
    vector<Field> getCellDataList();
    virtual vector<Field> getStartParameters() = 0;
    virtual vector<Field> getContextVisualParameters() = 0;
    virtual vector<QString> getRanges() = 0;
    virtual bool updateStartParameter(QString field, QString value) = 0;
    virtual bool setContextRange(QString field) = 0;

    GraphicObject* extrapolateContext(QString extrapolation);

    int getN();

    virtual point* operator[](int i);

protected:
    vector<point*> raw_data;

    int N;

private:
    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    virtual double iteration() = 0;
    virtual bool initialize() = 0;
    void finish();
    vector<QString> getExtrapolations();
    virtual bool updateContextVisualParameter(QString field, QString value) = 0;
    virtual QString getLiveParameterList() = 0;
    virtual QString getLiveParameter(QString name) = 0;
    virtual DataType getLiveParameterDataType(QString name) = 0;
    virtual void mouse_press_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool) = 0;
    virtual void mouse_move_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool) = 0;
};

#endif // LINEGRAPHIC_H

#ifndef GRAPHICOBJECT_H
#define GRAPHICOBJECT_H

#include <iostream>

#include <QString>
#include <QColor>
#include <QFont>
#include <QPainter>
#include <QEventLoop>
#include <QElapsedTimer>


#include <vector>
#include <GL/glu.h>
#include <GL/gl.h>
#include <QMouseEvent>
#include <ctime>
#include <chrono>
#include <functional>
#include <omp.h>
#include "smartobject.h"
#include "model/wise/point.h"
#include "colorbar.h"
#include "colorbar/brasil.h"
#include "colorbar/aero.h"
#include "colorbar/black.h"
#include "colorbar/blue.h"
#include "colorbar/brasil.h"
#include "colorbar/cyan.h"
#include "colorbar/green.h"
#include "colorbar/magenta.h"
#include "colorbar/red.h"
#include "colorbar/yellow.h"
#include "colorbar/summer.h"
#include "colorbar/winter.h"
#include "colorbar/colorcube.h"
#include "../helper/mathHelper.h"
#include "../helper/vtkfile.h"
#include "smartlogmessage.h"
#include "../window/dialog/graphiccomponentdialog.h"
#include "../window/dialog/graphiclimitdialog.h"

#define ZERO 0
#define ONE  1
#define INT_MAX  2147483647

#define ITERATIONS_LIMIT 999999999
#define PLOT_SIZE 8.0

#define NEAR 1.0
#define FAR 1000.0
#define FOVY 45.0

#define LINE_SPACING 0.025

#define INFINITY_PLUS 32767
#define INFINITY_MINUS -32767

struct Error{
    double absolute;
    int numel;
};


using namespace std;
using namespace placeholders;
using namespace GraphicSpace;
using namespace wisepoint;

class GraphicObject : public SmartObject{
Q_OBJECT
    public:
        GraphicObject(QString name, SmartObject* parent = NULL);
        ~GraphicObject();

        static QString statusToString(GraphicObjectStatus status);

        static void setDefaultRawPool(SmartObject* default_raw);
        static void purgeDefaultRawPool();

        //SMART OBJECT VIRTUAL METHODS
        virtual vector<QString> print() = 0;
        virtual vector<QString> data() = 0;
        virtual vector<QString> raw() = 0;

        //VIRTUAL METHODS
        virtual bool compatibility(VTKFile* file) = 0;
        virtual bool load(VTKFile* file) = 0;
        virtual void draw() = 0;
        virtual vector<Field> getPointDataList() = 0;
        virtual vector<Field> getCellDataList() = 0;
        virtual vector<Field> getStartParameters() = 0;
        virtual vector<Field> getContextVisualParameters() = 0;
        virtual vector<QString> getRanges() = 0;
        virtual bool updateStartParameter(QString field, QString value) = 0;
        virtual bool setContextRange(QString field) = 0;
        virtual void resizeTransform() = 0;

        void setLimit(int max_x, int max_y, int max_z, int  min_x, int  min_y, int  min_z);
        void setLimit(point limit_max, point limit_min);

        void mouse_press(int width, int height, point eye, QMouseEvent *event, GraphicTools tool);
        void mouse_move(int width, int height, point eye, QMouseEvent *event, GraphicTools tool);

        GraphicObject* extrapolate(QString extrapolation);

        vector<QString> extrapolations();

        QString liveParameterList();
        QString liveParameter(QString name);
        DataType liveParameterDataType(QString name);

        bool compatible(VTKFile* file);
        bool compatibleFields(VTKFile* file);
        bool start();
        double delay();
        double iterate(bool iterateSlaves = true);
        double iterate(int n);
        double iterate(double time);
        bool restart();
        void pause();
        void end();
        void crash();

        void drawObject();
        bool isIterating();
        bool hasIterated();
        point getScreenPoint(point p, point eye, float aspect);
        point getCanvasPoint(point p, point eye, float width, float height);
        point getModelPoint(point p, point eye, float width, float height, float z_offset = GraphicObject::z_offset, bool canvas_point = true);
        int getGID();

        void resetLimit();
        void resetGuide();
        void updateLimit(vector<point> p);
        void updateLimit(vector<point*> p);
        void updateLimit(point* p);
        void updateLimit(point p);
        void updateGuide(bool status, int id);


        point getLimitMax();
        point getLimitMin();
        point getTransformation();
        point getRotation();
        void setRotation(point r);
        double getScale();
        GraphicObjectStatus getStatus();
        double getMax();
        void setMax(double max);
        double getMin();
        void setMin(double min);
        void setMinMax(double val);
        QString getColorBar();
        void setColorBar(ColorBar c);
        void setColorBar(QString name);
        double getModeltime();
        double getDeltaT();

        //
        void setPainterMethod(function<QPainter*(int)> f, int CID);
        void purgePainterMethod();
        void drawText(point* position, QString text, QColor color = QColor("Black"), const QFont &font = QFont("Ubuntu Mono",12));
        double fps();
        void drawGuideLines();

        vector<Field> getVisualParameters();
        bool updateVisualParameter(QString field, QString value);
        bool setRange(QString field);
        QString getCurrentRange();


        bool operator ==(GraphicObject* gObj);
        bool operator ==(int gid);

        bool hasMaster();
        GraphicObject* getMaster();
        void setMaster(GraphicObject* gObj);
        void purgeMaster();

        bool hasSlaves();
        vector<GraphicObject*> getSlaves();
        void addSlave(GraphicObject* gObj);
        void removeSlave(int gid);

        bool isInIterationTree(GraphicObject* gObj);

        vector<GraphicObject*> getConcurrents();
        void addConcurrent(GraphicObject* gObj);
        void removeConcurrent(int gid);

        static vector<GraphicObject*> getGraphics();
        static GraphicObject* getGraphic(int gid);

        static vector<ColorBar> getPossibleColorbars();

        static double angles_per_pixel;
        static double scalar_step_per_pixel;
        static double move_per_pixel;
        static double z_offset;


        GraphicObject* next_graphic;
        GraphicObject* previous_graphic;

    signals:
        void print_raw(vector<QString> raw, QString fileName = "");

    protected:
        bool setGraphicComposerDialog(GraphicObjectComposer* component);

        bool setLimitDialog();

        QColor interpolateColor(double value);
        void setMaterialColor(QColor c);

        int getCilynderStacks();
        int getCilynderSlices();
        int getSphereStacks();
        int getSphereSlices();

        void drawSphere(point* p, double r, QColor color = QColor("Black"));
        void drawCilynder(point* start, point* end, double radius);
        void drawCilynder(point* start, point* end, double startRadius, double endRadius);
        void drawCilynder(point* start, point* end, double radius, vector<double> value);
        void drawCilynder(point* start, point* end, double radius, vector<double> value, QColor color);
        void drawCilynder(point* start, point* end, double startRadius, double endRadius, vector<double> value);
        void drawCilynder(point* start, point* end, double startRadius, double endRadius, vector<double> value, QColor color);

        void drawArrow(point* start, point* end, double arrow_end_offset, double triangle_size, QColor color = QColor("Black"));
        void drawTriangle(point* start, point* end, QColor color = QColor("Black"));

        void drawLine(point* start, point* end, QColor color = QColor("Black"));
        void drawGuideLineX(double y, double z, QColor color = QColor("Black"));
        void drawGuideLineY(double x, double z, QColor color = QColor("Black"));
        void drawGuideLineZ(double x, double y, QColor color = QColor("Black"));

        Clock time;
        ColorBar colorBar;
        point mouse_last_pos;
        bool mouse_being_used;
        Limit limit;
        Range range;
        GraphicObjectStatus status;
    private:
        bool isInIterationTree(GraphicObject* root, GraphicObject* gObj);
        bool isInIterationTreeAux(GraphicObject* root, GraphicObject* gObj);

        void updateTransform();

        void move(double dx, double dy, double dz = 0.0);

        //VIRTUAL METHODS
        virtual bool initialize() = 0;
        virtual double iteration() = 0;
        virtual void finish() = 0;
        virtual bool updateContextVisualParameter(QString field, QString value) = 0;
        virtual vector<QString> getExtrapolations() = 0;
        virtual QString getLiveParameterList() = 0;
        virtual QString getLiveParameter(QString name) = 0;
        virtual DataType getLiveParameterDataType(QString name) = 0;
        virtual GraphicObject* extrapolateContext(QString extrapolation) = 0;
        virtual void mouse_press_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool) = 0;
        virtual void mouse_move_event(int width, int height, point eye, QMouseEvent *event, GraphicTools tool) = 0;

        GraphicSpace::Transform trnsf;
        GenericVisualParams visualParams;

        //POINTER TO PAINTER IN CANVAS
        function<QPainter*(int)> getQPainter;
        int CID;
        bool QPainterSet;


    /************************************************************************************
            GUIDE LINES STATUS
            true | false
            [0] -> x axis grid
            [1] -> y axis grid
            [2] -> z axis grid
            [3] -> x axis fine grid
            [4] -> y axis fine grid
            [5] -> z axis fine grid
            [6] -> border
            [7] -> 3D-Gizmo
            [8] -> Colorbar
    ************************************************************************************/
        vector<bool> guide_lines_status;

        //GRAPHIC ID
        static unsigned int CURR_GID;
        static GraphicObject* first_graphic;
        static GraphicObject* last_graphic;

        const unsigned int GID;

        static SmartObject* default_raw;

        //ITERATION TREE
        GraphicObject* master;
        vector<GraphicObject*> slaves;

        //DRAW GRAPH
        vector<GraphicObject*> concurrent;
};

class LiveParamGraphic;;


#endif // GRAPHICOBJECT_H

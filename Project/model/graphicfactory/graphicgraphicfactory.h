#ifndef GRAPHICGRAPHICFACTORY_H
#define GRAPHICGRAPHICFACTORY_H

#include "model/wise/graphicobjectfactory.h"

#include "model/graphicobject/graphicgraphic.h"

#define GRAPHIC_FACTORY_NAME "GRAPHIC_GRAPHIC_FACTORY"
#define GRAPHIC_FACTORY_TYPE "GRAPHIC"

class GraphicGraphicFactory : public GraphicObjectFactory
{
public:
    GraphicGraphicFactory();

    void set_fields(WiseStructure* object, GraphicModel* model);

    bool iterate(WiseStructure* obj, GraphicModel* model);

private:

    unique_ptr<GraphicModel> create_graphic(WiseStructure* obj);
};

#endif // GRAPHICGRAPHICFACTORY_H

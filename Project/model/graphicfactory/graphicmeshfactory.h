#ifndef GRAPHICMESHFACTORY_H
#define GRAPHICMESHFACTORY_H

#include "model/wise/graphicobjectfactory.h"

#include "model/graphicobject/graphicmesh.h"

#define GRAPHIC_FACTORY_NAME "GRAPHIC_MESH_FACTORY"
#define GRAPHIC_FACTORY_TYPE "MESH"

class GraphicMeshfactory : public GraphicObjectFactory
{
public:
    GraphicMeshfactory();

    void set_fields(WiseStructure* object, GraphicModel* model);

    bool iterate(WiseStructure* obj, GraphicModel* model);

private:

    unique_ptr<GraphicModel> create_graphic(WiseStructure* obj);
};

#endif // GRAPHICMESHFACTORY_H

#include "graphicarterytreefactory.h"

GraphicArteryTreeFactory::GraphicArteryTreeFactory() : GraphicObjectFactory(GRAPHIC_FACTORY_NAME,GRAPHIC_FACTORY_TYPE)
{

}

void GraphicArteryTreeFactory::set_fields(WiseStructure* object, GraphicModel* model){
    object->add_field("VECTOR_VIEW","MEAN",GRAPHIC,STRING,LIVE);
    object->add_field("TENSOR_VIEW","MEAN",GRAPHIC,STRING,LIVE);

    vector<pair<string,string>> aux = model->pop()->get_static_parameters();

    for(auto p : aux){
        object->add_field(p.first,p.second,GRAPHIC,STRING,LIVE);
    }
}

bool GraphicArteryTreeFactory::iterate(WiseStructure* obj, GraphicModel* model){
    unique_ptr<GraphicObject> p  = unique_ptr<GraphicObject>{new GraphicArteryTree(obj)};

    if(p->rebuild()){
        model->push_frame(move(p));
        return true;
    }

    return false;
}


unique_ptr<GraphicModel> GraphicArteryTreeFactory::create_graphic(WiseStructure* obj){
    unique_ptr<GraphicObject> p  = unique_ptr<GraphicObject>{new GraphicArteryTree(obj)};

    if(p->rebuild())
        return unique_ptr<GraphicModel>{(new GraphicModel(move(p)))};

    return NULL;
}

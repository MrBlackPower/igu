#ifndef GRAPHICARTERYTREEFACTORY_H
#define GRAPHICARTERYTREEFACTORY_H

#include "model/wise/graphicobjectfactory.h"

#include "model/graphicobject/graphicarterytree.h"

#define GRAPHIC_FACTORY_NAME "GRAPHIC_ARTERY_TREE_FACTORY"
#define GRAPHIC_FACTORY_TYPE "ARTERY_TREE"

using namespace std;

class GraphicArteryTreeFactory : public GraphicObjectFactory
{
public:

    GraphicArteryTreeFactory();

    void set_fields(WiseStructure* object, GraphicModel* model);

    bool iterate(WiseStructure* obj, GraphicModel* model);

private:

    unique_ptr<GraphicModel> create_graphic(WiseStructure* obj);
};

#endif // GRAPHICARTERYTREEFACTORY_H

#ifndef GRAPHICPOLYFACTORY_H
#define GRAPHICPOLYFACTORY_H


#include "model/wise/graphicobjectfactory.h"

#include "model/graphicobject/graphicpoly.h"

#define GRAPHIC_FACTORY_NAME "GRAPHIC_POLY_FACTORY"
#define GRAPHIC_FACTORY_TYPE "POLY"

class GraphicPolyFactory : public GraphicObjectFactory
{
public:
    GraphicPolyFactory();

    void set_fields(WiseStructure* object, GraphicModel* model);

    bool iterate(WiseStructure* obj, GraphicModel* model);

private:

    unique_ptr<GraphicModel> create_graphic(WiseStructure* obj);
};

#endif // GRAPHICPOLYFACTORY_H

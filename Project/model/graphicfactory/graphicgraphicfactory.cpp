#include "graphicgraphicfactory.h"

GraphicGraphicFactory::GraphicGraphicFactory() : GraphicObjectFactory(GRAPHIC_FACTORY_NAME,GRAPHIC_FACTORY_TYPE)
{

}

void GraphicGraphicFactory::set_fields(WiseStructure* object, GraphicModel* model){
    object->add_field("SQUARE_SIZE","0.001",GRAPHIC,DOUBLE,LIVE);
    object->add_field("DRAW_LINES","1",GRAPHIC,INT,LIVE);
    object->add_field("DRAW_SQUARES","1",GRAPHIC,INT,LIVE);

    vector<pair<string,string>> aux = model->pop()->get_static_parameters();

    for(auto p : aux){
        object->add_field(p.first,p.second,GRAPHIC,STRING,LIVE);
    }
}

bool GraphicGraphicFactory::iterate(WiseStructure* obj, GraphicModel* model){
    unique_ptr<GraphicObject> p  = unique_ptr<GraphicObject>{new GraphicGraphic(obj)};

    if(p->rebuild()){
        model->push_frame(move(p));
        return true;
    }

    return false;
}


unique_ptr<GraphicModel> GraphicGraphicFactory::create_graphic(WiseStructure* obj){
    unique_ptr<GraphicObject> p  = unique_ptr<GraphicObject>{new GraphicGraphic(obj)};

    if(p->rebuild())
        return unique_ptr<GraphicModel>{(new GraphicModel(move(p)))};

    return NULL;
}

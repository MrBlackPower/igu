#include "graphicpolyfactory.h"

GraphicPolyFactory::GraphicPolyFactory() : GraphicObjectFactory(GRAPHIC_FACTORY_NAME,GRAPHIC_FACTORY_TYPE)
{

}

void GraphicPolyFactory::set_fields(WiseStructure* object, GraphicModel* model){
    object->add_field("SQUARE_SIZE","0.001",GRAPHIC,DOUBLE,LIVE);
    object->add_field("DRAW_LINES","1",GRAPHIC,INT,LIVE);
    object->add_field("DRAW_SQUARES","1",GRAPHIC,INT,LIVE);
    object->add_field("DRAW_CUBES","1",GRAPHIC,INT,LIVE);

    vector<pair<string,string>> aux = model->pop()->get_static_parameters();

    for(auto p : aux){
        object->add_field(p.first,p.second,GRAPHIC,STRING,LIVE);
    }
}

bool GraphicPolyFactory::iterate(WiseStructure* obj, GraphicModel* model){
    unique_ptr<GraphicObject> p  = unique_ptr<GraphicObject>{new GraphicPoly(obj)};

    if(p->rebuild()){
        model->push_frame(move(p));
        return true;
    }

    return false;
}


unique_ptr<GraphicModel> GraphicPolyFactory::create_graphic(WiseStructure* obj){
    unique_ptr<GraphicObject> p  = unique_ptr<GraphicObject>{new GraphicPoly(obj)};

    if(p->rebuild())
        return unique_ptr<GraphicModel>{(new GraphicModel(move(p)))};

    return NULL;
}

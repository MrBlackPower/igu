#ifndef CUBE_H
#define CUBE_H

#include <GL/gl.h>

#include "model/wise/point.h"
#include "model/wise/graphicelement.h"

#include "model/wise/wisestructure.h"

#include <QColor>

using namespace std;

using namespace wise;

#define GRAPHIC_ELEMENT_NAME "CUBE"

class Cube : public GraphicElement
{
public:
    Cube(point* points[8], double value[8], WiseCellType cell_type);

    void draw(ColorBar* color_bar);

    vector<pair<string,string>> get_context_paremeters();

    bool set_context_paremeters(string param, string val);

private:
};

#endif // CUBE_H

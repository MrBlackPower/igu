#include "line.h"

double Line::line_width = 2.0;

Line::Line(point* start, point* end, double value, WiseCellType cell_type, bool draw_black) : GraphicElement(GRAPHIC_ELEMENT_NAME,cell_type), draw_black(draw_black)
{
    points.push_back(start); //0
    points.push_back(end);   //1

    this->value.push_back(value);
}

Line::Line(point* start, point* end, vector<double> value, WiseCellType cell_type, bool draw_black) : GraphicElement(GRAPHIC_ELEMENT_NAME,cell_type), draw_black(draw_black)
{
    points.push_back(start); //0
    points.push_back(end);   //1

    for(auto v : value)
        this->value.push_back(v);
}


void Line::draw(ColorBar* color_bar){
    glLineWidth(line_width);
    if(!value.empty()){
        if(hatching == ZERO){
            if(value.size() == ONE){
                set_material_color(color_bar->interpolate_value(value.operator[](ZERO)));
            } else {
                point dx = (points[ONE] - points[ZERO])/value.size();
                for(int i = 0; i < (value.size() - 1 ); i++){
                    point a = (dx * i) + (*(points[ZERO]));
                    point b = (dx * (i+1)) + (*(points[ZERO]));
                    if(!draw_black){
                        glBegin(GL_LINES);
                        set_material_color(color_bar->interpolate_value(value.operator[](i)));
                        glVertex3f(a.X(),a.Y(),a.Z());
                        set_material_color(color_bar->interpolate_value(value.operator[](i+1)));
                        glVertex3f(b.X() ,b.Y() ,b.Z());
                        glEnd();
                    } else {
                        glBegin(GL_LINES);
                        set_material_color(QColor("Black"));
                        glVertex3f(a.X(),a.Y(),a.Z());
                        set_material_color(QColor("Black"));
                        glVertex3f(b.X() ,b.Y() ,b.Z());
                    }
                }
            }
        } else {
            set_material_color(hatching_colors[hatching]);
        }
    } else {
        set_material_color(QColor("Black"));
    }
    glBegin(GL_LINES);
        glVertex3f(points[ZERO]->X(),points[ZERO]->Y(),points[ZERO]->Z());
        glVertex3f(points[ONE]->X() ,points[ONE]->Y() ,points[ONE]->Z());
    glEnd();
}

vector<pair<string,string>> Line::get_context_paremeters(){
    stringstream aux;
    vector<pair<string,string>> r;

    aux << line_width;
    pair<string,string> cylinder_stacks("LINES_WIDTH",aux.str());
    r.push_back(cylinder_stacks);

    return r;
}

bool Line::set_context_paremeters(string param, string val){
    if(param == "LINES_WIDTH"){
        line_width = stod(val);
        return true;
    }

    return false;
}

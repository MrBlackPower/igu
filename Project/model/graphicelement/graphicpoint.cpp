#include "graphicpoint.h"

double GraphicPoint::point_height = 2;
double GraphicPoint::point_width = 2;
double GraphicPoint::point_depth = 2;

GraphicPoint::GraphicPoint(point* pt, double value, WiseCellType cell_type, bool draw_black) : GraphicElement(GRAPHIC_ELEMENT_NAME,cell_type), draw_black(draw_black)
{
    for(int i = 0; i < 8; i++)
        points.push_back(new point());

    double height = point_height;
    double width = point_width;
    double depth = point_depth;

    points[0]->operator=(point(0,(pt->X() - (width /2)),(pt->Y() - (height/2)),(pt->Z() - (depth/2))));
    points[1]->operator=(point(0,(pt->X() + (width /2)),(pt->Y() - (height/2)),(pt->Z() - (depth/2))));
    points[2]->operator=(point(0,(pt->X() + (width /2)),(pt->Y() + (height/2)),(pt->Z() - (depth/2))));
    points[3]->operator=(point(0,(pt->X() - (width /2)),(pt->Y() + (height/2)),(pt->Z() - (depth/2))));
    points[4]->operator=(point(0,(pt->X() - (width /2)),(pt->Y() - (height/2)),(pt->Z() + (depth/2))));
    points[5]->operator=(point(0,(pt->X() + (width /2)),(pt->Y() - (height/2)),(pt->Z() + (depth/2))));
    points[6]->operator=(point(0,(pt->X() + (width /2)),(pt->Y() + (height/2)),(pt->Z() + (depth/2))));
    points[7]->operator=(point(0,(pt->X() - (width /2)),(pt->Y() + (height/2)),(pt->Z() + (depth/2))));

    for(int i = 0; i < 8; i++)
        this->value[i] = value;
}
GraphicPoint::GraphicPoint(point* pt, double value, double size, WiseCellType cell_type, bool draw_black) : GraphicElement(GRAPHIC_ELEMENT_NAME,cell_type), draw_black(draw_black)
{
    for(int i = 0; i < 8; i++)
        points.push_back(new point());

    double height = size;
    double width = size;
    double depth = size;

    points[0]->operator=(point(0,(pt->X() - (width /2)),(pt->Y() - (height/2)),(pt->Z() - (depth/2))));
    points[1]->operator=(point(0,(pt->X() + (width /2)),(pt->Y() - (height/2)),(pt->Z() - (depth/2))));
    points[2]->operator=(point(0,(pt->X() + (width /2)),(pt->Y() + (height/2)),(pt->Z() - (depth/2))));
    points[3]->operator=(point(0,(pt->X() - (width /2)),(pt->Y() + (height/2)),(pt->Z() - (depth/2))));
    points[4]->operator=(point(0,(pt->X() - (width /2)),(pt->Y() - (height/2)),(pt->Z() + (depth/2))));
    points[5]->operator=(point(0,(pt->X() + (width /2)),(pt->Y() - (height/2)),(pt->Z() + (depth/2))));
    points[6]->operator=(point(0,(pt->X() + (width /2)),(pt->Y() + (height/2)),(pt->Z() + (depth/2))));
    points[7]->operator=(point(0,(pt->X() - (width /2)),(pt->Y() + (height/2)),(pt->Z() + (depth/2))));

    for(int i = 0; i < 8; i++)
        this->value[i] = value;
}

void GraphicPoint::draw(ColorBar* color_bar){
    glBegin(GL_QUADS);
    if (draw_black){
        //FACE 0
        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(QColor("Black"));

        //FACE 1
        glVertex3f(points[7]->X(),points[7]->Y(),points[7]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[6]->X(),points[6]->Y(),points[6]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[5]->X(),points[5]->Y(),points[5]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[4]->X(),points[4]->Y(),points[4]->Z());
        set_material_color(QColor("Black"));

        //FACE 2
        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[7]->X(),points[7]->Y(),points[7]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[6]->X(),points[6]->Y(),points[6]->Z());
        set_material_color(QColor("Black"));

        //FACE 3
        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[5]->X(),points[5]->Y(),points[5]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[6]->X(),points[6]->Y(),points[6]->Z());
        set_material_color(QColor("Black"));

        //FACE 4
        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[7]->X(),points[7]->Y(),points[7]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[4]->X(),points[4]->Y(),points[4]->Z());
        set_material_color(QColor("Black"));

        //FACE 5
        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[4]->X(),points[4]->Y(),points[4]->Z());
        set_material_color(QColor("Black"));

        glVertex3f(points[5]->X(),points[5]->Y(),points[5]->Z());
        set_material_color(QColor("Black"));

    } else if(hatching == 0){
        //FACE 0
        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(color_bar->interpolate_value(value[0]));

        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(color_bar->interpolate_value(value[1]));

        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(color_bar->interpolate_value(value[2]));

        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(color_bar->interpolate_value(value[3]));

        //FACE 1
        glVertex3f(points[7]->X(),points[7]->Y(),points[7]->Z());
        set_material_color(color_bar->interpolate_value(value[7]));

        glVertex3f(points[6]->X(),points[6]->Y(),points[6]->Z());
        set_material_color(color_bar->interpolate_value(value[6]));

        glVertex3f(points[5]->X(),points[5]->Y(),points[5]->Z());
        set_material_color(color_bar->interpolate_value(value[5]));

        glVertex3f(points[4]->X(),points[4]->Y(),points[4]->Z());
        set_material_color(color_bar->interpolate_value(value[4]));

        //FACE 2
        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(color_bar->interpolate_value(value[3]));

        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(color_bar->interpolate_value(value[2]));

        glVertex3f(points[7]->X(),points[7]->Y(),points[7]->Z());
        set_material_color(color_bar->interpolate_value(value[7]));

        glVertex3f(points[6]->X(),points[6]->Y(),points[6]->Z());
        set_material_color(color_bar->interpolate_value(value[6]));

        //FACE 3
        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(color_bar->interpolate_value(value[2]));

        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(color_bar->interpolate_value(value[1]));

        glVertex3f(points[5]->X(),points[5]->Y(),points[5]->Z());
        set_material_color(color_bar->interpolate_value(value[5]));

        glVertex3f(points[6]->X(),points[6]->Y(),points[6]->Z());
        set_material_color(color_bar->interpolate_value(value[6]));

        //FACE 4
        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(color_bar->interpolate_value(value[0]));

        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(color_bar->interpolate_value(value[3]));

        glVertex3f(points[7]->X(),points[7]->Y(),points[7]->Z());
        set_material_color(color_bar->interpolate_value(value[7]));

        glVertex3f(points[4]->X(),points[4]->Y(),points[4]->Z());
        set_material_color(color_bar->interpolate_value(value[4]));

        //FACE 5
        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(color_bar->interpolate_value(value[1]));

        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(color_bar->interpolate_value(value[0]));

        glVertex3f(points[4]->X(),points[4]->Y(),points[4]->Z());
        set_material_color(color_bar->interpolate_value(value[4]));

        glVertex3f(points[5]->X(),points[5]->Y(),points[5]->Z());
        set_material_color(color_bar->interpolate_value(value[5]));

    } else {

        //FACE 0
        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(hatching_colors[hatching-1]);

        //FACE 1
        glVertex3f(points[7]->X(),points[7]->Y(),points[7]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[6]->X(),points[6]->Y(),points[6]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[5]->X(),points[5]->Y(),points[5]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[4]->X(),points[4]->Y(),points[4]->Z());
        set_material_color(hatching_colors[hatching-1]);

        //FACE 2
        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[7]->X(),points[7]->Y(),points[7]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[6]->X(),points[6]->Y(),points[6]->Z());
        set_material_color(hatching_colors[hatching-1]);

        //FACE 3
        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[5]->X(),points[5]->Y(),points[5]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[6]->X(),points[6]->Y(),points[6]->Z());
        set_material_color(hatching_colors[hatching-1]);

        //FACE 4
        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[7]->X(),points[7]->Y(),points[7]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[4]->X(),points[4]->Y(),points[4]->Z());
        set_material_color(hatching_colors[hatching-1]);

        //FACE 5
        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[4]->X(),points[4]->Y(),points[4]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[5]->X(),points[5]->Y(),points[5]->Z());
        set_material_color(hatching_colors[hatching-1]);
    }

    glEnd();
}

vector<pair<string,string>> GraphicPoint::get_context_paremeters(){
    vector<pair<string,string>> aux;

    return aux;
}

bool GraphicPoint::set_context_paremeters(string param, string val){

    return false;
}

#include "quad.h"

Quad::Quad(point* points[4], double value[4], WiseCellType cell_type) : GraphicElement(GRAPHIC_ELEMENT_NAME,cell_type)
{
    for(int i = 0; i < 4; i++)
        this->points.push_back(NULL);

    this->points[0] = (points[0]);
    this->points[1] = (points[1]);
    this->points[2] = (points[2]);
    this->points[3] = (points[3]);

    for(int i = 0; i < 4; i++)
        this->value[i] = value[i];
}

void Quad::draw(ColorBar* color_bar){
    glBegin(GL_QUADS);

    if(hatching == 0){
        //FACE 0
        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(color_bar->interpolate_value(value[0]));

        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(color_bar->interpolate_value(value[1]));

        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(color_bar->interpolate_value(value[2]));

        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(color_bar->interpolate_value(value[3]));
    } else {
        //FACE 0
        glVertex3f(points[0]->X(),points[0]->Y(),points[0]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[1]->X(),points[1]->Y(),points[1]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[2]->X(),points[2]->Y(),points[2]->Z());
        set_material_color(hatching_colors[hatching-1]);

        glVertex3f(points[3]->X(),points[3]->Y(),points[3]->Z());
        set_material_color(hatching_colors[hatching-1]);
    }

    glEnd();
}

vector<pair<string,string>> Quad::get_context_paremeters(){
    vector<pair<string,string>> aux;

    return aux;
}

bool Quad::set_context_paremeters(string param, string val){

    return false;
}

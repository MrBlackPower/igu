#ifndef CYLINDER_H
#define CYLINDER_H

#include "model/wise/graphicelement.h"
#include "model/wise/point.h"
#include "helper/igumath.h"

#include <utility>
#include <vector>
#include <QColor>

#include <GL/gl.h>

#include "model/wise/wisestructure.h"

#define GRAPHIC_ELEMENT_NAME "CYLINDER"

using namespace std;

using namespace wise;

class Cylinder : public GraphicElement
{
public:
    Cylinder(point* start, point* end, WiseCellType cell_type, double value);
    Cylinder(point* start, point* end, WiseCellType cell_type, double radius, double value);
    Cylinder(point* start, point* end, WiseCellType cell_type, pair<double,double> radius, double value);
    Cylinder(point* start, point* end, WiseCellType cell_type, pair<double,double> radius, vector<double> value);

    void draw(ColorBar* color_bar);

    vector<pair<string,string>> get_context_paremeters();

    bool set_context_paremeters(string param, string val);

private:
    double start_radius;
    double end_radius;

    static int cylinder_stacks;
    static int cylinder_slices;
};

#endif // CYLINDER_H

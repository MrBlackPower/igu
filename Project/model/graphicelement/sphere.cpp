#include "sphere.h"

int Sphere::sphere_stacks = 10;
int Sphere::sphere_slices = 10;

Sphere::Sphere(point* center, double radius, WiseCellType cell_type, double value) : GraphicElement(GRAPHIC_ELEMENT_NAME,cell_type)
{
    points.push_back(center);

    this->radius = radius;
}

void Sphere::draw(ColorBar* color_bar){
    if(!value.empty()){
        if (hatching == 0){
            set_material_color(color_bar->interpolate_value(value.front()));
        } else {
            set_material_color(hatching_colors[hatching]);
        }
    } else {
        set_material_color(QColor("Black"));
    }
    glPushMatrix();
    point* p = points.front();
    glTranslatef(p->X()  , p->Y()  , p->Z());
    GLUquadric* q = gluNewQuadric();
    gluSphere(q,radius,sphere_slices,sphere_stacks);
    glPopMatrix();
}

vector<pair<string,string>> Sphere::get_context_paremeters(){
    stringstream aux;
    vector<pair<string,string>> r;

    aux << sphere_stacks;
    pair<string,string> cylinder_stacks("SPHERE_STACKS",aux.str());
    r.push_back(cylinder_stacks);

    aux.clear();
    aux << sphere_slices;
    pair<string,string> cylinder_slices("SPHERE_SLICES",aux.str());
    r.push_back(cylinder_slices);

    return r;
}

bool Sphere::set_context_paremeters(string param, string val){
    if(param == "SPHERE_STACKS"){
        sphere_stacks = stoi(val);
        return true;
    }
    if(param == "SPHERE_SLICES"){
        sphere_stacks = stoi(val);
        return true;
    }

    return false;
}

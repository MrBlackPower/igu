#ifndef QUAD_H
#define QUAD_H

#include "model/wise/graphicelement.h"
#include "model/wise/point.h"

#include <utility>
#include <vector>
#include <QColor>

#include <GL/gl.h>

#include "model/wise/wisestructure.h"

#define GRAPHIC_ELEMENT_NAME "QUAD"

using namespace std;

using namespace wise;

class Quad : public GraphicElement
{
public:
    Quad(point* points[4], double value[4], WiseCellType cell_type);

    void draw(ColorBar* color_bar);

    vector<pair<string,string>> get_context_paremeters();

    bool set_context_paremeters(string param, string val);
private:

};

#endif // QUAD_H

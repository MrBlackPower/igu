#ifndef SPHERE_H
#define SPHERE_H

#include "model/wise/graphicelement.h"
#include "model/wise/point.h"

#include <utility>
#include <vector>
#include <QColor>

#include <GL/gl.h>
#include <GL/freeglut.h>

#include "model/wise/wisestructure.h"

#define GRAPHIC_ELEMENT_NAME "SPHERE"

using namespace std;

using namespace wise;

class Sphere : public GraphicElement
{
public:
    Sphere(point* center, double radius, WiseCellType cell_type, double value);

    void draw(ColorBar* color_bar);

    vector<pair<string,string>> get_context_paremeters();

    bool set_context_paremeters(string param, string val);

private:
    double radius;

    static int sphere_stacks;
    static int sphere_slices;
};

#endif // SPHERE_H

#include "cylinder.h"

int Cylinder::cylinder_stacks = 100;
int Cylinder::cylinder_slices = 10;

Cylinder::Cylinder(point* start, point* end, WiseCellType cell_type, double value) : GraphicElement(GRAPHIC_ELEMENT_NAME,cell_type)
{
    start_radius = 1;
    end_radius = 1;
    
    points.push_back(start);
    points.push_back(end);

    this->value.push_back(value);
    
    this->value.push_back(value);
}
Cylinder::Cylinder(point* start, point* end, WiseCellType cell_type, double radius, double value) : GraphicElement(GRAPHIC_ELEMENT_NAME,cell_type)
{
    start_radius = radius;
    end_radius = radius;
        
    points.push_back(start);
    points.push_back(end);

    this->value.push_back(value);
    
    this->value.push_back(value);
}

Cylinder::Cylinder(point* start, point* end, WiseCellType cell_type, pair<double,double> radius, double value): GraphicElement(GRAPHIC_ELEMENT_NAME,cell_type)
{
    start_radius = radius.first;
    end_radius = radius.second;
    
    points.push_back(start);
    points.push_back(end);

    this->value.push_back(value);
    
    this->value.push_back(value);
}

Cylinder::Cylinder(point* start, point* end, WiseCellType cell_type, pair<double,double> radius, vector<double> value): GraphicElement(GRAPHIC_ELEMENT_NAME,cell_type)
{
    start_radius = radius.first;
    end_radius = radius.second;
    
    points.push_back(start);
    points.push_back(end);
    if(!value.empty()){
        for(double v : value)
            this->value.push_back(v);

    } else {
        this->value.push_back(ZERO);
    }
    this->value.operator=(value);
}

void Cylinder::draw(ColorBar* color_bar){
    glPushMatrix();
    glTranslatef(points[0]->X()  , points[0]->Y()  , points[0]->Z());
    point aux = point(1,(points[1]->X() - points[0]->X()),(points[1]->Y() - points[0]->Y()),(points[1]->Z() - points[0]->Z()));
    glRotatef(aux.get_theta(),ZERO,ZERO,ONE);
    glRotatef(aux.get_phi(),ZERO,-ONE,ZERO);
    //DRAWS THE CILYNDER ITSELF
    for(int i = 0; i <= cylinder_slices; i++){
        double theta = i * (360 / cylinder_slices);
        double next_theta = (i + 1) * (360 / cylinder_slices);
        bool odd_1 = (i % 2 == 1);
        glBegin(GL_QUAD_STRIP);
        for (int j = 0; j <= cylinder_stacks; j++){
            bool odd_2 = (j%2 == 1);

            //SETS COLOR
            if((odd_1 && odd_2) && (hatching != 0)){
                set_material_color(hatching_colors.operator[](hatching - 1));

            }else{
                double id = ((j/cylinder_stacks) * (value.size() - 1));
                double pct = id - ((int) id );
                double val = ((int)id == value.size())? value.operator[]((int)id) : value[(int)id] * (pct) + value[(int)id+1] * (1 - pct);
                set_material_color(color_bar->interpolate_value(val));
            }

            double deltaR = (end_radius - start_radius);
            double radius = ((j * 1.0 / cylinder_stacks) * deltaR) + start_radius;
            glNormal3f(ZERO, igumath::cosine(theta), igumath::sine(theta));
            glVertex3f(j * (aux.abs()/cylinder_stacks), radius * igumath::cosine(theta), radius * igumath::sine(theta));
            glNormal3f(ZERO, igumath::cosine(next_theta), igumath::sine(next_theta));
            glVertex3f(j * (aux.abs()/cylinder_stacks), radius * igumath::cosine(next_theta), radius * igumath::sine(next_theta));
        }
        glEnd();
    }
    glPopMatrix();
}

vector<pair<string,string>> Cylinder::get_context_paremeters(){
    stringstream aux;
    vector<pair<string,string>> r;

    aux << cylinder_stacks;
    pair<string,string> cylinder_stacks("CYLINDER_STACKS",aux.str());
    r.push_back(cylinder_stacks);

    aux.clear();
    aux << cylinder_slices;
    pair<string,string> cylinder_slices("CYLINDER_SLICES",aux.str());
    r.push_back(cylinder_slices);

    return r;
}

bool Cylinder::set_context_paremeters(string param, string val){
    if(param == "CYLINDER_STACKS"){
        cylinder_stacks = stoi(val);
        return true;
    }
    if(param == "CYLINDER_SLICES"){
        cylinder_stacks = stoi(val);
        return true;
    }

    return false;
}

#ifndef GRAPHICPOINT_H
#define GRAPHICPOINT_H

#include <GL/gl.h>

#include "model/wise/point.h"
#include "model/wise/graphicelement.h"

#include "model/wise/wisestructure.h"

#include <QColor>

using namespace std;

using namespace wise;

#define GRAPHIC_ELEMENT_NAME "POINT"

class GraphicPoint : public GraphicElement
{
public:
    GraphicPoint(point* pt, double value, WiseCellType cell_type, bool draw_black = false);
    GraphicPoint(point* pt, double value, double size, WiseCellType cell_type, bool draw_black = false);

    void draw(ColorBar* color_bar);

    vector<pair<string,string>> get_context_paremeters();

    bool set_context_paremeters(string param, string val);

private:

    static double point_height;
    static double point_width;
    static double point_depth;

    bool draw_black;
};

#endif // GRAPHICPOINT_H

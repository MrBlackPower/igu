#ifndef LINE_H
#define LINE_H

#include "model/wise/graphicelement.h"
#include "helper/igumath.h"
#include "model/wise/point.h"

#include <utility>
#include <vector>
#include <QColor>

#include <GL/gl.h>

#include "model/wise/wisestructure.h"

#define GRAPHIC_ELEMENT_NAME "LINE"

using namespace std;

using namespace wise;

class Line : public GraphicElement
{
public:
    Line(point* start, point* end, double value, WiseCellType cell_type, bool draw_black = false);

    Line(point* start, point* end, vector<double> value, WiseCellType cell_type, bool draw_black = false);

    void draw(ColorBar* color_bar);

    vector<pair<string,string>> get_context_paremeters();

    bool set_context_paremeters(string param, string val);
private:
    static double line_width;

    bool draw_black;
};

#endif // LINE_H

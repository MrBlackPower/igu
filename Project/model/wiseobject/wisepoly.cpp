#include "wisepoly.h"

WisePoly::WisePoly(string name, string dna) : WiseElement(name,DEFAULT_POLY_TYPE)
{

}

WisePoly::~WisePoly(){

}

bool WisePoly::set_grid(point a, point b, double dx, double dy, double dz){
    if(a.X() > b.X()){
        double aux = a.X();
        b.X(a.X());
        a.X(aux);
    }
    if(a.Y() > b.Y()){
        double aux = a.Y();
        b.Y(a.Y());
        a.Y(aux);
    }
    if(a.Z() > b.Z()){
        double aux = a.Z();
        b.Z(a.Z());
        a.Z(aux);
    }

    if(dx <= 0)
        return false;

    this->a = a.X();
    this->b = b.X();
    c = a.Y();
    d = b.Y();
    e = a.Z();
    f = b.Z();

    this->dx = dx;

    if(dy <= 0)
        this->dy = dx;
    else
        this->dy = dy;

    if(dz <= 0)
        this->dz = dx;
    else
        this->dz = dz;

    n = (((b.X()-a.X())/dx) + 1);
    m = (((b.Y()-a.Y())/dy) + 1);
    l = (((b.Z()-a.Z())/dz) + 1);
    N =(n * m * l);

    for(int i = 0; i < n; i++){
        grid.emplace_back(vector<vector<point>>());
        value.emplace_back(vector<vector<double>>());
        for(int j = 0; j < m; j++){
            grid.operator[](i).emplace_back(vector<point>());
            value.operator[](i).emplace_back(vector<double>());
            for(int k = 0; k < l; k++){
                point p(((i*(n))+j)*m+k,i*dx,j*dy,k*dz);
                grid.operator[](i).operator[](j).push_back(p);
                value.operator[](i).operator[](j).push_back(0);
            }
        }
    }

    return true;
}

bool WisePoly::set_grid(point a, point b, int n, int m, int l){
    if(a.X() > b.X()){
        double aux = a.X();
        b.X(a.X());
        a.X(aux);
    }
    if(a.Y() > b.Y()){
        double aux = a.Y();
        b.Y(a.Y());
        a.Y(aux);
    }
    if(a.Z() > b.Z()){
        double aux = a.Z();
        b.Z(a.Z());
        a.Z(aux);
    }

    if(n <= 0)
        return false;

    if(m <= 0){
        m = n;
        this->m = n;
    } else
        this->m = m;

    if(l <= 0){
        l = n;
        this->l = n;
    }else
        this->l = l;

    this->n = n;

    this->a = a.X();
    this->b = b.X();
    c = a.Y();
    d = b.Y();
    e = a.Z();
    f = b.Z();

    N =(this->n * this->m * this->l);

    dx = (this->b-this->a)/(n-1);
    dy = (this->d-this->c)/(m-1);
    dz = (this->f-this->e)/(l-1);

    for(int i = 0; i < n; i++){
        grid.emplace_back(vector<vector<point>>());
        value.emplace_back(vector<vector<double>>());
        for(int j = 0; j < m; j++){
            grid.operator[](i).emplace_back(vector<point>());
            value.operator[](i).emplace_back(vector<double>());
            for(int k = 0; k < l; k++){
                point p((i*(n))+j,i*dx,j*dy,k*dz);
                grid.operator[](i).operator[](j).push_back(p);
                value.operator[](i).operator[](j).push_back(0);
            }
        }
    }
}

bool WisePoly::set_point(int x, int y ,int z, double value){
    if(x >= grid.size() or x < 0)
        return false;

    if(y >= grid.operator[](x).size() or y < 0)
        return false;

    if(z >= grid.operator[](x).operator[](y).size() or z < 0)
        return false;

    this->value.operator[](x).operator[](y).operator[](z) = value;

    return true;
}

bool WisePoly::set_point(int id, double value){
    tuple<int,int,int> a = get_point_id(id);

    return set_point(get<0>(a),get<1>(a),get<2>(a),value);
}

int WisePoly::get_point_id(int x, int y, int z){
    return ((x * n) + y) * m + z;
}

tuple<int,int,int> WisePoly::get_point_id(int id){
    if(id < 0 or id >= N)
        return tuple<int,int,int>{-1,-1,-1};

    int x = id % (n);
    int y = (id / (n * m)) % m;
    int z = (id / (n * m * l)) % l;

    return tuple<int,int,int>{x,y,z};
}

vector<vector<vector<double>>>* WisePoly::get_vector(){
    return &value;
}

vector<vector<vector<point>>>* WisePoly::get_grid(){
    return &grid;
}

double WisePoly::get_dx(){
    return dx;
}

double WisePoly::get_dy(){
    return dy;
}

double WisePoly::get_dz(){
    return dz;
}

int WisePoly::get_m(){
    return m;
}

int WisePoly::get_n(){
    return n;
}

int WisePoly::get_l(){
    return l;
}

void WisePoly::update_structure(){
    clear();

    add_point_info("VALUE");

    for (int i = 0; i < grid.size() && (grid.size() == value.size()); i++) {
        for (int j = 0; j < grid[i].size() && (grid[i].size() == value[i].size()); j++) {
            for (int k = 0; k < grid[i][j].size() && (grid[i][j].size() == value[i][j].size()); k++) {
                add_point(point{0,grid[i][j][k].X(),grid[i][j][k].Y(),grid[i][j][k].Z()});
            }
        }
    }

    for(vector<vector<double>> v1 : value){
        for(vector<double> v2 : v1){
            for(double d : v2){
                add_point_data(QString::asprintf("%.10f",d).toStdString(),"VALUE");
            }
        }
    }

    get_params()->status = wise::HOT;

    update_xml();
}

void WisePoly::delete_abstract_data(){
    grid.clear();
    value.clear();

    a = 0;
    b = 0;
    c = 0;
    d = 0;
    e = 0;
    f = 0;
    dx = 0;
    dy = 0;
    dz = 0;
    N = 0;
    n = 0;
    m = 0;
    l = 0;
}

unsigned int WisePoly::size(){
    return N;
}

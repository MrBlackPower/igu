#ifndef WISEARTERYTREE_H
#define WISEARTERYTREE_H


#include "model/wise/wiseelement.h"
#include "model/wiseobject/wiseartery.h"
#include <vector>
#include <iostream>


#define DEFAULT_TYPE_NAME "ARTERY_TREE"

#define ALMOST_ZERO 0.0001

using namespace std;

using namespace wise;

class WiseArteryTree : public WiseElement
{
public:
    friend class WiseArteryTreeFactory;

    struct WiseArteryTreeParameters{
        point start = 0;
        int n = 0;
    };

    WiseArteryTree(string name = ("wise_artery_tree_"), string dna = "NONE");

    ~WiseArteryTree();

    int get_n();

    point* get_start();

    WiseArtery* get_root();

    void set_start_point(point p);

    bool add_node(point a, point b, double radius, double young_modulus, double density, double viscosity);

    bool add_node(point a, WiseArtery::WiseArteryParams params);

    /*******************************************************************************************************************************************
     * VIRTUAL METHODS
     *******************************************************************************************************************************************/
    void update_structure();

    void delete_abstract_data();

protected:

    void update_structure_point(WiseArtery* ar, int pts = 0);

    WiseArtery* get_artery(point p, double radius = ALMOST_ZERO);

private:
    WiseArteryTreeParameters params;

    WiseArtery* root;

    bool done_phase_one;
};

#endif // WISEARTERYTREE_H

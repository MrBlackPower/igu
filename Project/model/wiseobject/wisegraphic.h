#ifndef WISEGRAPHIC_H
#define WISEGRAPHIC_H

#include "model/wise/wiseelement.h"
#include <vector>

#define DEFAULT_GRAPHIC_TYPE "GRAPHIC"

using namespace std;

class WiseGraphic : public WiseElement
{
public:
    WiseGraphic(string name, string dna = "NONE");
    ~WiseGraphic();

    bool set_grid(double a, double b, double dx);

    bool set_grid(double a, double b, int n);

    bool set_point(int x, double val);

    vector<double>* get_vector();

    vector<point>* get_grid();

    pair<double,double> limits();

    unsigned int size();

    /*******************************************************************************************************************************************
     * VIRTUAL METHODS
     *******************************************************************************************************************************************/

    void update_structure();

    void delete_abstract_data();

protected:

private:
    int n;

    //X - limits
    double a;
    double b;

    double dx;

    vector<point> grid;

    vector<double> value;
};

#endif // WISEGRAPHIC_H

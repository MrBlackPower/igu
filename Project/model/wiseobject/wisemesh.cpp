#include "wisemesh.h"

WiseMesh::WiseMesh(string name, string dna) : WiseElement(name,DEFAULT_MESH_TYPE)
{

}

WiseMesh::~WiseMesh(){

}

bool WiseMesh::set_grid(pair<double,double> a, pair<double,double> b, double dx, double dy){
    if(a.first > b.first){
        double aux = a.first;
        b.first = a.first;
        a.first = aux;
    }
    if(a.second > b.second){
        double aux = a.second;
        b.second = a.second;
        a.second = aux;
    }

    if(dx <= 0)
        return false;

    this->a = a.first;
    this->b = b.first;
    c = a.second;
    d = b.second;

    this->dx = dx;

    if(dy <= 0)
        this->dy = dx;
    else
        this->dy = dy;

    n = (((b.first-a.first)/dx) + 1);
    m = (((b.second-a.second)/dy) + 1);
    N = (n * m);

    for(int i = 0; i < n; i++){
        grid.emplace_back(vector<point>());
        value.emplace_back(vector<double>());
        for(int j = 0; j < m; j++){
            point p((i*(n))+j,i*dx,j*dy);
            grid.operator[](i).push_back(p);
            value.operator[](j).push_back(0);
        }
    }
}

bool WiseMesh::set_grid(pair<double,double> a, pair<double,double> b, int n, int m){
    if(a.first > b.first){
        double aux = a.first;
        b.first = a.first;
        a.first = aux;
    }
    if(a.second > b.second){
        double aux = a.second;
        b.second = a.second;
        a.second = aux;
    }

    if(n <= 0)
        return false;

    if(m <= 0){
        this->m = n;
        m = this->m;
    } else {
        this->m = m;
    }

    this->n = n;

    this->a = a.first;
    this->b = b.first;
    c = a.second;
    d = b.second;

    N = (this->n * this->m);

    dx = (this->b-this->a)/(n-1);
    dy = (this->d-this->c)/(m-1);

    for(int i = 0; i < n; i++){
        grid.emplace_back(vector<point>());
        value.emplace_back(vector<double>());
        for(int j = 0; j < m; j++){
            point p((i*(n))+j,i*dx,j*dy);
            grid.operator[](i).push_back(p);
            value.operator[](i).push_back(0);
        }
    }

    return true;
}

bool WiseMesh::set_point(int x, int y, double value){
    if(x >= grid.size() or x < 0)
        return false;

    if(y >= grid.operator[](x).size() or y < 0)
        return false;

    this->value.operator[](x).operator[](y) = value;

    return true;
}

bool WiseMesh::set_point(int id, double value){
    pair<int,int> a = get_point_id(id);

    return set_point(a.first,a.second,value);
}

int WiseMesh::get_point_id(int x, int y){
    return x * n + y;
}

pair<int,int> WiseMesh::get_point_id(int id){
    if(id < 0 or id >= N)
        return {-1,-1};

    int x = id % n;
    int y = id / n;

    return {x,y};
}

bool WiseMesh::set_vector(vector<vector<double>> data){
    if(data.size() != n)
        return false;

    if(data.front().size() != m)
        return false;

    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++)
            value[i][j] = data[i][j];

    return true;
}

vector<vector<double>>* WiseMesh::get_vector(){
    return &value;
}

vector<vector<point>>* WiseMesh::get_grid(){
    return &grid;
}

unsigned int WiseMesh::size(){
    return N;
}

double WiseMesh::dX(){
    return dx;
}

double WiseMesh::dY(){
    return dy;
}

int WiseMesh::get_m(){
    return m;
}

int WiseMesh::get_n(){
    return n;
}

pair<double,double> WiseMesh::limits_x(){
    return {a,b};
}

pair<double,double> WiseMesh::limits_y(){
    return {c,d};
}

void WiseMesh::update_structure(){
    clear();

    add_point_info("VALUE");

    for (int i = 0; i < grid.size() && (grid.size() == value.size()); i++) {
        for(int j = 0; j < grid[i].size() && (grid[i].size() == value[i].size()); j++){
            add_point(point{0,grid[i][j].X(),grid[i][j].Y(),value[i][j]});
        }
    }

    for(vector<double> v : value){
        for(double d : v){
            add_point_data(QString::asprintf("%.10f",d).toStdString(),"VALUE");
        }
    }

    get_params()->status = wise::HOT;

    update_xml();
}

void WiseMesh::delete_abstract_data(){
    grid.clear();
    value.clear();

    a = 0;
    b = 0;
    c = 0;
    d = 0;
    dx = 0;
    dy = 0;
    N = 0;
    n = 0;
    m = 0;
}

#ifndef WISEARTERY_H
#define WISEARTERY_H

#include <complex>
#include <vector>

#include "model/wise/point.h"
#include "helper/igumath.h"

#define PI 3.14159265359
#define WAVE_PLOT_SIZE 100

using namespace std;

class WiseArtery
{
public:
    struct WiseArteryParams{
        point p;

        vector<double> flow_wave;
        vector<double> pressure_wave;

        //COMPLEX VALUES
        complex<double> beta          = -1;
        complex<double> cv            = -1;
        complex<double> Ec            = -1;
        complex<double> flow          = -1;
        complex<double> pressure      = -1;
        complex<double> _pressure     = -1;
        complex<double> R             = -1;
        complex<double> viscousFactor = -1;
        complex<double> _Y            = -1;
        complex<double> Ye            = -1;

        //ABSOLUTE VALUES
        double beta_abs               = -1;
        double cv_abs                 = -1;
        double Ec_abs                 = -1;
        double flow_abs               = -1;
        double pressure_abs           = -1;
        double _pressure_abs          = -1;
        double R_abs                  = -1;
        double viscousFactor_abs      = -1;
        double _Y_abs                 = -1;
        double Ye_abs                 = -1;

        //DOUBLE VALUES
        double          alpha         = -1;
        double          c             = -1;
        double          density       = -1;
        double          E             = -1;
        double          h             = -1;
        double          length        = -1;
        double          phi           = -1;
        double          r             = -1;
        double          viscosity     = -1;
        double          w             = -1;

        int             id            =  0;
    };

    WiseArtery(int id, point* p, double length, double youngModulus, double density, double viscosity,double radius);
    WiseArtery(int id, WiseArteryParams params);
    ~WiseArtery();

    WiseArtery* find_artery(point p, double radius);

    bool has_left();
    bool has_right();
    bool has_father();

    WiseArteryParams params;

    complex<double> get_pressure(double x);

    complex<double> get_flow(double x, complex<double> root_admittance);

    WiseArtery* father;

    WiseArtery* left;

    WiseArtery* right;

private:
};

#endif // WISEARTERY_H

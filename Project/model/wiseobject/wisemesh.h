#ifndef WISEMESH_H
#define WISEMESH_H

#include "model/wise/wiseelement.h"
#include <vector>

#define DEFAULT_MESH_TYPE "MESH"

using namespace std;

class WiseMesh : public WiseElement
{
public:
    WiseMesh(string name, string dna = "NONE");
    ~WiseMesh();

    bool set_grid(pair<double,double> a, pair<double,double> b, double dx, double dy = -1);

    bool set_grid(pair<double,double> a, pair<double,double> b, int n, int m = -1);

    bool set_point(int x, int y, double value);

    bool set_point(int id, double value);

    int get_point_id(int x, int y);

    pair<int,int> get_point_id(int id);

    bool set_vector(vector<vector<double>> data);

    vector<vector<double>>* get_vector();

    vector<vector<point>>* get_grid();

    unsigned int size();

    double dX();

    double dY();

    int get_m();

    int get_n();

    pair<double,double> limits_x();

    pair<double,double> limits_y();

    /*******************************************************************************************************************************************
     * VIRTUAL METHODS
     *******************************************************************************************************************************************/

    void update_structure();

    void delete_abstract_data();

protected:


private:
    int N;

    int n;
    int m;

    //X - limits
    double a;
    double b;

    double dx;

    //Y - limits
    double c;
    double d;

    double dy;

    vector<vector<point>> grid;

    vector<vector<double>> value;

};

#endif // WISEMESH_H

#ifndef WISEPOLY_H
#define WISEPOLY_H

#include "model/wise/wiseelement.h"
#include <vector>

#define DEFAULT_POLY_TYPE "POLY"

using namespace std;

class WisePoly : public WiseElement
{
public:
    WisePoly(string name, string dna = "NONE");
    ~WisePoly();

    bool set_grid(point a, point b, double dx, double dy = -1, double dz = -1);

    bool set_grid(point a, point b, int n, int m = 0, int l = 0);

    bool set_point(int x, int y, int z, double val);

    bool set_point(int id, double value);

    int get_point_id(int x, int y, int z);

    tuple<int,int,int> get_point_id(int id);

    vector<vector<vector<double>>>* get_vector();

    vector<vector<vector<point>>>* get_grid();

    double get_dx();

    double get_dy();

    double get_dz();

    int get_m();

    int get_n();

    int get_l();

    unsigned int size();

    /*******************************************************************************************************************************************
     * VIRTUAL METHODS
     *******************************************************************************************************************************************/

    void update_structure();

    void delete_abstract_data();

protected:


private:
    int N;

    int n;
    int m;
    int l;

    //X - limits
    double a;
    double b;

    double dx;

    //Y - limits
    double c;
    double d;

    double dy;


    //Z - limits
    double e;
    double f;

    double dz;

    vector<vector<vector<point>>> grid;

    vector<vector<vector<double>>> value;
};

#endif // WISEPOLY_H

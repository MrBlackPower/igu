#include "wisegraphic.h"

WiseGraphic::WiseGraphic(string name, string dna) : WiseElement(name,DEFAULT_GRAPHIC_TYPE)
{

}

WiseGraphic::~WiseGraphic(){

}

bool WiseGraphic::set_grid(double a, double b, double dx){
    if(a > b){
        double aux = a;
        b = a;
        a = aux;
    }

    if(dx <= 0)
        return false;

    this->a = a;
    this->b = b;

    this->dx = dx;
    n = ((b-a)/dx) + 1;

    for(int i = 0; i < n; i++){
        grid.emplace_back(point{i,a+(i*dx),0});
        value.emplace_back(0);
    }

    return true;
}

bool WiseGraphic::set_grid(double a, double b, int n){
    if(a > b){
        double aux = a;
        b = a;
        a = aux;
    }

    this->n = n+1;

    this->a = a;
    this->b = b;
    dx = (b-a)/n;

    for(int i = 0; i < this->n; i++){
        grid.emplace_back(point{i,a+(i*dx),0});
        value.emplace_back(0);
    }

    return true;
}

bool WiseGraphic::set_point(int x, double val){
    if(x >= grid.size() or x < 0)
        return false;

    value.operator[](x) = val;

    return true;
}

vector<double>* WiseGraphic::get_vector(){
    return &value;
}

vector<point>* WiseGraphic::get_grid(){
    return &grid;
}

pair<double,double> WiseGraphic::limits(){
    return {a,b};
}

unsigned int WiseGraphic::size(){
    return n;
}

void WiseGraphic::update_structure(){
    clear();

    for(int i = 0; i < grid.size() && (grid.size() == value.size()); i++){
        add_point(point{0,grid[i].X(),value[i]});
    }

    add_point_info("VALUE");

    for(double d : value){
        add_point_data(QString::asprintf("%.10f",d).toStdString(),"VALUE");
    }

    get_params()->status = wise::HOT;

    update_xml();
}

void WiseGraphic::delete_abstract_data(){
    grid.clear();
    value.clear();

    a = 0;
    b = 0;
    n = 0;
    dx = 0;
}

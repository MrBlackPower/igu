#include "wisearterytree.h"

WiseArteryTree::WiseArteryTree(string name, string dna) : WiseElement(name,DEFAULT_TYPE_NAME), done_phase_one(false)
{
    root = NULL;
}

WiseArteryTree::~WiseArteryTree(){
    if(root)
        delete root;
}

int WiseArteryTree::get_n(){
    return params.n;
}

point* WiseArteryTree::get_start(){
    return &params.start;
}

WiseArtery* WiseArteryTree::get_root(){
    return root;
}

void WiseArteryTree::set_start_point(point p){
    params.start = p;
}

WiseArtery* WiseArteryTree::get_artery(point p, double radius){
    if(params.n == 0)
        return  NULL;

    if(igumath::euclidean_distance(&params.start,&p) <= radius)
        return root;

    return root->find_artery(p,radius);
}


bool WiseArteryTree::add_node(point a, point b, double radius, double young_modulus, double density, double viscosity){
    WiseArtery* ar;

    if(params.n != 0){
        WiseArtery* f = get_artery(a);

        if(!f)
            return false;

        if(!f->has_left()){
            ar = new WiseArtery (params.n,&b,igumath::euclidean_distance(&a,&b),young_modulus,density,viscosity,radius);

            f->left = (ar);
            ar->father = (f);
        } else if(!f->has_right()){
            ar = new WiseArtery (params.n,&b,igumath::euclidean_distance(&a,&b),young_modulus,density,viscosity,radius);

            f->right = (ar);
            ar->father = (f);
        } else {
            return false;
        }

    } else {
        set_start_point(a);

        ar = new WiseArtery (params.n,&b,igumath::euclidean_distance(&a,&b),young_modulus,density,viscosity,radius);
        root = ar;
    }

    if(!ar)
        return false;

    params.n++;

    return true;
}

bool WiseArteryTree::add_node(point a, WiseArtery::WiseArteryParams params){
    WiseArtery* ar;

    if(this->params.n != 0){
        WiseArtery* f = get_artery(a);

        if(!f)
            return false;

        if(!f->has_left()){
            ar = new WiseArtery (this->params.n,params);

            f->left = (ar);
            ar->father = (f);
        } else if(!f->has_right()){
            ar = new WiseArtery (this->params.n,params);

            f->right = (ar);
            ar->father = (f);
        } else {
            return false;
        }

    } else {
        set_start_point(a);

        ar = new WiseArtery (this->params.n,params);
        root = ar;
    }

    if(!ar)
        return false;

    this->params.n++;

    return true;
}

void WiseArteryTree::update_structure(){
    clear();

    //START POINT
    if(root != NULL){
        add_point(params.start);
    }

    //ENTRY DATA
    add_cell_info("RADIUS");
    add_cell_info("DENSITY");
    add_cell_info("YOUNG_MODULUS");
    add_cell_info("VISCOSITY");

    //OUT DATA
    add_cell_info("ALPHA");
    add_cell_info("WAVE_SPEED");
    add_cell_info("WALL_THICKNESS");
    add_cell_info("LENGTH");
    add_cell_info("PHI");
    add_cell_info("ANGULAR_FREQUENCY");

    //COMPLEX OUT DATA
    add_cell_info("BETA_ABS");
    add_cell_info("BETA_REAL");
    add_cell_info("BETA_IMAGE");

    add_cell_info("VISCOUS_WAVE_SPEED_ABS");
    add_cell_info("VISCOUS_WAVE_SPEED_REAL");
    add_cell_info("VISCOUS_WAVE_SPEED_IMAGE");

    add_cell_info("VISCOUS_YOUNG_MODULUS_ABS");
    add_cell_info("VISCOUS_YOUNG_MODULUS_REAL");
    add_cell_info("VISCOUS_YOUNG_MODULUS_IMAGE");

    add_cell_info("FLOW_ABS");
    add_cell_info("FLOW_REAL");
    add_cell_info("FLOW_IMAGE");

    add_cell_info("PRESSURE_ABS");
    add_cell_info("PRESSURE_REAL");
    add_cell_info("PRESSURE_IMAGE");

    add_cell_info("MEDIUM_PRESSURE_ABS");
    add_cell_info("MEDIUM_PRESSURE_REAL");
    add_cell_info("MEDIUM_PRESSURE_IMAGE");

    add_cell_info("REFLECTION_COEFFICIENT_ABS");
    add_cell_info("REFLECTION_COEFFICIENT_REAL");
    add_cell_info("REFLECTION_COEFFICIENT_IMAGE");

    add_cell_info("ADMITTANCE_ABS");
    add_cell_info("ADMITTANCE_REAL");
    add_cell_info("ADMITTANCE_IMAGE");

    add_cell_info("EFFECTIVE_ADMITTANCE_ABS");
    add_cell_info("EFFECTIVE_ADMITTANCE_REAL");
    add_cell_info("EFFECTIVE_ADMITTANCE_IMAGE");


    add_field("FLOW_WAVE",WISE,VECTOR);
    add_field("PRESSURE_WAVE",WISE,VECTOR);

    update_structure_point(root);

    update_xml();

    WiseElement::params.status = HOT;
}

void WiseArteryTree::delete_abstract_data(){
    delete root;

    params.n = 0;
    params.start = {0,0,0,0};
    root = NULL;
}

void WiseArteryTree::update_structure_point(WiseArtery* ar, int parent){
    if(ar == NULL)
        return;

    add_point(ar->params.p);

    int this_id = get_points_size() - 1;

    add_line(parent,this_id);

    //DATA
    add_cell_data(QString::asprintf("%.10f",ar->params.r                ).toStdString(),"RADIUS");
    add_cell_data(QString::asprintf("%.10f",ar->params.density          ).toStdString(),"DENSITY");
    add_cell_data(QString::asprintf("%.10f",ar->params.E                ).toStdString(),"YOUNG_MODULUS");
    add_cell_data(QString::asprintf("%.10f",ar->params.viscosity        ).toStdString(),"VISCOSITY");

    //OUT DATA
    add_cell_data(QString::asprintf("%.10f",ar->params.alpha            ).toStdString(),"ALPHA");
    add_cell_data(QString::asprintf("%.10f",ar->params.c                ).toStdString(),"WAVE_SPEED");
    add_cell_data(QString::asprintf("%.10f",ar->params.h                ).toStdString(),"WALL_THICKNESS");
    add_cell_data(QString::asprintf("%.10f",ar->params.length           ).toStdString(),"LENGTH");
    add_cell_data(QString::asprintf("%.10f",ar->params.phi              ).toStdString(),"PHI");
    add_cell_data(QString::asprintf("%.10f",ar->params.w                ).toStdString(),"ANGULAR_FREQUENCY");

    //COMPLEX DATA OUT
    add_cell_data(QString::asprintf("%.10f",sqrt(pow(ar->params.beta.real(),2)+pow(ar->params.beta.imag(),2))        ).toStdString(),"BETA_ABS");
    add_cell_data(QString::asprintf("%.10f",ar->params.beta.real()                                                   ).toStdString(),"BETA_REAL");
    add_cell_data(QString::asprintf("%.10f",ar->params.beta.imag()                                                   ).toStdString(),"BETA_IMAGE");

    add_cell_data(QString::asprintf("%.10f",sqrt(pow(ar->params.cv.real(),2)+pow(ar->params.cv.imag(),2))        ).toStdString(),"VISCOUS_WAVE_SPEED_ABS");
    add_cell_data(QString::asprintf("%.10f",ar->params.cv.real()                                                   ).toStdString(),"VISCOUS_WAVE_SPEED_REAL");
    add_cell_data(QString::asprintf("%.10f",ar->params.cv.imag()                                                   ).toStdString(),"VISCOUS_WAVE_SPEED_IMAGE");

    add_cell_data(QString::asprintf("%.10f",sqrt(pow(ar->params.Ec.real(),2)+pow(ar->params.Ec.imag(),2))        ).toStdString(),"VISCOUS_YOUNG_MODULUS_ABS");
    add_cell_data(QString::asprintf("%.10f",ar->params.Ec.real()                                                   ).toStdString(),"VISCOUS_YOUNG_MODULUS_REAL");
    add_cell_data(QString::asprintf("%.10f",ar->params.Ec.imag()                                                   ).toStdString(),"VISCOUS_YOUNG_MODULUS_IMAGE");

    add_cell_data(QString::asprintf("%.10f",sqrt(pow(ar->params.flow.real(),2)+pow(ar->params.flow.imag(),2))        ).toStdString(),"FLOW_ABS");
    add_cell_data(QString::asprintf("%.10f",ar->params.flow.real()                                                   ).toStdString(),"FLOW_REAL");
    add_cell_data(QString::asprintf("%.10f",ar->params.flow.imag()                                                   ).toStdString(),"FLOW_IMAGE");

    add_cell_data(QString::asprintf("%.10f",sqrt(pow(ar->params.pressure.real(),2)+pow(ar->params.pressure.imag(),2))        ).toStdString(),"PRESSURE_ABS");
    add_cell_data(QString::asprintf("%.10f",ar->params.pressure.real()                                                   ).toStdString(),"PRESSURE_REAL");
    add_cell_data(QString::asprintf("%.10f",ar->params.pressure.imag()                                                   ).toStdString(),"PRESSURE_IMAGE");

    add_cell_data(QString::asprintf("%.10f",sqrt(pow(ar->params._pressure.real(),2)+pow(ar->params._pressure.imag(),2))        ).toStdString(),"MEDIUM_PRESSURE_ABS");
    add_cell_data(QString::asprintf("%.10f",ar->params._pressure.real()                                                   ).toStdString(),"MEDIUM_PRESSURE_REAL");
    add_cell_data(QString::asprintf("%.10f",ar->params._pressure.imag()                                                   ).toStdString(),"MEDIUM_PRESSURE_IMAGE");

    add_cell_data(QString::asprintf("%.10f",sqrt(pow(ar->params.R.real(),2)+pow(ar->params.R.imag(),2))        ).toStdString(),"REFLECTION_COEFFICIENT_ABS");
    add_cell_data(QString::asprintf("%.10f",ar->params.R.real()                                                   ).toStdString(),"REFLECTION_COEFFICIENT_REAL");
    add_cell_data(QString::asprintf("%.10f",ar->params.R.imag()                                                   ).toStdString(),"REFLECTION_COEFFICIENT_IMAGE");

    add_cell_data(QString::asprintf("%.10f",sqrt(pow(ar->params._Y.real(),2)+pow(ar->params._Y.imag(),2))        ).toStdString(),"ADMITTANCE_ABS");
    add_cell_data(QString::asprintf("%.10f",ar->params._Y.real()                                                   ).toStdString(),"ADMITTANCE_REAL");
    add_cell_data(QString::asprintf("%.10f",ar->params._Y.imag()                                                   ).toStdString(),"ADMITTANCE_IMAGE");

    add_cell_data(QString::asprintf("%.10f",sqrt(pow(ar->params.Ye.real(),2)+pow(ar->params.Ye.imag(),2))        ).toStdString(),"EFFECTIVE_ADMITTANCE_ABS");
    add_cell_data(QString::asprintf("%.10f",ar->params.Ye.real()                                                   ).toStdString(),"EFFECTIVE_ADMITTANCE_REAL");
    add_cell_data(QString::asprintf("%.10f",ar->params.Ye.imag()                                                   ).toStdString(),"EFFECTIVE_ADMITTANCE_IMAGE");

    if(ar->has_left())
        update_structure_point(ar->left,this_id);

    if(ar->has_right())
        update_structure_point(ar->right,this_id);
}

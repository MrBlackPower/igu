#include "wiseartery.h"

WiseArtery::WiseArtery(int id, point* p, double length, double youngModulus, double density, double viscosity,double radius)
{
    params.alpha = -1;
    father = NULL;
    left = NULL;
    right = NULL;
    params.id = id;
    params.length = length;
    params.phi = 0.0;
    params.r = radius;
    params.E = youngModulus;
    params.viscosity = viscosity;
    params.density = density;
    params.w = 0.0;
    params.p = p;
    params.h = params.r * 0.1;

}

WiseArtery::WiseArtery(int id, WiseArteryParams params){
    this->params = params;
    this->params.id = id;
    father = NULL;
    left = NULL;
    right = NULL;
    this->params.h = params.r * 0.1;
}

WiseArtery::~WiseArtery(){
    if(left)
        delete left;

    if(right)
        delete right;
}

WiseArtery* WiseArtery::find_artery(point p, double radius){
    if(igumath::euclidean_distance(&params.p,&p) <= radius)
        return this;

    WiseArtery* r = NULL;

    if(has_left())
        r = left->find_artery(p,radius);

    if(!r && has_right())
        r = right->find_artery(p,radius);

    return r;
}

bool WiseArtery::has_left(){
    if(left)
        return true;

    return false;
}

bool WiseArtery::has_right(){
    if(right)
        return true;

    return false;
}

bool WiseArtery::has_father(){
    if(father)
        return true;

    return false;
}

complex<double> WiseArtery::get_pressure(double x){
    if(x < 0 || x > 1)
        return -1;

    if(params._pressure == complex<double> (-1.0,0.0))
        return -1;

    if(params.beta == complex<double> (-1.0 , 0.0))
        return -1;

    complex<double> aux1 (imag(params.beta) * x , real(-params.beta) * x);
    complex<double> aux2 (imag(-params.beta) * x , real(params.beta) * x);
    complex<double> aux3 (imag(params.beta) * 2 , real(-params.beta) * 2);

    return params._pressure * (exp(aux1) + params.R * exp(aux2) * exp(aux3));
}

complex<double> WiseArtery::get_flow(double x, complex<double> root_admittance){
    if(x < 0 || x > 1)
        return -1;

    if(params._pressure == complex<double>(-1.0,0.0))
        return -1;

    if(params.beta == complex<double> (-1.0 , 0.0))
        return -1;

    if(params.Ye == complex<double>(-1.0,0.0))
        return -1;

    complex<double> M = params.Ye / root_admittance;
    complex<double> aux1 (imag(params.beta) * x , real(-params.beta) * x);
    complex<double> aux2 (imag(-params.beta) * x , real(params.beta) * x);
    complex<double> aux3 (imag(params.beta) * 2 , real(-params.beta) * 2);

    return M * params._pressure * exp(aux1) - params.R * exp(aux2) * exp(aux3);
}

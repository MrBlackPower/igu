#ifndef NODE_H
#define NODE_H

#include "../smartobject.h"
#include <QColor>

template <class n_type>
class Node : public SmartObject
{
public:
    Node(int id, QString name, n_type data, double x, double y, double z = 0.0, SmartObject* parent = NULL);

    void setPoint(point p);
    point getPoint();

    int getId();

    //SMART OBJECT METHODS
    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();

    bool equals(Node<n_type>* n);
    bool operator==(Node<n_type>* n);

    n_type buffer;

    QColor c;

private:
    point p;
    int id;
};

#endif // NODE_H

#include "node.h"

template <class n_type>
Node<n_type>::Node(int id, QString name, n_type dat, double x, double y, double z, SmartObject* parent) : SmartObject(name, parent)
{
    this->id = id;
    p.X(x);
    p.Y(y);
    p.Z(z);
    buffer = dat;
    c = QColor("Black");
}

template <class n_type>
void Node<n_type>::setPoint(point p){
    this->p = p;
}

template <class n_type>
point Node<n_type>::getPoint(){
    return p;
}

template <class n_type>
int Node<n_type>::getId(){
    return id;
}

//SMART OBJECT METHODS
template <class n_type>
vector<QString> Node<n_type>::print(){
    vector<QString> txt;
    QString line;

    line = line.asprintf("NODE [%s/%d]", getName().toLatin1().data(),id);
    txt.push_back(line);

    return txt;
}

template <class n_type>
vector<QString> Node<n_type>::data(){
    return print();
}

template <class n_type>
vector<QString> Node<n_type>::raw(){
    return print();
}

template <class n_type>
bool Node<n_type>::equals(Node<n_type>* n){
    if(n->getId() == id)
        if(n->getPoint() == p)
            return true;

    return false;
}

template <class n_type>
bool Node<n_type>::operator==(Node<n_type>* n){
    return equals(n);
}

template class Node<int>;
template class Node<float>;
template class Node<double>;
template class Node<QString>;


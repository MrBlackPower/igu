#ifndef WISECOLLECTION_H
#define WISECOLLECTION_H

#include "wiseelement.h"
#include "wiseelementfactory.h"

#include <QDateTime>
#include <memory>

#include "tinyxml/tinyxml.h"

using namespace std;

class WiseCollection
{
    friend class WiseObject;
    friend class WiseObjectFactory;
    friend class GraphicObjectFactories;
    friend class WiseIterationFactories;

public:
    bool push_frame();

    bool operator==(int id);

    WiseElement* operator[](int i);

    WiseElement* operator[](double i);

    WiseElement* pop();

    unique_ptr<WiseElement> pop_hot();

    bool place_hot(unique_ptr<WiseElement> hot);

    int id();

    ~WiseCollection();

    static WiseCollection* first();

    static WiseCollection* last();

    static WiseCollection* obj(int id);

    TiXmlElement* to_XML();

    void scale_through(vector<tuple<WiseCellType,string,double>> scaling);

    const string type;

    WiseCollection* next;

    WiseCollection* previous;

private:
    WiseCollection(unique_ptr<WiseElement> hot_object ,vector<unique_ptr<WiseElement>> cold_objects, unique_ptr<WiseElementFactory> obj_factory);
    WiseCollection(unique_ptr<WiseElement> hot_object ,unique_ptr<WiseElement> cold_object, unique_ptr<WiseElementFactory> obj_factory);

    WiseCollectionParams params;

    static int CURR_ID;

    static int collection_N;

    static WiseCollection* first_object;

    static WiseCollection* last_object;
};

#endif // WISECOLLECTION_H

#include "wiseobjectfactories.h"

WiseObjectFactories::WiseObjectFactories()
{
    WiseObjectFactory* af = new ArteryTreeWiseObjectFactory();
    WiseObjectFactory* gf = new GraphicWiseObjectFactory();
    WiseObjectFactory* mf = new MeshWiseObjectFactory();
    WiseObjectFactory* pf = new PolyWiseObjectFactory();

    factories[af->get_type()] = unique_ptr<WiseObjectFactory>{af};
    factories[gf->get_type()] = unique_ptr<WiseObjectFactory>{gf};
    factories[mf->get_type()] = unique_ptr<WiseObjectFactory>{mf};
    factories[pf->get_type()] = unique_ptr<WiseObjectFactory>{pf};
}

vector<string> WiseObjectFactories::get_types(){
    vector<string> v;
    for(map<string,unique_ptr<WiseObjectFactory>>::iterator it = factories.begin(); it != factories.end(); ++it) {
      v.push_back(it->first);
    }

    return  v;
}

unique_ptr<WiseObject> WiseObjectFactories::make_xml(TiXmlElement* doc){
    TiXmlElement* pElem;

    // block: OBJECT_PARAMS
    pElem = doc->FirstChildElement("OBJECT_PARAMS");
    // should always have a valid root but handle gracefully if it does

    if(!pElem)
        return 0;

    QString name = pElem->Attribute("NAME");
    QString type = pElem->Attribute("TYPE");

    if(!has_type(type.toStdString().data()))
        return 0;

    return factories.operator [](type.toStdString().data())->xml_load(doc);
}

unique_ptr<WiseObject> WiseObjectFactories::make_aus_element(string name,WiseElement* element){
    unique_ptr<WiseObject> o;

    if(!element)
        return NULL;

    string type = element->get_params()->type;
    map<string,unique_ptr<WiseObjectFactory>>::iterator it = factories.find(type);
    if(it == factories.end()){
        return NULL;
    }

    o = it->second->make(name,element_factories.clone(element->get_name(),element,wise::HOT));

    return o;
}

unique_ptr<WiseObject> WiseObjectFactories::make_aus_element(string name, unique_ptr<WiseElement> element){
    unique_ptr<WiseObject> o;

    if(!element.get())
        return NULL;

    string type = element->get_params()->type;
    map<string,unique_ptr<WiseObjectFactory>>::iterator it = factories.find(type);
    if(it == factories.end()){
        return NULL;
    }

    o = it->second->make(name,move(element));

    return o;
}

unique_ptr<WiseObject> WiseObjectFactories::clone(WiseObject* obj){
    map<string,unique_ptr<WiseObjectFactory>>::iterator it = factories.find(obj->get_params()->type);

    if(it != factories.end()){
        unique_ptr<WiseObject> obJ = it->second->clone(obj->get_name(),obj);
        return move(obJ);
    }

    return NULL;
}

bool WiseObjectFactories::has_type(string type){
    vector<string> s = get_types();

    for(string ss : s){
        if(ss == type)
            return true;
    }

    return false;
}

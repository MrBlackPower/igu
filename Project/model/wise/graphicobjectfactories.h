#ifndef GRAPHICFACTORIES_H
#define GRAPHICFACTORIES_H


#include <memory>
#include <map>
#include "graphicobjectfactory.h"
#include "wiseelementfactories.h"

using namespace std;

class GraphicObjectFactories
{
public:
    GraphicObjectFactories();
    ~GraphicObjectFactories();

    vector<string> get_types();

    vector<string> get_factories_name(string type = "");

    unique_ptr<GraphicModel> make_graphic(WiseCollection* obj, string factory);

private:
    map<string, map<string,unique_ptr<GraphicObjectFactory>>> factories;

    int ID;

    static int CURR_ID;
};

#endif // GRAPHICFACTORIES_H

#ifndef WISEOBJECT_H
#define WISEOBJECT_H

#include <string>
#include <vector>
#include <stack>
#include <memory>
#include <tuple>
#include <QString>

#include "helper/igumath.h"
#include "helper/igufile.h"

#include "model/wise/point.h"

#include "wisestructure.h"
#include "wisepeer.h"

using namespace wise;
using namespace std;

/**
 * @brief The WiseObject class contais a "wise" object, meaning this object can print itself and do more complicated operations as tasks.
 */
class WiseElement : public WisePeer
{
    friend class WiseElementFactory;
    friend class WiseObjectFactory;
    friend class GraphicObjectFactory;
    friend class GraphicObjectFactories;
    friend class WiseIterationFactories;

public:
    /**
     * @brief WiseObject
     * @param name name of the wise object
     * @param sub_classes stack of subclasse of current object
     */
    WiseElement(string name, string type); // CREATED OBJECT
    WiseElement(string name, string type, string element_key, string structure_key, WiseLoadType load_type, string filename); //IMPORTED OBJECT

    ~WiseElement();

    int ID();

    string key();

    string dna();

    WiseElementStatus status();

    bool clear(bool deep_clean = false);

    bool clear_data(bool deep_clean = false);

    void update_xml();

    WiseElementParams* get_params();

    void print(string filename, bool xml = true);

    /**
     * @brief to_string
     *
     *
     * @return string that represents this object
     */
    string to_string(bool xml = true);

    /**
     * @brief to_VTK is a method resposible for the otput a a VTK document updated
     *
     * @return Tiny VTK Document corresponding to this object
     */
    string to_VTK();

    /**
     * @brief to_XML is a method resposible for the otput a a xml document updated
     *
     * @return Tiny XML Document corresponding to this object
     */
    TiXmlElement* to_XML();

    /***
     * ADD METHODS
     * */

    bool add_cell(vector<int> p);

    bool add_cell_data(string data, string info_name);


    bool add_cell_info(string name, WiseDataType type = DOUBLE);


    bool add_field(string name,WiseFieldFamily family = WISE, WiseDataType type = DOUBLE, WiseFieldType field_type = INITIAL);

    bool add_field(string name, string data, WiseFieldFamily family = WISE, WiseDataType type = DOUBLE, WiseFieldType field_type = INITIAL);

    bool add_field_data(string data, string info_name);



    bool add_line(int a, int b);

    bool add_line_data(string data, string info_name);

    bool add_line_info(string name, WiseDataType type = DOUBLE);


    bool add_point(double x, double y, double z = 0);

    bool add_point(point p);

    bool add_point_info(string name, WiseDataType type = DOUBLE);

    bool add_point_data(string data, string info_name);


    /*******************************************************************************************************************************************
     * WISE STRUCTURE
     *******************************************************************************************************************************************/

    void set_default_fields();


    string get_name();

    bool set_status(WiseElementStatus status);


    bool set_all_point_data(string data, string info_name);

    bool set_point_data(int id, string data, string info_name);

    vector<string> get_point_data(string info_name);


    bool set_all_cell_data( string data, string info_name);

    bool set_cell_data(int id, string data, string info_name);

    bool set_cell_type(int id, int type);

    vector<string> get_cell_data(string info_name);


    bool set_all_line_data(string data, string info_name);

    bool set_line_data(int id, string data, string info_name);

    vector<string> get_line_data(string info_name);



    string get_field(string info_name, int id = -1);

    vector<string> get_field_data(string info_name);


    bool has_field(string info_name);

    bool field_set(string info_name);

    bool field_set(string info_name, string data_type);

    bool field_set(string info_name, WiseDataType data_type);

    bool set_all_field_data(string data, string info_name);

    bool set_field_data(int id, string data, string info_name);

    bool operator==(WiseElement* obj);

    bool operator==(int id);

    /****
     *  GETTER FOR STRUCT
     * **/

    vector<point> get_points();

    int get_points_size();

    vector<WiseCell> get_cells();

    vector<WiseLine> get_lines();

    vector<tuple<WiseInfo,WiseFieldFamily,WiseFieldType>> get_fields();

    //TODO - ARITHMETICS

    double t();

    void t(double t);

    double dt();

    void dt(double dt);

    bool scale_through(vector<pair<string,double>> points_scale, vector<pair<string,double>> lines_scale,
                       vector<pair<string,double>> cells_scale, vector<pair<string,double>> fields_scale);

    void scale_through(vector<tuple<WiseCellType,string,double>> scaling);

    /*******************************************************************************************************************************************
     * VIRTUAL METHODS
     *******************************************************************************************************************************************/
    /**
     * @brief update_structure Updates the relying structure (points, cells, lines and fields). NOW it's the wise_structure.
     *
     */
    virtual void update_structure() = 0;

    /**
     * @brief delete_abstract_data Deletes the abstract structure to free memory;
     *
     */
    virtual void delete_abstract_data() = 0;

protected:

    WiseElementParams params;

private:

    static long int CURR_ID;

    static long int CURR_N;

};

#endif // WISEOBJECT_H

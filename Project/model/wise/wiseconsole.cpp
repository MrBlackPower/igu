#include "wiseconsole.h"

unsigned int         WiseConsole::CURR_ID = 0;
int                  WiseConsole::console_max_size = 10000;
int                  WiseConsole::console_max_show = 1000;
vector<WiseConsole*> WiseConsole::consoles = vector<WiseConsole*>();

QString console::console_message_type_to_QString(ConsoleMessageType type){
    switch (type) {
    case COMMAND:
        return "COMMAND";
    case OUT:
        return "OUT";
    case INPUT:
        return "INPUT";
    case ERROR:
        return "ERROR";
    case WARNING:
        return "WARNING";
    }
}

QString console::console_message_type_to_signal(ConsoleMessageType type){
    switch (type) {
    case COMMAND:
        return ">>";
    case OUT:
        return ".";
    case INPUT:
        return "<<";
    case ERROR:
        return "E>";
    case WARNING:
        return "W>";
    }
}

ConsoleMessageType console::QString_to_console_message_type(QString type){
    if(type == "COMMAND"){
        return COMMAND;
    } else if(type == "OUT"){
        return OUT;
    } else if(type == "INPUT"){
        return INPUT;
    } else if(type == "ERROR"){
        return ERROR;
    } else if(type == "WARNING"){
        return WARNING;

        static WiseElementFactories element_factories;
    }

    return OUT;
}

WiseConsole::WiseConsole(QString console_file, QObject* parent) : QObject(parent), ID(CURR_ID++)
{
    project = NULL;
    last_msg = 0;
    has_project = false;
    clear_on_update = false;
    this->console_file = console_file;
    test_batch = 0;

    consoles.push_back(this);

    WisePeer::set_consoles_function(get_qconsoles);

    QDir current_dir = QDir::currentPath();

    bool has_xml = igufile::file_exists(current_dir.path().toStdString() + QString(QDir::separator()).toStdString() + console_file.toStdString());

    if(has_xml)
        load(QString::asprintf("%s%s%s",current_dir.path().toStdString().data(),QString(QDir::separator()).toStdString().data(),console_file.toStdString().data()));

    bool has_log = igufile::file_exists(current_dir.path().toStdString() + QString(QDir::separator()).toStdString() + QString::asprintf(WISE_CONSOLE_LOG_PATTERN,ID).toStdString());

    if(has_log)
        igufile::remove(current_dir.path().toStdString() + QString(QDir::separator()).toStdString() + QString::asprintf(WISE_CONSOLE_LOG_PATTERN,ID).toStdString());

    msgs = vector<pair<ConsoleMessageType,QString>>();

    receiver(console::OUT,"   /=======================================================================/");
    receiver(console::OUT,"  /====/       IGU (Iterador Gráfico Universal)       /===================/");
    receiver(console::OUT," /====/        (or, Universal Graphic Iterator)      /===================/ ");
    receiver(console::OUT,"/=======================================================================/");
    receiver(console::OUT,VERSION);
    receiver(console::OUT,"/=======================================================================/");
    receiver(console::OUT,QDateTime::currentDateTime().toString(QString::asprintf("dddd dd/MM/yyyy hh:mm:ss zzz")));
    receiver(console::OUT,"/=======================================================================/");
}

WiseConsole::~WiseConsole(){    
    QString current_dir = QDir::currentPath();

    receiver(console::OUT,"Saving Wise Project...");
    receiver(console::OUT,QString::asprintf("%s%s%s",current_dir.toStdString().data(),QString(QDir::separator()).toStdString().data(),console_file.toStdString().data()));
    save(QString::asprintf("%s%s%s",current_dir.toStdString().data(),QString(QDir::separator()).toStdString().data(),console_file.toStdString().data()));


    receiver(console::OUT,"Saving Wise messages...");
    save_msgs(QString::asprintf("%s%s%s",current_dir.toStdString().data(),QString(QDir::separator()).toStdString().data(),QString::asprintf(WISE_CONSOLE_LOG_PATTERN,ID).toStdString().data()));

    if(has_project)
        delete project;
}

bool WiseConsole::operator==(WiseConsole* c){
    return ID == c->ID;
}

bool WiseConsole::save(QString filename){
    TiXmlPrinter* printer = new TiXmlPrinter();
    printer->SetIndent( "    " );

    TiXmlDocument doc;
    TiXmlElement* root = to_XML();
    TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );

    doc.LinkEndChild(decl);
    doc.LinkEndChild(root);

    doc.Accept(printer);

    igufile::save_raw(printer->CStr(),filename.toStdString().data());

    receiver(console::OUT,"Saving process finished...");

    return true;
}

bool WiseConsole::save_msgs(QString filename){
    vector<string> r;

    for(pair<ConsoleMessageType,QString> m : msgs){
        r.push_back(QString::asprintf("%s : %s", console_message_type_to_QString(m.first).toStdString().data(),m.second.toStdString().data()).toStdString());
    }

    igufile::save_raw(r,filename.toStdString().data());

    receiver(console::OUT,"Saving process finished...");

    return true;
}

bool WiseConsole::load(QString filename){
    receiver(console::OUT,"[WISE CONSOLE] LOAD invoked");
    TiXmlDocument doc;
    FILE* f;

    f = fopen(filename.toStdString().data(),"r");

    if(!f){
        receiver(console::ERROR,"[WISE CONSOLE] Load Error: Could not open file!");
        receiver(console::ERROR,"[WISE CONSOLE] File: " + filename);
        return false;
    } else {
        if (!doc.LoadFile(f)){
            receiver(console::ERROR,"[WISE CONSOLE] Load Error: Could not parse XML!");
            receiver(console::ERROR,"[WISE CONSOLE] File: " + filename);
            fclose(f);
            return false;
        }
    }

    TiXmlHandle hDoc(&doc);
    TiXmlElement* pElem;
    TiXmlElement* project;
    TiXmlHandle hRoot(0);

    // block: WISE_CONSOLE
    pElem = hDoc.FirstChildElement("WISE_CONSOLE").Element();

    if(!pElem){
        fclose(f);
        return false;
    }

    //bock: WISE_PROJECT
    project = pElem->FirstChildElement("WISE_PROJECT");

    if(!project){
        fclose(f);
        return false;
    }

    this->project = pjt_fctry.create_project(project).release();

    if(!this->project){
        receiver(console::ERROR,"[WISE CONSOLE] Load Error: Could not create project!");
        receiver(console::ERROR,"[WISE CONSOLE] File: " + filename);
    }

    has_project = true;

    receiver(console::OUT,"[WISE CONSOLE] LOAD concluded");

    fclose(f);
    return true;
}

TiXmlElement* WiseConsole::to_XML(){
    TiXmlComment * comment;
    string s;

    TiXmlElement * root = new TiXmlElement("WISE_CONSOLE");

    comment = new TiXmlComment();
    s = " XML for Wise Console ";
    comment->SetValue(s.c_str());

    root->LinkEndChild( comment );

    //ADD RELYING STRUCTURES

    if(has_project){
        root->LinkEndChild( project->to_XML() );
    }

    return root;
}

void WiseConsole::run_cmd(QString command_line){
    QStringList args_raw = command_line.split(" ");
    QStringList args = command_line.toLower().split(" ");

    if(args.size() < 1)
        return;

    QString scope = args.front();

    receiver(console::OUT,command_line);
    receiver(console::OUT,QString::asprintf("SCOPE: %s",scope.toStdString().data()));

    if(scope.contains("test") || scope.contains("teste")){
        args.erase(args.begin());
        args_raw.erase(args_raw.begin());

        bool all = false;
        int  test_id = 0;

        if(!args.empty()){
            QString f (args.front());

            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            //ONLY ONE ARG IF ANY
            if(!args.empty())
                return;

            if(f.contains("all")){
                all = true;
            } else {
                test_id = f.toInt();

                if(test_id < 0)
                    return;
            }
        } else {
            all = true;
        }

        if(has_project){
            delete project;
            has_project = false;

            receiver(console::ERROR,"NO PROJECT SELECTED !!");
        }

        test_batch = 0;
        int test_successfull = 0;

        /**
        * TEST #1 - BLANK PROJECT LOAD AND SAVE
        **/

        if(test_id == 1 || all){
            if(test_blank_project())
                test_successfull++;
        }

        /**
        * TEST #2 - PROJECT W/ ELEMENT (ARTERY TREE - DUAN & ZAMIR) LOAD AND SAVE
        **/

        if(test_id == 2 || all){
            if(test_element_crud("artery_tree","duan_and_zamir",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #3 - PROJECT W/ ELEMENT (ARTERY TREE - SIMPLE ONE) LOAD AND SAVE
        **/

        if(test_id == 3 || all){
            if(test_element_crud("artery_tree","simple_one",test_batch+1))
                    test_successfull++;
        }

        /**
        * TEST #4 - PROJECT W/ ELEMENT (ARTERY TREE - SIMPLE TWO) LOAD AND SAVE
        **/

        if(test_id == 4 || all){
            if(test_element_crud("artery_tree","simple_two",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #5 - PROJECT W/ ELEMENT (GRAPHIC - SIN) LOAD AND SAVE
        **/

        if(test_id == 5 || all){
            if(test_element_crud("graphic","sin",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #6 - PROJECT W/ ELEMENT (GRAPHIC - COS) LOAD AND SAVE
        **/

        if(test_id == 6 || all){
            if(test_element_crud("graphic","cos",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #7 - PROJECT W/ ELEMENT (MESH - COSINOIDAL_SATURATION) LOAD AND SAVE
        **/

        if(test_id == 7 || all){
            if(test_element_crud("mesh","cosinoidal_saturation",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #8 - PROJECT W/ ELEMENT (MESH - COSINOIDAL_X_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 8 || all){
            if(test_element_crud("mesh","cosinoidal_x_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #9 - PROJECT W/ ELEMENT (COSINOIDAL_Y_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 9 || all){
            if(test_element_crud("mesh","cosinoidal_y_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #10 - PROJECT W/ ELEMENT (COSINOIDAL_FONT) LOAD AND SAVE
        **/

        if(test_id == 10 || all){
            if(test_element_crud("mesh","cosinoidal_font",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #11 - PROJECT W/ ELEMENT (POLY - HYPERBOLICAL_SATURATION) LOAD AND SAVE
        **/

        if(test_id == 11 || all){
            if(test_element_crud("poly","hyperbolical_saturation",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #12 - PROJECT W/ ELEMENT (POLY - HYPERBOLICAL_EXACT) LOAD AND SAVE
        **/

        if(test_id == 12 || all){
            if(test_element_crud("poly","hyperbolical_exact",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #13 - PROJECT W/ ELEMENT (POLY - HYPERBOLICAL_X_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 13 || all){
            if(test_element_crud("poly","hyperbolical_x_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #14 - PROJECT W/ ELEMENT (POLY - HYPERBOLICAL_Y_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 14 || all){
            if(test_element_crud("poly","hyperbolical_y_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #15 - PROJECT W/ ELEMENT (POLY - HYPERBOLICAL_Z_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 15 || all){
            if(test_element_crud("poly","hyperbolical_z_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #16 - PROJECT W/ ELEMENT (POLY - SINOIDAL_SATURATION) LOAD AND SAVE
        **/

        if(test_id == 16 || all){
            if(test_element_crud("poly","sinoidal_saturation",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #17 - PROJECT W/ ELEMENT (POLY - SINOIDAL_EXACT) LOAD AND SAVE
        **/

        if(test_id == 17 || all){
            if(test_element_crud("poly","sinoidal_exact",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #18 - PROJECT W/ ELEMENT (POLY - SINOIDAL_X_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 18 || all){
            if(test_element_crud("poly","sinoidal_x_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #19 - PROJECT W/ ELEMENT (POLY - SINOIDAL_Y_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 19 || all){
            if(test_element_crud("poly","sinoidal_y_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #20 - PROJECT W/ ELEMENT (POLY - SINOIDAL_Z_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 20 || all){
            if(test_element_crud("poly","sinoidal_z_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #21 - PROJECT W/ ELEMENT (POLY - ALPHA_SATURATION) LOAD AND SAVE
        **/

        if(test_id == 21 || all){
            if(test_element_crud("poly","alpha_saturation",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #22 - PROJECT W/ ELEMENT (POLY - ALPHA_EXACT) LOAD AND SAVE
        **/

        if(test_id == 22 || all){
            if(test_element_crud("poly","alpha_exact",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #23 - PROJECT W/ ELEMENT (POLY - ALPHA_X_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 23 || all){
            if(test_element_crud("poly","alpha_x_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #24 - PROJECT W/ ELEMENT (POLY - ALPHA_Y_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 24 || all){
            if(test_element_crud("poly","alpha_y_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #25 - PROJECT W/ ELEMENT (POLY - ALPHA_Z_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 25 || all){
            if(test_element_crud("poly","alpha_z_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #26 - PROJECT W/ ELEMENT (POLY - ALPHA_CONVECTION_SATURATION) LOAD AND SAVE
        **/

        if(test_id == 26 || all){
            if(test_element_crud("poly","alpha_convection_saturation",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #27 - PROJECT W/ ELEMENT (POLY - ALPHA_CONVECTION_EXACT) LOAD AND SAVE
        **/

        if(test_id == 27 || all){
            if(test_element_crud("poly","alpha_convection_exact",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #28 - PROJECT W/ ELEMENT (POLY - SINOIDAL_X_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 28 || all){
            if(test_element_crud("poly","alpha_convection_x_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #29 - PROJECT W/ ELEMENT (POLY - ALPHA_CONVECTION_Y_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 29 || all){
            if(test_element_crud("poly","alpha_convection_y_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #30 - PROJECT W/ ELEMENT (POLY - ALPHA_CONVECTION_Z_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 30 || all){
            if(test_element_crud("poly","alpha_convection_z_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #31 - PROJECT W/ OBJECT (ARTERY_TREE - DUAN_AND_ZAMIR) LOAD AND SAVE
        **/

        if(test_id == 31 || all){
            if(test_object_crud("artery_tree","duan_and_zamir",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #32 - PROJECT W/ OBJECT (ARTERY_TREE - SIMPLE_ONE) LOAD AND SAVE
        **/

        if(test_id == 32 || all){
            if(test_object_crud("artery_tree","simple_one",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #33 - PROJECT W/ OBJECT (ARTERY_TREE - SIMPLE_ONE) LOAD AND SAVE
        **/

        if(test_id == 33 || all){
            if(test_object_crud("artery_tree","simple_one",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #34 - PROJECT W/ OBJECT (ARTERY_TREE - SIMPLE_TWO) LOAD AND SAVE
        **/

        if(test_id == 34 || all){
            if(test_object_crud("artery_tree","simple_two",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #35 - PROJECT W/ OBJECT (GRAPHIC - SIN) LOAD AND SAVE
        **/

        if(test_id == 35 || all){
            if(test_object_crud("graphic","sin",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #36 - PROJECT W/ OBJECT (GRAPHIC - COS) LOAD AND SAVE
        **/

        if(test_id == 36 || all){
            if(test_object_crud("graphic","cos",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #37 - PROJECT W/ OBJECT (MESH - COSINOIDAL_SATURATION) LOAD AND SAVE
        **/

        if(test_id == 37 || all){
            if(test_object_crud("mesh","cosinoidal_saturation",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #38 - PROJECT W/ OBJECT (MESH - COSINOIDAL_X_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 38 || all){
            if(test_object_crud("mesh","cosinoidal_x_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #39 - PROJECT W/ OBJECT (COSINOIDAL_Y_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 39 || all){
            if(test_object_crud("mesh","cosinoidal_y_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #40 - PROJECT W/ OBJECT (COSINOIDAL_font) LOAD AND SAVE
        **/

        if(test_id == 40 || all){
            if(test_object_crud("mesh","cosinoidal_font",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #41 - PROJECT W/ OBJECT (POLY - HYPERBOLICAL_SATURATION) LOAD AND SAVE
        **/

        if(test_id == 41 || all){
            if(test_object_crud("poly","hyperbolical_saturation",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #42 - PROJECT W/ OBJECT (POLY - HYPERBOLICAL_EXACT) LOAD AND SAVE
        **/

        if(test_id == 42 || all){
            if(test_object_crud("poly","hyperbolical_exact",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #43 - PROJECT W/ OBJECT (POLY - HYPERBOLICAL_X_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 43 || all){
            if(test_object_crud("poly","hyperbolical_x_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #44 - PROJECT W/ OBJECT (POLY - HYPERBOLICAL_Y_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 44 || all){
            if(test_object_crud("poly","hyperbolical_y_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #45 - PROJECT W/ OBJECT (POLY - HYPERBOLICAL_Z_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 45 || all){
            if(test_object_crud("poly","hyperbolical_z_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #46 - PROJECT W/ OBJECT (POLY - SINOIDAL_SATURATION) LOAD AND SAVE
        **/

        if(test_id == 46 || all){
            if(test_object_crud("poly","sinoidal_saturation",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #47 - PROJECT W/ OBJECT (POLY - SINOIDAL_EXACT) LOAD AND SAVE
        **/

        if(test_id == 47 || all){
            if(test_object_crud("poly","sinoidal_exact",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #48 - PROJECT W/ OBJECT (POLY - SINOIDAL_X_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 48 || all){
            if(test_object_crud("poly","sinoidal_x_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #49 - PROJECT W/ OBJECT (POLY - SINOIDAL_Y_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 49 || all){
            if(test_object_crud("poly","sinoidal_y_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #50 - PROJECT W/ OBJECT (POLY - SINOIDAL_Z_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 50 || all){
            if(test_object_crud("poly","sinoidal_z_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #51 - PROJECT W/ OBJECT (POLY - ALPHA_SATURATION) LOAD AND SAVE
        **/

        if(test_id == 51 || all){
            if(test_object_crud("poly","alpha_saturation",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #52 - PROJECT W/ OBJECT (POLY - ALPHA_EXACT) LOAD AND SAVE
        **/

        if(test_id == 52 || all){
            if(test_object_crud("poly","alpha_exact",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #53 - PROJECT W/ OBJECT (POLY - ALPHA_X_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 53 || all){
            if(test_object_crud("poly","alpha_x_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #54 - PROJECT W/ OBJECT (POLY - ALPHA_Y_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 54 || all){
            if(test_object_crud("poly","alpha_y_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #55 - PROJECT W/ OBJECT (POLY - ALPHA_Z_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 55 || all){
            if(test_object_crud("poly","alpha_z_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #56 - PROJECT W/ OBJECT (POLY - ALPHA_CONVECTION_saturation) LOAD AND SAVE
        **/

        if(test_id == 56 || all){
            if(test_object_crud("poly","alpha_convection_saturation",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #57 - PROJECT W/ OBJECT (POLY - ALPHA_CONVECTION_EXACT) LOAD AND SAVE
        **/

        if(test_id == 57 || all){
            if(test_object_crud("poly","alpha_convection_exact",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #58 - PROJECT W/ OBJECT (POLY - SINOIDAL_X_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 58 || all){
            if(test_object_crud("poly","alpha_convection_x_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #59 - PROJECT W/ OBJECT (POLY - ALPHA_CONVECTION_Y_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 59 || all){
            if(test_object_crud("poly","alpha_convection_y_velocity",test_batch+1))
                test_successfull++;
        }

        /**
        * TEST #60 - PROJECT W/ OBJECT (POLY - ALPHA_CONVECTION_Z_VELOCITY) LOAD AND SAVE
        **/

        if(test_id == 60 || all){
            if(test_object_crud("poly","alpha_convection_z_velocity",test_batch+1))
                test_successfull++;
        }

        receiver(console::OUT,QString::asprintf("%d OUT OF %d TEST SUCCESSFULL !",test_successfull,test_batch));
        receiver(console::OUT,QString::asprintf("<%f pct>",(((double)test_successfull/test_batch)*100)));

        return;
    }

    if(scope.contains("project") || scope.contains("projeto")){
        if(args.empty())
            return;

        args.erase(args.begin());
        args_raw.erase(args_raw.begin());
        QString cmd = args.front();

        //PROJECT - CREATE
        // <name>
        if(cmd.contains("create") || cmd.contains("criar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.empty())
                return;

            receiver(console::OUT,"project create command started");

            if(!has_project){
                project = pjt_fctry.create_project(args[0]).release();
            }else {
                delete project;

                project = pjt_fctry.create_project(args[0]).release();
            }

            has_project = true;

            receiver(console::OUT,"project create command finished");

            return;
        }

        //PROJECT - PRINT
        //
        if(cmd.contains("print") || cmd.contains("print")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            receiver(console::OUT,"project print command started");

            if(!has_project){
                receiver(console::ERROR,"No project selected!");
                return;
            }

            TiXmlPrinter* printer = new TiXmlPrinter();
            TiXmlElement* doc = project->to_XML();
            printer->SetIndent( "    " );

            doc->Accept( printer );

            QString prnt(printer->CStr());

            receiver(console::OUT,prnt.toStdString().data());

            receiver(console::OUT,"project print command finished");

            delete doc;  delete printer; // DELETES XML

            return;

        }

        //PROJECT - DELETE
        //
        if(cmd.contains("delete") || cmd.contains("deleta")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            receiver(console::OUT,"project delete command started");

            if(!has_project){
                receiver(console::ERROR,"No project selected!");
                return;
            }

            delete project;

            project = NULL;

            has_project = false;

            receiver(console::OUT,"project delete finished");

            return;

        }

        //PROJECT - SAVE
        // <filename>
        if(cmd.contains("save") || cmd.contains("salvar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.empty())
                return;

            receiver(console::OUT,"project save command started");

            if(!has_project){
                receiver(console::ERROR,"No project selected!");
                return;
            }

            TiXmlPrinter* printer = new TiXmlPrinter();
            TiXmlDocument doc;
            TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "", "" );
            doc.LinkEndChild( decl );
            doc.LinkEndChild(project->to_XML());


            printer->SetIndent( "    " );

            doc.Accept( printer );

            string prnt(printer->CStr());
            igufile::save_raw(prnt.data(),args_raw[0].toStdString().data());

            receiver(console::OUT,"project save command finished");

            return;

        }

        //PROJECT - LOAD
        // <filename>
        if(cmd.contains("load") || cmd.contains("abrir")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.empty())
                return;

            receiver(console::OUT,"project load started");

            TiXmlDocument doc;
            FILE* f;
            QString filename = args_raw[0];
            f = fopen(filename.toStdString().data(),"rw");

            if(!f){
                receiver(console::ERROR,"[WISE CONSOLE] Load Error: Could not open file!");
                receiver(console::ERROR,"[WISE CONSOLE] File: " + filename);
                return;
            } else {
                if (!doc.LoadFile(f)){
                    receiver(console::ERROR,"[WISE CONSOLE] Load Error: Could not parse XML!");
                    receiver(console::ERROR,"[WISE CONSOLE] File: " + filename);
                    fclose(f);
                    return;
                }
            }

            TiXmlHandle hDoc(&doc);
            TiXmlElement* pElem;
            // block: WISE_CONSOLE
            pElem = hDoc.FirstChildElement("WISE_PROJECT").Element();

            project = pjt_fctry.create_project(pElem).release();

            if(!project)
                receiver(console::ERROR,"[WISE CONSOLE] Load Error: Could not parse WiseProject!");
            else
                has_project = true;

            receiver(console::OUT,"project load /home/igor/Documents/git/igu/Debug/wise_console2.wcsl");



            return;

        }
    }

    if(scope.contains("element") || scope.contains("element")){
        if(args.empty())
            return;

        args.erase(args.begin());
        args_raw.erase(args_raw.begin());
        QString cmd = args.front();

        //ELEMENT - PRINT
        // <name>
        if(cmd.contains("print") || cmd.contains("print")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 1)
                return;

            QString name = args_raw[0];

            vector<pair<int,string>> els = project->get_elements_list();
            for(pair<int,string> p : els){
                if(name == QString(p.second.data())){
                    receiver(console::OUT,project->get_element(p.first)->to_string().data());
                }
            }

            return;
        }


        //ELEMENT - CREATE
        // <type> <example> <name>
        if(cmd.contains("create") || cmd.contains("criar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 3)
                return;

            unique_ptr<WiseElement> ptr = pjt_fctry.element_factories.make_example(args_raw[0].toUpper().toStdString(),args_raw[2].toStdString(),args_raw[1].toUpper().toStdString());

            if(ptr.get() == NULL){
                receiver(console::ERROR,"Could not find example!");
                return;
            }


            project->add_element(move(ptr));

            receiver(console::OUT,QString::asprintf("ELEMENT [%s] CREATED",args_raw[2].toStdString().data()));

            return;
        }


        //ELEMENT - LIST
        //
        if(cmd.contains("list") || cmd.contains("listar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(has_project){
                vector<pair<int,string>> v = project->get_elements_list();

                for(pair<int,string> p : v){
                    receiver(console::OUT,QString::asprintf("%d - %s",p.first,p.second.data()));
                }
            }

            return;
        }


        //ELEMENT - SAVE
        // <element_name> <load_type> <filename>
        if(cmd.contains("save") || cmd.contains("salvar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 3)
                return;

            wise::WiseLoadType save_type = string_to_wise_load_type(args_raw[1].toStdString());

            if(has_project){
                vector<pair<int,string>> v = project->get_elements_list();

                for(pair<int,string> p : v){
                    if(p.second == args_raw[0].toStdString()){
                        if(save_type == XML){
                            TiXmlPrinter* printer = new TiXmlPrinter();
                            TiXmlDocument doc;
                            TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "", "" );
                            doc.LinkEndChild( decl );
                            doc.LinkEndChild(project->get_element(p.first)->to_XML());


                            printer->SetIndent( "    " );

                            doc.Accept( printer );

                            string prnt(printer->CStr());
                            igufile::save_raw(prnt.data(),args_raw[2].toStdString().data());
                        } else {
                            igufile::save_raw(project->get_element(p.first)->to_VTK(),args_raw[2].toStdString().data());
                        }

                        return;
                    }

                }
            }

            return;
        }


        //ELEMENT - LOAD
        // <load_type> <...>
        // <xml> <filename>
        // <vtk> <filename> <type> <name>
        if(cmd.contains("load") || cmd.contains("carregar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 2)
                return;

            wise::WiseLoadType load_type = string_to_wise_load_type(args_raw[0].toUpper().toStdString());

            if(load_type == VTK && args.size() < 4)
                return;

            if(!has_project){
                receiver(console::ERROR,"No project selected!");
                return;
            }

            unique_ptr<WiseElement> ptr = NULL;

            if(load_type == XML){
                TiXmlDocument doc;
                FILE* f;
                QString filename = args_raw[1];
                f = fopen(filename.toStdString().data(),"rw");

                if(!f){
                    receiver(console::ERROR,"[WISE CONSOLE] Load Error: Could not open file!");
                    receiver(console::ERROR,"[WISE CONSOLE] File: " + filename);
                    return;
                } else {
                    if (!doc.LoadFile(f)){
                        receiver(console::ERROR,"[WISE CONSOLE] Load Error: Could not parse XML!");
                        receiver(console::ERROR,"[WISE CONSOLE] File: " + filename);
                        fclose(f);
                        return;
                    }
                }

                ptr = pjt_fctry.element_factories.make_xml(doc.ToElement());
            } else if(load_type == VTK){
                ptr = pjt_fctry.element_factories.make_vtk(args_raw[2].toUpper().toStdString(),args_raw[3].toStdString(),args_raw[1].toStdString());
            }

            if(ptr.get() == NULL)
                return;

            project->add_element(move(ptr));

            receiver(console::OUT,QString::asprintf("ELEMENT [%s] LOADED",args_raw[1].toStdString().data()));
            return;
        }


        //ELEMENT - DELETE
        // <element_name>
        if(cmd.contains("delete") || cmd.contains("deletar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 1)
                return;


            if(has_project){
                vector<pair<int,string>> v = project->get_elements_list();

                for(pair<int,string> p : v){
                    if(p.second == args_raw[0].toStdString()){
                        if(project->delete_element(p.first))
                            receiver(console::OUT,QString::asprintf("ELEMENT [%s] DELETED",args_raw[0].toStdString().data()));
                        else
                            receiver(console::OUT,QString::asprintf("ELEMENT [%s] DELETE FAILED",args_raw[0].toStdString().data()));

                        return;
                    }

                }
            }

            return;
        }


        //ELEMENT - SCALE
        // <element_name> <cell_type> <field> <scale>
        if(cmd.contains("scale") || cmd.contains("escalar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 4)
                return;

            if(!has_project)
                return;

            QString      element_name = args_raw[0];
            WiseCellType cell_type    = wise_cell_type_to_string(args_raw[1].toUpper().toStdString());
            string       field        = args_raw[2].toStdString();
            double       scale        = args_raw[3].toDouble();
            vector<tuple<WiseCellType,string,double>> t{tuple<WiseCellType,string,double>{cell_type,field,scale}};

            unique_ptr<WiseElement> el = project->release_element(project->get_element(element_name.toStdString()));

            if(!el.get())
                return;

            el->scale_through(t);
            el->update_xml();
            el->delete_abstract_data();

            int i = project->add_element(pjt_fctry.element_factories.recreate(move(el)));

            project->get_element(i)->update_structure();

            receiver(console::OUT,QString::asprintf("ELEMENT [%s] SCALED",args_raw[0].toStdString().data()));

            return;
        }


        //ELEMENT - SET_FIELD
        // <element_name> <cell_type> <field_name> <id> <data>
        if(cmd.contains("set_field") || cmd.contains("set_campo")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 5)
                return;

            if(!has_project)
                return;

            QString      element_name = args_raw[0];
            WiseCellType cell_type    = wise_cell_type_to_string(args_raw[1].toUpper().toStdString());
            QString      field_name   = args_raw[2];
            int          id           = args_raw[3].toInt();
            QString      data         = args_raw[4];

            unique_ptr<WiseElement> el = project->release_element(project->get_element(element_name.toStdString()));

            if(!el.get()){
                receiver(console::ERROR,QString::asprintf("ELEMENT [%s] NOT FOUND",args_raw[0].toStdString().data()));

                return;
            }

            bool result = false;

            switch(cell_type){
            case POINT:
                result = el->set_point_data(id,data.toStdString(),field_name.toStdString());
                break;
            case LINE:
                result = el->set_line_data(id,data.toStdString(),field_name.toStdString());
                break;
            case CELL:
                result = el->set_cell_data(id,data.toStdString(),field_name.toStdString());
                break;
            case FIELD:
                result = el->set_field_data(id,data.toStdString(),field_name.toStdString());
                break;
            }

            if(!result){
                receiver(console::ERROR,QString::asprintf("ELEMENT [%s] NOT UPDATED",args_raw[0].toStdString().data()));
            }else{
                receiver(console::OUT,QString::asprintf("ELEMENT [%s] UPDATED",args_raw[0].toStdString().data()));
            }
            el->update_xml();
            el->delete_abstract_data();

            int i = project->add_element(pjt_fctry.element_factories.recreate(move(el)));

            project->get_element(i)->update_structure();

            return ;
        }


        //ELEMENT - SET_ALL_FIELD
        // <element_name> <cell_type> <field_name> <data>
        if(cmd.contains("set_all_field") || cmd.contains("set_todo_campo")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 4)
                return;

            if(!has_project)
                return;

            QString      element_name = args_raw[0];
            WiseCellType cell_type    = wise_cell_type_to_string(args_raw[1].toUpper().toStdString());
            QString      field_name   = args_raw[2];
            QString      data         = args_raw[3];

            unique_ptr<WiseElement> el = project->release_element(project->get_element(element_name.toStdString()));

            if(!el.get()){
                receiver(console::ERROR,QString::asprintf("ELEMENT [%s] NOT FOUND",args_raw[0].toStdString().data()));

                return;
            }

            bool result = false;

            switch(cell_type){
            case POINT:
                result = el->set_all_point_data(data.toStdString(),field_name.toStdString());
                break;
            case LINE:
                result = el->set_all_line_data(data.toStdString(),field_name.toStdString());
                break;
            case CELL:
                result = el->set_all_cell_data(data.toStdString(),field_name.toStdString());
                break;
            case FIELD:
                result = el->set_all_field_data(data.toStdString(),field_name.toStdString());
                break;
            }

            if(!result){
                receiver(console::ERROR,QString::asprintf("ELEMENT [%s] NOT UPDATED",args_raw[0].toStdString().data()));
            }else{
                receiver(console::OUT,QString::asprintf("ELEMENT [%s] UPDATED",args_raw[0].toStdString().data()));
            }
            el->update_xml();
            el->delete_abstract_data();

            int i = project->add_element(pjt_fctry.element_factories.recreate(move(el)));

            project->get_element(i)->update_structure();

            return ;
        }


        //ELEMENT - CLONE
        // <element_name> <name>
        if(cmd.contains("clone") || cmd.contains("clone")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 1)
                return;

            if(!has_project)
                return;

            pjt_fctry.clone_element((args.size() < 2)? "" : args_raw[1].toStdString(),args_raw[0].toStdString(),project);

            receiver(console::OUT,QString::asprintf("ELEMENT [%s] CLONED",args_raw[0].toStdString().data()));

            return;
        }

        //ELEMENT - FACTORIES
        if(cmd.contains("factories") || cmd.contains("fabricas")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.empty())
                return;

            QString cmd_sub = args.front();


            //ELEMENT - FACTORIES - LIST
            //
            if(cmd_sub.contains("list") || cmd_sub.contains("listar")){

                vector<pair<string,string>> r = pjt_fctry.element_factories.get_factories_info();

                for(pair<string,string> p : r){
                    receiver(console::OUT,QString::asprintf("%s [%s]",p.first.data(),p.second.data()));
                }

                return;
            }


            //ELEMENT - FACTORIES - EXAMPLES
            // <factory>
            if(cmd_sub.contains("examples") || cmd_sub.contains("exemplos")){
                args.erase(args.begin());
                args_raw.erase(args_raw.begin());

                if(args.empty())
                    return;

                vector<string> r = pjt_fctry.element_factories.get_factories_examples(args_raw[0].toUpper().toStdString().data());

                for(string p : r){
                    receiver(console::OUT,QString::asprintf("%s",p.data()));
                }

                return;
            }

        }
    }

    if(scope.contains("object") || scope.contains("objeto")){
        if(args.empty())
            return;

        args.erase(args.begin());
        args_raw.erase(args_raw.begin());
        QString cmd = args.front();

        //OBJECT - PRINT
        // <name>
        if(cmd.contains("print") || cmd.contains("print")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 1)
                return;

            QString name = args_raw[0];

            vector<pair<int,string>> els = project->get_models_list();
            for(pair<int,string> p : els){
                if(name == QString(p.second.data())){
                    receiver(console::OUT,project->get_object(p.first)->to_string().data());
                }
            }

            return;
        }


        //OBJECT - CREATE
        // <name> <element_name>
        // <type>    <example>   <name> <element_name>
        if(cmd.contains("create") || cmd.contains("criar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() != 2 && args.size() != 4){
                receiver(console::ERROR,"NOT ENOUGHT ARGUMENTS!");
                for(QString s : args)
                    receiver(console::ERROR,QString::asprintf("%d,",args.size())+s);
                return;
            }

            //CREATE FROM ELEMENT
            if(args.size() == 2){
                QString name         = args_raw[0];
                QString element_name = args_raw[1];

                WiseElement* ptr_aux = project->get_element(project->get_element(element_name.toStdString()));

                if(ptr_aux == NULL){
                    receiver(console::ERROR,"Could not find element!");
                    return;
                }

                unique_ptr<WiseObject> ptr = pjt_fctry.object_factories.make_aus_element(name.toStdString(),ptr_aux);

                if(ptr.get() == NULL){
                    receiver(console::ERROR,"Could not find example!");
                    return;
                }


                project->add_object(move(ptr));

                receiver(console::OUT,QString::asprintf("OBJECT [%s] CREATED",args_raw[0].toStdString().data()));
            } else {
                QString type         = args_raw[0];
                QString example      = args_raw[1];
                QString object_name  = args_raw[2];
                QString element_name = args_raw[3];

                unique_ptr<WiseElement> ptr_aux = pjt_fctry.element_factories.make_example(type.toUpper().toStdString(),element_name.toStdString(),example.toUpper().toStdString());

                if(ptr_aux.get() == NULL){
                    receiver(console::ERROR,"Could not find element!");
                    return;
                }

                unique_ptr<WiseObject> ptr = pjt_fctry.object_factories.make_aus_element(object_name.toStdString(),move(ptr_aux));

                if(ptr.get() == NULL){
                    receiver(console::ERROR,"Could not find example!");
                    return;
                }


                project->add_object(move(ptr));

                receiver(console::OUT,QString::asprintf("OBJECT [%s] CREATED",args_raw[2].toStdString().data()));
            }

            return;
        }


        //OBJECT - LIST
        //
        if(cmd.contains("list") || cmd.contains("listar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(has_project){
                vector<pair<int,string>> v = project->get_models_list();

                for(pair<int,string> p : v){
                    receiver(console::OUT,QString::asprintf("%d - %s",p.first,p.second.data()));
                }
            }

            return;
        }


        //OBJECT - SAVE
        // <name> <filename>
        if(cmd.contains("save") || cmd.contains("salvar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 2)
                return;

            if(has_project){
                if(cmd.contains("save_vtk") || cmd.contains("salvar_vtk")){
                    vector<pair<int,string>> v = project->get_models_list();

                    for(pair<int,string> p : v){
                        if(p.second == args_raw[0].toStdString()){
                                igufile::save_raw(project->get_object(p.first)->get_params()->obj->pop()->to_string(false),args_raw[1].toStdString().data());
                        }
                    }

                    return;
                 }

                vector<pair<int,string>> v = project->get_models_list();

                for(pair<int,string> p : v){
                    if(p.second == args_raw[0].toStdString()){
                            TiXmlPrinter* printer = new TiXmlPrinter();
                            TiXmlDocument doc;
                            TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "", "" );
                            doc.LinkEndChild( decl );
                            doc.LinkEndChild(project->get_object(p.first)->to_XML());


                            printer->SetIndent( "    " );

                            doc.Accept( printer );

                            string prnt(printer->CStr());
                            igufile::save_raw(prnt.data(),args_raw[1].toStdString().data());

                        return;
                    }

                }
            }

            return;
        }


        //OBJECT - LOAD
        // <filename>
        if(cmd.contains("load") || cmd.contains("carregar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 1)
                return;

            if(!has_project){
                receiver(console::ERROR,"No project selected!");
                return;
            }

            unique_ptr<WiseObject> ptr = NULL;

            TiXmlDocument doc;
            FILE* f;
            QString filename = args_raw[0];
            f = fopen(filename.toStdString().data(),"rw");

            if(!f){
                receiver(console::ERROR,"[WISE CONSOLE] Load Error: Could not open file!");
                receiver(console::ERROR,"[WISE CONSOLE] File: " + filename);
                return;
            } else {
                if (!doc.LoadFile(f)){
                    receiver(console::ERROR,"[WISE CONSOLE] Load Error: Could not parse XML!");
                    receiver(console::ERROR,"[WISE CONSOLE] File: " + filename);
                    fclose(f);
                    return;
                }
            }

            ptr = pjt_fctry.object_factories.make_xml(doc.ToElement());

            if(ptr.get() == NULL)
                return;

            project->add_object(move(ptr));

            receiver(console::OUT,QString::asprintf("OBJECT [%s] LOADED",args_raw[1].toStdString().data()));
            return;
        }


        //OBJECT - DELETE
        // <name>
        if(cmd.contains("delete") || cmd.contains("deletar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 1)
                return;


            if(has_project){
                vector<pair<int,string>> v = project->get_models_list();

                for(pair<int,string> p : v){
                    if(p.second == args_raw[0].toStdString()){
                        if(project->delete_object(p.first))
                            receiver(console::OUT,QString::asprintf("OBJECT [%s] DELETED",args_raw[0].toStdString().data()));
                        else
                            receiver(console::OUT,QString::asprintf("OBJECT [%s] DELETE FAILED",args_raw[0].toStdString().data()));

                        return;
                    }

                }
            }

            return;
        }


        //OBJECT - SCALE
        // <object_name> <cell_type> <field> <scale>
        if(cmd.contains("scale") || cmd.contains("escalar")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 4)
                return;

            if(!has_project)
                return;

            QString      object_name = args_raw[0];
            WiseCellType cell_type    = wise_cell_type_to_string(args_raw[1].toUpper().toStdString());
            string       field        = args_raw[2].toStdString();
            double       scale        = args_raw[3].toDouble();
            vector<tuple<WiseCellType,string,double>> t{tuple<WiseCellType,string,double>{cell_type,field,scale}};

            WiseObject* o = project->operator[](object_name.toStdString());
            unique_ptr<WiseElement> el = o->pop_hot();

            if(!el.get())
                return;

            el->scale_through(t);
            el->update_xml();
            el->delete_abstract_data();

            o->place_hot(pjt_fctry.element_factories.recreate(move(el)));
            o->get_hot()->update_structure();

            receiver(console::OUT,QString::asprintf("ELEMENT [%s] SCALED",args_raw[0].toStdString().data()));

            return;
        }


        //OBJECT - SET_FIELD
        // <object_name> <cell_type> <field_name> <id> <data>
        if(cmd.contains("set_field") || cmd.contains("set_campo")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 5)
                return;

            if(!has_project)
                return;

            QString      object_name = args_raw[0];
            WiseCellType cell_type    = wise_cell_type_to_string(args_raw[1].toUpper().toStdString());
            QString      field_name   = args_raw[2];
            int          id           = args_raw[3].toInt();
            QString      data         = args_raw[4];

            WiseObject* o = project->operator[](object_name.toStdString());
            unique_ptr<WiseElement> el = o->pop_hot();

            if(!el.get()){
                receiver(console::ERROR,QString::asprintf("ELEMENT [%s] NOT FOUND",args_raw[0].toStdString().data()));

                return;
            }

            bool result = false;

            switch(cell_type){
            case POINT:
                result = el->set_point_data(id,data.toStdString(),field_name.toStdString());
                break;
            case LINE:
                result = el->set_line_data(id,data.toStdString(),field_name.toStdString());
                break;
            case CELL:
                result = el->set_cell_data(id,data.toStdString(),field_name.toStdString());
                break;
            case FIELD:
                result = el->set_field_data(id,data.toStdString(),field_name.toStdString());
                break;
            }

            if(!result){
                receiver(console::ERROR,QString::asprintf("ELEMENT [%s] NOT UPDATED",args_raw[0].toStdString().data()));
            }else{
                receiver(console::OUT,QString::asprintf("ELEMENT [%s] UPDATED",args_raw[0].toStdString().data()));

                el->update_xml();
                el->delete_abstract_data();
            }

            o->place_hot(pjt_fctry.element_factories.recreate(move(el)));
            o->get_hot()->update_structure();

            return ;
        }


        //OBJECT - SET_ALL_FIELD
        // <object_name> <cell_type> <field_name> <data>
        if(cmd.contains("set_all_field") || cmd.contains("set_todo_campo")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 4)
                return;

            if(!has_project)
                return;

            QString      object_name = args_raw[0];
            WiseCellType cell_type    = wise_cell_type_to_string(args_raw[1].toUpper().toStdString());
            QString      field_name   = args_raw[2];
            QString      data         = args_raw[3];

            WiseObject* o = project->operator[](object_name.toStdString());
            unique_ptr<WiseElement> el = o->pop_hot();

            if(!el.get()){
                receiver(console::ERROR,QString::asprintf("ELEMENT [%s] NOT FOUND",args_raw[0].toStdString().data()));

                return;
            }

            bool result = false;

            switch(cell_type){
            case POINT:
                result = el->set_all_point_data(data.toStdString(),field_name.toStdString());
                break;
            case LINE:
                result = el->set_all_line_data(data.toStdString(),field_name.toStdString());
                break;
            case CELL:
                result = el->set_all_cell_data(data.toStdString(),field_name.toStdString());
                break;
            case FIELD:
                result = el->set_all_field_data(data.toStdString(),field_name.toStdString());
                break;
            }

            if(!result){
                receiver(console::ERROR,QString::asprintf("ELEMENT [%s] NOT UPDATED",args_raw[0].toStdString().data()));
            }else{
                receiver(console::OUT,QString::asprintf("ELEMENT [%s] UPDATED",args_raw[0].toStdString().data()));

                el->update_xml();
                el->delete_abstract_data();
            }

            o->place_hot(pjt_fctry.element_factories.recreate(move(el)));
            o->get_hot()->update_structure();

            return ;
        }


        //OBJECT - CLONE
        // <name> <<clone_name>>
        if(cmd.contains("clone") || cmd.contains("clone")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 1)
                return;

            if(!has_project)
                return;

            string clone_name = (args.size() < 2)? "" : args_raw[1].toStdString();

            pjt_fctry.clone_object(clone_name,args_raw[0].toStdString(),project);

            receiver(console::OUT,QString::asprintf("OBJECT [%s] CLONED",args_raw[0].toStdString().data()));

            return;
        }


        //OBJECT - SET
        // <name>
        if(cmd.contains("set") || cmd.contains("set")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 1)
                return;

            if(!has_project)
                return;

            string object_name =  args_raw[0].toStdString();

            WiseObject* o = project->operator[](object_name);

            if(o->ready_set())
                receiver(console::OUT,QString::asprintf("OBJECT [%s] SET",args_raw[0].toStdString().data()));
            else
                receiver(console::OUT,QString::asprintf("OBJECT [%s] NOT SET",args_raw[0].toStdString().data()));

            return;
        }


        //OBJECT - GO
        // <name>
        if(cmd.contains("go") || cmd.contains("go")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.size() < 1)
                return;

            if(!has_project)
                return;

            string object_name =  args_raw[0].toStdString();

            WiseObject* o = project->operator[](object_name);

            if(o->set_go())
                receiver(console::OUT,QString::asprintf("OBJECT [%s] GONE",args_raw[0].toStdString().data()));
            else
                receiver(console::OUT,QString::asprintf("OBJECT [%s] NOT GONE",args_raw[0].toStdString().data()));

            return;
        }

        //OBJECT - ITERATION_FACTORIES
        if(cmd.contains("iteration_factories") || cmd.contains("iteracao_fabricas")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.empty())
                return;

            QString cmd_sub = args.front();

            //OBJECT - FACTORIES - LIST
            // <object_name>
            if(cmd_sub.contains("list") || cmd_sub.contains("listar")){
                args.erase(args.begin());
                args_raw.erase(args_raw.begin());

                if(args.size() < 1)
                    return;

                QString     object_name = args_raw[0];
                WiseObject* o           = project->operator[](object_name.toStdString());

                if(!o){
                    receiver(console::ERROR,QString::asprintf("OBJECT NOT FOUND %s",object_name.toStdString().data()));
                    return;
                }

                vector<string> r = pjt_fctry.iteration_factories.get_factories_name(o->get_params()->type);

                for(string p : r){
                    receiver(console::OUT,QString::asprintf("%s",p.data()));
                }

                return;
            }

            //OBJECT - FACTORIES - SET
            // <object_name> <iteration_factory>
            if(cmd_sub.contains("set") || cmd_sub.contains("set")){
                args.erase(args.begin());
                args_raw.erase(args_raw.begin());

                if(args.size() < 2)
                    return;

                QString     object_name     = args_raw[0];
                QString     iteration_fctry = args_raw[1];
                WiseObject* o               = project->operator[](object_name.toStdString());

                o->set_iteration_factory(pjt_fctry.iteration_factories.get_factory_clone(o->get_params()->type,iteration_fctry.toStdString()));

                return;
            }
        }

        //OBJECT - FACTORIES
        if(cmd.contains("factories") || cmd.contains("fabricas")){
            args.erase(args.begin());
            args_raw.erase(args_raw.begin());

            if(args.empty())
                return;

            QString cmd_sub = args.front();


            //OBJECT - FACTORIES - LIST
            //
            if(cmd_sub.contains("list") || cmd_sub.contains("listar")){
                vector<pair<string,string>> r = pjt_fctry.element_factories.get_factories_info();

                for(pair<string,string> p : r){
                    receiver(console::OUT,QString::asprintf("%s [%s]",p.first.data(),p.second.data()));
                }

                return;
            }


            //OBJECT - FACTORIES - EXAMPLES
            // <factory>
            if(cmd_sub.contains("examples") || cmd_sub.contains("exemplos")){
                args.erase(args.begin());
                args_raw.erase(args_raw.begin());

                if(args.empty())
                    return;

                vector<string> r = pjt_fctry.element_factories.get_factories_examples(args_raw[0].toUpper().toStdString().data());

                for(string p : r){
                    receiver(console::OUT,QString::asprintf("%s",p.data()));
                }

                return;
            }

        }
    }


//    if(scope == "create_project" or scope == "criar_projeto"){
//        if(args.size() != 1)
//            return;

//    }

//    if(scope == "listar_projetos" or scope == "list_projects"){
//        if(args.size() != 0)
//            return;

//        vector<pair<int,QString>> aux = WiseProject::get_projects();
//        receiver(console::OUT,"/===========================WISE PROJECTS============================/");
//        for (auto p : aux) {
//            receiver(console::OUT,QString::asprintf("WISE_ID = %d  // WISE_PROJECT_NAME %s",p.first,p.second.toStdString().data()));
//        }

//        receiver(console::OUT,"list_projects command finished");
//    }

//    if(scope == "delete_project" or scope == "deletar_projeto"){
//        if(args.size() != 1)
//            return;

//        int id = args[0].toInt();

//        if(project != NULL)
//            if(id == project->id())
//                project = NULL;

//        if(WiseProject::delete_project(id))
//            receiver(console::OUT,QString::asprintf("WISE PROJECT ID %d DELETED",id));
//        else
//            receiver(ERROR,QString::asprintf("FAILED TO DELETE WISE PROJECT ID %d",id));

//        receiver(console::OUT,"delete_project command finished");
//    }

//    if(scope == "use_project" or scope == "usar_projeto"){
//        if(args.size() != 1)
//            return;

//        int id = args[0].toInt();

//        project = WiseProject::get_project(id);

//        if(project)
//            receiver(console::OUT,QString::asprintf("WISE PROJECT ID %d SELECTED",id));
//        else
//            receiver(ERROR,QString::asprintf("FAILED TO SELECT WISE PROJECT ID %d",id));

//        receiver(console::OUT,"select_project command finished");
//    }

//    if(scope == "list_element_factories" or scope == "listar_fabrica_elementos"){
//        if(args.size() != 0)
//            return;

//        vector<pair<string,string>> aux = element_factories.get_factiories_info();

//        receiver(console::OUT,"/=======================WISE ELEMENT FACTORIES=======================/");
//        for(auto p : aux){
//            receiver(console::OUT,QString::asprintf("WISE_TYPE = %s  // WISE_ELEMENT_FACTORY_NAME %s",p.first.data(),p.second.data()));
//        }

//        receiver(console::OUT,"list_element_factories command finished");
//    }

//    if(scope == "list_element_factory_examples" or scope == "listar_fabrica_elemento_exemplos"){
//        if(args.size() != 1)
//            return;

//        vector<string> aux = element_factories.get_factory_examples(args[0].toUpper().toStdString());//GET FACTORY BY TYPE

//        receiver(console::OUT,"/==================WISE ELEMENT FACTORY EXAMPLES=====================/");
//        receiver(console::OUT,QString::asprintf("/================== WISE_TYPE = %s ====================/",args[0].toUpper().toStdString().data()));
//        for(auto p : aux){
//            receiver(console::OUT,QString::asprintf("WISE_ELEMENT_EXAMPLE_NAME = %s",p.data()));
//        }

//        receiver(console::OUT,"list_element_factory_examples command finished");
//    }

//    if(scope == "create_element" or scope == "criar_elemento"){
//        if(args.size() != 4)
//            return;

//        vector<string> aux = element_factories.get_factory_examples(args[0].toUpper().toStdString());//GET FACTORY BY TYPE
//        QString load_type = args[1].toUpper();
//        QString filename = args_raw[2];
//        QString name = args_raw[3];

//        //IF USING PROJECT
//        //IF HAS PROJECT BUT NOT USING
//        //IF NOT CREATE A DEFAULT
//        if(project == NULL){
//            if(WiseProTiXmlPrinter* printer = new TiXmlPrinter();
//                project = WiseProject::pop_project();
//            } else {
//                project = new WiseProject("default_wise_project");
//            }
//        }

//        //INSERTS NEW ELEMENT IN WISE OBJECT
//        WiseLoadType tp = string_to_wise_load_type(load_type.toStdString());
//        switch (tp) {
//        case VTK:{
//            unique_ptr<WiseElement> el = element_factories.make_vtk(args[0].toUpper().toStdString(),name.toStdString(),filename.toStdString());

//            if(el)
//                project->add_element(std::move(el));
//            else
//                receiver(console::ERROR,"ELEMENT NOT CREATED");
//            break;
//        }
//        case XML:{
//            unique_ptr<WiseElement> el = element_factories.make_xml(args[0].toUpper().toStdString(),name.toStdString(),filename.toStdString());

//            if(el)
//                project->add_element(std::move(el));
//            else
//                receiver(console::ERROR,"ELEMENT NOT CREATED");
//            break;
//        }
//        case EXAMPLE:{
//            unique_ptr<WiseElement> el = element_factories.make_example(args[0].toUpper().toStdString(),name.toStdString(),filename.toUpper().toStdString());

//            if(el)
//                project->add_element(std::move(el));
//            else
//                receiver(console::ERROR,"ELEMENT NOT CREATED");
//            break;
//        }
//        case IDK:{
//            receiver(console::ERROR,"WISE_LOAD_TYPE NOT FOUND");
//            break;
//        }
//        }

//        receiver(console::OUT,"create_element command finished");
//    }

//    if(scope == "list_elements" or scope == "listar_elementos"){
//        if(args.size() != 0)
//            return;

//        receiver(console::OUT,"/=========================WISE ELEMENTS==============================/");
//        if(project){
//            receiver(console::OUT,QString::asprintf("/================== WISE_PROJECT = %s ====================/",project->get_name().toStdString().data()));
//            vector<pair<int,string>> aux = project->get_elements_list();
//            for(auto p : aux){
//                receiver(console::OUT,QString::asprintf("WISE_ELEMENT_ID = %d  //   WISE_ELEMENT_NAME= %s",p.first,p.second.data()));
//            }

//        }
//        receiver(console::OUT,"list_element_factory_examples command finished");
//    }

//    if(scope == "delete_element" or scope == "deletar_elemento"){
//        if(args.size() != 1)
//            return;

//        if(project){
//            if(project->delete_element(args[0].toInt())){
//                receiver(console::OUT,QString::asprintf("WISE_ELEMENT WISE_ID = %d DELETED",args[0].toInt()));
//            } else {
//                receiver(console::OUT,QString::asprintf("WISE_ELEMENT WISE_ID = %d NOT DELETED",args[0].toInt()));
//            }
//        }
//        receiver(console::OUT,"delete_element command finished");
//    }

//    if(scope == "save_element" or scope == "salvar_elemento"){
//        if(args.size() != 3)
//            return;

//        int id = args[0].toInt();
//        WiseLoadType load_type = string_to_wise_load_type(args[1].toUpper().toStdString());
//        QString filename = args_raw[2];

//        if(project){
//            WiseElement* el = project->get_element(id);

//            if(el != NULL){
//                bool xml;

//                switch (load_type) {
//                case XML:
//                    xml = true;
//                    break;
//                case VTK:
//                    xml = false;
//                    break;
//                default:
//                    xml = true;
//                    break;
//                }

//                el->print(filename.toStdString(),xml);
//            } else {
//                receiver(console::ERROR,QString::asprintf("WISE_ELEMENT WISE_ID = %d NOT FOUND",args[0].toInt()));
//            }
//        } else {
//            receiver(console::ERROR,"NO WISE_PROJECT SELECTED");
//        }

//        receiver(console::OUT,"save_element command finished");
//    }

//    if(scope == "print_element" or scope == "imprimir_elemento"){
//        if(args.size() != 2)
//            return;

//        int id = args[0].toInt();
//        WiseLoadType load_type = string_to_wise_load_type(args[1].toUpper().toStdString());

//        if(project){
//            WiseElement* el = project->get_element(id);

//            if(el != NULL){
//                bool xml;

//                switch (load_type) {
//                case XML:
//                    xml = true;
//                    break;
//                case VTK:
//                    xml = false;
//                    break;
//                default:
//                    xml = true;
//                    break;
//                }

//                receiver(console::OUT,QString{el->to_string(xml).data()});
//            } else {
//                receiver(console::ERROR,QString::asprintf("WISE_ELEMENT WISE_ID = %d NOT FOUND",args[0].toInt()));
//            }
//        } else {
//            receiver(console::ERROR,"NO WISE_PROJECT SELECTED");
//        }

//        receiver(console::OUT,"print_element command finished");
//    }

    receiver(console::ERROR, QString::asprintf("Foi não viu! Comando novo?"));

    return;
}

vector<WiseConsole*> WiseConsole::get_consoles(){
    return consoles;
}

vector<QObject*> WiseConsole::get_qconsoles(){
    vector<QObject*> aux;

    for(auto c: consoles)
        aux.push_back(c);

    return aux;
}

void WiseConsole::receiver(ConsoleMessageType type, QString msg){
    if(msg == "")
        return;

    if(type == COMMAND){
        return run_cmd(msg);
    }

    if(!msg.contains("\n")){
        msgs.push_back({type,msg});

        cout << QString::asprintf("%s %s",console_message_type_to_signal(type).toStdString().data(),msg.toStdString().data()).toStdString().data() << endl;
    }else{
        QStringList ls = msg.split("\n");
        for(QString a : ls){
            msgs.push_back({type,a});

            cout << QString::asprintf("%s %s",console_message_type_to_signal(type).toStdString().data(),a.toStdString().data()).toStdString().data() << endl;
        }
    }
}

void WiseConsole::receiver(QString type, QString msg){
    return receiver(QString_to_console_message_type(type),msg);
}

bool WiseConsole::test_blank_project(){
    test_batch++;

    //SAVES BLANK PROJECT
    run_cmd("project create projeto_teste");

    TiXmlPrinter* printer = new TiXmlPrinter();
    TiXmlElement* doc = project->to_XML();
    printer->SetIndent( "    " );

    doc->Accept( printer );

    QString pjt_comp2(printer->CStr());
    delete doc;  delete printer; // DELETES XML  delete printer; // DELETES XML
    printer = new TiXmlPrinter();

    run_cmd("project save /home/igor/Documents/git/igu/Debug/wcsl2.wcsl");
    run_cmd("project delete");

    //LOADS AND SAVES BLANK PROJECT
    run_cmd("project load /home/igor/Documents/git/igu/Debug/wcsl2.wcsl");


    doc = project->to_XML();
    printer->SetIndent( "    " );

    doc->Accept( printer );

    QString pjt_comp1(printer->CStr());
    delete doc;  delete printer; // DELETES XML

    if(pjt_comp1 == pjt_comp2){
        receiver(console::OUT,"TEST SUCCESSFULL !");
        receiver(console::OUT,"BLANK PROJECT CRUD IS WORKING");

        run_cmd("project save /home/igor/Documents/git/igu/Debug/wcsl3.wcsl");
        run_cmd("project delete");

        return true;
    }

    run_cmd("project save /home/igor/Documents/git/igu/Debug/wcsl3.wcsl");
    run_cmd("project delete");

    return false;
}

bool WiseConsole::test_element_crud(string type, string example, int test_n){
    test_batch++;

    //SAVES PROJECT W/ ELEMENT
    run_cmd("project create projeto_teste");
    run_cmd(QString::asprintf("element create %s %s elemento_teste",type.data(),example.data()));


    TiXmlPrinter* printer = new TiXmlPrinter();
    TiXmlElement* doc = project->to_XML();

    doc->Accept( printer );

    QString pjt_comp1(printer->CStr());
    delete doc;  delete printer; // DELETES XML
    printer = new TiXmlPrinter();

    run_cmd(QString::asprintf("project save /home/igor/Documents/git/igu/Debug/wcsl%d.wcsl",test_n*2));
    run_cmd("project delete");

    //LOADS AND SAVES BLANK PROJECT W/ ELEMENT
    run_cmd(QString::asprintf("project load /home/igor/Documents/git/igu/Debug/wcsl%d.wcsl",test_n*2));


    doc = project->to_XML();
    printer->SetIndent( "    " );

    doc->Accept( printer );

    QString pjt_comp2(printer->CStr());
    delete doc;  delete printer; // DELETES XML

    if(pjt_comp1 == pjt_comp2){
        receiver(console::OUT,"TEST SUCCESSFULL !");
        receiver(console::OUT,QString::asprintf("PROJECT W/ ELEMENT (%s - %s) CRUD IS WORKING",type.data(),example.data()));

        run_cmd(QString::asprintf("project save /home/igor/Documents/git/igu/Debug/wcsl%d.wcsl",(test_n*2)+1));
        run_cmd("project delete");

        return true;
    }

    run_cmd(QString::asprintf("project save /home/igor/Documents/git/igu/Debug/wcsl%d.wcsl",(test_n*2)+1));
    run_cmd("project delete");

    return false;
}

bool WiseConsole::test_object_crud (string type, string example, int test_n){
    test_batch++;

    //SAVES PROJECT W/ OBJECT
    run_cmd("project create projeto_teste");
    run_cmd(QString::asprintf("object create %s %s objeto_teste elemento_teste",type.data(),example.data()));


    TiXmlPrinter* printer = new TiXmlPrinter();
    TiXmlElement* doc = project->to_XML();

    doc->Accept( printer );

    QString pjt_comp1(printer->CStr());
    delete doc;  delete printer; // DELETES XML
    printer = new TiXmlPrinter();

    run_cmd(QString::asprintf("project save /home/igor/Documents/git/igu/Debug/wcsl%d.wcsl",test_n*2));
    run_cmd("project delete");

    //LOADS AND SAVES BLANK PROJECT W/ ELEMENT
    run_cmd(QString::asprintf("project load /home/igor/Documents/git/igu/Debug/wcsl%d.wcsl",test_n*2));


    doc = project->to_XML();
    printer->SetIndent( "    " );

    doc->Accept( printer );

    QString pjt_comp2(printer->CStr());
    delete doc;  delete printer; // DELETES XML

    if(pjt_comp1 == pjt_comp2){
        receiver(console::OUT,"TEST SUCCESSFULL !");
        receiver(console::OUT,QString::asprintf("PROJECT W/ ELEMENT (%s - %s) CRUD IS WORKING",type.data(),example.data()));

        run_cmd(QString::asprintf("project save /home/igor/Documents/git/igu/Debug/wcsl%d.wcsl",(test_n*2)+1));
        run_cmd("project delete");

        return true;
    }

    run_cmd(QString::asprintf("project save /home/igor/Documents/git/igu/Debug/wcsl%d.wcsl",(test_n*2)+1));
    run_cmd("project delete");

    return false;
}

#include "graphicobjectfactory.h"

GraphicObjectFactory::GraphicObjectFactory(string name, string type) : name(name), type(type)
{

}

GraphicObjectFactory::~GraphicObjectFactory(){

}

string GraphicObjectFactory::get_name(){
    return name;
}

string GraphicObjectFactory::get_type(){
    return type;
}


unique_ptr<GraphicModel> GraphicObjectFactory::create(WiseCollection* obj){
    return create_graphic(obj->operator[](ZERO)->params.wise_structure.get());
}

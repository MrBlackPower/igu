#include "graphicobjectfactories.h"

int GraphicObjectFactories::CURR_ID = 0;

GraphicObjectFactories::GraphicObjectFactories()
{
    //ADDS FACTORIES
}

GraphicObjectFactories::~GraphicObjectFactories(){

}


vector<string> GraphicObjectFactories::get_types(){
    vector<string> v;
    for(map<string, map<string,unique_ptr<GraphicObjectFactory>>>::iterator it = factories.begin(); it != factories.end(); ++it) {
        v.push_back(it->first);
        cout << it->first << "\n";
    }

    return  v;
}

vector<string> GraphicObjectFactories::get_factories_name(string type){
    vector<string> v;

    for(map<string,map<string,unique_ptr<GraphicObjectFactory>>>::iterator it_aux = factories.begin(); it_aux != factories.end(); ++it_aux){
        if(it_aux->first == type){
            for(map<string,unique_ptr<GraphicObjectFactory>>::iterator it = it_aux->second.begin(); it != it_aux->second.end(); ++it_aux){
                v.push_back(it->first);
                cout << it->first << "\n";
            }
        }
    }

    return  v;
}

unique_ptr<GraphicModel> GraphicObjectFactories::make_graphic(WiseCollection* obj, string factory){
    unique_ptr<GraphicModel> ob = factories[obj->type].operator[](factory)->create(obj);
    return ob;
}

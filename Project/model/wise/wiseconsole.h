﻿#ifndef WISECONSOLE_H
#define WISECONSOLE_H

#include <QObject>
#include <QString>
#include <QFile>
#include <QDir>

#include <vector>
#include <string>
#include <memory>
#include <iostream>

#include "wiseprojectfactory.h"
#include "wiseproject.h"
#include "wisepeer.h"

#include "helper/igufile.h"

#define VERSION "1.1"

#define WISE_CONSOLE_LOG_PATTERN "wise_console_log_%d.wmsgs"

namespace console {
    enum ConsoleMessageType{
        COMMAND,
        OUT,
        INPUT,
        ERROR,
        WARNING
    };

    QString console_message_type_to_QString(ConsoleMessageType type);
    QString console_message_type_to_signal(ConsoleMessageType type);
    ConsoleMessageType QString_to_console_message_type(QString type);
}

using namespace std;
using namespace console;
using namespace wise;

class WiseConsole : public QObject
{
Q_OBJECT
public:
    WiseConsole(QString console_file = "wise_console.wcsl", QObject* parent = nullptr);
    ~WiseConsole();

    bool operator==(WiseConsole* c);

    bool save(QString filename);

    bool save_msgs(QString filename);

    bool load(QString filename);

    TiXmlElement* to_XML();

    static vector<WiseConsole*> get_consoles();

    static vector<QObject*> get_qconsoles();

public slots:

    void run_cmd(QString command_line);

    void receiver(ConsoleMessageType type, QString msg);

    void receiver(QString type, QString msg);

private:

    int test_batch;

    bool test_blank_project();

    bool test_element_crud(string type, string example, int test_n);

    bool test_object_crud (string type, string example, int test_n);

    const unsigned int ID;

    static unsigned int CURR_ID;

    WiseProject*            project;
    bool                    has_project;
    QString                 console_file;

    WiseProjectFactory pjt_fctry;

    vector<pair<ConsoleMessageType,QString>> msgs;

    int last_msg;

    bool clear_on_update;

    static int console_max_size;

    static int console_max_show;

    static vector<WiseConsole*> consoles;
};

#endif // WISECONSOLE_H

#ifndef GRAPHICFACTORY_H
#define GRAPHICFACTORY_H

#include "wisecollection.h"
#include "graphicobject.h"
#include "graphicmodel.h"

#include <memory>
#include <map>

using namespace std;

class GraphicObjectFactory
{
public:
    enum MultipleViewType{
        MAXIMUM,
        MINIMUM,
        MEAN
    };

    GraphicObjectFactory(string name, string type);
    ~GraphicObjectFactory();

    string get_name();

    string get_type();

    virtual void set_fields(GraphicModel* model) = 0;

    virtual bool iterate(GraphicModel* model) = 0;

    unique_ptr<GraphicModel> create(WiseCollection* obj);

private:
    virtual unique_ptr<GraphicModel> create_graphic(WiseStructure* obj) = 0;

    const string type;
    const string name;
};

#endif // GRAPHICFACTORY_H

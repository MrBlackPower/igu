#include "wisestructure.h"

long int WiseStructure::CURR_ID = 0;
long int WiseStructure::CURR_N = 0;

WiseStructure::WiseStructure(string name): creation_time(QDateTime::currentDateTime()){
    doc = new TiXmlElement("WISE_STRUCTURE");
    params.structure_id = CURR_ID++;
    CURR_N++;

    assigned_type = "NONE";

    params.structure_key = igumath::random_key(10,KEY_ALPHABET);
    params.name = name;

    params.imported = false;
    params.import_type = IDK;
    params.import_filename = "UNTITLED";

    params.n_points         = 0;
    params.n_cells          = 0;
    params.n_lines          = 0;
    params.n_fields         = 0;


    set_default_fields();
}

WiseStructure::WiseStructure(string name, string structure_key, WiseLoadType load_type, string filename): creation_time(QDateTime::currentDateTime()){
    doc = new TiXmlElement("WISE_STRUCTURE");
    params.structure_id = CURR_ID++;
    CURR_N++;
    params.name = name;

    params.structure_key = structure_key;
    params.imported = true;
    params.import_type = load_type;
    params.import_filename = filename;

    params.n_points         = 0;
    params.n_cells          = 0;
    params.n_lines          = 0;
    params.n_fields         = 0;

    set_default_fields();
}


WiseStructure::~WiseStructure(){
    delete doc;
    CURR_N--;

    if(CURR_N == 0){
        CURR_ID = 0;
    }
}

bool WiseStructure::clear(bool deep_clean){
    params.n_points = 0;
    params.n_lines  = 0;
    params.n_cells  = 0;
    params.n_fields = 0;

    //point data
    points.clear();
    points_info.clear();
    point_data_matrix.clear();

    //cell data
    cells.clear();
    cells_info.clear();
    cell_data_matrix.clear();


    //line data
    lines.clear();
    lines_info.clear();
    line_data_matrix.clear();

    if(deep_clean){
        //field data
        fields.clear();
        field_data_matrix.clear();
    }
}

bool WiseStructure::clear_data(bool deep_clean){
    //point data
    points.clear();
    points_info.clear();
    point_data_matrix.clear();

    //cell data
    cells_info.clear();
    cell_data_matrix.clear();

    //line data
    lines_info.clear();
    line_data_matrix.clear();

    if(deep_clean){
        fields.clear();
        field_data_matrix.clear();
    }
}

void WiseStructure::set_default_fields(){

}

void WiseStructure::update_xml(){
    doc->Clear();

    TiXmlComment * comment;
    string s;

    comment = new TiXmlComment();
    s = " XML for Wise Object " + params.name + " ";
    comment->SetValue(s.c_str());

    doc->LinkEndChild( comment );

    TiXmlElement * header = new TiXmlElement("STRUCTURE_PARAMETERS");
    doc->LinkEndChild( header );

    TiXmlElement * structure = new TiXmlElement("STRUCTURE");
    doc->LinkEndChild( structure );

    TiXmlElement * data = new TiXmlElement("DATA");
    doc->LinkEndChild( data );

    string type(assigned_type.toStdString());

    //PARAMETERS - START

    /********************
     * Header
     ******************* */
    header->SetAttribute("NAME"            ,params.name.data());
    header->SetAttribute("STRUCTURE_ID"    ,params.structure_id);
    header->SetAttribute("STRUCTURE_KEY"   ,params.structure_key.data());
    header->SetAttribute("N_POINTS"        ,params.n_points);
    header->SetAttribute("N_CELLS"         ,params.n_cells);
    header->SetAttribute("N_LINES"         ,params.n_lines);
    header->SetAttribute("N_FIELDS"        ,params.n_fields);
    header->SetAttribute("MAX_X"           ,params.max.X());
    header->SetAttribute("MAX_Y"           ,params.max.Y());
    header->SetAttribute("MAX_Z"           ,params.max.Z());
    header->SetAttribute("MIN_X"           ,params.min.X());
    header->SetAttribute("MIN_Y"           ,params.min.Y());
    header->SetAttribute("MIN_Z"           ,params.min.Z());
    header->SetAttribute("IMPORTED"        ,(params.imported)?"1":"0");
    header->SetAttribute("IMPORT_TYPE"     ,(params.import_type));
    header->SetAttribute("IMPORT_FILENAME" ,params.import_filename.data());

    //PARAMETERS - END

    /********************
     * Structure
     ******************* */
    //Points

    TiXmlElement * points = new TiXmlElement( "POINTS" );

    for(int i = 0; i < this->points.size(); i++){
        TiXmlElement * p = new TiXmlElement( "POINT" );
        p->SetAttribute("ID",this->points.operator[](i).ID());
        p->SetAttribute("X",QString::asprintf("%.10f",this->points.operator[](i).X()).toStdString().data());
        p->SetAttribute("Y",QString::asprintf("%.10f",this->points.operator[](i).Y()).toStdString().data());
        p->SetAttribute("Z",QString::asprintf("%.10f",this->points.operator[](i).Z()).toStdString().data());

        points->LinkEndChild( p );
    }
    structure->LinkEndChild( points );

    //Line
    TiXmlElement * lines = new TiXmlElement( "LINES" );
    structure->LinkEndChild( lines );

    for(int i = 0; i < this->lines.size(); i++){
        TiXmlElement * p = new TiXmlElement( "LINE" );
        WiseLine l = this->lines.operator[](i);
        p->SetAttribute("ID",l.id);
        p->SetAttribute("A",l.a);
        p->SetAttribute("B",l.b);
        lines->LinkEndChild( p );
    }

    //Cell
    TiXmlElement * cells = new TiXmlElement( "CELLS" );
    structure->LinkEndChild( cells );

    for(int i = 0; i < this->cells.size(); i++){
        TiXmlElement * p = new TiXmlElement( "CELL" );
        cells->LinkEndChild( p );
        p->SetAttribute("ID",this->cells.operator[](i).id);

        vector<int> points  (this->cells.operator[](i).points);

        for(int j = 0; j < points.size(); j++){
            TiXmlElement * c = new TiXmlElement( "CELL_POINT" );
            p->LinkEndChild( c );
            c->SetAttribute("ID",points.operator[](j));
        }
    }

    /********************
     * Data
     ******************* */
    //point
    TiXmlElement * point_data = new TiXmlElement( "POINT_DATA" );
    data->LinkEndChild( point_data );

    for(int i = 0; i < this->points_info.size(); i++){
        TiXmlElement * p = new TiXmlElement( "INFO" );
        point_data->LinkEndChild( p );
        p->SetAttribute("NAME",points_info[i].name.data());
        p->SetAttribute("TYPE",wise_data_type_to_string(points_info[i].type).data());

        for(int j = 0; j < this->point_data_matrix.operator[](i).size(); j++){
            TiXmlElement * c = new TiXmlElement( "DATA" );
            p->LinkEndChild( c );

            stringstream s(point_data_matrix.operator[](i).operator[](j).data());
            string data;
            s >> data;
            c->SetAttribute("VALUE",data.data());
        }
    }

    //Cell
    TiXmlElement * cell_data = new TiXmlElement( "CELL_DATA" );
    data->LinkEndChild( cell_data );

    for(int i = 0; i < this->cells_info.size(); i++){
        TiXmlElement * p = new TiXmlElement( "INFO" );
        cell_data->LinkEndChild( p );
        p->SetAttribute("NAME",cells_info[i].name.data());
        p->SetAttribute("TYPE",wise_data_type_to_string(cells_info[i].type).data());

        for(int j = 0; j < this->cell_data_matrix.operator[](i).size(); j++){
            TiXmlElement * c = new TiXmlElement( "DATA" );
            p->LinkEndChild( c );

            stringstream s(cell_data_matrix.operator[](i).operator[](j).data());
            string data;
            s >> data;
            c->SetAttribute("VALUE",data.data());
        }
    }

    //Line
    TiXmlElement * line_data = new TiXmlElement( "LINE_DATA" );
    data->LinkEndChild( line_data );

    for(int i = 0; i < this->lines_info.size(); i++){
        TiXmlElement * p = new TiXmlElement( "INFO" );
        line_data->LinkEndChild( p );
        p->SetAttribute("NAME",lines_info[i].name.data());
        p->SetAttribute("TYPE",wise_data_type_to_string(lines_info[i].type).data());
        for(int j = 0; j < this->line_data_matrix.operator[](i).size(); j++){
            TiXmlElement * c = new TiXmlElement( "DATA" );
            p->LinkEndChild( c );

            stringstream s(line_data_matrix.operator[](i).operator[](j).data());
            string data;
            s >> data;
            c->SetAttribute("VALUE",data.data());
        }
    }

    //Field
    TiXmlElement * field = new TiXmlElement( "FIELD" );
    data->LinkEndChild( field );

    for(int i = 0; i < this->fields.size(); i++){
        TiXmlElement * p = new TiXmlElement( "INFO" );
        field->LinkEndChild( p );
        p->SetAttribute("NAME",get<0>(fields[i]).name.data());
        p->SetAttribute("DATA_TYPE",wise_data_type_to_string(get<0>(fields[i]).type).data());
        p->SetAttribute("TYPE",wise_field_type_to_string(get<2>(fields[i])).data());
        p->SetAttribute("FAMILY",wise_field_family_to_string(get<1>(fields[i])).data());
        for(int j = 0; j < this->field_data_matrix.operator[](i).size(); j++){
            TiXmlElement * c = new TiXmlElement( "DATA" );
            p->LinkEndChild( c );

            stringstream s(field_data_matrix.operator[](i).operator[](j).data());
            string data;
            s >> data;
            c->SetAttribute("VALUE",data.data());
        }
    }
}

string WiseStructure::to_VTK(){
    stringstream aux;

    aux << "# vtk DataFile Version 3.0" << endl;
    aux << "vtk output" << endl;
    aux << "ASCII" << endl;
    aux << "DATASET POLYDATA" << endl;

    aux << "POINTS " << points.size() << " double" << endl;

    for(point p : points)
        aux << p.X() << " " << p.Y() << " " << p.Z() << endl;

    aux << endl;

    aux << "LINES " << lines.size() << " " << (3 * lines.size()) << endl;

    for(WiseLine l : lines)
        aux << l.N << " " << l.a << " " << l.b << endl;

    aux << endl;

    aux << "CELLS " << cells.size() << " " << (((cells.empty())? 0 : cells[0].N + 1) * cells.size())  << endl;

    for(WiseCell l : cells){
        aux << l.N << " " ;

        for(int i : l.points)
            aux << i << " ";

        aux << endl;
    }

    aux << endl;

    if(!points_info.empty()){
        aux << "POINT_DATA " << points.size() << endl;

        for(int i = 0; i < points_info.size(); i ++){
            aux << "scalars " << points_info[i].name << " " << wise_data_type_to_string(points_info[i].type) << endl;
            aux << "LOOKUP_TABLE default" << endl;

            for(string s : point_data_matrix.operator [](i))
                aux << s << endl;
        }

        aux << endl;
    }

    if(!lines_info.empty()){
        aux << "LINE_DATA " << lines.size() << endl;

        for(int i = 0; i < lines_info.size(); i ++){
            aux << "scalars " << lines_info[i].name << " " << wise_data_type_to_string(lines_info[i].type) << endl;
            aux << "LOOKUP_TABLE default" << endl;

            for(string s : line_data_matrix.operator [](i))
                aux << s << endl;
        }

        aux << endl;
    }

    if(!cells_info.empty()){
        aux << "CELL_DATA " << cells.size() << endl;

        for(int i = 0; i < cells_info.size(); i ++){
            aux << "scalars " << cells_info[i].name << " " << wise_data_type_to_string(cells_info[i].type) << endl;
            aux << "LOOKUP_TABLE default" << endl;

            for(string s : cell_data_matrix.operator [](i))
                aux << s << endl;
        }

        aux << endl;

    }

    if(!fields.empty()){
        aux << "FIELDS " << fields.size() << endl;

        for(int i = 0; i < fields.size(); i ++){
            aux << "scalars " << get<0>(fields[i]).name << " " << wise_data_type_to_string(get<0>(fields[i]).type) << endl;
            aux << "LOOKUP_TABLE default" << endl;

            for(string s : field_data_matrix.operator [](i))
                aux << s << endl;
        }
    }

    return aux.str();
}

TiXmlElement* WiseStructure::to_XML(){
    return doc->Clone()->ToElement();
}

double WiseStructure::get_elapsed_time(){
    return (QDateTime::currentDateTime().toMSecsSinceEpoch() - creation_time.toMSecsSinceEpoch())/1000;
}

vector<pair<string,string>> WiseStructure::get_wise_cell_data(WiseCellType cell_type, int id){
    vector<pair<string,string>> aux;

    switch (cell_type) {
    case POINT:
        aux = get_point_data(id);
    break;
    case CELL:
        aux = get_cell_data(id);
    break;
    case LINE:
        aux = get_line_data(id);
    break;
    }

    return aux;
}

vector<pair<string,string>> WiseStructure::get_point_data(int id){
    vector<pair<string,string>> aux;

    if(id < points.size() && id>= 0){
        for(int i = 0; i < points_info.size(); i++){
            WiseInfo info = points_info.operator[](i);
            string data = point_data_matrix.operator[](i).operator[](id);

            aux.push_back(pair<string,string>(info.name,data));
        }
    }

    return aux;
}

vector<pair<string,string>> WiseStructure::get_cell_data(int id){
    vector<pair<string,string>> aux;

    if(id < cells.size() && id>= 0){
        for(int i = 0; i < cells_info.size(); i++){
            WiseInfo info = cells_info.operator[](i);
            string data = cell_data_matrix.operator[](i).operator[](id);

            aux.push_back(pair<string,string>(info.name,data));
        }
    }

    return aux;
}

vector<pair<string,string>> WiseStructure::get_line_data(int id){
    vector<pair<string,string>> aux;

    if(id < lines.size() && id>= 0){
        for(int i = 0; i < lines_info.size(); i++){
            WiseInfo info = lines_info.operator[](i);
            string data = line_data_matrix.operator[](i).operator[](id);

            aux.push_back(pair<string,string>(info.name,data));
        }
    }

    return aux;
}

vector<string> WiseStructure::get_data(WiseCellType cell_type,string info_name){
    switch (cell_type) {
    case POINT:{
        return get_point_data(info_name);
    }
    case CELL:{
        return get_cell_data(info_name);
    }
    case LINE:{
        return get_line_data(info_name);
    }
    case FIELD:{
        return get_field_data(info_name);
    }
    }
}

vector<string> WiseStructure::get_point_data(string info_name){
    vector<string> aux;

    if(points_info.empty())
        return aux;

    int id = 0;
    string it = points_info.operator[](id).name;
    bool found = (info_name == it);
    while (!found && id < (points_info.size() - 1)) {
        id++;
        it = points_info.operator[](id).name;
    }

    if(found && (id < point_data_matrix.size() && id>= 0)){
        for(int i = 0; i < point_data_matrix.operator[](id).size(); i++){
            string data = point_data_matrix.operator[](id).operator[](i);

            aux.push_back(data);
        }
    }

    return aux;
}

vector<string> WiseStructure::get_cell_data(string info_name){
    vector<string> aux;

    if(cells_info.empty())
        return aux;

    int id = 0;
    string it = cells_info.operator[](id).name;
    bool found = (info_name == it);
    while (!found && id < (cells_info.size() - 1)) {
        id++;
        it = cells_info.operator[](id).name;

        found = (info_name == it);
    }

    if(found && (id < cell_data_matrix.size() && id>= 0)){
        for(int i = 0; i < cell_data_matrix.operator[](id).size(); i++){
            string data = cell_data_matrix.operator[](id).operator[](i);

            aux.push_back(data);
        }
    }

    return aux;
}

vector<string> WiseStructure::get_line_data(string info_name){
    vector<string> aux;

    if(lines_info.empty())
        return aux;

    int id = 0;
    string it = lines_info.operator[](id).name;
    bool found = (info_name == it);
    while (!found && id < (lines_info.size() - 1)) {
        id++;
        it = lines_info.operator[](id).name;
    }

    if(found && (id < line_data_matrix.size() && id>= 0)){
        for(int i = 0; i < line_data_matrix.operator[](id).size(); i++){
            string data = line_data_matrix.operator[](id).operator[](i);

            aux.push_back(data);
        }
    }

    return aux;
}

vector<string> WiseStructure::get_field_data(string info_name){
    vector<string> aux;

    if(fields.empty())
        return aux;

    int id = 0;
    string it = get<0>(fields.operator[](id)).name;
    bool found = (info_name == it);
    while (!found && id < (fields.size() - 1)) {
        id++;
        it = get<0>(fields.operator[](id)).name;
    }

    if(found && (id < field_data_matrix.size() && id>= 0)){
        for(int i = 0; i < field_data_matrix.operator[](id).size(); i++){
            string data = field_data_matrix.operator[](id).operator[](i);

            aux.push_back(data);
        }
    }

    return aux;
}

WiseDataType WiseStructure::get_type(WiseCellType cell_type,string info_name){
    switch (cell_type) {
    case POINT:
        return get_point_type(info_name);
    case CELL:
        return get_cell_type(info_name);
    case LINE:
        return get_line_type(info_name);
    case FIELD:
        return get_field_type(info_name);
    }
}

WiseDataType WiseStructure::get_point_type(string info_name){

    if(points_info.empty())
        return UNKNOWN;

    int id = 0;
    string it = points_info.operator[](id).name;
    while (info_name != it && id < (points_info.size() - 1)) {
        id++;
        it = points_info.operator[](id).name;
    }

    if(id < point_data_matrix.size() && id>= 0){
        return points_info.operator[](id).type;
    }

    return UNKNOWN;
}

WiseDataType WiseStructure::get_cell_type(string info_name){
    if(cells_info.empty())
        return UNKNOWN;

    int id = 0;
    string it = cells_info.operator[](id).name;
    while (info_name != it && id < (cells_info.size() - 1)) {
        id++;
        it = cells_info.operator[](id).name;
    }

    if(id < cell_data_matrix.size() && id>= 0){
        return cells_info.operator[](id).type;
    }

    return UNKNOWN;
}

WiseDataType WiseStructure::get_line_type(string info_name){
    if(lines_info.empty())
        return UNKNOWN;

    int id = 0;
    string it = lines_info.operator[](id).name;
    while (info_name != it && id < (lines_info.size() - 1)) {
        id++;
        it = lines_info.operator[](id).name;
    }

    if(id < line_data_matrix.size() && id>= 0){
        return lines_info.operator[](id).type;
    }

    return UNKNOWN;
}

WiseDataType WiseStructure::get_field_type(string info_name){
    if(fields.empty())
        return UNKNOWN;

    int id = 0;
    string it = get<0>(fields.operator[](id)).name;
    while (info_name != it && id < (fields.size() - 1)) {
        id++;
        it = get<0>(fields.operator[](id)).name;
    }

    if(id < field_data_matrix.size() && id>= 0){
        return get<0>(fields.operator[](id)).type;
    }

    return UNKNOWN;
}

string WiseStructure::get_point_data(string info_name, int id){
    if(points_info.empty())
        return "";

    int id_aux = 0;
    string it = points_info.operator[](id_aux).name;
    while (info_name != it && id_aux < (points_info.size() - 1)) {
        id_aux++;
        it = points_info.operator[](id_aux).name;
    }

    if(id_aux < point_data_matrix.size() && id_aux>= 0){
        if(id < point_data_matrix.operator[](id_aux).size()){
            return point_data_matrix.operator[](id_aux).operator[](id);
        }
    }

    return "";
}

string WiseStructure::get_cell_data(string info_name, int id){
    if(cells_info.empty())
        return "";

    int id_aux = 0;
    string it = cells_info.operator[](id_aux).name;
    while (info_name != it && id_aux < (cells_info.size() - 1)) {
        id_aux++;
        it = cells_info.operator[](id_aux).name;
    }

    if(id_aux < cell_data_matrix.size() && id_aux>= 0){
        if(id < cell_data_matrix.operator[](id_aux).size()){
            return cell_data_matrix.operator[](id_aux).operator[](id);
        }
    }

    return "";
}
string WiseStructure::get_line_data(string info_name, int id){
    if(lines_info.empty())
        return "";

    int id_aux = 0;
    string it = lines_info.operator[](id_aux).name;
    while (info_name != it && id_aux < (lines_info.size() - 1)) {
        id_aux++;
        it = lines_info.operator[](id_aux).name;
    }

    if(id_aux < line_data_matrix.size() && id_aux>= 0){
        if(id < line_data_matrix.operator[](id_aux).size()){
            return line_data_matrix.operator[](id_aux).operator[](id);
        }
    }

    return "";
}

string WiseStructure::get_field_data(string info_name, int id){
    if(fields.empty())
        return "";

    int id_aux = 0;
    string it = get<0>(fields.operator[](id_aux)).name;
    while (info_name != it && id_aux < (fields.size() - 1)) {
        id_aux++;
        it = get<0>(fields.operator[](id_aux)).name;
    }

    if(id_aux < field_data_matrix.size() && id_aux >= 0){
        if(id < field_data_matrix.operator[](id_aux).size() && id >=0){
            return field_data_matrix.operator[](id_aux).operator[](id);
        }
    }

    return "";
}


WiseStructureParams* WiseStructure::get_params(){
    return &params;
}

int WiseStructure::get_points_size(){
    return points.size();
}

int WiseStructure::get_lines_size(){
    return lines.size();
}

int WiseStructure::get_cells_size(){
    return cells.size();
}

int WiseStructure::get_fields_size(){
    return fields.size();
}

pair<point,point> WiseStructure::get_max_min(){
    if(points.empty())
        return {0,0};

    point max = points.front();
    point min = points.front();

    for(int i = 1; i < points.size(); i++){
        if(max.X() < points[i].X())
            max.X(points[i].X());

        if(max.Y() < points[i].Y())
            max.Y(points[i].Y());

        if(max.Z() < points[i].Z())
            max.Z(points[i].Z());

        if(min.X() > points[i].X())
            min.X(points[i].X());

        if(min.Y() > points[i].Y())
            min.Y(points[i].Y());

        if(min.Z() > points[i].Z())
            min.Z(points[i].Z());
    }

    return {max,min};
}

double WiseStructure::get_max_size(){
    pair<point,point> p = get_max_min();

    double h = abs(p.first.X() - p.second.X());
    double w = abs(p.first.Y() - p.second.Y());
    double d = abs(p.first.Z() - p.second.Z());

    if ( h > w && h > d)
        return h;

    if ( w > h && w > d)
        return w;

    return d;
}

double WiseStructure::get_height(){
    pair<point,point> p = get_max_min();

    return abs(p.first.Y() - p.second.Y());
}

double WiseStructure::get_widht(){
    pair<point,point> p = get_max_min();

    return abs(p.first.X() - p.second.X());
}

double WiseStructure::get_depht(){
    pair<point,point> p = get_max_min();

    return abs(p.first.Z() - p.second.Z());
}

vector<point> WiseStructure::get_points(){
    vector<point> aux;

    for(point p : points){
        aux.push_back(p);
    }

    return aux;
}

vector<WiseCell> WiseStructure::get_cells(){
    vector<WiseCell> aux;

    for(WiseCell p : cells){
        aux.push_back(p);
    }

    return aux;
}

vector<WiseLine> WiseStructure::get_lines(){
    vector<WiseLine> aux;

    for(WiseLine p : lines){
        aux.push_back(p);
    }

    return aux;
}

vector<tuple<WiseInfo,WiseFieldFamily,WiseFieldType>> WiseStructure::get_fields(){
    vector<tuple<WiseInfo,WiseFieldFamily,WiseFieldType>> aux;

    for(tuple<WiseInfo,WiseFieldFamily,WiseFieldType> p : fields){
        aux.push_back(p);
    }

    return aux;
}

bool WiseStructure::set_wise_cell_info(WiseCellType cell_type, int id, string name, string data){
    bool aux;

    switch (cell_type) {
    case POINT:
        aux = set_point_data(id,name,data);
    break;
    case CELL:
        aux = set_cell_data(id,name,data);
    break;
    case LINE:
        aux = set_line_data(id,name,data);
    break;
    case FIELD:
        aux = set_field_data(id,name,data);
    break;
    }

    return aux;
}

bool WiseStructure::set_point_data(int id, string name, string data){
    if(id < lines.size() && id >= 0){
        for(int i = 0; i < points_info.size(); i++){
            WiseInfo info = points_info.operator[](i);

            if(info == name){
                point_data_matrix.operator[](i).operator[](id) = data;

                return true;
            }
        }
    }
    return false;
}

bool WiseStructure::set_cell_data(int id, string name, string data){
    if(id < lines.size() && id >= 0){
        for(int i = 0; i < cells_info.size(); i++){
            WiseInfo info = cells_info.operator[](i);

            if(info == name){
                cell_data_matrix.operator[](i).operator[](id) = data;

                return true;
            }
        }
    }
    return false;
}

bool WiseStructure::set_line_data(int id, string name, string data){
    if(id < lines.size() && id >= 0){
        for(int i = 0; i < lines_info.size(); i++){
            WiseInfo info = lines_info.operator[](i);

            if(info == name){
                line_data_matrix.operator[](i).operator[](id) = data;

                return true;
            }
        }
    }
    return false;
}

bool WiseStructure::set_field_data(int id, string name, string data){
    if(id < fields.size() && id >= 0){
        for(int i = 0; i < fields.size(); i++){
            WiseInfo info = get<0>(fields.operator[](i));

            if(info == name){
                if(field_data_matrix.operator[](i).size() > id){
                    field_data_matrix.operator[](i).operator[](id) = data;
                    return true;
                }

               return false;
            }
        }
    } else if (id < 0) {
        for(int i = 0; i < fields.size(); i++){
            WiseInfo info = get<0>(fields.operator[](i));

            if(info == name){
                if(field_data_matrix.operator[](i).size() == ONE){
                    field_data_matrix.operator[](i).operator[](ZERO) = data;
                    return true;
                }

               return false;
            }
        }
    }
    return false;
}

bool WiseStructure::set_all_point_data(string name, string data){
    for(int i = 0; i < points_info.size(); i++){
        WiseInfo info = points_info.operator[](i);

        if(info == name){
            for(int id = 0; id <  point_data_matrix.operator[](i).size(); id++)
                point_data_matrix.operator[](i).operator[](id) = data;

            return true;
        }
    }
    return false;
}
bool WiseStructure::set_all_cell_data(string name, string data){
    for(int i = 0; i < cells_info.size(); i++){
        WiseInfo info = cells_info.operator[](i);

        if(info == name){
            for(int id = 0; id <  cell_data_matrix.operator[](i).size(); id++)
                cell_data_matrix.operator[](i).operator[](id) = data;

            return true;
        }
    }

    return false;
}
bool WiseStructure::set_all_line_data(string name, string data){
    for(int i = 0; i < lines_info.size(); i++){
        WiseInfo info = lines_info.operator[](i);

        if(info == name){
            for(int id = 0; id <  line_data_matrix.operator[](i).size(); id++)
                line_data_matrix.operator[](i).operator[](id) = data;

            return true;
        }
    }

    return false;
}
bool WiseStructure::set_all_field_data(string name, string data){
    for(int i = 0; i < fields.size(); i++){
        WiseInfo info = get<0>(fields.operator[](i));

        if(info == name){
            for(int id = 0; id <  field_data_matrix.operator[](i).size(); id++){
                field_data_matrix.operator[](i).operator[](id) = data;
            }

           return true;
        }
    }

    return false;
}

bool WiseStructure::set_cell_type(int id, int type){
    if(id >= 0 && id < cells.size()){
        cells.operator[](id).type = type;
        return true;
    }
    return false;
}

void WiseStructure::set_max_min(double x, double y, double z){
    if(params.max.X() < params.min.X()){
        params.max = point(0,x,y,z);
        params.min = params.max;
        return;
    }

    //MAX
    if(x > params.max.X()){
        params.max.X(x);
    }
    if(y > params.max.Y()){
        params.max.Y(y);
    }
    if(z > params.max.Z()){
        params.max.Z(z);
    }

    //MIN
    if(x < params.min.X()){
        params.min.X(x);
    }
    if(y < params.min.Y()){
        params.min.Y(y);
    }
    if(z < params.min.Z()){
        params.min.Z(z);
    }
}

void WiseStructure::set_assigned_type(QString type){
    assigned_type = type;
}

bool WiseStructure::add_point(double x, double y, double z){
    points.emplace_back(point{int(points.size()),x,y,z});

    params.n_points++;
    set_max_min(x,y,z);

    return true;
}

bool WiseStructure::add_point(point p){
    points.emplace_back(point{int(points.size()),p.X(),p.Y(),p.Z()});

    params.n_points++;
    set_max_min(p.X(),p.Y(),p.Z());

    return true;
}

bool WiseStructure::add_point_info(string name, WiseDataType type){
    WiseInfo info;

    info.name = name;
    info.type = type;


    points_info.push_back(info);
    point_data_matrix.push_back( vector<string>() );

    return true;
}

bool WiseStructure::add_point_data(string info, string data, WiseAddMode mode){
    //Finds info
    int info_index = 0;

    if(points_info.empty())
        return false;

    while(info != points_info[info_index].name && info_index < points_info.size())
        info_index ++;

    //Reached the end of the vector and did not find the info
    if(info_index > points_info.size())
        return false;

    //adds data to selected vector
    if(point_data_matrix[info_index].size() < points.size())
        if(mode == BACK)
            point_data_matrix[info_index].push_back(data);
        else if (mode == FRONT)
            point_data_matrix[info_index].emplace(point_data_matrix[info_index].begin(),data);
    else
        return false;

    return true;
}

bool WiseStructure::add_line(int a, int b){
    WiseLine c;

    if(!(a < points.size()) && !( b < points.size()))
        return false;

    c.id = lines.size();
    c.a = a;
    c.b = b;

    lines.push_back(c);

    return true;
}

bool WiseStructure::add_line_info(string name, WiseDataType type){
    WiseInfo info;

    info.name = name;
    info.type = type;


    lines_info.push_back(info);
    line_data_matrix.push_back( vector<string>() );

    return true;
}

bool WiseStructure::add_line_data(string info, string data, WiseAddMode mode){
    if (mode == FRONT){
        int it = -1;
        for(int i  = 0; i < lines_info.size(); i++){
            if(lines_info[i].name == info){
                it = i;
                break;
            }
        }

        if(it != -1){
            line_data_matrix.operator[](it).insert(line_data_matrix.operator[](it).begin(),data);

            return true;
        }
    }
    if(mode == BACK){
        int it = -1;
        for(int i  = 0; i < lines_info.size(); i++){
            if(lines_info[i].name == info){
                it = i;
                break;
            }
        }

        if(it != -1){
            line_data_matrix.operator[](it).push_back(data);

            return true;
        }
    }
    return false;
}

bool WiseStructure::add_cell(vector<int> pts){
    if(pts.empty() or pts.size() == 1)
        return false;

    WiseCell c;

    c.N = pts.size();
    c.id = cells.size();

    for (int i = 0; i < pts.size(); i++) {
        c.points.push_back(pts[i]);
    }

    cells.push_back(c);

    return true;
}

bool WiseStructure::add_cell_info(string name, WiseDataType type){
    WiseInfo info;

    info.name = name;
    info.type = type;

    cells_info.push_back(info);
    cell_data_matrix.push_back( vector<string>() );

    return true;
}

bool WiseStructure::add_cell_data(string info, string data, WiseAddMode mode){
    //Finds info
    int info_index = 0;

    if(cells_info.empty())
        return false;

    while(info != cells_info[info_index].name && info_index < cells_info.size())
        info_index ++;

    //Reached the end of the vector and did not find the info
    if(info_index > cells_info.size())
        return false;

    //adds data to selected vector
    if(mode == BACK){
        cell_data_matrix[info_index].push_back(data);
    }else if (mode == FRONT){
        cell_data_matrix[info_index].emplace(cell_data_matrix[info_index].begin(),data);
    }

    return true;
}

bool WiseStructure::add_field(string name, WiseFieldFamily family, WiseDataType data_type, WiseFieldType type){
    if(has_field(name))
        return false;

    WiseInfo info;

    info.name = name;
    info.type = data_type;

    fields.push_back(tuple<WiseInfo,WiseFieldFamily,WiseFieldType>{info,family,type});
    field_data_matrix.push_back( vector<string>() );

    return true;

}

bool WiseStructure::add_field(string name, string data, WiseFieldFamily family, WiseDataType data_type, WiseFieldType type){
    if(has_field(name))
        return false;

    WiseInfo info;

    info.name = name;
    info.type = data_type;

    fields.push_back(tuple<WiseInfo,WiseFieldFamily,WiseFieldType>{info,family,type});
    field_data_matrix.push_back( vector<string>{data} );

    return true;
}

bool WiseStructure::add_field_data(string data, string info_name){
    //Finds info
    int info_index = 0;

    if(fields.empty())
        return false;

    while(info_name != get<0>(fields[info_index]).name && info_index < fields.size())
        info_index ++;

    //Reached the end of the vector and did not find the info
    if(info_index > fields.size())
        return false;

    //adds data to selected vector
    if(get<0>(fields[info_index]).type == VECTOR or get<0>(fields[info_index]).type == TENSOR){
        field_data_matrix[info_index].push_back(data);
    } else {
        field_data_matrix[info_index].clear();
        field_data_matrix[info_index].push_back(data);
    }

    return true;
}

bool WiseStructure::has_field(string name){
    for(tuple<WiseInfo,WiseFieldFamily,WiseFieldType> i : fields){
        if(get<0>(i).name == name)
            return true;
    }

    return false;
}

bool WiseStructure::field_set(string info_name){
    int id = 0;
    for(tuple<WiseInfo,WiseFieldFamily,WiseFieldType> i : fields){
        if(get<0>(i).name == info_name){
            if(!field_data_matrix.operator[](id).empty()){
                return true;
            } else {
                return false;
            }
        }
        id ++;
    }

    return false;
}

bool WiseStructure::field_set(string info_name, WiseDataType data_type){
    int id = 0;
    for(tuple<WiseInfo,WiseFieldFamily,WiseFieldType> i : fields){
        if(get<0>(i).name == info_name){
            if(!field_data_matrix.operator[](id).empty() && get<0>(i).type == data_type){
                return true;
            } else {
                return false;
            }
        }
        id ++;
    }

    return false;
}

bool WiseStructure::consistent(){
    if (!points.empty()){
        //POINT DATA

        if(!points_info.empty()){
            if(point_data_matrix.size() != points_info.size())
                return false;

            for(vector<string> data : point_data_matrix)
                if(data.size() != points.size())
                    return false;

        } else {
            if(!point_data_matrix.empty())
                return false;
        }

        if(!cells.empty()){
            //CELLS IDS

            //CELLS DATA
            if(!cells_info.empty()){
                if(cell_data_matrix.size() != cells_info.size())
                    return false;

                for(vector<string> data : cell_data_matrix)
                    if(data.size() != cells.size())
                        return false;

            } else {
                if(!cell_data_matrix.empty())
                    return false;
            }

        }

        if(!lines.empty()){
            //LINES IDS

            //LINE DATA
            if(!lines_info.empty()){
                if(line_data_matrix.size() != lines_info.size())
                    return false;

                for(vector<string> data : line_data_matrix)
                    if(data.size() != lines.size())
                        return false;

            } else {
                if(!line_data_matrix.empty())
                    return false;
            }
        }
    } else {
        if(!cells.empty())
            return false;

        if(!lines.empty())
            return false;
    }

    return true;
}

bool WiseStructure::has_parameter(QString parameter, WiseCellType cell_type){
    switch (cell_type) {
        case LINE:
        for(WiseInfo i : lines_info){
            if(i.name == parameter.toStdString())
                return true;
        }
        break;
        case CELL:
        for(WiseInfo i : cells_info){
            if(i.name == parameter.toStdString())
                return true;
        }
        break;
        case POINT:
        for(WiseInfo i : points_info){
            if(i.name == parameter.toStdString())
                return true;
        }
        break;
        case FIELD:
        for(tuple<WiseInfo,WiseFieldFamily,WiseFieldType> i : fields){
            if(get<0>(i).name == parameter.toStdString())
                return true;
        }
        break;
    }

    return false;
}

bool WiseStructure::scale_through(vector<pair<string,double>> points_scale, vector<pair<string,double>> lines_scale,
                               vector<pair<string,double>> cells_scale, vector<pair<string,double>> fields_scale){
    if(points_scale.empty())
        return false;

    //POINT DATA
    bool points_coordinates = false;
    vector<bool> scaled;
    for(int i = 0; i < get_points_size(); i++)
        scaled.push_back(false);

    for(pair<string,double> p : points_scale){
        if(p.first == "POINTS_COORDINATES"){

            for(point px : points)
                px * p.second;


            points_coordinates = true;
        } else {
            for(int i = 0; i < points_info.size(); i++){
                if(points_info.operator[](i).operator==(p.first)){
                    scaled.operator[](i) = true;

                    if(!scale_through(&point_data_matrix.operator[](i),p.second,points_info.operator[](i).type))
                        return false;
                }
            }
        }
    }

    if (!points_coordinates){
        return false;
    }

    //ERASES FIELDS NOT SCALED
    for(int i = 0; i < scaled.size(); i++){
        if(!scaled.operator[](i)){
            scaled.erase(scaled.begin() + i);
            points_info.erase(points_info.begin() + i);
            point_data_matrix.erase(point_data_matrix.begin() + i);

            i--;
        }
    }

    //CELL DATA
    vector<bool> cell_scaled;
    for(int i = 0; i < cells_info.size(); i++)
        scaled.push_back(false);

    for(pair<string,double> p : cells_scale){
        for(int i = 0; i < cells_info.size(); i++){
            if(cells_info.operator[](i).operator==(p.first)){
                scaled.operator[](i) = true;

                if(!scale_through(&cell_data_matrix.operator[](i),p.second,cells_info.operator[](i).type))
                    return false;
            }
        }
    }

    //ERASES FIELDS NOT SCALED
    for(int i = 0; i < cell_scaled.size(); i++){
        if(!cell_scaled.operator[](i)){
            cell_scaled.erase(cell_scaled.begin() + i);
            cells_info.erase(cells_info.begin() + i);
            cell_data_matrix.erase(cell_data_matrix.begin() + i);

            i--;
        }
    }

    //LINE DATA
    vector<bool> line_scaled;
    for(int i = 0; i < lines_info.size(); i++)
        scaled.push_back(false);

    for(pair<string,double> p : lines_scale){
        for(int i = 0; i < lines_info.size(); i++){
            if(lines_info.operator[](i).operator==(p.first)){
                scaled.operator[](i) = true;

                if(!scale_through(&line_data_matrix.operator[](i),p.second,lines_info.operator[](i).type))
                    return false;
            }
        }
    }

    //ERASES FIELDS NOT SCALED
    for(int i = 0; i < line_scaled.size(); i++){
        if(!line_scaled.operator[](i)){
            line_scaled.erase(line_scaled.begin() + i);
            lines_info.erase(lines_info.begin() + i);
            line_data_matrix.erase(line_data_matrix.begin() + i);

            i--;
        }
    }

    //FIELD DATA
    vector<bool> field_scaled;
    for(int i = 0; i < fields.size(); i++)
        scaled.push_back(false);

    for(pair<string,double> p : fields_scale){
        for(int i = 0; i < fields.size(); i++){
            if(get<0>(fields.operator[](i)).operator==(p.first)){
                scaled.operator[](i) = true;

                if(!scale_through(&field_data_matrix.operator[](i),p.second,get<0>(fields.operator[](i)).type))
                    return false;
            }
        }
    }

    //ERASES FIELDS NOT SCALED
    for(int i = 0; i < field_scaled.size(); i++){
        if(!field_scaled.operator[](i)){
            field_scaled.erase(field_scaled.begin() + i);
            fields.erase(fields.begin() + i);
            field_data_matrix.erase(field_data_matrix.begin() + i);

            i--;
        }
    }
}

void WiseStructure::scale_through(vector<tuple<WiseCellType,string,double>> scaling){
    if(scaling.empty())
        return;

    for(tuple<WiseCellType,string,double> t : scaling){
        switch(get<0>(t)){
        case wise::POINT:
            if(get<1>(t) == "POINTS_COORDINATES" || get<1>(t) == "points_coordinates"){

                for(int i = 0; i < points.size(); i++){
                    point px = points[i];
                    points[i] = px * get<2>(t);
                }
            } else {
                for(int i = 0; i < points_info.size(); i++){
                    if(points_info.operator[](i).operator==(get<1>(t))){
                        if(!scale_through(&point_data_matrix.operator[](i),get<2>(t),points_info.operator[](i).type))
                            return;
                    }
                }
            }
            break;
        case wise::LINE:
            for(int i = 0; i < lines_info.size(); i++){
                if(lines_info.operator[](i).operator==(get<1>(t))){
                    if(!scale_through(&line_data_matrix.operator[](i),get<2>(t),lines_info.operator[](i).type))
                        return;
                }
            }
            break;
        case wise::CELL:
            for(int i = 0; i < cells_info.size(); i++){
                if(cells_info.operator[](i).operator==(get<1>(t))){
                    if(!scale_through(&cell_data_matrix.operator[](i),get<2>(t),cells_info.operator[](i).type))
                        return;
                }
            }
            break;
        case wise::FIELD:
            for(int i = 0; i < fields.size(); i++){
                if(get<0>(fields.operator[](i)).operator==(get<1>(t))){
                    if(!scale_through(&field_data_matrix.operator[](i),get<2>(t),get<0>(fields.operator[](i)).type))
                        return;
                }
            }
            break;
        }
    }
}


bool WiseStructure::scale_through(vector<string>* values, double scale, WiseDataType type){
    stringstream ss;
    vector<string> new_values;
    switch (type) {
    case INT:
        for(int i = 0; i < values->size(); i++){
            int aux = QString(values->operator[](i).data()).toDouble();
            aux *= scale;
            new_values.push_back(QString::asprintf("%d",aux).toStdString());
        }
        break;
    case FLOAT:
        for(int i = 0; i < values->size(); i++){
            float aux = QString(values->operator[](i).data()).toDouble();
            aux *= scale;
            new_values.push_back(QString::asprintf("%f",aux).toStdString());
        }
        break;
    case DOUBLE:
        for(int i = 0; i < values->size(); i++){
            double aux = QString(values->operator[](i).data()).toDouble();
            aux *= scale;
            new_values.push_back(QString::asprintf("%f",aux).toStdString());
        }
        break;
    case VECTOR:
        for(int i = 0; i < values->size(); i++){
            ss << values->operator[](i) ;
            double x,y,z;

            ss >> x >> y >>z;
            x *= scale;
            y *= scale;
            z *= scale;
            ss.clear();

            ss << x << y << z;
            new_values.push_back(ss.str());
        }
        break;
    case TENSOR:
        for(int i = 0; i < values->size(); i++){
            ss << values->operator[](i) ;
            double x,y,z;
            double x1,y1,z1;
            double x2,y2,z2;

            ss >> x >> y >>z;
            ss >> x1 >> y1 >>z1;
            ss >> x2 >> y2 >>z2;
            x *= scale;
            y *= scale;
            z *= scale;
            x1 *= scale;
            y1 *= scale;
            z1 *= scale;
            x2 *= scale;
            y2 *= scale;
            z2 *= scale;
            ss.clear();

            ss << x << y << z;
            ss << x1 << y1 << z1;
            ss << x2 << y2 << z2;
            new_values.push_back(ss.str());
        }
        break;
    default:
        return false; // FALSE RETURNED ON PURPOSE !!!
    }

    values->clear();

    for(string str : new_values)
        values->emplace_back(str);

    return true;
}

void WiseStructure::operator=(WiseStructure* st){
    if(st->consistent()){
        clear(true);

        points = st->points;
        points_info = st->points_info;
        point_data_matrix = st->point_data_matrix;

        cells = st->cells;
        cells_info = st->cells_info;
        cell_data_matrix = st->cell_data_matrix;

        lines = st->lines;
        lines_info = st->lines_info;
        line_data_matrix = st->line_data_matrix;

        fields = st->fields;
        field_data_matrix = st->field_data_matrix;

        params = st->params;
    }
}

void WiseStructure::operator=(WiseStructure& st){
    if(st.consistent()){
        points = st.points;
        points_info = st.points_info;
        point_data_matrix = st.point_data_matrix;

        cells = st.cells;
        cells_info = st.cells_info;
        cell_data_matrix = st.cell_data_matrix;

        lines = st.lines;
        lines_info = st.lines_info;
        line_data_matrix = st.line_data_matrix;

        fields = st.fields;
        field_data_matrix = st.field_data_matrix;

        params = st.params;
    }
}

bool WiseStructure::operator==(WiseStructure& st){
    bool r = (params.n_points == st.params.n_points);
    r = r && (point_data_matrix.size() == st.point_data_matrix.size());
    r = r && (params.n_cells == st.params.n_lines);
    r = r && (line_data_matrix.size() == st.line_data_matrix.size());
    r = r && (params.n_cells == st.params.n_cells);
    r = r && (cell_data_matrix.size() == st.cell_data_matrix.size());
    r = r && (params.n_cells == st.params.n_fields);
    r = r && (field_data_matrix.size() == st.field_data_matrix.size());

    //POINTS
    for(int i = 0; i < params.n_points && r; i++){
        r = r && (points[i] == st.points[i]);
    }

    //CELLS
    for(int i = 0; i < params.n_cells && r; i++){
        r = r && (cells[i] == st.cells[i]);
    }

    //LINES
    for(int i = 0; i < params.n_points && r; i++){
        r = r && (points[i] == st.points[i]);
    }

    //DATA MATRIX
    //POINTS
    for(int i = 0; i < point_data_matrix.size() && r; i++){
        if(point_data_matrix[i].size() != st.point_data_matrix[i].size())
            return false;

        for(int j = 0;  j < point_data_matrix[i].size() && r; i++){
            r = r && point_data_matrix[i][j] == st.point_data_matrix[i][j];
        }
    }

    //CELLS
    for(int i = 0; i < cell_data_matrix.size() && r; i++){
        if(cell_data_matrix[i].size() != st.cell_data_matrix[i].size())
            return false;

        for(int j = 0;  j < cell_data_matrix[i].size() && r; i++){
            r = r && cell_data_matrix[i][j] == st.cell_data_matrix[i][j];
        }
    }
    //LINES
    for(int i = 0; i < line_data_matrix.size() && r; i++){
        if(line_data_matrix[i].size() != st.line_data_matrix[i].size())
            return false;

        for(int j = 0;  j < line_data_matrix[i].size() && r; i++){
            r = r && line_data_matrix[i][j] == st.line_data_matrix[i][j];
        }
    }
    //FIELDS
    for(int i = 0; i < field_data_matrix.size() && r; i++){
        if(field_data_matrix[i].size() != st.field_data_matrix[i].size())
            return false;

        for(int j = 0;  j < field_data_matrix[i].size() && r; i++){
            r = r && field_data_matrix[i][j] == st.field_data_matrix[i][j];
        }
    }


    return r;
}

string wise::wise_data_type_to_string(WiseDataType t){
    switch (t) {
    case INT:
        return "INT";
    case FLOAT:
        return "FLOAT";
    case DOUBLE:
        return "DOUBLE";
    case STRING:
        return "STRING";
   case VECTOR:
        return "VECTOR";
    case TENSOR:
        return "TENSOR";
    }

    return "UNKNOWN";
}

WiseDataType wise::wise_data_type_to_string(string t){
    if(t == "INT")
        return INT;
    else if (t == "DOUBLE")
        return DOUBLE;
    else if (t == "FLOAT")
        return FLOAT;
    else if (t == "STRING")
        return STRING;
    else if (t == "VECTOR")
        return VECTOR;
    else if (t == "TENSOR")
        return TENSOR;

    return UNKNOWN;
}

string wise::wise_field_type_to_string(WiseFieldType t){
    switch (t) {
    case INITIAL:
        return "INITIAL";
    case LIVE:
        return "LIVE";
    case OUT:
        return "OUT";
    }

    return "UNKNOWN";
}

WiseFieldType wise::wise_field_type_to_string(string t){
    if(t == "INITIAL")
        return INITIAL;
    else if (t == "LIVE")
        return LIVE;
    else if (t == "OUT")
        return OUT;

    return INITIAL;
}

string wise::wise_field_family_to_string(WiseFieldFamily t){
    switch (t) {
    case WISE:
        return "WISE";
    case ITERATION:
        return "ITERATION";
    case GRAPHIC:
        return "GRAPHIC";
    }

    return "UNKNOWN";
}

WiseFieldFamily wise::wise_field_family_to_string(string t){
    if(t == "WISE")
        return WISE;
    else if (t == "ITERATION")
        return ITERATION;
    else if (t == "GRAPHIC")
        return GRAPHIC;

    return WISE;
}



string wise::wise_cell_type_to_string(WiseCellType t){
    switch (t) {
    case POINT:
        return "POINT";
    case CELL:
        return "CELL";
    case LINE:
        return "LINE";
    case FIELD:
        return "FIELD";
    }

    return "UNKNOWN";
}

WiseCellType wise::wise_cell_type_to_string(string t){

    if(t == "POINT")
        return POINT;
    else if (t == "CELL")
        return CELL;
    else if (t == "LINE")
        return LINE;
    else if (t == "FIELD")
        return FIELD;

    return FIELD;
}

string wise::wise_load_type_to_string(WiseCellType t){
    switch (t) {
    case VTK:
        return "VTK";
    case XML:
        return "XML";
    case EXAMPLE:
        return "EXAMPLE";
    }

    return "IDK";
}

WiseLoadType wise::string_to_wise_load_type(string t){

    if(t == "VTK")
        return VTK;
    else if (t == "XML")
        return XML;
    else if (t == "EXAMPLE")
        return EXAMPLE;

    return IDK;
}

string wise::wise_object_status_to_string(WiseObjectStatus t){
    switch (t) {
    case READY:
        return "READY";
    case SET:
        return "SET";
    case GO:
        return "GO";
    case FINISHED:
        return "FINISHED";
    }

    return "FINISHED";
}

WiseObjectStatus wise::wise_object_status_to_string(string t){
    if(t == "READY")
        return READY;
    else if (t == "SET")
        return SET;
    else if (t == "GO")
        return GO;
    else if (t == "FINISHED")
        return FINISHED;

    return FINISHED;
}

string wise::wise_element_status_to_string(WiseElementStatus t){
    switch (t) {
    case WARMING:
        return "WARMING";
    case COOLING:
        return "COOLING";
    case HOT:
        return "HOT";
    case COLD:
        return "COLD";
    }

    return "UNKNOWN";

}

WiseElementStatus wise::wise_element_status_to_string(string t){
    if(t == "WARMING")
        return WARMING;
    else if (t == "COOLING")
        return COOLING;
    else if (t == "HOT")
        return HOT;
    else if (t == "COLD")
        return COLD;

    return COLD;
}

vector<vector<double>> wise::string_to_matrix(vector<string> vec, WiseDataType type){
    switch (type) {
    case DOUBLE:{
        vector<vector<double>> aux;
        for(auto v : igumath::string_to_double(vec))
            aux.push_back({v});
        return aux;
    }
    case INT:{
        vector<vector<double>> aux;
        for(auto v : igumath::string_to_double(vec))
            aux.push_back({(double)((int)v)});
        return aux;
    }
    case FLOAT:{
        vector<vector<double>> aux;
        for(auto v : igumath::string_to_double(vec))
            aux.push_back({v});
        return aux;
    }
    case VECTOR:{
        return igumath::string_to_vector(vec);
    }
    case TENSOR:{
        return igumath::string_to_tensor(vec);
    }
    default:{
        vector<vector<double>> aux;
        for(auto v : vec)
            aux.push_back({ZERO});
        return aux;
    }
    }
}

vector<double> wise::string_to_value(vector<string> vec, WiseDataType type){
    switch (type) {
    case DOUBLE:{
        return igumath::string_to_double(vec);
    }
    case INT:{
        vector<double> aux =  igumath::string_to_double(vec);

        for(int i = 0; i < aux.size(); i++){
            aux.operator[](i) = ((int) aux.operator[](i));
        }

    }
    case FLOAT:{
        return igumath::string_to_double(vec);
    }
    case VECTOR:{
        return igumath::matrix_mean(igumath::string_to_vector(vec));
    }
    case TENSOR:{
        return igumath::matrix_mean(igumath::string_to_tensor(vec));
    }
    default:{
        vector<double> aux;
        for(auto v : vec)
            aux.push_back(ZERO);
        return aux;
    }
    }
}

#ifndef WISEPROJECT_H
#define WISEPROJECT_H

#include <QString>
#include <vector>
#include <memory>

#include "tinyxml/tinyxml.h"

#include "wiseobject.h"
#include "wisepeer.h"

using namespace std;

class WiseProject : public WisePeer
{
    friend class WiseProjectFactory;

public:
    ~WiseProject();

    int id();
    
    QString get_name();
    void set_name(QString val);

    vector<pair<int,string>> get_models_list();

    vector<pair<int,string>> get_elements_list();

    bool operator==(WiseProject* obj);

    bool operator==(int id);

    //OBJECT HANDLING
    int         add_object(unique_ptr<WiseObject> model);
    WiseObject* pop();
    WiseObject* get_object(int id);
    int         get_object(string name);
    WiseObject* release_object(int id);
    bool        pop_object(int i);
    bool        delete_object(int i);
    WiseObject* operator[](int i);
    WiseObject* operator[](string name);


    //ELEMENT HANDLING
    int          add_element(unique_ptr<WiseElement> element);
    WiseElement* pop_element();
    WiseElement* get_element(int id);
    int          get_element(string name);
    bool         pop_element(int i);
    bool         delete_element(int i);
    WiseElement* element(int i);
    WiseElement* element(string name);

    unique_ptr<WiseElement> release_element(int id);

    WiseProject* next;
    WiseProject* previous;

    TiXmlElement* to_XML();

    static void create_project(QString name);

    static void edit_project(int id, QString name);

    static WiseProject* get_project(int id);

    static WiseProject* pop_project();

    static bool delete_project(int id);

    static int project_size();

    static bool has_project();
    
    static vector<pair<int,QString>> get_projects();

    static vector<tuple<int,QString,vector<QString>>> get_projects_and_models();

private:
    WiseProject(QString name);

    int ID;

    vector<unique_ptr<WiseObject>> models;
    
    vector<unique_ptr<WiseElement>> elements;

    static int CURR_ID;

    static WiseProject* first;
    static WiseProject* last;
};

#endif // WISEPROJECT_H

#ifndef WISEMODELFACTORY_H
#define WISEMODELFACTORY_H

#include <vector>
#include <string>
#include <memory>
#include <tuple>

#include "wiseobject.h"
#include "wiseelementfactory.h"
#include "wiseiterationfactories.h"
#include "graphicobjectfactories.h"

using namespace std;

class WiseObjectFactory
{
public:
    WiseObjectFactory(unique_ptr<WiseElementFactory> obj_factory);

    string get_type();

    vector<string> examples_list();

    unique_ptr<WiseObject> xml_load(TiXmlElement* doc);

    unique_ptr<WiseObject> clone(string name, WiseObject* obj);

    //Creates an empty vessel for the wise structure to be inserted
    virtual unique_ptr<WiseObject> make(string name, unique_ptr<WiseElement> element) = 0;
    virtual unique_ptr<WiseObject> make(string name, unique_ptr<WiseElement> hot, vector<unique_ptr<WiseElement>> freezer) = 0;

private:
    string type;

    unique_ptr<WiseElementFactory> element_factory;

    WiseIterationFactories iteration_factories;
};

#endif // WISEMODELFACTORY_H

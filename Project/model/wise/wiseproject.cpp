﻿
#include "wiseproject.h"

WiseProject* WiseProject::first = NULL;
WiseProject* WiseProject::last = NULL;


int WiseProject::CURR_ID = 0;

WiseProject::WiseProject(QString name) : WisePeer(name.toStdString(),"WISE_PROJECT"), ID(CURR_ID++)
{
    if(first == NULL){ //FIRST NODE
        first = this;
        last = this;

        next = NULL;
        previous = NULL;
    } else {
        last->next = this;
        previous = last;
        last = this;

        next = NULL;
    }

    send_msg(QString::asprintf("WISE_ID [%i] (NAME: %s) WISE PROJECT CREATED",ID,name.toStdString().data()));
}

WiseProject::~WiseProject(){
    if(first == this){//FIRST NODE
        first = next;
        if(next != NULL){
            next->previous = NULL;
        }
        if(last = this){//(ALSO REDUCES CURR_ID)
            last = NULL;
            CURR_ID = 0;
        }
    } else if(last == this){//LAST NODE (ALSO REDUCES CURR_ID)
        CURR_ID = ID;
        last = previous;
        if(previous != NULL){
            previous->next = NULL;
        }
    } else{//MIDDLE NODE
        if(next != NULL && previous!= NULL){
        previous->next = next;
        next->previous = previous;
        }
    }

    //send_msg(QString::asprintf("%i (%s) WISE PROJECT DELETED",ID,name.data()));
}

bool WiseProject::operator==(WiseProject* obj){
    return ID == obj->id();
}

bool WiseProject::operator==(int id){
    return ID == id;
}

TiXmlElement* WiseProject::to_XML(){
    TiXmlElement* root = new TiXmlElement("WISE_PROJECT");

    TiXmlComment* comment = new TiXmlComment();
    string s = " XML for Wise Project " + name + " ";
    comment->SetValue(s.c_str());

    root->LinkEndChild( comment );

    TiXmlElement * p = new TiXmlElement("PROJECT_PARAMS");

    //PARAMETERS - START

    p->SetAttribute("NAME",name.data());
    p->SetAttribute("ID",QString::asprintf("%d",ID).toStdString().data());

    root->LinkEndChild( p );
    //PARAMETERS - END

    TiXmlElement * elements = new TiXmlElement("ELEMENTS_LIST");
    root->LinkEndChild( elements );

    //ADDS ELEMENTS XMLS
    for(int i = 0; i < this->elements.size(); i++){
        WiseElement* e = this->elements[i].get();

         TiXmlElement * element = e->to_XML();
         elements->LinkEndChild(element);

    }

    TiXmlElement * objects = new TiXmlElement("OBJECTS_LIST");
    root->LinkEndChild( objects );

    //ADDS OBJECTS XMLS
    for(int i = 0; i < this->models.size(); i++){
        WiseObject* e = this->models[i].get();

         TiXmlElement * element = e->to_XML();
         objects->LinkEndChild(element);
    }

    return root;
}

int WiseProject::add_object(unique_ptr<WiseObject> model){
    //VERIFIES IF NAME IS UNIQUE
    for(int i = 0; i < models.size(); i++){
        if(models[i]->get_name() == model->get_name())
            return -1;
    }

    int r = model->ID();

    models.push_back(move(model));

    return r;
}

WiseObject* WiseProject::get_object(int id){
    for(int i = 0; i < models.size(); i++){
        if(models[i]->ID() == id){
            return models[id].get();
        }
    }

    return NULL;
}

WiseObject* WiseProject::release_object(int id){
    for(int i = 0; i < models.size(); i++){
        if(models[i]->ID() == id){
            WiseObject* r = models[id].release();

            models.erase(models.begin() + i);

            return r;
        }
    }

    return NULL;
}

int WiseProject::get_object(string name){
    for(int i = 0; i > models.size(); i++){
        if(models[i]->get_params()->name == name){
            return i;
        }
    }

    return 0;
}

WiseObject* WiseProject::pop(){
    if(models.empty())
        return NULL;

    return models[(models.size() -1)].get();
}

bool WiseProject::pop_object(int i){
    if(i >= 0 && i < models.size()){
        models.erase(models.begin() + i);

        return true;
    }

    return false;
}
bool WiseProject::delete_object(int i){
    for(int id = 0; id < models.size(); id++){
        if(models[id]->ID() == i){
            WiseObject* el = models[id].release();
            delete el;

            models.erase(models.begin() + id);
            return true;
        }
    }

    return false;
}

WiseObject* WiseProject::operator[](int i){
    if(i >= 0 && i < models.size()){
        WiseObject* m;
        m = models.operator[](i).get();

        return m;
    }

    return NULL;
}
WiseObject* WiseProject::operator[](string name){
    for(int i = 0; i < models.size(); i++){
        if(models[i]->get_params()->name == name){
            WiseObject* m;
            m = models.operator[](i).get();

            return m;
        }
    }

    return NULL;
}

int WiseProject::add_element(unique_ptr<WiseElement> element){
    //VERIFIES IF NAME IS UNIQUE
    for(int i = 0; i < elements.size(); i++){
        if(elements[i]->get_name() == element->get_name())
            return -1;
    }

    int r = element->ID();

    elements.push_back(move(element));

    return r;
}

WiseElement* WiseProject::pop_element(){
    if(elements.empty())
        return NULL;

    return elements[(elements.size() -1)].get();
}

WiseElement* WiseProject::get_element(int id){
    for(int i = 0; i < elements.size(); i++){
        if(elements[i]->ID() == id){
            return elements[id].get();
        }
    }

    return NULL;
}

unique_ptr<WiseElement> WiseProject::release_element(int id){
    for(int i = 0; i < elements.size(); i++){
        if(elements[i]->ID() == id){
            unique_ptr<WiseElement> r = move(elements[id]);

            elements.erase(elements.begin() + i);

            return move(r);
        }
    }

    return NULL;
}

int WiseProject::get_element(string name){
    for(int i = 0; i < elements.size(); i++){
        if(elements[i]->get_params()->name == name){
            return i;
        }
    }

    return -1;
}

bool WiseProject::pop_element(int i){
    if(i >= 0 && i < elements.size()){
        elements.erase(elements.begin() + i);

        return true;
    }

    return false;
}

bool WiseProject::delete_element(int i){
    for(int id = 0; id < elements.size(); id++){
        if(elements[id]->ID() == i){
            WiseElement* el = elements[id].release();
            delete el;

            elements.erase(elements.begin() + id);
            return true;
        }
    }

    return false;
}

WiseElement* WiseProject::element(int i){
    if(i >= 0 && i < elements.size()){
        WiseElement* m;
        m = elements.operator[](i).get();

        return m;
    }

    return NULL;
}

WiseElement* WiseProject::element(string name){
    for(int i = 0; i < elements.size(); i++){
        if(elements[i]->get_params()->name == name){
            WiseElement* m;
            m = elements.operator[](i).get();

            return m;
        }
    }

    return NULL;
}

void WiseProject::create_project(QString name){
    new WiseProject(name);
}

void WiseProject::edit_project(int id, QString name){
    WiseProject* it = first;
    while (it != NULL) {
        if(it->operator==(id)){
            it->set_name(name);
            return;
        }
        it = it->next;
    }
}

WiseProject* WiseProject::get_project(int id){
    WiseProject* it = first;
    while (it != NULL) {
        if(it->operator==(id)){
            return it;
        }
        it = it->next;
    }

    return NULL;
}

WiseProject* WiseProject::pop_project(){
    return last;
}

bool WiseProject::delete_project(int id){
    WiseProject* it = first;
    while (it != NULL) {
        if(it->operator==(id)){
            delete it;
            return true;
        }
        it = it->next;
    }

    return false;
}

int WiseProject::project_size(){
    return CURR_ID;
}

bool WiseProject::has_project(){
    if(last)
        return true;

    return false;
}

vector<pair<int,QString>> WiseProject::get_projects(){
    vector<pair<int,QString>> r;

    WiseProject* it = first;
    while (it != NULL) {
        pair<int,QString> p{it->id(),it->get_name()};
        r.push_back(p);
        it = it->next;
    }

    return r;
}

vector<tuple<int,QString,vector<QString>>> WiseProject::get_projects_and_models(){
    vector<tuple<int,QString,vector<QString>>> r;

    WiseProject* it = first;
    while (it != NULL) {
        tuple<int,QString,vector<QString>> p{it->id(),it->get_name(),vector<QString>()};

        for(int i = 0; i < it->models.size(); i++){
            QString s;
            s += QString::asprintf("%s %s",it->models.operator[](i).operator->()->dna().data(),it->models.operator[](i).operator->()->get_name().data());
            get<2>(p).push_back(s);
        }

        r.push_back(p);
        it = it->next;
    }

    return r;
}

int WiseProject::id(){
    return ID;
}

QString WiseProject::get_name(){
    return QString(name.data());
}

void WiseProject::set_name(QString val){
    name = val.toStdString();
}

vector<pair<int,string>> WiseProject::get_models_list(){
    vector<pair<int,string>> r;

    for(int i = 0; i < models.size(); i++){
        r.push_back({models[i]->ID(),models[i]->get_name()});
    }

    return r;
}

vector<pair<int,string>> WiseProject::get_elements_list(){
    vector<pair<int,string>> r;

    for(int i = 0; i < elements.size(); i++){
        r.push_back({elements[i]->ID(),elements[i]->get_name()});
    }

    return r;
}

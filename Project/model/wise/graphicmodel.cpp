#include "graphicmodel.h"
int GraphicModel::forward_frames = 10;

int GraphicModel::backward_frames = 10;

GraphicModel::GraphicModel(unique_ptr<GraphicObject> first_raw) : color_bar(vector<QColor>{QColor("Green"),QColor("Yellow"),QColor("Blue")})
{
    raw_drawing = -1;

    if(first_raw = NULL){
        delete this;
    } else {
        raw_list.push_back(move(first_raw));
    }
}

void GraphicModel::draw(){
    if(raw_drawing >= ZERO && raw_drawing < raw_list.size()){
        raw_list.operator[](raw_drawing)->draw(&color_bar);
    }
}

int GraphicModel::get_frames(){
    return raw_list.size();
}

void GraphicModel::next_frame(){
    if(raw_drawing + 1 < raw_list.size())
        raw_drawing ++;

}

void GraphicModel::previous_frame(){
    if(raw_drawing - 1 > ZERO)
        raw_drawing --;
}

bool GraphicModel::push_frame(unique_ptr<GraphicObject> raw){
    if(raw == NULL)
        return false;

    raw_list.push_back(move(raw));
}

void GraphicModel::set_frame(int frame){
    if(frame > ZERO && frame < raw_list.size())
        raw_drawing = frame;

}

GraphicObject* GraphicModel::pop(){
    return raw_list.back().get();
}

GraphicObject* GraphicModel::operator[](int i){
    if(i < 0 or i >= raw_list.size())
        return NULL;

    return raw_list.operator[](i).get();
}

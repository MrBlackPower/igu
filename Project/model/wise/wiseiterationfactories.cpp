#include "wiseiterationfactories.h"

WiseIterationFactories::WiseIterationFactories()
{
    vector<string> types = clone_factories.get_factories_names();

    for(string s : types)
        factories[s] = map<string,unique_ptr<WiseIterationFactory>>();

    unique_ptr<WiseIterationFactory> duan  = unique_ptr<WiseIterationFactory>{new DuanAndZamirIterationFactory()};
    unique_ptr<WiseIterationFactory> adi2d = unique_ptr<WiseIterationFactory>{new ADI2DIterationFactory()};
    unique_ptr<WiseIterationFactory> adi3d = unique_ptr<WiseIterationFactory>{new ADI3DIterationFactory()};
    unique_ptr<WiseIterationFactory> walk  = unique_ptr<WiseIterationFactory>{new GraphicWalkIterationFactory()};

    factories["ARTERY_TREE"]["DUAN_AND_ZAMIR"] = move(duan);
    factories["MESH"]["ADI_2D"]         = move(adi2d);
    factories["POLY"]["ADI_3D"]         = move(adi3d);
    factories["GRAPHIC"]["GRAPHIC_WALK"]   = move(walk);
}

WiseIterationFactories::~WiseIterationFactories()
{

}

vector<string> WiseIterationFactories::get_types(){
    vector<string> v;
    for(map<string, map<string,unique_ptr<WiseIterationFactory>>>::iterator it = factories.begin(); it != factories.end(); ++it) {
        v.push_back(it->first);
        cout << it->first << "\n";
    }

    return  v;
}

vector<string> WiseIterationFactories::get_factories_name(string type){
    vector<string> v;

    for(map<string,map<string,unique_ptr<WiseIterationFactory>>>::iterator it_aux = factories.begin(); it_aux != factories.end(); ++it_aux){
        if(it_aux->first == type){
            for(map<string,unique_ptr<WiseIterationFactory>>::iterator it = it_aux->second.begin(); it != it_aux->second.end(); ++it){
                v.push_back(it->first);
            }
        }
    }

    return  v;
}

unique_ptr<WiseIterationFactory> WiseIterationFactories::get_factory_clone(string type, string name){
    return factories[type][name]->clone();
}

bool WiseIterationFactories::iterate(WiseCollection* obj, string factory){
    obj->push_frame();

    bool ans = factories[obj->type].operator[](factory)->iterate(obj);

    obj->pop()->set_default_fields();

    return ans;
}

void WiseIterationFactories::set_fields(WiseCollection* obj, string factory){
    factories[obj->type].operator[](factory)->set_fields(obj);
}

#ifndef WISEPROJECTFACTORY_H
#define WISEPROJECTFACTORY_H

#include <QString>

#include "wiseproject.h"
#include "wiseiterationfactories.h"
#include "wiseobjectfactories.h"
#include "wiseelementfactories.h"

class WiseProjectFactory
{
public:
    unique_ptr<WiseProject> create_project (QString name);

    unique_ptr<WiseProject> create_project (TiXmlElement* doc);

    bool                    clone_element  (string name, string element_name, WiseProject* pjt);

    bool                    clone_object   (string name, string object_name, WiseProject* pjt);

    WiseElementFactories   element_factories;

    WiseObjectFactories    object_factories;

    WiseIterationFactories iteration_factories;

private:
};

#endif // WISEPROJECTFACTORY_H

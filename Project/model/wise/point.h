#ifndef POINT_H
#define POINT_H

#include "model/Eigen/Dense"

#define PI 3.14159265359

using namespace std;

using namespace Eigen;

namespace wisepoint {
    class point
    {
    public:
        struct point_raw{
            double x;
            double y;
            double z;
        };

        point(int id = 0);
        point(int id, double x, double y, double z = 0.0);

        ~point();

        int ID();

        double X();
        double Y();
        double Z();

        void X(double x);
        void Y(double y);
        void Z(double z);

        double operator[](int i);

        void operator=(int c);
        void operator=(float c);
        void operator=(double c);
        void operator=(point c);
        void operator=(point* c);

        bool operator==(point& p);
        bool operator==(point* p);

        point& operator+( int c );
        point& operator+( float c );
        point& operator+( double c );
        point& operator+( point c );

        point& operator-( int c );
        point& operator-( float c );
        point& operator-( double c );
        point& operator-( point c );


        point& operator*( int c );
        point& operator*( float c );
        point& operator*( double c );
        point& operator*( point c );

        point& operator/( int c );
        point& operator/( float c );
        point& operator/( double c );

        bool operator<( point b);
        bool operator>( point b);

        point::point_raw raw();

        double abs();
        double get_phi();
        double get_theta();

    private:
        int point_id;

        static int CURR_ID;
        static int point_N;
        int id;

        double x;
        double y;
        double z;
    };
};

#endif // POINT_H

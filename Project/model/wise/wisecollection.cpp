#include "wisecollection.h"

int WiseCollection::CURR_ID = 0;
int WiseCollection::collection_N = 0;
WiseCollection* WiseCollection::first_object = 0;
WiseCollection* WiseCollection::last_object = 0;

WiseCollection::WiseCollection(unique_ptr<WiseElement> hot_object ,vector<unique_ptr<WiseElement>> cold_objects, unique_ptr<WiseElementFactory> obj_factory){
    params.ID = CURR_ID++;
    params.obj_factory = (move(obj_factory));

    for(int i = 0; i < cold_objects.size(); i++)
        params.freezer.push_back(move(cold_objects.operator[](i)));

    params.oven = move(hot_object);
    params.N    = params.freezer.size();

    params.time_list.push_back(QDateTime::currentDateTime());
    params.current_raw = (params.freezer.end()->get());

    collection_N ++;

    if(first_object == NULL){
        first_object = this;
        last_object = this;

        next = NULL;
        previous = next;
    } else {
        last_object->next = this;
        previous = last_object;

        last_object = this;
    }
}

WiseCollection::WiseCollection(unique_ptr<WiseElement> hot_object ,unique_ptr<WiseElement> cold_object, unique_ptr<WiseElementFactory> obj_factory)
{
    params.ID          = CURR_ID++;
    params.current_raw = (cold_object.get());
    params.obj_factory = (move(obj_factory));
    params.freezer     .push_back(move(cold_object));
    params.oven        = move(hot_object);
    params.time_list   .push_back(QDateTime::currentDateTime());
    params.N           = params.freezer.size();

    next = NULL;
    previous = NULL;

    collection_N ++;

    if(first_object == NULL){
        first_object = this;
        last_object = this;

        next = NULL;
        previous = next;
    } else {
        last_object->next = this;
        previous = last_object;

        last_object = this;
    }
}

WiseCollection* WiseCollection::first(){
    return first_object;
}

WiseCollection* WiseCollection::last(){
    return last_object;
}

WiseCollection* WiseCollection::obj(int id){
    WiseCollection* it = first_object;
    while(it != NULL){
        if(it->operator==(id))
            return it;

        it = it->next;
    }

    return NULL;
}

WiseCollection::~WiseCollection()
{
    collection_N --;

    if(first_object == this){
        if(last_object == this){
            first_object = NULL;
            last_object = NULL;

            CURR_ID = 0;
        } else {
            first_object = next;
        }
    } else {
        previous->next = next;

        if(next != NULL){
            next->previous = previous;
        } else {
            CURR_ID --;
        }
    }
}


bool WiseCollection::push_frame(){
    unique_ptr<WiseElement> frame = params.obj_factory->clone("",params.oven.get(),COLD);

    if(frame){
        if(frame->status() == COLD){
            params.current_raw = frame.get();

            params.freezer.push_back(move(frame));
            params.time_list.push_back(QDateTime::currentDateTime());

            params.N ++;

            return true;
        }
    }

    return false;
}

bool WiseCollection::operator==(int id){
    return params.ID == id;
}

WiseElement* WiseCollection::operator[](int i){
    if(i >= 0 && i < params.freezer.size())
        return params.freezer.operator[](i).get();

    return NULL;
}

WiseElement* WiseCollection::operator[](double i){
    stringstream t;
    double time;
    t << params.current_raw->get_field("MODEL_TIME");
    t >> time;
    if(i >= 0 && i < time){
        for(int j = 0; j < params.freezer.size(); i++){

            stringstream t2;
            double c_time;
            t2 << params.freezer.operator[](j)->get_field("MODEL_TIME");
            t2 >> c_time;

            if(c_time <= i)
                return params.freezer.operator[](j).get();
        }
    }

    return NULL;
}

WiseElement* WiseCollection::pop(){
    return params.oven.get();
}

unique_ptr<WiseElement> WiseCollection::pop_hot(){
    if(params.oven.get() == NULL)
        return NULL;

    return move(params.oven);
}

bool WiseCollection::place_hot(unique_ptr<WiseElement> hot){
    if(hot.get() == NULL)
        return false;

    params.oven = move(hot);

    return true;
}


int WiseCollection::id(){
    return params.ID;
}

TiXmlElement * WiseCollection::to_XML(){
    TiXmlComment * comment;
    string s;

    TiXmlElement * root = new TiXmlElement("WISE_COLLECTION");

    TiXmlElement * p = new TiXmlElement("COLLECTION_PARAMS");
    root->LinkEndChild( p );

    //PARAMETERS - START

    p->SetAttribute("ID"          ,QString::asprintf("%d",params.ID).toStdString().data());
    p->SetAttribute("N"           ,QString::asprintf("%d",params.N).toStdString().data());

    //PARAMETERS - END


    //OVEN

    TiXmlElement * oven_element = new TiXmlElement("WISE_OVEN");
    root->LinkEndChild( oven_element );

    TiXmlElement* e_root = params.oven->to_XML();
    oven_element->LinkEndChild(e_root);

    //FREEZER

    TiXmlElement * freezer_element = new TiXmlElement("WISE_FREEZER");
    root->LinkEndChild( freezer_element );

    for(int i = 0; i < params.freezer.size(); i++){

        TiXmlElement* f_root = params.freezer[i]->to_XML();
        freezer_element->LinkEndChild(f_root);

    }

    return root;
}

void WiseCollection::scale_through(vector<tuple<WiseCellType,string,double>> scaling){
    if(params.oven.get() == NULL)
        return;

    params.oven->scale_through(scaling);
}


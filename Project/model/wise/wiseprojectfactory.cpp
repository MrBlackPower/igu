#include "wiseprojectfactory.h"

unique_ptr<WiseProject> WiseProjectFactory::create_project(QString name)
{
    return unique_ptr<WiseProject>{new WiseProject(name)};
}

unique_ptr<WiseProject> WiseProjectFactory::create_project(TiXmlElement* doc)
{
    //LOADS DOCUMENT
    unique_ptr<WiseProject> pt;
    TiXmlHandle hRoot(0);
    TiXmlElement* subElem = NULL;
    TiXmlElement* subsubElem = NULL;

    // block: WISE_PROJECT
    // should always have a valid root but handle gracefully if it does
    if(doc){
        //ACHEI a Tag
        QString name;
        bool still_gracefull = true;

        // save this for later
        hRoot = TiXmlHandle(doc);

        // block: PROJECT_PARAMS
        subElem = hRoot.FirstChild( "PROJECT_PARAMS" ).Element();

        if(!subElem){
            still_gracefull = false;
        } else {
            name = subElem->Attribute("NAME");
        }

        pt = unique_ptr<WiseProject>{new WiseProject(name)};

        // block: ELEMENTS
        subElem = hRoot.FirstChild( "ELEMENTS_LIST" ).Element();

        if(!subElem){
            still_gracefull = false;
        } else {
            //subblock: WISE_ELEMENTS
            for( subsubElem = subElem->FirstChildElement("WISE_ELEMENT"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
                unique_ptr<WiseElement> ptr = element_factories.make_xml(subsubElem);

                if(ptr != NULL)
                    pt->add_element(move(ptr));
            }
        }

        // block: OBJECTS
        subElem = hRoot.FirstChild( "OBJECTS_LIST" ).Element();

        if(!subElem){
            still_gracefull = false;
        } else {
            //subblock: WISE_OBJECTS
            for( subsubElem = subElem->FirstChildElement("WISE_OBJECT"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
                pt->add_object(object_factories.make_xml(subsubElem));
            }
        }


        if(!still_gracefull){
            pt->models.clear();
            pt->elements.clear();

            return NULL;
        }

        return move(pt);
    }

    return NULL;
}

bool WiseProjectFactory::clone_element(string name, string element_name, WiseProject* pjt){
    WiseElement* el = pjt->element(element_name);

    if(el == NULL)
        return false;

    if(name == ""){
        name = QString(element_name.data()).toStdString();
        name = (QString(name.data()).contains("_i"))? QString::asprintf("%s%s%d",QString(name.data()).split("_i").front().toStdString().data(),"_i",el->get_params()->instance+1).toStdString()
                                    : QString::asprintf("%s%s%d",name.data(),"_i",el->get_params()->instance+1).toStdString();

    }

    unique_ptr<WiseElement> ptr = element_factories.clone(name,el,wise::HOT);

    if(ptr.get() == NULL)
        return false;

    //DELETES ELEMENT EQUALLY NAMED
    int has_el =pjt->get_element(ptr->get_name());
    if(has_el >= 0){
        pjt->delete_element(has_el);
    }

    pjt->add_element(move(ptr));

    return true;
}

bool WiseProjectFactory::clone_object(string name, string object_name, WiseProject* pjt){
    WiseObject* el = pjt->operator[](object_name);

    if(el == NULL)
        return false;

    if(name == ""){
        name = QString(object_name.data()).toStdString();
        name = (QString(name.data()).contains("_i"))? QString::asprintf("%s%s%d",QString(name.data()).split("_i").front().toStdString().data(),"_i",el->ID()).toStdString()
                                    : QString::asprintf("%s%s%d",name.data(),"_i",el->ID()).toStdString();
    }

    unique_ptr<WiseObject> ptr = object_factories.clone(el);

    if(ptr.get() == NULL)
        return false;

    //DELETES ELEMENT EQUALLY NAMED
    int has_el =pjt->get_object(ptr->get_name());
    if(has_el >= 0){
        pjt->delete_element(has_el);
    }

    pjt->add_object(move(ptr));

    return true;
}

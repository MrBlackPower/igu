#ifndef WISEMODELFACTORIES_H
#define WISEMODELFACTORIES_H

#include "model/wise/wiseobjectfactory.h"
#include "model/wise/wiseelementfactories.h"
#include "model/wiseobjectfactory/arterytreewiseobjectfactory.h"
#include "model/wiseobjectfactory/graphicwiseobjectfactory.h"
#include "model/wiseobjectfactory/meshwiseobjectfactory.h"
#include "model/wiseobjectfactory/polywiseobjectfactory.h"

#include <memory>
#include <string>


using namespace std;

class WiseObjectFactories
{
public:
    WiseObjectFactories();

    vector<string> get_types();

    unique_ptr<WiseObject> make_xml(TiXmlElement* doc);

    unique_ptr<WiseObject> make_aus_element(string name, WiseElement* element);

    unique_ptr<WiseObject> make_aus_element(string name, unique_ptr<WiseElement> element);

    unique_ptr<WiseObject> clone(WiseObject* obj);

    bool has_type(string type);
private:
    map<string,unique_ptr<WiseObjectFactory>> factories;

    WiseElementFactories element_factories;
};

#endif // WISEMODELFACTORIES_H

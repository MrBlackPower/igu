#include "graphicobject.h"

unsigned int GraphicObject::CURR_ID = 0;

GraphicObject::GraphicObject(WiseStructure* structure, QString parameter, WiseCellType parameter_cell_type){
    params.ID = CURR_ID++;
    params.wise_structure = structure;
    params.QPainter_set = false;

    params.graphic_status = graphic::COLD;
}

GraphicObject::~GraphicObject(){

}

bool GraphicObject::add_field(string name,WiseFieldFamily family, WiseDataType type, WiseFieldType field_type){
    return params.wise_structure->add_field(name,family,type,field_type);
}

bool GraphicObject::add_field(string name, string data, WiseFieldFamily family, WiseDataType type, WiseFieldType field_type){
    return params.wise_structure->add_field(name,data,family,type,field_type);
}

void GraphicObject::draw(ColorBar *color_bar){
    double elapsed;
    QElapsedTimer timer;
    timer.start();

    //DRAWS OBJECT
    glPushMatrix();
    glRotatef(params.rotation.Z(), 0.0, 0.0, 1.0);
    glRotatef(params.rotation.Y(), 0.0, 1.0, 0.0);
    glRotatef(params.rotation.X(), 1.0, 0.0, 0.0);
    glScalef(params.scale,params.scale,params.scale);
    glTranslatef(params.translate.X(),params.translate.Y(),params.translate.Z());
        draw_guide_lines();
        for(map<string,vector<unique_ptr<GraphicElement>>>::iterator it = params.elements.begin(); it != params.elements.end(); ++it) {
          for(int i = 0; i < it->second.size(); i++){
              it->second.operator[](i)->draw(color_bar);
          }
        }
    glPopMatrix();

    if(params.guide_lines_status[6]){
        QColor c;
        switch (params.wise_element->status()) {
            //SETS BORDER COLOR ACCORDING TO ELEMENT STATUS
        }
        //SETS BORDER WIDTH
        glLineWidth(5.0);

        glBegin(GL_LINES);
        glVertex3f(-0.8,-0.8,8.0);
        glVertex3f(0.8,-0.8,8.0);

        glVertex3f(0.8,-0.8,8.0);
        glVertex3f(0.8,0.8,8.0);

        glVertex3f(0.8,0.8,8.0);
        glVertex3f(-0.8,0.8,8.0);

        glVertex3f(-0.8,0.8,8.0);
        glVertex3f(-0.8,-0.8,8.0);
        glEnd();
        //RETURNS WIDTH TO DEFAULT VALUE
        glLineWidth(1.0);
    }
    if(params.guide_lines_status[8]){
        point start = point(1,0.75,0.4,8.0);
        point end = point(1,0.7,-0.4,8.0);
        QColor c;
        GraphicElement::set_material_color(c.fromRgb(0.0,0.0,0.0));
        for(int i = 0; i < 100; i++){
            double pct = (double)i/100.0;
            double pct_1 = (double)(i + 1)/100.0;
            double size = (color_bar->get_max() - color_bar->get_min() > 0) ? color_bar->get_max() - color_bar->get_min() : 1.0;
            if(size != 1.0){
                GraphicElement::set_material_color(color_bar->interpolate_intensity(color_bar->get_min() + (pct * size)));
            } else {
                GraphicElement::set_material_color(color_bar->interpolate_intensity((color_bar->get_min() + (pct * (color_bar->get_max() - color_bar->get_min())))));
            }
            glBegin(GL_QUADS);
                point start_i = point(1,start.X(),((start.Y() * pct) + (end.Y() * (1-pct))),start.Z());
                point end_i = point(1,end.X(),((start.Y() * pct_1) + (end.Y() * (1-pct_1))),start.Z());
                glVertex3f(start_i.X(),start_i.Y(),start.Z());
                glVertex3f(start_i.X(),end_i.Y(),start.Z());
                glVertex3f(end_i.X(),end_i.Y(),start.Z());
                glVertex3f(end_i.X(),start_i.Y(),start.Z());
            glEnd();
        }
        glBegin(GL_LINES);

        glVertex3f(start.X(),start.Y(),start.Z());
        glVertex3f(start.X(),end.Y(),start.Z());

        glVertex3f(start.X(),end.Y(),start.Z());
        glVertex3f(end.X(),end.Y(),start.Z());

        glVertex3f(end.X(),end.Y(),start.Z());
        glVertex3f(end.X(),start.Y(),start.Z());

        glVertex3f(end.X(),start.Y(),start.Z());
        glVertex3f(start.X(),start.Y(),start.Z());

        glEnd();
    }

    //DRAWS TEXT INFO
    if(params.QPainter_set){
        //DRAW TITLE
        point p = point(1,-0.9625,0.925);
        draw_text(&p,params.wise_structure->get_params()->name.data());

        //DRAW LIMITS
        QString x;
        QString y;
        QString z;
        x = x.asprintf("x in [%.4f,%.4f]",params.min.X(),params.max.X());
        y = y.asprintf("y in [%.4f,%.4f]",params.min.Y(),params.max.Y());
        z = z.asprintf("z in [%.4f,%.4f]",params.min.Z(),params.max.Z());
        p.Y(p.Y() - LINE_SPACING);
        draw_text(&p,x);
        p.Y(p.Y() - LINE_SPACING);
        draw_text(&p,y);
        p.Y(p.Y() - LINE_SPACING);
        draw_text(&p,z);

        //DRAW CLOCK
        QString aux;
//        aux = aux.asprintf("FPS = %.4f - [%.4f,%.4f,%.4f,%.4f]",wise_structure->fps,wise_structure->frame_time[0],wise_structure->frame_time[1],wise_structure->frame_time[2],wise_structure->frame_time[3],wise_structure->frame_time[4]);
//        p.Y(p.Y() - LINE_SPACING);
//        draw_text(&p,aux);
//        aux = aux.asprintf("IPS = %.4f - [%.4f,%.4f,%.4f,%.4f]",wise_structure->ips,wise_structure->iteration_time[0],wise_structure->iteration_time[1],wise_structure->iteration_time[2],wise_structure->iteration_time[3],wise_structure->iteration_time[4]);
//        p.Y(p.Y() - LINE_SPACING);
//        draw_text(&p,aux);
//        aux = aux.asprintf("Elapsed Time = %.4f s",wise_structure->get_elapsed_time());
//        p.Y(p.Y() - LINE_SPACING);
//        draw_text(&p,aux);
        aux = aux.asprintf("Model Time = %.4f s",params.wise_element->get_params()->model_time);
        p.Y(p.Y() - LINE_SPACING);
        draw_text(&p,aux);
        aux = aux.asprintf("Frames = %d",params.frame);
        p.Y(p.Y() - LINE_SPACING);
        draw_text(&p,aux);
        aux = aux.asprintf("Iterations = %d",params.wise_element->get_params()->instance);
        p.Y(p.Y() - LINE_SPACING);
        draw_text(&p,aux);
        aux = aux.asprintf("Time Step = %.4f s",params.wise_element->get_params()->dt);
        p.Y(p.Y() - LINE_SPACING);
        draw_text(&p,aux);

        //DRAW RANGE
        p = point(1,0.85,0.55);
        draw_text(&p,params.parameter.data());
        p.X(0.84);
        p.Y(p.Y() - LINE_SPACING);
        draw_text(&p,aux.asprintf("%.4f",color_bar->get_max()));
        p = point(1,0.84,-0.52);
        draw_text(&p,aux.asprintf("%.4f",color_bar->get_min()));
    }

    elapsed = (double)timer.elapsed()/1000;
//    int id = params.frame % 5;
//    wise_structure->frame_time[id] = elapsed;
//    wise_structure->set_frames(wise_structure->get_params().frame + 1);
}

void GraphicObject::update_xml(){
    vector<string> v;
    TiXmlElement* root = new TiXmlElement("GRAPHIC_OBJECT");
    root->SetAttribute("SCALE",params.scale);

    root->SetAttribute("MAX_X",params.max[0]);
    root->SetAttribute("MAX_Y",params.max[1]);
    root->SetAttribute("MAX_Z",params.max[2]);

    root->SetAttribute("MIN_X",params.min[0]);
    root->SetAttribute("MIN_Y",params.min[1]);
    root->SetAttribute("MIN_Z",params.min[2]);

    root->SetAttribute("TRANSLATE_X",params.translate[0]);
    root->SetAttribute("TRANSLATE_Y",params.translate[1]);
    root->SetAttribute("TRANSLATE_Z",params.translate[2]);

    root->SetAttribute("ROTATION_X",params.rotation[0]);
    root->SetAttribute("ROTATION_Y",params.rotation[1]);
    root->SetAttribute("ROTATION_Z",params.rotation[2]);

    root->SetAttribute("GUIDE_LINE_0",params.guide_lines_status[0]);
    root->SetAttribute("GUIDE_LINE_1",params.guide_lines_status[1]);
    root->SetAttribute("GUIDE_LINE_2",params.guide_lines_status[2]);
    root->SetAttribute("GUIDE_LINE_3",params.guide_lines_status[3]);
    root->SetAttribute("GUIDE_LINE_4",params.guide_lines_status[4]);
    root->SetAttribute("GUIDE_LINE_5",params.guide_lines_status[5]);
    root->SetAttribute("GUIDE_LINE_6",params.guide_lines_status[6]);
    root->SetAttribute("GUIDE_LINE_7",params.guide_lines_status[7]);
    root->SetAttribute("GUIDE_LINE_8",params.guide_lines_status[8]);

    //ELEMENTS BY TYPE
    for(map<string,vector<unique_ptr<GraphicElement>>>::iterator it = params.elements.begin(); it != params.elements.end(); ++it) {
      TiXmlElement* element_type = new TiXmlElement((it->first + "_ELEMENTS").data());
      root->LinkEndChild(element_type);

      element_type->SetAttribute("QUANTITY", it->second.size());

      for(int i = 0; i < it->second.size(); i++){
          GraphicElement* el = it->second.operator[](i).get();

          TiXmlElement* elemelon = new TiXmlElement(it->first.data());
          elemelon->SetAttribute("PARAMETER", el->get_parameter().data());
          elemelon->SetAttribute("TYPE", el->get_type().data());
          elemelon->SetAttribute("CELL_TYPE", el->get_cell_type().data());

          //POINTS
          TiXmlElement* points = new TiXmlElement("POINTS");
          for(int i : el->get_points()){
              TiXmlElement* elemeleon = new TiXmlElement("POINT");
              elemelon->SetAttribute("ID",i);

              points->LinkEndChild(elemeleon);
          }

          if(!points->NoChildren())
              elemelon->LinkEndChild(points);

          //VALUES
          TiXmlElement* values = new TiXmlElement("VALUES");
          for(double i : el->get_value()){
              TiXmlElement* elemeleon = new TiXmlElement("VALUE");
              elemelon->SetAttribute("VAL",i);

              values->LinkEndChild(elemeleon);
          }

          if(!values->NoChildren())
              elemelon->LinkEndChild(values);

          element_type->LinkEndChild(elemelon);
      }

    }
}

void GraphicObject::update_max_min(){
    params.max = -1;
    params.min = 1;
    for(map<string,vector<unique_ptr<GraphicElement>>>::iterator it = params.elements.begin(); it != params.elements.end(); ++it) {
        for(int i = 0; i < it->second.size(); i++){
            if(params.max < params.min){
                pair<point,point> ax = it->second.operator[](i)->get_max_min();
                params.max = ax.first;
                params.min = ax.second;
            } else {
                pair<point,point> ax = it->second.operator[](i)->get_max_min();

                if(params.max.X() < ax.first.X())
                    params.max.X(ax.first.X());
                if(params.max.Y() < ax.first.Y())
                    params.max.Y(ax.first.Y());
                if(params.max.Z() < ax.first.Z())
                    params.max.Z(ax.first.Z());

                if(params.min.X() > ax.second.X())
                    params.min.X(ax.second.X());
                if(params.min.Y() > ax.second.Y())
                    params.min.Y(ax.second.Y());
                if(params.min.Z() > ax.second.Z())
                    params.min.Z(ax.second.Z());
            }
        }
    }

    //IF STILL UNSET, SET [-10,10]
    if(params.max < params.min){
        params.max = point(0,10,10,10);
        params.min = point(0,-10,-10,-10);
    }
}

void GraphicObject::update_max_min(GraphicElement* element){
    if(params.max < params.min){
        pair<point,point> ax = element->get_max_min();
        params.max = ax.first;
        params.min = ax.second;
    } else {
        pair<point,point> ax = element->get_max_min();

        if(params.max.X() < ax.first.X())
            params.max.X(ax.first.X());
        if(params.max.Y() < ax.first.Y())
            params.max.Y(ax.first.Y());
        if(params.max.Z() < ax.first.Z())
            params.max.Z(ax.first.Z());

        if(params.min.X() > ax.second.X())
            params.min.X(ax.second.X());
        if(params.min.Y() > ax.second.Y())
            params.min.Y(ax.second.Y());
        if(params.min.Z() > ax.second.Z())
            params.min.Z(ax.second.Z());
    }
}

void GraphicObject::auto_scale(){

    double w = abs(params.max.X() - params.min.X());
    double h = abs(params.max.Y() - params.min.Y());
    double d = abs(params.max.Z() - params.min.Z());

    point dist(1,-(params.min.X() + w/2),-(params.min.Y() + h/2),-(params.min.Z() + d/2));
    params.translate = dist;

    //biggest scaling fator
    vector<double> scales;

    //WIDTH SCALE
    if(w != 0)
        scales.push_back((PLOT_SIZE/w) * 0.6);
    else
        scales.push_back(1e+20);

    //HEIGHT SCALE
    if(h != 0)
        scales.push_back((PLOT_SIZE/h) * 0.6);
    else
        scales.push_back(1e+20);

    //DEPTH SCALE
    if(d != 0)
        scales.push_back((PLOT_SIZE/d) * 0.6);
    else
        scales.push_back(1e+20);

    params.scale = igumath::vector_min(scales);
}

void GraphicObject::clear(){
    
}

vector<pair<string,string>> GraphicObject::get_static_parameters(){
    vector<pair<string,string>> r;

    for(map<string,vector<unique_ptr<GraphicElement>>>::iterator it = params.elements.begin(); it != params.elements.end(); ++it) {
        vector<pair<string,string>> front_param = it->second.front()->get_static_paremeters();

        for(pair<string,string> p : front_param){
            r.push_back(p);
        }
    }
}

bool GraphicObject::set_parameter(QString parameter, WiseCellType cell_type){
    if(params.wise_structure->has_parameter(parameter,cell_type)){
        params.parameter = parameter.toStdString().data();
        params.cell_type = cell_type;

        return true;
    }

    return false;
}

bool GraphicObject::push_graphic_element(unique_ptr<GraphicElement> element){
    map<string,vector<unique_ptr<GraphicElement>>>::iterator i = params.elements.find(element->get_type());

    update_max_min(element.get());

    if(i != params.elements.end()){
        i->second.push_back(move(element));
        return true;
    } else {
        params.elements[element->get_type()].push_back(move(element));
        return true;
    }

    return false;
}

void GraphicObject::draw_guide_lines(){
    if(params.guide_lines_status[0] || params.guide_lines_status[1] || params.guide_lines_status[2] ||
            params.guide_lines_status[3] || params.guide_lines_status[4] || params.guide_lines_status[5]){
        vector<double> aux;
        aux.push_back((params.max.X() - params.min.X()));
        aux.push_back((params.max.Y() - params.min.Y()));
        aux.push_back((params.max.Z() - params.min.Z()));
        double max_limit = igumath::vector_max(aux);
        int safety_repeat = 10;

        //FINE GRID
        if(params.guide_lines_status[0] || params.guide_lines_status[1] || params.guide_lines_status[2]){
            double step = pow(10,(((int)log10(max_limit))))/4;
            QColor c = QColor("Gray");
            if(params.guide_lines_status[0]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    if(i % 4 != 0){
                        draw_guide_line_Y(i*step,i*step,c);
                        draw_guide_line_Y(-i*step,-i*step,c);
                    }
                }
            }
            if(params.guide_lines_status[1]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    if(i % 4 != 0){
                        draw_guide_line_Z(i*step,i*step,c);
                        draw_guide_line_Z(-i*step,-i*step,c);
                    }
                }
            }
            if(params.guide_lines_status[2]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    if(i % 4 != 0){
                        draw_guide_line_X(i*step,i*step,c);
                        draw_guide_line_X(-i*step,-i*step,c);
                    }
                }
            }
        }

        //COARSE GRID
        if(params.guide_lines_status[0] || params.guide_lines_status[1] || params.guide_lines_status[2]){
            double step = pow(10,(((int)log10(max_limit))));
            if(params.guide_lines_status[0]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    draw_guide_line_Y(i*step,i*step);
                    draw_guide_line_Y(-i*step,-i*step);
                }
            }
            if(params.guide_lines_status[1]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    draw_guide_line_Z(i*step,i*step);
                    draw_guide_line_Z(-i*step,-i*step);
                }
            }
            if(params.guide_lines_status[2]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    draw_guide_line_X(i*step,i*step);
                    draw_guide_line_X(-i*step,-i*step);
                }
            }
        }

        draw_guide_line_X(ZERO,ZERO,QColor("Blue"));
        draw_guide_line_Y(ZERO,ZERO,QColor("Red"));
        draw_guide_line_Z(ZERO,ZERO,QColor("Green"));
    }
}

void GraphicObject::draw_guide_line_X(double y, double z, QColor color){
    GraphicElement::set_material_color(color);
    glBegin(GL_LINES);
        point a = point(INFINITY_PLUS,y,z);
        point b = point(INFINITY_MINUS,y,z);
        glVertex3f(b.X(), b.Y(), b.Z());
        glVertex3f(a.X(), a.Y(), a.Z());
    glEnd();
}
void GraphicObject::draw_guide_line_Y(double x, double z, QColor color){
    GraphicElement::set_material_color(color);
    glBegin(GL_LINES);
        point a = point(x,INFINITY_PLUS,z);
        point b = point(x,INFINITY_MINUS,z);

        glVertex3f(a.X(), a.Y(), a.Z());
        glVertex3f(b.X(), b.Y(), b.Z());
    glEnd();
}
void GraphicObject::draw_guide_line_Z(double x, double y, QColor color){
    GraphicElement::set_material_color(color);
    glBegin(GL_LINES);
        point a = point(x,y,INFINITY_PLUS);
        point b = point(x,y,INFINITY_MINUS);

        glVertex3f(a.X(), a.Y(), a.Z());
        glVertex3f(b.X(), b.Y(), b.Z());
    glEnd();
}

void GraphicObject::draw_text(point* position, QString text, QColor color, const QFont &font){
    if(!igumath::is_between(position->X(),ONE,-ONE))
        return;

    if(!igumath::is_between(position->Y(),ONE,-ONE))
        return;

    point* pos = new point();
    pos = position;

    if(params.QPainter_set){
        QPainter* painter = params.get_QPainter(params.CID);
        if(painter != NULL){
            //SETS POSITION
            pos->X((int)(((pos->X() + ONE)/2) * painter->window().width()));
            pos->Y((int)(((pos->Y() + ONE)/2) * painter->window().height()));
            pos->Y((int)(painter->window().height() - pos->Y()));

            painter->setCompositionMode(QPainter::CompositionMode_Source);
            painter->setFont(font);
            painter->setPen(color);
            painter->drawText(pos->X(),pos->Y(),text);
        }

        delete painter;
    }

    delete pos;
}

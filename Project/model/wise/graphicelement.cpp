#include "graphicelement.h"

unsigned long int GraphicElement::CURR_ID = 0;
vector<QColor> GraphicElement::hatching_colors {QColor("Yellow"),QColor("Red")};

GraphicObjectStatus graphic_object_status_to_string(string status){
    if(status == "WARMING")
        return graphic::WARMING;
    else if (status == "COOLING")
        return graphic::COOLING;
    else if (status == "HOT")
        return graphic::HOT;
    else if (status == "COLD")
        return graphic::COLD;

    return graphic::COLD;
}

GraphicElement::GraphicElement(string type, WiseCellType cell_type, string parameter)
{
    params.ID        = (CURR_ID++);
    params.type      = (type);
    params.cell_type = (cell_type);
    params.parameter = (parameter);
}

void GraphicElement::set_material_color(QColor c){
    qreal r, g, b, a;
    c.getRgbF(&r,&g,&b,&a);
    vector<float> matAmbient = { (float)r, (float)g, (float)b, (float)a };
    vector<float> matDiffuse = { (float)r * (float) 0.1, (float) g * (float) 0.1, (float) b * (float) 0.1, (float)a };
    glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient.data());
    glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse.data());
}

vector<pair<string,string>> GraphicElement::get_static_paremeters(){
    stringstream aux;
    vector<pair<string,string>> vec;

    for(int i = 0; i < hatching_colors.size(); i++){
        QColor hc = hatching_colors.operator[](i);

        aux.clear();
        aux << hc.red();
        QString s = QString::asprintf("HATCHING_COLOR_%d_RED",i);
        pair<string,string> cylinder_HCR(s.toStdString().data(),aux.str());
        vec.push_back(cylinder_HCR);

        aux.clear();
        aux << hc.green();
        s = QString::asprintf("HATCHING_COLOR_%d_GREEN",i);
        pair<string,string> cylinder_HCG(s.toStdString().data(),aux.str());
        vec.push_back(cylinder_HCG);

        aux.clear();
        aux << hc.blue();
        s = QString::asprintf("HATCHING_COLOR_%d_BLUE",i);
        pair<string,string> cylinder_HCB(s.toStdString().data(),aux.str());
        vec.push_back(cylinder_HCB);

        aux.clear();
        aux << hc.alpha();
        s = QString::asprintf("HATCHING_COLOR_%d_ALPHA",i);
        pair<string,string> cylinder_HCA(s.toStdString().data(),aux.str());
        vec.push_back(cylinder_HCA);
    }

    return vec;
}

bool GraphicElement::set_static_paremeters(string param, string val){
    if(QString(param.data()).contains("HATCHING_COLOR_")){
        QString p(param.data());
        p.remove("HATCHING_COLOR_");
        p.replace("_"," ");
        stringstream ss(p.toStdString().data());

        int id;
        string color;

        ss >> id;
        ss >> color;

        if(color == "RED"){
            hatching_colors.operator[](id).setRed(stoi(val));
        }
        if(color == "GREEN"){
            hatching_colors.operator[](id).setGreen(stoi(val));
        }
        if(color == "BLUE"){
            hatching_colors.operator[](id).setBlue(stoi(val));
        }
    }
}

bool GraphicElement::set_parameter(string parameter){
    if(parameter == "" or params.parameter == parameter)
        return false;

    params.parameter = parameter;

    return true;
}

string GraphicElement::get_parameter(){
    return params.parameter;
}

string GraphicElement::get_type(){
    return params.type;
}

string GraphicElement::get_cell_type(){
    return wise_cell_type_to_string(params.cell_type);
}

vector<int> GraphicElement::get_points(){
    vector<int> aux;

    for(point* p : points)
        aux.push_back(p->ID());
}

vector<double> GraphicElement::get_value(){
    return value;
}

pair<point,point> GraphicElement::get_max_min(){
    if(!points.empty()){
        point max;
        max.operator=(points.front());
        point min;
        min.operator=(points.front());

        for(int i = 1;  i < points.size(); i++){
            point b;
            b.operator=(points.operator[](i));

            if(max.X() < b.X())
                max.X(b.X());
            if(max.Y() < b.Y())
                max.Y(b.Y());
            if(max.Z() < b.Z())
                max.Z(b.Z());

            if(min.X() > b.X())
                min.X(b.X());
            if(min.Y() > b.Y())
                min.Y(b.Y());
            if(min.Z() > b.Z())
                min.Z(b.Z());
        }

        return {max,min};
    }

    return {0.0,0.0};
}


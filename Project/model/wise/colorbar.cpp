#include "colorbar.h"

ColorBar::ColorBar(QColor color)
{
    colors_intensity.push_back({color,0.0});
    colors_intensity.push_back({color,1.0});

    colors_alpha.push_back({1.0,0.0});
    colors_alpha.push_back({1.0,1.0});
}

ColorBar::ColorBar(vector<QColor> color){
    double dx = 1/(color.size()-1);

    for(int i = 0; i < color.size(); i++){
        colors_intensity.push_back({color.operator[](i),i*dx});
    }

    colors_alpha.push_back({1.0,0.0});
    colors_alpha.push_back({1.0,1.0});
}

ColorBar::ColorBar(vector<QColor> color, vector<double> alpha){
    if(alpha.empty() || color.empty()){
        delete this;
        return;
    }
    if(alpha.size() == 1){
        colors_alpha.push_back({alpha.front(),0.0});
        colors_alpha.push_back({alpha.front(),1.0});
    } else {
        double dx = 1/(alpha.size() - 1);
        for(int i = 0; i < alpha.size(); i++){
            colors_alpha.push_back({alpha.operator[](i),i*dx});
        }
    }

    if(color.size() == 1 ){
        colors_intensity.push_back({color.front(),0.0});
        colors_intensity.push_back({color.front(),1.0});
    } else {
        double dx = 1/(color.size() - 1);
        for(int i = 0; i < color.size(); i++){
            colors_intensity.push_back({color.operator[](i),i*dx});
        }
    }

}

ColorBar::ColorBar(vector<pair<QColor,double>> color, vector<pair<double,double>> alpha){
    if(alpha.empty() || color.empty()){
        delete this;
        return;
    }
    if(alpha.size() == 1){
        colors_alpha.push_back({alpha.front().first,0.0});
        colors_alpha.push_back({alpha.front().first,1.0});
    } else {
        for(int i = 0; i < alpha.size(); i++){
            colors_alpha.push_back({alpha.operator[](i).first,alpha.operator[](i).second});
        }
    }

    if(color.size() == 1 ){
        colors_intensity.push_back({color.front().first,0.0});
        colors_intensity.push_back({color.front().first,1.0});
    } else {
        for(int i = 0; i < color.size(); i++){
            colors_intensity.push_back({color.operator[](i).first,color.operator[](i).second});
        }
    }
}

double ColorBar::get_max(){
    return max;
}

double ColorBar::get_min(){
    return min;
}

void ColorBar::reset_max_min(){
    max = -1;
    min = 1;
}

void ColorBar::update_max_min(double value){
    if(max < min){
        max = value;
        min = value;
    } else {
        if(value > max)
            max = value;

        if(value < min)
            min = value;
    }
}

TiXmlElement* ColorBar::get_xml_element(){
    TiXmlElement* elemelon = new TiXmlElement("COLOR_BAR");
    elemelon->SetAttribute("MAX",max);
    elemelon->SetAttribute("MIN",min);

    //COLORS
    TiXmlElement* colors = new TiXmlElement("COLORS");
    elemelon->LinkEndChild(colors);

    for(pair<QColor,double> c : colors_intensity){
        TiXmlElement* elemeleon = new TiXmlElement("COLOR");
        elemeleon->SetAttribute("X",c.second);
        elemeleon->SetAttribute("COLOR_R",c.first.red());
        elemeleon->SetAttribute("COLOR_G",c.first.green());
        elemeleon->SetAttribute("COLOR_B",c.first.blue());

        elemeleon->LinkEndChild(colors);
    }

    //ALPHAS
    TiXmlElement* alphas = new TiXmlElement("ALPHAS");
    elemelon->LinkEndChild(alphas);

    for(pair<double,double> c : colors_alpha){
        TiXmlElement* elemeleon = new TiXmlElement("ALPHA");
        elemeleon->SetAttribute("X",c.second);
        elemeleon->SetAttribute("ALPHA",c.first);

        elemeleon->LinkEndChild(alphas);
    }


    return elemelon;
}

QColor ColorBar::interpolate_intensity(double val){
    if(val < 0.0)
        return QColor(colors_intensity.front().first.red(),colors_intensity.front().first.green(),colors_intensity.front().first.blue(),colors_alpha.front().first);

    if(val > 1.0)
        return QColor(colors_intensity.back().first.red(),colors_intensity.back().first.green(),colors_intensity.back().first.blue(),colors_alpha.back().first);

    int larger_id = 1;

    while(val > colors_intensity.operator[](larger_id).second){
        if(colors_intensity.size() > larger_id + 1){
            larger_id ++;
        } else {
            return QColor(colors_intensity.back().first.red(),colors_intensity.back().first.green(),colors_intensity.back().first.blue(),colors_alpha.back().first);
        }
    }

    int lesser_id = larger_id -1;
    double pct = (val - colors_intensity.operator[](lesser_id).second) / (colors_intensity.operator[](larger_id).second - colors_intensity.operator[](lesser_id).second);

    int larger_alpha_id = 1;

    while(val > colors_alpha.operator[](larger_id).second){
        if(colors_alpha.size() > larger_id + 1){
            larger_id ++;
        } else {
            return QColor(colors_intensity.back().first.red(),colors_intensity.back().first.green(),colors_intensity.back().first.blue(),colors_alpha.back().first);
        }
    }

    int lesser_alpha_id = larger_id -1;
    double pct_alpha = (val - colors_alpha.operator[](lesser_id).second) / (colors_alpha.operator[](larger_id).second - colors_alpha.operator[](lesser_id).second);

    QColor c;

    c.setRed(pct * (colors_intensity.operator[](larger_id).first.red()) +  (1-pct) * (colors_intensity.operator[](lesser_id).first.red()));
    c.setGreen(pct * (colors_intensity.operator[](larger_id).first.green()) +  (1-pct) * (colors_intensity.operator[](lesser_id).first.green()));
    c.setBlue(pct * (colors_intensity.operator[](larger_id).first.blue()) +  (1-pct) * (colors_intensity.operator[](lesser_id).first.blue()));
    c.setAlpha(pct_alpha * (colors_alpha.operator[](larger_alpha_id).first) + (1-pct) * ( colors_alpha.operator[](lesser_id).first));

    return c;
}

QColor ColorBar::interpolate_value(double val){
    if(max < min)
        return QColor("Black");

    return interpolate_intensity((val - min) / (max - min));
}

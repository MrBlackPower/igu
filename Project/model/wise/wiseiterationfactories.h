#ifndef WISEITERATIONFACTORIES_H
#define WISEITERATIONFACTORIES_H

#include <memory>
#include <map>
#include "wiseiterationfactory.h"

#include "model/iterationfactory/duanandzamiriterationfactory.h"
#include "model/iterationfactory/adi2diterationfactory.h"
#include "model/iterationfactory/adi3diterationfactory.h"
#include "model/iterationfactory/graphicwalkiterationfactory.h"
#include "model/iterationfactory/staticiterationfactory.h"

#include "wiseelementfactories.h"

using namespace std;

class WiseIterationFactories
{
public:
    WiseIterationFactories();
    ~WiseIterationFactories();

    vector<string> get_types();

    vector<string> get_factories_name(string type = "");

    unique_ptr<WiseIterationFactory> get_factory_clone(string type, string name);

    bool iterate(WiseCollection* obj, string factory);

    void set_fields(WiseCollection* obj, string factory);

private:
    map<string,map<string,unique_ptr<WiseIterationFactory>>> factories;

    WiseElementFactories clone_factories;
};

#endif // WISEITERATIONFACTORIES_H

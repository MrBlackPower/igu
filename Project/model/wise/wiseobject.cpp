#include "wiseobject.h"

long int WiseObject::CURR_ID = 0;
WiseObject::WiseObject(string name, string type, WiseObjectStatus status, double dt, double model_time, int frames, int iterations, unique_ptr<WiseElement> hot_object , vector<unique_ptr<WiseElement>> cold_objects, unique_ptr<WiseElementFactory> obj_factory): WisePeer(name,"WISE_OBJECT"),id(CURR_ID++)
{
    params.obj = unique_ptr<WiseCollection>{new WiseCollection(move(hot_object),move(cold_objects),move(obj_factory))};
    params.name = name;
    params.type = type;

    params.status = status;

    params.dt = dt;
    params.model_time = model_time;
    params.frames = frames;
    params.iterations = iterations;
}

WiseObject::WiseObject(string name, unique_ptr<WiseElement> hot_object ,unique_ptr<WiseElement> cold_object , unique_ptr<WiseElementFactory> obj_factory) : WisePeer(hot_object->get_name(),"WISE_OBJECT"),id(CURR_ID++)
{
    params.name   = name;
    params.type   = hot_object->get_params()->type;
    params.obj    = unique_ptr<WiseCollection>{new WiseCollection(move(hot_object),move(cold_object),move(obj_factory))};
    gr_factory    = NULL;
    it_factory    = NULL;

    params.status = READY;

    send_msg(QString::asprintf("WISE_ID [%i] (NAME: %s) WISE OBJECT CREATED",id,params.obj->pop()->get_name().data()));
}

WiseObject::WiseObject(string name, unique_ptr<WiseElement> hot_object , vector<unique_ptr<WiseElement>> freezer , unique_ptr<WiseElementFactory> obj_factory) : WisePeer(hot_object->get_name(),"WISE_OBJECT"),id(CURR_ID++){
    params.name   = name;
    params.type   = hot_object->get_params()->type;
    params.obj    = unique_ptr<WiseCollection>{new WiseCollection(move(hot_object),move(freezer),move(obj_factory))};
    gr_factory    = NULL;
    it_factory    = NULL;
    params.status = READY;

    send_msg(QString::asprintf("WISE_ID [%i] (NAME: %s) WISE OBJECT CREATED",id,params.obj->pop()->get_name().data()));
}

WiseObject::~WiseObject(){
    send_msg(QString::asprintf("%i (%s) WISE OBJECT DELETED",id,params.name.data()));
}

int WiseObject::ID(){
    return id;
}

string WiseObject::get_name(){
    return params.name;
}

string WiseObject::dna(){
    return params.obj->pop()->dna();
}

QString WiseObject::get_iteration_factory_name(){
    return QString(it_factory->get_operation().data());
}

vector<string> WiseObject::get_iteration_factory_candidates(){
    WiseIterationFactories ifs;
    return ifs.get_factories_name(params.type);
}

void WiseObject::set_iteration_factory(unique_ptr<WiseIterationFactory> factory){
    if(factory != NULL){
        if(factory->get_type() == params.type && params.status == wise::READY){
            if(!params.it_factory_set){
                it_factory = move(factory);
                params.it_factory_set = true;
            } else {
                delete it_factory.release();
                it_factory = move(factory);
            }
        }
    }
}

void set_iteration_factory(string factory);

QString WiseObject::get_graphic_factory_name(){
    return QString(gr_factory->get_name().data());
}

void WiseObject::set_graphic_factory(unique_ptr<GraphicObjectFactory> factory){
    if(factory != NULL){
        if(factory->get_type() == params.type && params.status == wise::READY){
            if(!params.gr_factory_set){
                gr_factory = move(factory);
                params.gr_factory_set = true;
            } else {
                delete gr_factory.release();
                gr_factory = move(factory);
            }
        }
    }
}

bool WiseObject::ready_set(){
    if(!params.it_factory_set or params.status != READY)
        return false;

    it_factory->set_fields(params.obj.get());

    if(params.gr_factory_set){
        params.is_graphic = true;

        gr_factory->set_fields(gr_model.get());

        if(params.is_graphic_created){
            delete gr_model.release();
        }

        gr_model = gr_factory->create(params.obj.get());
        params.is_graphic_created = true;
    }

    WiseElement* el = params.obj->pop();
    el->update_xml();

    params.status = SET;
}

TiXmlElement* WiseObject::to_XML(){
    TiXmlComment * comment;
    string s;

    TiXmlElement * root = new TiXmlElement("WISE_OBJECT");

    comment = new TiXmlComment();
    s = " XML for Wise Object " + params.name + " ";
    comment->SetValue(s.c_str());

    root->LinkEndChild( comment );

    TiXmlElement * p = new TiXmlElement("OBJECT_PARAMS");
    root->LinkEndChild( p );

    //PARAMETERS - START

    p->SetAttribute("NAME"                  ,params.name.data());
    p->SetAttribute("STATUS"                ,wise_object_status_to_string(params.status).data());
    p->SetAttribute("TYPE"                  ,params.type.data());
    p->SetAttribute("DT"                    ,QString::asprintf("%f",params.dt).toStdString().data());
    p->SetAttribute("MODEL_TIME"            ,QString::asprintf("%f",params.model_time).toStdString().data());
    p->SetAttribute("FRAMES"                ,QString::asprintf("%d",params.iterations).toStdString().data());
    p->SetAttribute("ITERATIONS"            ,QString::asprintf("%d",params.frames).toStdString().data());
    p->SetAttribute("FRAMES"                ,QString::asprintf("%d",params.iterations).toStdString().data());
    p->SetAttribute("ITERATION_FACTORY_SET" ,(params.it_factory_set)?"1":"0");    
    if(params.it_factory_set)
        p->SetAttribute("ITERATION_FACTORY" , it_factory->get_operation().data());

    p->SetAttribute("GRAPHIC_FACTORY_SET"   ,(params.gr_factory_set)?"1":"0");
    if(params.gr_factory_set)
        p->SetAttribute("GRAPHIC_FACTORY" , gr_factory->get_name().data());

    p->SetAttribute("GRAPHIC_MODEL_SET"     ,(params.gr_model_set)?"1":"0");


    //PARAMETERS - END

    //ADD RELYING STRUCTURES
    //WISE COLLECTION
    TiXmlElement * p2 = params.obj->to_XML();
    root->LinkEndChild( p2 );

    return root;
}

string WiseObject::to_string(){
    TiXmlPrinter printer;
    printer.SetIndent( "    " );

    TiXmlElement * root = to_XML();

    root->Accept(&printer);

    return printer.CStr();
}

WiseObjectParams* WiseObject::get_params(){
    return &params;
}

void WiseObject::scale_through(vector<tuple<WiseCellType,string,double>> scaling){
    params.obj->scale_through(scaling);
}

WiseElement* WiseObject::get_hot(){
    return params.obj->pop();
}

unique_ptr<WiseElement> WiseObject::pop_hot(){
    unique_ptr<WiseElement> pt = params.obj->pop_hot();

    if(pt.get() == NULL)
        return NULL;

    return move(pt);
}

bool WiseObject::place_hot(unique_ptr<WiseElement> ptr){
    return params.obj->place_hot(move(ptr));
}

bool WiseObject::set_go(){
    if(!params.it_factory_set || (params.status != SET && params.status != GO))
        return false;

    return go_go();
}

bool WiseObject::go_go(){
    if(!params.it_factory_set || params.status == FINISHED || params.status == READY)
        return false;

    if(it_factory.get() == NULL)
        return false;

    it_factory->iterate(params.obj.get());

    params.status = GO;



    return true;
}

bool WiseObject::go_finished(){

}

bool WiseObject::go_set(){

}

bool WiseObject::finished_set(){

}

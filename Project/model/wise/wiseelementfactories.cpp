#include "wiseelementfactories.h"

WiseElementFactories::WiseElementFactories(){
    factories[ARTERY_TREE_TYPE] = unique_ptr<WiseElementFactory>(new WiseArteryTreeFactory());
    factories[GRAPHIC_TYPE    ] = unique_ptr<WiseElementFactory>(new WiseGraphicFactory());
    factories[MESH_TYPE       ] = unique_ptr<WiseElementFactory>(new WiseMeshFactory());
    factories[POLY_TYPE       ] = unique_ptr<WiseElementFactory>(new WisePolyFactory());
}

WiseElementFactories::~WiseElementFactories(){

}

vector<string> WiseElementFactories::get_factories_names(){
    vector<string> v;
    for(map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.begin(); it != factories.end(); ++it) {
      v.push_back(it->first);
    }

    return  v;
}

vector<pair<string,string>> WiseElementFactories::get_factories_info(){
    vector<pair<string,string>> v;
    for(map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.begin(); it != factories.end(); ++it) {
        v.push_back({it->first,it->second->get_name()});
    }

    return  v;
}

vector<string> WiseElementFactories::get_factories_examples(const string& type){
    vector<string> v;

    map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.find(type);
    if(it != factories.end()){
        return it->second->examples_list();
    }

    return  v;
}

unique_ptr<WiseElement> WiseElementFactories::make_empty(const string& type, const string& name){
    map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.find(type);

    if(it != factories.end()){
        unique_ptr<WiseElement> obj = it->second->make(name);
        return move(obj);
    }

    return NULL;
}

unique_ptr<WiseElement> WiseElementFactories::clone(string name, WiseElement* obj, WiseElementStatus clone_type){
    map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.find(obj->get_params()->type);

    if(it != factories.end()){
        unique_ptr<WiseElement> obJ = it->second->clone(name,obj,clone_type);
        return move(obJ);
    }

    return NULL;
}

unique_ptr<WiseElement> WiseElementFactories::make_vtk(const string& type, const string& name, const string& filename){
    map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.find(type);

    if(it != factories.end()){
        unique_ptr<WiseElement> obj = factories[type]->vtk_load(name,filename);
        return move(obj);
    }

    return NULL;
}

unique_ptr<WiseElement> WiseElementFactories::make_xml(TiXmlElement* doc){
    TiXmlHandle hRoot(0);

    TiXmlElement* subElem = NULL;

    if (!doc){
        return 0;
    }

    // save this for later
    hRoot = TiXmlHandle(doc);

    subElem = hRoot.FirstChildElement("ELEMENT_PARAMS").Element();

    QString name = subElem->Attribute("NAME");
    QString type = subElem->Attribute("TYPE");


    map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.find(type.toStdString());

    if(it != factories.end()){
        unique_ptr<WiseElement> obj = factories[type.toStdString()]->xml_load(doc);

        return move(obj);
    }

    return NULL;
}

unique_ptr<WiseElement> WiseElementFactories::make_xml(const string& type, const string& name, const string& filename){
    map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.find(type);

    if(it != factories.end()){
        unique_ptr<WiseElement> obj = factories[type]->xml_load(name,filename);
        return move(obj);
    }

    return NULL;
}

unique_ptr<WiseElement> WiseElementFactories::make_example(const string& type, const string& name, const string& example){
    map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.find(type);

    if(it != factories.end()){
        unique_ptr<WiseElement> obj = it->second->make_example(example,name);
        return move(obj);
    }

    return NULL;
}

unique_ptr<WiseElement> WiseElementFactories::recreate(unique_ptr<WiseElement> ptr){
    map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.find(ptr->get_params()->type);

    if(it != factories.end()){
        unique_ptr<WiseElement> obj = it->second->recreate(move(ptr));
        return move(obj);
    }

    return NULL;
}


unique_ptr<WiseElement> WiseElementFactories::scale_through(unique_ptr<WiseElement>& obj,vector<pair<string,double>> points_scale, vector<pair<string,double>> lines_scale,
                                     vector<pair<string,double>> cells_scale, vector<pair<string,double>> fields_scale){
    map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.find(obj->get_params()->type);

    if(it != factories.end()){
        unique_ptr<WiseElement> _obj  = factories[obj->get_params()->type]->scale_through(obj.get(),points_scale,lines_scale,cells_scale,fields_scale);

        return move(_obj);
    }

    return NULL;
}

void WiseElementFactories::scale_through(WiseElement* obj,vector<tuple<WiseCellType,string,double>> scaling){
    map<string,unique_ptr<WiseElementFactory>>::iterator it = factories.find(obj->get_params()->type);

    if(it != factories.end()){
        factories[obj->get_params()->type]->scale_through(obj,scaling);
    }
}

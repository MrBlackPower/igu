﻿#include "wiseobjectfactory.h"

WiseObjectFactory::WiseObjectFactory(unique_ptr<WiseElementFactory> obj_factory) : element_factory(move(obj_factory)), type(obj_factory->get_type())
{

}

string WiseObjectFactory::get_type(){
    return type;
}

vector<string> WiseObjectFactory::examples_list(){
    return element_factory->examples_list();
}

unique_ptr<WiseObject> WiseObjectFactory::xml_load(TiXmlElement* doc){
    TiXmlElement* pElem;

    // block: OBJECT_PARAMS
    pElem = doc->FirstChildElement("OBJECT_PARAMS");
    // should always have a valid root but handle gracefully if it does

    if(!pElem)
        return NULL;

    string name              = pElem->Attribute("NAME");
    string type              = pElem->Attribute("TYPE");

    WiseObjectStatus status = wise_object_status_to_string(pElem->Attribute("STATUS"));

    double  dt               = QString(pElem->Attribute("DT")).toDouble();
    double  model_time       = QString(pElem->Attribute("MODEL_TIME")).toDouble();

    int     frames           = QString(pElem->Attribute("FRAMES")).toInt();
    int     iterations       = QString(pElem->Attribute("ITERATIONS")).toInt();

    bool     it_factory_set  = QString(pElem->Attribute("ITERATION_FACTORY_SET")).toInt();
    bool     gr_factory_set  = QString(pElem->Attribute("GRAPHIC_FACTORY_SET")).toInt();
    bool     gr_model_set    = QString(pElem->Attribute("GRAPHIC_MODEL_SET")).toInt();

    string gr_factory = "";
    string it_factory = "";

    if(it_factory_set)
        it_factory = pElem->Attribute("ITERATION_FACTORY");

    if(gr_factory_set)
        gr_factory = pElem->Attribute("GRAPHIC_FACTORY");

    if(this->type != type)
        return NULL;

    TiXmlElement* collection = doc->FirstChildElement("WISE_COLLECTION");

    if(!collection)
        return NULL;

    TiXmlElement* ovenElem    = collection->FirstChildElement("WISE_OVEN");
    TiXmlElement* freezerElem = collection->FirstChildElement("WISE_FREEZER");

    //OVEN
    TiXmlElement* hotElem = ovenElem->FirstChildElement("WISE_ELEMENT");
    QString element_name  = hotElem->FirstChildElement("ELEMENT_PARAMS")->Attribute("NAME");
    unique_ptr<WiseElement> hot = element_factory->xml_load(element_name.toStdString(),hotElem);

    //FREEZER
    vector<unique_ptr<WiseElement>> colr;
    for (TiXmlElement* sse = freezerElem->FirstChildElement("WISE_ELEMENT"); sse; sse=sse->NextSiblingElement()) {
        unique_ptr<WiseElement> aux = element_factory->xml_cold_load(element_name.toStdString(),sse);
        colr.push_back(move(aux));
    }

    if(hot.get() == NULL)
        return NULL;
    unique_ptr<WiseObject> obj = unique_ptr<WiseObject>{new WiseObject(name,move(hot),move(colr),element_factory->clone_factory())};

    obj->get_params()->dt             = dt;
    obj->get_params()->model_time     = model_time;
    obj->get_params()->frames         = frames;
    obj->get_params()->dt             = iterations;
    obj->get_params()->it_factory_set = it_factory_set;
    obj->get_params()->gr_factory_set = gr_factory_set;

    if(it_factory_set){
        obj->set_iteration_factory(iteration_factories.get_factory_clone(type,it_factory));
    }

    obj->get_params()->status = status;

    return move(obj);
}

unique_ptr<WiseObject> WiseObjectFactory::clone(string name, WiseObject* obj){
    if(!obj)
        return NULL;

    if(obj->get_params()->type != type)
        return NULL;

    //HOT
    unique_ptr<WiseElement> hot = element_factory->clone(obj->get_params()->obj->pop()->name
                                                         ,obj->get_params()->obj->pop(),wise::HOT);

    //FREEZER
    vector<unique_ptr<WiseElement>> freezer;
    for(int i = 0; i < obj->get_params()->obj->params.freezer.size(); i++){
        WiseElement* pt = obj->get_params()->obj->params.freezer.operator[](i).get();
        unique_ptr<WiseElement> f = element_factory->clone(pt->name,pt,wise::COLD);
    }

    unique_ptr<WiseObject> r = move(make(name,move(hot),move(freezer)));

    if(obj->get_params()->it_factory_set){
        obj->set_iteration_factory(iteration_factories.get_factory_clone(type,obj->it_factory->get_operation()));
    }

    r->get_params()->operator=(obj->get_params());

    return r;
}

#include "wiseiterationfactory.h"

WiseIterationFactory::WiseIterationFactory(string type, string operation) : type(type), operation(operation)
{

}

string WiseIterationFactory::get_type(){
    return type;
}

string WiseIterationFactory::get_operation(){
    return operation;
}

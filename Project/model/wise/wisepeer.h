#ifndef WISEPEER_H
#define WISEPEER_H

#include <QObject>
#include <functional>

using namespace std;

class WisePeer : public QObject
{
    Q_OBJECT
public:
    WisePeer(string name, string class_string, QObject* parent = nullptr);
    ~WisePeer();

    void send_msg(QString msg);
    void send_warning(QString msg);
    void send_error(QString msg);

    void run_cmd(QString msg);

    bool operator==(WisePeer* peer);

    static bool set_consoles_function(function<vector<QObject*>(void)> get_consoles);

signals:
    void send_message(QString type, QString msg);

protected:

    void connect_to_existing_consoles();

    string name;

    string class_string;

private:

    const long int id;

    static long int CURR_ID;

    static function<vector<QObject*>(void)> get_consoles;
};

#endif // WISEPEER_H

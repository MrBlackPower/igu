#include "wisepeer.h"

long int WisePeer::CURR_ID = 0;
function<vector<QObject*>(void)> WisePeer::get_consoles = nullptr;

WisePeer::WisePeer(string name, string class_string, QObject* parent): QObject(parent),class_string(class_string) , name(name), id(CURR_ID++)
{
    connect_to_existing_consoles();
}

WisePeer::~WisePeer(){

}

void WisePeer::send_msg(QString msg){
    emit send_message("OUT",QString::asprintf("<%s:%s> '%s'",name.data(),class_string.data(),msg.toStdString().data()));
}

void WisePeer::send_warning(QString msg){
    emit send_message("WARNING",QString::asprintf("<%s:%s> '%s'",name.data(),class_string.data(),msg.toStdString().data()));
}

void WisePeer::send_error(QString msg){
    emit send_message("ERROR",QString::asprintf("<%s:%s> '%s'",name.data(),class_string.data(),msg.toStdString().data()));
}

void WisePeer::run_cmd(QString msg){
    emit send_message("COMMAND",msg);
}

bool WisePeer::operator==(WisePeer* peer){
    return id == peer->id;
}

bool WisePeer::set_consoles_function(function<vector<QObject*>(void)> get_consoles){
    if(get_consoles == nullptr)
        return false;

    WisePeer::get_consoles = get_consoles;

    return true;
}

void WisePeer::connect_to_existing_consoles(){
    if(get_consoles != nullptr){
        vector<QObject*> consoles = get_consoles.operator()();

        for(auto c: consoles)
            QObject::connect(this,SIGNAL(send_message(QString,QString)),c,SLOT(receiver(QString,QString)));
    }
}

#ifndef GRAPHICELEMENT_H
#define GRAPHICELEMENT_H

#include "model/wise/point.h"
#include "wisestructure.h"
#include "wiseelement.h"

#include "colorbar.h"

#include <QPainter>

#include <memory>
#include <vector>
#include <string>
#include <QColor>
#include <GL/gl.h>

class GraphicElement;

namespace graphic{
    using namespace wisepoint;

    enum GraphicObjectStatus{
        HOT,
        WARMING,
        COOLING,
        COLD
    };

    struct GraphicElementParameters{
        unsigned long int ID;
        string         type;
        WiseCellType   cell_type;
        string         parameter;
    };

    struct GraphicObjectParameters{
        unsigned int ID;
        GraphicObjectStatus graphic_status;
        /**
         * @brief max [right,top,far]
         */
        point max;
        /**
         * @brief min [left,bottom,near]
         */
        point min;
        point rotation;
        point translate;
        double scale;
        /************************************************************************************
                GUIDE LINES STATUS
                true | false
                [0] -> x axis grid
                [1] -> y axis grid
                [2] -> z axis grid
                [3] -> x axis fine grid
                [4] -> y axis fine grid
                [5] -> z axis fine grid
                [6] -> border
                [7] -> 3D-Gizmo
                [8] -> Colorbar
        ************************************************************************************/
        bool guide_lines_status[9];
        map<string,vector<unique_ptr<GraphicElement>>> elements;

        int frame = 0;

        //VISUALIZING
        WiseCellType   cell_type;
        string         parameter;

        //STRUCTURE
        WiseStructure* wise_structure;

        //ELEMENT
        WiseElement* wise_element;

        //POINTER TO PAINTER IN CANVAS
        function<QPainter*(int)> get_QPainter;
        int                      CID;
        bool                     QPainter_set;
    };

    GraphicObjectStatus graphic_object_status_to_string(string status);
    string graphic_object_status_to_string(GraphicObjectStatus status);

    struct GraphicModelParameters{

    };
}

using namespace std;

using namespace wise;

using namespace graphic;

class GraphicElement
{
public:
    GraphicElement(string type, WiseCellType cell_type , string parameter = "NONE");

    /**
     * @brief set_material_color <OPENGL> sets the material to specified color.
     * @param c
     */
    static void set_material_color(QColor c);

    vector<pair<string,string>> get_static_paremeters();

    bool set_static_paremeters(string param, string val);


    virtual void draw(ColorBar* color_bar) = 0;


    /******************
     * GET & SETS     *
     * ****************/

    bool   set_parameter(string parameter);

    string get_parameter();

    string get_type();

    string get_cell_type();

    vector<int> get_points();

    vector<double> get_value();

    pair<point,point> get_max_min();

    virtual vector<pair<string,string>> get_context_paremeters() = 0;

    virtual bool set_context_paremeters(string param, string val) = 0;


protected:

    /**
     * @brief hatching_color vector that contains the first hatching colors used:
     * 0 - Yellow
     * 1 - Red
     */
    static vector<QColor> hatching_colors;

    /**
     * @brief hatching integer that identifies the type of hatching used for this graphic element.
     * 0 - none
     * 1 - <default> yellow
     * 2 - <default> red
     */
    int hatching = 0;

    vector<point*> points;

    vector<double> value;
    
private:

    GraphicElementParameters params;

    static unsigned  long int CURR_ID;
};

#endif // GRAPHICELEMENT_H

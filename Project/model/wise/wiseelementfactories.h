#ifndef WISEOBJECTFACTORIES_H
#define WISEOBJECTFACTORIES_H

#include "model/wise/wiseelementfactory.h"

#include <functional>
#include <memory>
#include <map>

#define ARTERY_TREE_TYPE "ARTERY_TREE"
#define GRAPHIC_TYPE "GRAPHIC"
#define MESH_TYPE "MESH"
#define POLY_TYPE "POLY"

/******************** ************
 * FACTORIES INCLUDES
 *********************************/
#include "model/wiseelementfactory/wisearterytreefactory.h"
#include "model/wiseelementfactory/wisegraphicfactory.h"
#include "model/wiseelementfactory/wisemeshfactory.h"
#include "model/wiseelementfactory/wisepolyfactory.h"

using namespace std;

class WiseElementFactories
{
public:
    friend class WiseIterationFactories;
    friend class GraphicFactories;

    WiseElementFactories();
    ~WiseElementFactories();

    vector<string> get_factories_names();

    vector<pair<string,string>> get_factories_info();

    vector<string> get_factories_examples(const string& type);

    unique_ptr<WiseElement> make_empty(const string& type, const string& name);

    unique_ptr<WiseElement> clone(string name, WiseElement* obj, WiseElementStatus clone_type = wise::COLD);

    unique_ptr<WiseElement> make_vtk(const string& type, const string& name, const string& filename);

    unique_ptr<WiseElement> make_xml(const string& type, const string& name, const string& filename);

    unique_ptr<WiseElement> make_xml(TiXmlElement* doc);

    unique_ptr<WiseElement> make_example(const string& type, const string& name, const string& example);

    unique_ptr<WiseElement> recreate(unique_ptr<WiseElement> ptr);

    unique_ptr<WiseElement> scale_through(unique_ptr<WiseElement>& obj,vector<pair<string,double>> points_scale, vector<pair<string,double>> lines_scale,
                                         vector<pair<string,double>> cells_scale, vector<pair<string,double>> fields_scale);

    void scale_through(WiseElement* obj,vector<tuple<WiseCellType,string,double>> scaling);

private:
    map<string,unique_ptr<WiseElementFactory>> factories;
};

#endif // WISEOBJECTFACTORIES_H

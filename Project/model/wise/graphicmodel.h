#ifndef GRAPHICMODEL_H
#define GRAPHICMODEL_H

#include "graphicobject.h"
#include "colorbar.h"

class GraphicModel
{
    friend class GraphicObjectFactory;

public:
    GraphicModel(unique_ptr<GraphicObject> first_raw);

    void draw();

    int get_frames();

    void next_frame();

    void previous_frame();

    bool push_frame(unique_ptr<GraphicObject> raw);

    void set_frame(int frame);

    GraphicObject* pop();

    GraphicObject* operator[](int i);

private:

    ColorBar color_bar;

    vector<unique_ptr<GraphicObject>> raw_list;

    int raw_drawing;

    static int forward_frames;

    static int backward_frames;

    string dna;

    string type;
};

#endif // GRAPHICMODEL_H

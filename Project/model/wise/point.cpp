#include "model/wise/point.h"

int wisepoint::point::CURR_ID = 0;
int wisepoint::point::point_N = 0;

wisepoint::point::point(int id)
{
    this->id = id;
    x = 0;
    y = 0;
    z = 0;

    point_id = CURR_ID;
    CURR_ID++;
    point_N++;
}

wisepoint::point::point(int id, double x, double y, double z){
    this->id = id;
    this->x = x;
    this->y = y;
    this->z = z;

    point_id = CURR_ID;
    CURR_ID++;
    point_N++;
}

wisepoint::point::~point(){
    point_N--;
}

int wisepoint::point::ID(){
    return id;
}

double wisepoint::point::X(){
    return x;
}

double wisepoint::point::Y(){
    return y;
}

double wisepoint::point::Z(){
    return z;
}

void wisepoint::point::X(double x){
    this->x = x;
}

void wisepoint::point::Y(double y){
    this->y = y;
}

void wisepoint::point::Z(double z){
    this->z = z;
}

double wisepoint::point::operator[](int i){
    i = i % 3;

    switch (i) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
    }

    return x;
}

void wisepoint::point::operator=(int c){
    x = c;
    y = c;
    z = c;
}

void wisepoint::point::operator=(float c){
    x = c;
    y = c;
    z = c;
}

void wisepoint::point::operator=(double c){
    x = c;
    y = c;
    z = c;
}

void wisepoint::point::operator=(point c){
    x = c.X();
    y = c.Y();
    z = c.Z();
}

void wisepoint::point::operator=(point* c){
    x = c->X();
    y = c->Y();
    z = c->Z();
}

bool wisepoint::point::operator==(wisepoint::point& p){
    return ((x == p.X()) && (y == p.Y()) && (z == p.Z()));
}

bool wisepoint::point::operator==(point* p){
    return ((x == p->X()) && (y == p->Y()) && (z == p->Z()));
}

wisepoint::point& wisepoint::point::operator+( int c ){
    x += c;
    y += c;
    z += c;

    return *this;
}

wisepoint::point& wisepoint::point::operator+( float c ){
    x += c;
    y += c;
    z += c;

    return *this;
}

wisepoint::point& wisepoint::point::operator+( double c ){
    x += c;
    y += c;
    z += c;

    return *this;
}

wisepoint::point& wisepoint::point::operator+( point c ){
    x += c.X();
    y += c.Y();
    z += c.Z();

    return *this;
}

wisepoint::point& wisepoint::point::operator-( int c ){
    x -= c;
    y -= c;
    z -= c;

    return *this;
}

wisepoint::point& wisepoint::point::operator-( float c ){
    x -= c;
    y -= c;
    z -= c;

    return *this;
}

wisepoint::point& wisepoint::point::operator-( double c ){
    x -= c;
    y -= c;
    z -= c;

    return *this;
}

wisepoint::point& wisepoint::point::operator-( point c ){
    x += c.X();
    y += c.Y();
    z += c.Z();

    return *this;
}


wisepoint::point& wisepoint::point::operator*( int c ){
    x *= c;
    y *= c;
    z *= c;

    return *this;
}

wisepoint::point& wisepoint::point::operator*( float c ){
    x *= c;
    y *= c;
    z *= c;

    return *this;
}

wisepoint::point& wisepoint::point::operator*( double c ){
    x *= c;
    y *= c;
    z *= c;

    return *this;
}

wisepoint::point& wisepoint::point::operator*( point c ){
    x *= c.X();
    y *= c.Y();
    z *= c.Z();

    return *this;
}

wisepoint::point& wisepoint::point::operator/( int c ){
    x /= c;
    y /= c;
    z /= c;

    return *this;
}

wisepoint::point& wisepoint::point::operator/( float c ){
    x /= c;
    y /= c;
    z /= c;

    return *this;
}

wisepoint::point& wisepoint::point::operator/( double c ){
    x /= c;
    y /= c;
    z /= c;

    return *this;
}

bool wisepoint::point::operator<( point b ){
    if(x < b.X())
        if(y < b.Y())
            if(z < b.Z())
                return true;

    return false;
}

bool wisepoint::point::operator>( point b ){
    if(x > b.X())
        if(y > b.Y())
            if(z > b.Z())
                return true;

    return false;
}

wisepoint::point::point_raw wisepoint::point::raw(){
    point_raw p;

    p.x = x;
    p.y = y;
    p.z = z;

    return p;
}

double wisepoint::point::abs(){
    return sqrt(pow(x,2.0) + pow(y,2.0) + pow(z,2.0));
}

double wisepoint::point::get_phi(){
    double phi = (asin(z/abs()) * 180 / PI);

    if(phi < 0.0)
        while(phi< 0.0) phi += 360;

    if(phi > 360)
        while(phi > 360) phi -= 360;

    return phi;
}

double wisepoint::point::get_theta(){
    double theta = (atan(y/x) * 180 / PI);

    if(x < 0.0)
        theta += 180;

    if(theta < 0.0)
        while(theta< 0.0) theta += 360;

    if(theta > 360)
        while(theta > 360) theta -= 360;

    return theta;
}

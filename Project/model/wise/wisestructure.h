﻿#ifndef WISESTRUCTURE_H
#define WISESTRUCTURE_H

#include <QString>
#include <QDateTime>
#include <QPointer>
#include <vector>
#include <utility>

#include "model/wise/point.h"
#include "helper/igumath.h"

#include "tinyxml/tinyxml.h"

#define ONE 1
#define ZERO 0

class WiseStructure;
class WiseElement;
class WiseElementFactory;
class WiseCollection;

namespace wise {

    using namespace std;
    using namespace wisepoint;

    #define KEY_ALPHABET "ABCDEFGHIJKLMNOPQRSTUVXWYZ0123456789abcdefghijklmnopqrstuvxwyz"
    #define FRAME_TIMES_SIZE 5
    #define ITERATION_TIMES_SIZE 5

    enum WiseObjectStatus{
        READY,
        SET,
        GO,
        FINISHED
    };

    enum WiseElementStatus{
        HOT,
        WARMING,
        COOLING,
        COLD
    };

    enum WiseLoadType{
        VTK,
        XML,
        EXAMPLE,
        IDK
    };

    /**
     * @brief The WiseParams struct contais all parameters used only in this
     * class-level of the object.
     */
    struct WiseElementParams{
        string name         = "";
        string filename_vtk = "";
        string filename_xml = "";
        string type         = "WISE_ELEMENT";

        string element_key  = "NONE";
        int    ID           = 0;

        double dt           = 1;
        double model_time   = 0;
        int    instance    = 0;

        unique_ptr<WiseStructure> wise_structure;

        WiseElementStatus status = COLD;

        void set_filename(int id){
            filename_vtk = QString::asprintf("wise_%s_%s_%d_%d.vtk",name.data(),element_key.data(),ID,instance).toStdString();
            filename_xml = QString::asprintf("wise_%s_%s_%d_%d.xml",name.data(),element_key.data(),ID,instance).toStdString();
        };
    };

    struct WiseStructureParams{
        int          structure_id     = 0;
        string       structure_key    = "";
        string       name             = "UNAMED";
        int          n_points         = 0;
        int          n_cells          = 0;
        int          n_lines          = 0;
        int          n_fields         = 0;
        point        max              = point(0,0,0);
        point        min              = point(0,1,1);

        bool         imported        = false;
        WiseLoadType import_type     = IDK;
        string       import_filename = "NONE";

        bool operator==(WiseStructureParams& params){
            bool r = (structure_key == params.structure_key);
            r = r && (name == params.name);
            r = r && (n_points == params.n_points);
            r = r && (n_cells == params.n_cells);
            r = r && (n_lines == params.n_lines);
            r = r && (n_fields == params.n_fields);
            r = r && (min == params.min);
            r = r && (max == params.max);
        }

        void operator=(WiseStructureParams& params){
            structure_key = params.structure_key;
            n_points = params.n_points;
            n_cells = params.n_cells;
            n_fields = params.n_fields;
            max = params.max;
            min = params.min;
            imported = params.imported;
            import_type = params.import_type;
            import_filename = params.import_filename;
        }
    };

    struct WiseCollectionParams{
        unique_ptr<WiseElement>         oven;
        unique_ptr<WiseElementFactory>  obj_factory;

        vector<unique_ptr<WiseElement>> freezer;
        WiseElement*                    current_raw;
        vector<QDateTime>               time_list;

        unsigned int                    ID  = 0;
        unsigned int                    N   = 0;
    };

    struct WiseObjectParams{
        string name;
        string type;

        WiseObjectStatus status = READY;

        unique_ptr<WiseCollection> obj;

        double dt = 1;
        double model_time = 0;
        int frames = 0;
        int iterations = 0;

        bool it_factory_set = false;
        bool gr_factory_set = false;
        bool gr_model_set = false;

        bool is_graphic = false;
        bool is_graphic_created = false;

        void operator=(WiseObjectParams p){
            name = p.name;
            type = p.type;
            dt = p.dt;
            model_time = p.model_time;
            iterations = p.iterations;
        }

        void operator=(WiseObjectParams* p){
            name = p->name;
            type = p->type;
            dt = p->dt;
            model_time = p->model_time;
            iterations = p->iterations;
        }
    };

    enum WiseAddMode{
        FRONT,
        BACK
    };

    enum WiseCellType{
        LINE,
        CELL,
        POINT,
        FIELD
    };

    enum WiseDataType{
        INT,
        FLOAT,
        DOUBLE,
        STRING,
        VECTOR,
        TENSOR,
        UNKNOWN
    };

    enum WiseFieldType{
        INITIAL,
        LIVE,
        OUT
    };

    enum WiseFieldFamily{
        WISE,
        ITERATION,
        GRAPHIC
    };

    struct WiseInfo{
        WiseDataType type = UNKNOWN;
        string name;

        bool operator==(WiseInfo i){
            return ((name == i.name) && (type == i.type));
        }

        bool operator==(string i){
            return (name == i);
        }
    };

    struct WiseCell{
        int id;
        int N;
        vector<int> points;
        int type = 0;

        bool operator==(WiseCell& c){
            bool r =  ((N == c.N));
            r = r && (id == c.id);
            r = r && (type == c.type);

            for(int i = 0; i < points.size() && r; i++){
                r = r && (points[i] == c.points[i]);
            }

            return r;
        }
    };

    struct WiseLine{
        int id;
        int N = 2;
        int a;
        int b;

        bool operator==(WiseLine& c){
            bool r =  ((N == c.N));
            r = r && (id == c.id);
            r = r && (a == c.a);
            r = r && (b == c.b);

            return r;
        }
    };

    string wise_data_type_to_string(WiseDataType t);

    WiseDataType wise_data_type_to_string(string t);


    string wise_field_type_to_string(WiseFieldType t);

    WiseFieldType wise_field_type_to_string(string t);


    string wise_field_family_to_string(WiseFieldFamily t);

    WiseFieldFamily wise_field_family_to_string(string t);


    string wise_cell_type_to_string(WiseCellType t);

    WiseCellType wise_cell_type_to_string(string t);


    string wise_load_type_to_string(WiseCellType t);

    WiseLoadType string_to_wise_load_type(string t);


    string wise_object_status_to_string(WiseObjectStatus t);

    WiseObjectStatus wise_object_status_to_string(string t);


    string wise_element_status_to_string(WiseElementStatus t);

    WiseElementStatus wise_element_status_to_string(string t);

    vector<vector<double>> string_to_matrix(vector<string> vec, WiseDataType type);

    vector<double> string_to_value(vector<string> vec, WiseDataType type);
}

using namespace wise;

class WiseStructure
{
public:
    WiseStructure(string name);
    WiseStructure(string name, string structure_key, WiseLoadType load_type, string filename);
    ~WiseStructure();

    void set_default_fields();

    /**
     * @brief update_xml is a mthod that updates the underlying XML document.
     */
    void update_xml();

    /**
     * @brief to_VTK is a method resposible for the otput a a VTK document updated
     *
     * @return string corresponding to this object
     */
    string to_VTK();

    /**
     * @brief to_XML is a method resposible for the otput a a xml document updated
     *
     * @return Tiny XML Document corresponding to this object
     */
    TiXmlElement* to_XML();

    /***
     * GET METHODS
     * ***/
    double get_elapsed_time();

    vector<pair<string,string>> get_wise_cell_data(WiseCellType cell_type, int id);

    vector<pair<string,string>> get_point_data(int id);
    vector<pair<string,string>> get_cell_data(int id);
    vector<pair<string,string>> get_line_data(int id);

    vector<string> get_data(WiseCellType cell_type,string info_name);
    vector<string> get_point_data(string info_name);
    vector<string> get_cell_data(string info_name);
    vector<string> get_line_data(string info_name);
    vector<string> get_field_data(string info_name);

    WiseDataType get_type(WiseCellType cell_type,string info_name);
    WiseDataType get_point_type(string info_name);
    WiseDataType get_cell_type(string info_name);
    WiseDataType get_line_type(string info_name);
    WiseDataType get_field_type(string info_name);

    string get_point_data(string info_name, int id);
    string get_cell_data(string info_name, int id);
    string get_line_data(string info_name, int id);
    string get_field_data(string info_name, int id);

    WiseStructureParams* get_params();

    int get_points_size();
    int get_lines_size();
    int get_cells_size();
    int get_fields_size();

    pair<point,point> get_max_min();

    double get_max_size();

    double get_height();
    double get_widht();
    double get_depht();

    vector<point> get_points();
    vector<WiseCell> get_cells();
    vector<WiseLine> get_lines();
    vector<tuple<WiseInfo,WiseFieldFamily,WiseFieldType>> get_fields();

    /***
     * SET METHODS
     * ***/

    bool set_wise_cell_info(WiseCellType cell_type, int id, string name, string data);

    bool set_point_data(int id, string name, string data);
    bool set_cell_data(int id, string name, string data);
    bool set_line_data(int id, string name, string data);
    bool set_field_data(int id, string name, string data);

    bool set_all_point_data(string name, string data);
    bool set_all_cell_data(string name, string data);
    bool set_all_line_data(string name, string data);
    bool set_all_field_data(string name, string data);

    bool set_cell_type(int id, int type);

    void set_max_min(double x, double y, double z);

    void set_assigned_type(QString type);

    /***
     * ADD METHODS
     * ***/

    bool add_point(double x, double y, double z = 0);
    bool add_point(point p);
    bool add_point_info(string name, WiseDataType type);

    /**
     * @brief add_point_data is a function that feeds data to the point_data_mtatrix at the last position(DEFAULT).
     * @param info in which info data is being add.
     * @param data raw data.
     * @param mode BACK or FRONT.
     *
     * @return if added.
     */
    bool add_point_data(string info, string data, WiseAddMode mode = BACK);

    bool add_line(int a, int b);
    bool add_line_info(string name, WiseDataType type);

    /**
     * @brief add_cell_data is a function that feeds data to the line_data_mtatrix at the last position(DEFAULT).
     * @param info info in which info data is being add.
     * @param data raw data.
     * @param mode BACK or FRONT.
     *
     * @return if added.
     */
    bool add_line_data(string info, string data, WiseAddMode mode = BACK);

    bool add_cell(vector<int> pts);
    bool add_cell_info(string name, WiseDataType type);

    /**
     * @brief add_cell_data is a function that feeds data to the cell_data_mtatrix at the last position(DEFAULT).
     * @param info info in which info data is being add.
     * @param data raw data.
     * @param mode BACK or FRONT.
     *
     * @return if added.
     */
    bool add_cell_data(string info, string data, WiseAddMode mode = BACK);

    bool add_field(string name, WiseFieldFamily family = WISE, WiseDataType data_type = DOUBLE, WiseFieldType type = INITIAL);
    bool add_field(string name, string data, WiseFieldFamily family = WISE, WiseDataType data_type = DOUBLE, WiseFieldType type = INITIAL);

    bool add_field_data(string data, string info_name);

    /***
     * REMOVE METHODS
     * ***/

    bool clear(bool deep_clean = false);

    bool clear_data(bool deep_clean = false);

    /***
     * HAS METHODS
     * **/
    bool has_field(string name);

    bool field_set(string info_name);

    bool field_set(string info_name, WiseDataType data_type);

    bool consistent();

    bool has_parameter(QString parameter, WiseCellType cell_type);

    bool scale_through(vector<pair<string,double>> points_scale, vector<pair<string,double>> lines_scale,
                       vector<pair<string,double>> cells_scale, vector<pair<string,double>> fields_scale);

    void scale_through(vector<tuple<WiseCellType,string,double>> scaling);

    void operator=(WiseStructure* st);

    void operator=(WiseStructure& st);

    bool operator==(WiseStructure& st);

    //point data
    vector<point> points;
    vector<WiseInfo> points_info;

    /**
     * @brief point_data_matrix Matrix with raw data for every info of every point.
     *  [infos,points]
     */
    vector<vector<string>> point_data_matrix;

    //cell data
    vector<WiseCell> cells;
    vector<WiseInfo> cells_info;
    vector<vector<string>> cell_data_matrix;

    //line data
    vector<WiseLine> lines;
    vector<WiseInfo> lines_info;
    vector<vector<string>> line_data_matrix;

    //field data
    vector<tuple<WiseInfo,WiseFieldFamily,WiseFieldType>> fields;
    vector<vector<string>> field_data_matrix;
private:

    static long int CURR_ID;

    static long int CURR_N;

    bool scale_through(vector<string>* values, double scale, WiseDataType type = DOUBLE);

    WiseStructureParams params;

    QString assigned_type;

    TiXmlElement* doc;

    long int id;

    QDateTime creation_time;
};

#endif // WISESTRUCTURE_H

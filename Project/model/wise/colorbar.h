#ifndef COLORBAR_H
#define COLORBAR_H

#include <QColor>
#include <vector>
#include <utility>

#include "tinyxml/tinyxml.h"

using namespace std;

class ColorBar
{
public:
    /**
     * @brief ColorBar One Color Color Bar.
     * @param color
     */
    ColorBar(QColor color);

    /**
     * @brief ColorBar Multi-color Color Bar, homogeneously spaced colors.
     * @param color
     */
    ColorBar(vector<QColor> color);

    /**
     * @brief ColorBar Multi-color Color Bar with alpha, homogeneously spaced colors.
     * @param color
     */
    ColorBar(vector<QColor> color, vector<double> alpha);

    /**
     * @brief ColorBar Multi-color Color Bar with alpha, heterogeneously spaced colors.
     * @param color
     */
    ColorBar(vector<pair<QColor,double>> color, vector<pair<double,double>> alpha);

    double get_max();
    double get_min();

    void reset_max_min();
    void update_max_min(double value);

    TiXmlElement* get_xml_element();

    /**
     * @brief interpolate_intensity interpolates a value between 0 and 1.
     * @param val in (0,1)
     * @return corresponding color
     */
    QColor interpolate_intensity(double val);

    /**
     * @brief interpolate_value interpolates a value between max and min.
     * @param val in (min,max)
     * @return corresponding color
     */
    QColor interpolate_value(double val);

    QString parameter = "DEFAULT";
private:

    /**
     * @brief colors_intensity Is a vectors with all of this colorbar's colors, contains also
     * the intensity(1-axis,[0,1]) on which every color sits.
     */
    vector<pair<QColor,double>> colors_intensity;

    vector<pair<double,double>> colors_alpha;


    double max = -1;
    double min = 1;
};

#endif // COLORBAR_H

#ifndef GRAPHICOBJECT_H
#define GRAPHICOBJECT_H

#include <memory>
#include <vector>

#include "tinyxml/tinyxml.h"

#include "model/wise/point.h"
#include "graphicelement.h"
#include "wiseelement.h"
#include "wisestructure.h"
#include "colorbar.h"

#include <QString>
#include <QColor>
#include <QFont>
#include <QPainter>
#include <QEventLoop>
#include <QElapsedTimer>

#define LINE_SPACING 0.025

#define PLOT_SIZE 1.0

#define INFINITY_PLUS 32767
#define INFINITY_MINUS -32767

using namespace std;

using namespace graphic;

class GraphicObject
{
    friend class GraphicObjectFactory;

public:

    GraphicObject(WiseStructure* structure, QString parameter, WiseCellType parameter_cell_type);
    ~GraphicObject();

    bool add_field(string name,WiseFieldFamily family = WISE, WiseDataType type = DOUBLE, WiseFieldType field_type = INITIAL);

    bool add_field(string name, string data, WiseFieldFamily family = WISE, WiseDataType type = DOUBLE, WiseFieldType field_type = INITIAL);

    void draw(ColorBar* color_bar);

    void update_xml();

    void update_max_min();

    void auto_scale();
    
    void clear();

    vector<pair<string,string>> get_static_parameters();

    bool set_parameter(QString parameter, WiseCellType cell_type);

    bool push_graphic_element(unique_ptr<GraphicElement> element);

    virtual bool                         hover_node(WiseCellType cell_type, int id) = 0;
    virtual bool                         select_node(WiseCellType cell_type, int id) = 0;
    virtual vector<pair<string,string>>  edit_node(WiseCellType cell_type, int id) = 0;
    virtual bool                         rebuild() = 0;

    void draw_guide_lines();
    void draw_guide_line_X(double y, double z, QColor color = QColor("Black"));
    void draw_guide_line_Y(double x, double z, QColor color = QColor("Black"));
    void draw_guide_line_Z(double x, double y, QColor color = QColor("Black"));

    void draw_text(point* position, QString text, QColor color = QColor("Black"), const QFont &font = QFont("Ubuntu Mono",12));

private:
    string dna;

    TiXmlDocument doc;
    
protected:

    GraphicObjectParameters params;

    void update_max_min(GraphicElement* element);

    static unsigned int CURR_ID;
};

#endif // GRAPHICOBJECT_H

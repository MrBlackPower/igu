﻿#include "wiseelement.h"

long int WiseElement::CURR_ID = 0;
long int WiseElement::CURR_N  = 0;

WiseElement::WiseElement(string name, string type) : WisePeer(name,"WISE_ELEMENT"){
    params.wise_structure = make_unique<WiseStructure>(name);
    send_msg(QString::asprintf("WISE_ID [%i] (NAME: %s) WISE ELEMENT CREATED",params.name.data(),get_name().data()));

    params.ID = CURR_ID++;
    CURR_N++;
    params.element_key = igumath::random_key(10,KEY_ALPHABET);
    params.type = type;
    params.name = name;


    params.set_filename(params.ID);
}

WiseElement::WiseElement(string name, string type, string element_key, string structure_key, WiseLoadType load_type, string filename) : WisePeer(name,"WISE_ELEMENT"){
    params.wise_structure
            = make_unique<WiseStructure>(name,structure_key,load_type,filename);
    send_msg(QString::asprintf("WISE_ID [%i] (NAME: %s) WISE ELEMENT CREATED",params.name.data(),get_name().data()));

    params.ID = CURR_ID++;
    CURR_N++;
    params.element_key = igumath::random_key(10,KEY_ALPHABET);

    params.set_filename(params.ID);
}

WiseElement::~WiseElement(){
    send_msg(QString::asprintf("%i (%s) WISE ELEMENT DELETED",params.ID,get_name().data()));
    CURR_N--;

    if(CURR_N == 0){
        CURR_ID = 0;
    }
}

string WiseElement::key(){
    return params.element_key;
}

bool WiseElement::clear(bool deep_clean){
    params.wise_structure->clear(deep_clean);

    if(deep_clean){
        set_default_fields();
    }
}

bool WiseElement::clear_data(bool deep_clean){
    //field data
    params.wise_structure->clear_data(deep_clean);

    if(deep_clean){
        set_default_fields();
    }
}

void WiseElement::update_xml(){
    params.wise_structure->update_xml();
}

WiseElementParams* WiseElement::get_params(){
    return &params;
}

string WiseElement::get_name(){
    return params.name;
}

bool WiseElement::set_status(WiseElementStatus status){
    return params.status=(status);
}

string WiseElement::to_string(bool xml){
    if(xml){
        TiXmlPrinter printer;
        printer.SetIndent( "    " );

        TiXmlComment * comment;
        string s;
        TiXmlElement * root = new TiXmlElement("WISE_ELEMENT");

        comment = new TiXmlComment();
        s = " XML for Wise Element " + params.name + " ";
        comment->SetValue(s.c_str());

        root->LinkEndChild( comment );

        TiXmlElement * p = new TiXmlElement("ELEMENT_PARAMS");
        root->LinkEndChild( p );

        //PARAMETERS - START

        p->SetAttribute("NAME"        ,params.name.data());
        p->SetAttribute("FILENAME_VTK",params.filename_vtk.data());
        p->SetAttribute("FILENAME_XML",params.filename_xml.data());
        p->SetAttribute("TYPE"        ,params.type.data());
        p->SetAttribute("ELEMENT_KEY" ,params.element_key.data());
        p->SetAttribute("ID"          ,QString::asprintf("%d",params.ID).toStdString().data());
        p->SetAttribute("DT"          ,QString::asprintf("%f",params.dt).toStdString().data());
        p->SetAttribute("MODEL_TIME"  ,QString::asprintf("%f",params.model_time).toStdString().data());
        p->SetAttribute("ITERATION"   ,QString::asprintf("%d",params.instance).toStdString().data());
        p->SetAttribute("STATUS"      ,wise_element_status_to_string(params.status).data());

        //PARAMETERS - END

        TiXmlElement * st = params.wise_structure->to_XML();
        root->LinkEndChild(st);

        root->Accept(&printer);

        return printer.CStr();
    } else {
        return to_VTK();
    }
}

void WiseElement::print(string filename, bool xml){
    string _this = to_string(xml);
    igufile::save_raw(_this,filename.c_str());
}

string WiseElement::to_VTK(){
    return params.wise_structure->to_VTK();
}

TiXmlElement* WiseElement::to_XML(){
    TiXmlComment * comment;
    string s;
    TiXmlElement * root = new TiXmlElement("WISE_ELEMENT");

    comment = new TiXmlComment();
    s = " XML for Wise Element " + params.name + " ";
    comment->SetValue(s.c_str());

    root->LinkEndChild( comment );

    TiXmlElement * p = new TiXmlElement("ELEMENT_PARAMS");
    root->LinkEndChild( p );

    //PARAMETERS - START

    p->SetAttribute("NAME"        ,params.name.data());
    p->SetAttribute("FILENAME_VTK",params.filename_vtk.data());
    p->SetAttribute("FILENAME_XML",params.filename_xml.data());
    p->SetAttribute("TYPE"        ,params.type.data());
    p->SetAttribute("ELEMENT_KEY" ,params.element_key.data());
    p->SetAttribute("ID"          ,QString::asprintf("%d",params.ID).toStdString().data());
    p->SetAttribute("DT"          ,QString::asprintf("%f",params.dt).toStdString().data());
    p->SetAttribute("MODEL_TIME"  ,QString::asprintf("%f",params.model_time).toStdString().data());
    p->SetAttribute("INSTANCE"   ,QString::asprintf("%d",params.instance).toStdString().data());
    p->SetAttribute("STATUS"      ,wise_element_status_to_string(params.status).data());

    //PARAMETERS - END

    TiXmlElement * st = params.wise_structure->to_XML();
    root->LinkEndChild(st);

    return root;
}

bool WiseElement::add_point(double x, double y, double z){
    return params.wise_structure->add_point(x,y,z);
}

bool WiseElement::add_point(point p){
    return params.wise_structure->add_point(p);
}

bool WiseElement::add_line(int a, int b){
    return params.wise_structure->add_line(a,b);
}

bool WiseElement::add_cell(vector<int> p){
    return params.wise_structure->add_cell(p);
}

bool WiseElement::add_field(string name, WiseFieldFamily family, WiseDataType data_type, WiseFieldType type){
    return params.wise_structure->add_field(name,family,data_type,type);
}

bool WiseElement::add_field(string name, string data, WiseFieldFamily family, WiseDataType type, WiseFieldType field_type){
    return params.wise_structure->add_field(name,data,family,type,field_type);
}

bool WiseElement::add_point_info(string name, WiseDataType type){
    return params.wise_structure->add_point_info(name,type);
}

bool WiseElement::add_cell_info(string name, WiseDataType type){
    return params.wise_structure->add_cell_info(name,type);
}

bool WiseElement::add_line_info(string name, WiseDataType type){
    return params.wise_structure->add_line_info(name,type);
}

bool WiseElement::add_point_data(string data, string info_name){
    return params.wise_structure->add_point_data(info_name,data);
}
bool WiseElement::set_all_point_data(string data, string info_name){
    return params.wise_structure->set_all_point_data(info_name,data);
}

bool WiseElement::set_point_data(int id, string data, string info_name){
    return params.wise_structure->set_point_data(id,info_name,data);
}


vector<string> WiseElement::get_point_data(string info_name){
    return params.wise_structure->get_point_data(info_name);
}


bool WiseElement::add_cell_data(string data, string info_name){
    return  params.wise_structure->add_cell_data(info_name,data);
}

bool WiseElement::set_all_cell_data( string data, string info_name){
    return params.wise_structure->set_all_cell_data(info_name,data);
}

bool WiseElement::set_cell_data(int id, string data, string info_name){
    return params.wise_structure->set_cell_data(id,info_name,data);
}

bool WiseElement::set_cell_type(int id, int type){
    return params.wise_structure->set_cell_type(id,type);
}

vector<string> WiseElement::get_cell_data(string info_name){
    return params.wise_structure->get_cell_data(info_name);
}

bool WiseElement::scale_through(vector<pair<string,double>> points_scale, vector<pair<string,double>> lines_scale,
                               vector<pair<string,double>> cells_scale, vector<pair<string,double>> fields_scale){
    return params.wise_structure->scale_through(points_scale,lines_scale,cells_scale,fields_scale);
}

void WiseElement::scale_through(vector<tuple<WiseCellType,string,double>> scaling){
    params.wise_structure->scale_through(scaling);
}


bool WiseElement::add_line_data(string data, string info_name){
    return params.wise_structure->add_line_data(info_name,data);
}

bool WiseElement::set_all_line_data(string data, string info_name){
    return params.wise_structure->set_all_line_data(info_name,data);
}

bool WiseElement::set_line_data(int id, string data, string info_name){
    return params.wise_structure->set_line_data(id,info_name,data);
}

vector<string> WiseElement::get_line_data(string info_name){
    return params.wise_structure->get_line_data(info_name);
}

bool WiseElement::add_field_data(string data, string info_name){
    return params.wise_structure->add_field_data(data,info_name);
}

string WiseElement::get_field(string info_name, int id){
    for (int i = 0; i < info_name.size(); i++) {
        vector<string> aux = get_field_data(info_name);

        if(!aux.empty()){
            if(id < 0 or id >= aux.size())
                return aux.back();
            return aux.operator[](id);
        }
    }

    return string();
}

vector<string> WiseElement::get_field_data(string info_name){
    return params.wise_structure->get_field_data(info_name);
}

bool WiseElement::has_field(string info_name){
    return params.wise_structure->has_field(info_name);
}

bool WiseElement::field_set(string info_name){
    return params.wise_structure->field_set(info_name);
}

bool WiseElement::field_set(string info_name, WiseDataType data_type){
    return params.wise_structure->field_set(info_name,data_type);
}

bool WiseElement::field_set(string info_name, string data_type){
    return field_set(info_name, wise_data_type_to_string(data_type));
}

void WiseElement::set_default_fields(){
    params.wise_structure->set_default_fields();
}

bool WiseElement::set_all_field_data(string data, string info_name){
    return params.wise_structure->set_all_field_data(info_name,data);
}

bool WiseElement::set_field_data(int id, string data, string info_name){
    return params.wise_structure->set_field_data(id,info_name,data);
}

int WiseElement::ID(){
    return params.ID;
}

bool WiseElement::operator==(WiseElement* obj){
    return (params.ID == obj->ID());
}

bool WiseElement::operator==(int id){
    return (params.ID == id);
}

vector<point> WiseElement::get_points(){
    return params.wise_structure->get_points();
}

int WiseElement::get_points_size(){
    return params.wise_structure->get_points_size();
}

vector<WiseCell> WiseElement::get_cells(){
    return params.wise_structure->get_cells();
}

vector<WiseLine> WiseElement::get_lines(){
    return params.wise_structure->get_lines();
}

vector<tuple<WiseInfo,WiseFieldFamily,WiseFieldType>> WiseElement::get_fields(){
    return params.wise_structure->get_fields();
}

string WiseElement::dna(){
    return params.element_key;
}

WiseElementStatus WiseElement::status(){
    return params.status;
}

double WiseElement::t(){
    return params.model_time;
}

void WiseElement::t(double t){
    params.model_time = (t);
}

double WiseElement::dt(){
    return params.dt;
}

void WiseElement::dt(double dt){
   params.dt = (dt);
}

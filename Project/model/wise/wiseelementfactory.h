#ifndef WISEOBJECTFACTORY_H
#define WISEOBJECTFACTORY_H

#include "wiseelement.h"

#include <QDir>

using namespace std;

class WiseElementFactory
{
public:

    WiseElementFactory(string wise_type, string name);
    ~WiseElementFactory();

    string get_name();

    string get_type();
    
    unique_ptr<WiseElement> xml_load(string name, string filename);

    unique_ptr<WiseElement> xml_load(TiXmlElement* doc);

    unique_ptr<WiseElement> xml_load(string name, TiXmlElement* doc);

    unique_ptr<WiseElement> xml_cold_load(string name, TiXmlElement* doc);
    
    unique_ptr<WiseElement> vtk_load(string name, string filename);

    unique_ptr<WiseElement> clone(string name, WiseElement* obj, WiseElementStatus clone_type);

    unique_ptr<WiseElementFactory> clone_factory();

    void save_vtk(WiseElement* obj);

    void save_xml(WiseElement* obj);

    unique_ptr<WiseElement> scale_through(WiseElement* obj, vector<pair<string,double>> points_scale, vector<pair<string,double>> lines_scale,
                                         vector<pair<string,double>> cells_scale, vector<pair<string,double>> fields_scale);

    void scale_through(WiseElement* obj,vector<tuple<WiseCellType,string,double>> scaling);
    /*********************
     * VIRTUAL METHODS
     *********************/

    virtual unique_ptr<WiseElementFactory> clone_factory_aux() = 0;

    //Creates an empty vessel for the wise structure to be inserted
    virtual unique_ptr<WiseElement>  make(string name, string dna = "NONE") = 0;

    //Recreates the wise object from the wise structure
    virtual unique_ptr<WiseElement> recreate(unique_ptr<WiseElement> ptr) = 0;

    virtual vector<string> examples_list() = 0;

    virtual unique_ptr<WiseElement> make_example(string example, string name) = 0;

private:
    const string path;

    const string name;

    const string wise_type;
};

#endif // WISEOBJECTFACTORY_H

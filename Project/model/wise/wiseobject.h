#ifndef WISEMODEL_H
#define WISEMODEL_H

#include "wisepeer.h"
#include "wisecollection.h"
#include "wiseiterationfactory.h"
#include "graphicobjectfactory.h"
#include "wiseiterationfactories.h"
#include "graphicobjectfactories.h"

#include "tinyxml/tinyxml.h"

#include <QString>

#include <memory>

using namespace std;

using namespace wise;

class WiseObject : public WisePeer
{
public:
    friend class WiseObjectFactory;
    friend class ArteryTreeWiseObjectFactory;
    friend class GraphicWiseObjectFactory;
    friend class MeshWiseObjectFactory;
    friend class PolyWiseObjectFactory;

    ~WiseObject();

    int ID();
    
    string get_name();

    string dna();

    QString get_iteration_factory_name();

    vector<string> get_iteration_factory_candidates();

    void set_iteration_factory(unique_ptr<WiseIterationFactory> factory);

    void set_iteration_factory(string factory);

    QString get_graphic_factory_name();

    void set_graphic_factory(unique_ptr<GraphicObjectFactory> factory);

    TiXmlElement* to_XML();

    string to_string();

    WiseObjectParams* get_params();

    void scale_through(vector<tuple<WiseCellType,string,double>> scaling);

    WiseElement* get_hot();

    unique_ptr<WiseElement> pop_hot();

    bool place_hot(unique_ptr<WiseElement> ptr);

    /***
     * STATUS CHANGES
     *
     * ***/

    bool ready_set();

    bool set_go();

    bool go_go(); //iterate

    bool go_finished();

    bool go_set();

    bool finished_set();

private:
    WiseObject(string name, string type, WiseObjectStatus status, double dt, double model_time, int frames, int iterations, unique_ptr<WiseElement> hot_object , vector<unique_ptr<WiseElement>> cold_objects, unique_ptr<WiseElementFactory> obj_factory);
    WiseObject(string name, unique_ptr<WiseElement> hot_object , unique_ptr<WiseElement> cold_object , unique_ptr<WiseElementFactory> obj_factory);
    WiseObject(string name, unique_ptr<WiseElement> hot_object , vector<unique_ptr<WiseElement>> freezer , unique_ptr<WiseElementFactory> obj_factory);
    const long int id;

    WiseObjectParams params;

    unique_ptr<WiseIterationFactory> it_factory;

    unique_ptr<GraphicObjectFactory> gr_factory;

    unique_ptr<GraphicModel> gr_model;

    static long int CURR_ID;
};

#endif // WISEMODEL_H

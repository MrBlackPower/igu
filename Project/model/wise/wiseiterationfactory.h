#ifndef WISEITERATIONFACTORY_H
#define WISEITERATIONFACTORY_H

#include <memory>
#include <map>

#include "wisecollection.h"

class WiseIterationFactory
{
public:
    WiseIterationFactory(string type, string operation);

    virtual unique_ptr<WiseIterationFactory> clone() = 0;

    virtual void set_fields(WiseCollection* object) = 0;

    virtual bool iterate(WiseCollection* object) = 0;

    string get_type();

    string get_operation();

private:
    const string type;
    const string operation;
};

#endif // WISEITERATIONFACTORY_H

#include "wiseelementfactory.h"

WiseElementFactory::WiseElementFactory(string wise_type, string name) : wise_type(wise_type), name(name), path(QDir::currentPath().toStdString()+ QDir::separator().toLatin1() + "cache" + QDir::separator().toLatin1())
{

}

WiseElementFactory::~WiseElementFactory()
{

}

string WiseElementFactory::get_name(){
    return name;
}

string WiseElementFactory::get_type(){
    return wise_type;
}

unique_ptr<WiseElement> WiseElementFactory::xml_load(TiXmlElement* doc){
    unique_ptr<WiseElement> obj;
    TiXmlHandle hDoc(doc);
    TiXmlElement* pElem;
    TiXmlHandle hRoot(0);
    // block: WISE_ELEMENT
    pElem = hDoc.FirstChildElement("ELEMENT_PARAMS").Element();
    // should always have a valid root but handle gracefully if it does

    if(!pElem){
        return 0;
    }

    QString name         = pElem->Attribute("NAME");

    obj = xml_load(name.toStdString(),doc);

    if(!obj){
        delete obj.get();
        return 0;
    }

    return move(obj);
}

unique_ptr<WiseElement> WiseElementFactory::xml_load(string name, string filename){
    TiXmlDocument doc(filename.data());

    if (!doc.LoadFile()){
        return 0;
    }

    return xml_load(name,doc.FirstChildElement("WISE_ELEMENT"));
}

unique_ptr<WiseElement> WiseElementFactory::xml_cold_load(string name, TiXmlElement* doc){
    unique_ptr<WiseElement> r = xml_load(name,doc);

    if(r.get() == NULL)
        return NULL;

    save_vtk((r.get()));

    save_xml((r.get()));

    r->clear(true);

    r->set_status(wise::COLD);

    return r;
}

unique_ptr<WiseElement> WiseElementFactory::xml_load(string name, TiXmlElement* doc){
    unique_ptr<WiseElement> obj = make(name);
    TiXmlElement* pElem;

    TiXmlElement* subElem = NULL;
    TiXmlElement* subsubElem = NULL;

    // block: ELEMENT_PARAMS
    pElem = doc->FirstChildElement( "ELEMENT_PARAMS" );

    if (!pElem){
        delete obj.release();
        return 0;
    }

    //PARAMETERS - START

    obj->get_params()->name         = pElem->Attribute("NAME");
    obj->get_params()->filename_vtk = pElem->Attribute("FILENAME_VTK");
    obj->get_params()->filename_xml = pElem->Attribute("FILENAME_XML");
    obj->get_params()->type         = pElem->Attribute("TYPE");
    obj->get_params()->element_key  = pElem->Attribute("ELEMENT_KEY");
    obj->get_params()->dt           = QString(pElem->Attribute("DT")).toDouble();
    obj->get_params()->model_time   = QString(pElem->Attribute("MODEL_TIME")).toDouble();
    obj->get_params()->instance    = QString(pElem->Attribute("INSTANCE")).toInt();
    obj->get_params()->status       = wise_element_status_to_string(pElem->Attribute("STATUS"));

    //PARAMETERS - END


    TiXmlElement* SElem;
    /**************
     * block: WISE_STRUCTURE
     * *****************/
    TiXmlHandle WSElem = doc->FirstChildElement("WISE_STRUCTURE" );
    /**************
     * block: STRUCTURE_PARAMETERS
     * *****************/
    SElem = WSElem.FirstChild( "STRUCTURE_PARAMETERS" ).Element();

    obj->get_params()->wise_structure->get_params()->structure_key = SElem->Attribute("STRUCTURE_KEY");
    /**************
     * block: STRUCTURE
     * *****************/
    SElem = WSElem.FirstChild( "STRUCTURE" ).Element();


    if (!SElem){
        delete obj.release();
        return 0;
    }

    //      subblock: POINTS
    subElem = SElem->FirstChildElement("POINTS");

    if (!subElem){
        delete obj.release();
        return 0;
    }

    for( subsubElem = subElem->FirstChildElement("POINT"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
        int id;
        double x,y,z;

        subsubElem->QueryIntAttribute("ID",&id);
        x = QString::asprintf("%s",subsubElem->Attribute("X")).toDouble();
        y = QString::asprintf("%s",subsubElem->Attribute("Y")).toDouble();
        z = QString::asprintf("%s",subsubElem->Attribute("Z")).toDouble();

        obj->add_point(x,y,z);
    }


    //      subblock: CELLS
    subElem = SElem->FirstChildElement("CELLS");

    if (!subElem){
        delete obj.release();
        return 0;
    }

    for( subsubElem = subElem->FirstChildElement("CELL"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
        int id;
        vector<int> points;

        subsubElem->QueryIntAttribute("ID",&id);

        for (TiXmlElement* subsubsubElem = subsubElem->FirstChildElement("CELL_POINT"); subsubsubElem; subsubsubElem=subsubsubElem->NextSiblingElement()) {
            int id_ = 0;
            subsubsubElem->QueryIntAttribute("ID",&id_);

            points.push_back(id_);
        }

        if(!points.empty()){
            obj->add_cell(points);
        }
    }


    //      subblock: LINES
    subElem = SElem->FirstChildElement("LINES");

    if (!subElem){
        delete obj.release();
        return 0;
    }

    for( subsubElem = subElem->FirstChildElement("LINE"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
        int id, a, b;

        subsubElem->QueryIntAttribute("ID",&id);
        subsubElem->QueryIntAttribute("A",&a);
        subsubElem->QueryIntAttribute("B",&b);

        obj->add_line(a,b);
    }

    /**************
    * block: DATA
    * *****************/

    TiXmlElement* DElem;

    /**************
     * block: STRUCTURE
     * *****************/
    DElem = WSElem.FirstChild( "DATA" ).Element();

    //      subblock: POINTS
    subElem = DElem->FirstChildElement("POINT_DATA");

    if (!subElem){
        delete obj.release();
        return 0;
    }

    for( subsubElem = subElem->FirstChildElement("INFO"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
        string name;

        WiseDataType type = UNKNOWN;
        string type_s = "";

        const char * p_name = subsubElem->Attribute("NAME");

        if(!p_name){
            delete obj.release();
            return 0;
        }
        name = p_name;

        p_name = subsubElem->Attribute("TYPE");

        if(!p_name){
            delete obj.release();
            return 0;
        }

        type = wise_data_type_to_string(p_name);

        obj->add_point_info(name,type);

        for (TiXmlElement* subsubsubElem = subsubElem->FirstChildElement("DATA"); subsubsubElem; subsubsubElem=subsubsubElem->NextSiblingElement()) {
            string data;

            data = subsubsubElem->Attribute("VALUE");

            obj->add_point_data(data,name);
        }

    }


    //      subblock: CELLS
    subElem = DElem->FirstChildElement("CELL_DATA");

    if (!subElem){
        delete obj.release();
        return 0;
    }

    for( subsubElem = subElem->FirstChildElement("INFO"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
        string name;

        WiseDataType type = UNKNOWN;
        string type_s = "";

        const char * p_name = subsubElem->Attribute("NAME");

        if(!p_name){
            delete obj.release();
            return 0;
        }

        name = p_name;

        p_name = subsubElem->Attribute("TYPE");

        if(!p_name){
            delete obj.release();
            return 0;
        }

        type = wise_data_type_to_string(p_name);

        obj->add_cell_info(name,type);

        for (TiXmlElement* subsubsubElem = subsubElem->FirstChildElement("DATA"); subsubsubElem; subsubsubElem=subsubsubElem->NextSiblingElement()) {
            string data;

            data = subsubsubElem->Attribute("VALUE");

            obj->add_cell_data(data,name);
        }

    }


    //      subblock: LINES
    subElem = DElem->FirstChildElement("LINE_DATA");

    if (!subElem){
        delete obj.release();
        return 0;
    }

    for( subsubElem = subElem->FirstChildElement("INFO"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
        string name;

        WiseDataType type = UNKNOWN;
        string type_s = "";

        const char * p_name = subsubElem->Attribute("NAME");

        if(!p_name){
            delete obj.release();
            return 0;
        }

        name = p_name;

        p_name = subsubElem->Attribute("TYPE");

        if(!p_name){
            delete obj.release();
            return 0;
        }

        type = wise_data_type_to_string(p_name);

        obj->add_line_info(name,type);

        for (TiXmlElement* subsubsubElem = subsubElem->FirstChildElement("DATA"); subsubsubElem; subsubsubElem=subsubsubElem->NextSiblingElement()) {
            string data;

            data = subsubsubElem->Attribute("VALUE");

            obj->add_line_data(data,name);
        }

    }


    //      subblock: FIELDS
    subElem = DElem->FirstChildElement("FIELD");

    if (!subElem){
        delete obj.release();
        return 0;
    }

    for( subsubElem = subElem->FirstChildElement("INFO"); subsubElem; subsubElem=subsubElem->NextSiblingElement()){
        string name;

        WiseDataType data_type = UNKNOWN;
        string type_s = "";
        WiseFieldType type = INITIAL;
        WiseFieldFamily family = WISE;

        const char * p_name = subsubElem->Attribute("NAME");

        if(!p_name){
            delete obj.release();
            return 0;
        }

        name = p_name;

        p_name = subsubElem->Attribute("DATA_TYPE");

        if(!p_name){
            delete obj.release();
            return 0;
        }

        data_type = wise_data_type_to_string(p_name);

        p_name = subsubElem->Attribute("TYPE");

        if(!p_name){
            delete obj.release();
            return 0;
        }

        type = wise_field_type_to_string(p_name);

        p_name = subsubElem->Attribute("FAMILY");

        if(!p_name){
            delete obj.release();
            return 0;
        }

        family = wise_field_family_to_string(p_name);

        obj->add_field(name,family,data_type,type);

        for (TiXmlElement* subsubsubElem = subsubElem->FirstChildElement("DATA"); subsubsubElem; subsubsubElem=subsubsubElem->NextSiblingElement()) {
            string data;

            data = subsubsubElem->Attribute("VALUE");

            obj->add_field_data(data,name);
        }

    }

    obj = recreate(move(obj));

    if(obj == 0)
        return 0;

    obj->update_structure();

    return obj;
}

unique_ptr<WiseElement> WiseElementFactory::vtk_load(string name, string filename){
    FILE *file;

    unique_ptr<WiseElement> obj = make(name);

    const char* fileName = filename.data();
    file = fopen(fileName,"r");

    if(file == NULL){
        delete obj.release();
        return 0;
    }

    char line[100];

    int nPoints = 0;

    int nLines = 0;

    int nCells = 0;

    while(fgets(line, 80, file) != NULL){
        /**
          * Points Coordinates
          *********************/
        if(strstr(line,"POINTS") != NULL){
            //Gets number of points
            sscanf (line, "%*s %d %*s", &nPoints);

            //Gets all points
            for(int i = 0; i < nPoints && fgets(line, 80, file) != NULL; i++){
                stringstream s(line);

                double x = 0.0;
                double y = 0.0;
                double z = 0.0;

                s >> x >> y >> z;

               obj->add_point(x, y, z);
            }
        }
        /**
          * Lines
          *********************/
        if(strstr(line,"LINES") != NULL){
            sscanf (line, "%*s %d %*s", &nLines);

            //Gets all lines
            for(int i = 0; i < nLines && fgets(line, 80, file) != NULL; i++){
                stringstream s(line);   // alias of size_t
                int n,a,b;

                s >> n >> a >> b;

                if(n != 2 or a == b){
                    delete obj.release();
                    return 0;
                }
                obj->add_line(a,b);
            }
        }

        /**
          * Cells
          *********************/
        if(strstr(line,"CELLS") != NULL || strstr(line,"POLYGON") != NULL){
            sscanf (line, "%*s %d %*s", &nCells);

            //Gets all cells
            for(int i = 0; i < nCells && fgets(line, 80, file) != NULL; i++){
                stringstream s(line);

                vector<int> id;
                for(int i = 0; s; i++){
                    int aux = 0;
                    s >> aux;
                    id.push_back(aux);
                }

                obj->add_cell(id);
            }
        }

        /**
          * Cell Types
          *********************/
        if((strstr(line,"CELL_TYPES") != NULL || strstr(line,"POLYGON") != NULL) && nCells > 0){
            int nTypes = 0;
            sscanf (line, "%*s %d", &nTypes);

            if(nCells != nTypes){
                delete obj.release();
                return 0;
            }

            //Gets all cell types
            for(int i = 0; i < nCells && fgets(line, 80, file) != NULL; i++){
                stringstream s(line);

                int type = 0;
                s >> type;
                obj->set_cell_type(i,type);
            }
        }

        /**
          * point Data
          *********************/
        if(strstr(line,"POINT_DATA") != NULL){
            int aux;
            sscanf (line, "%*s %d", &aux);

            if(aux != nPoints || !igufile::jump_read_line(line,file)){
                delete obj.release();
                return 0;
            }

            while(true){
                string tag = "";
                string name = "";
                string type = "";

                stringstream s(line);

                s >> tag >> name >> type;

                tag = QString(tag.data()).toLower().toStdString();
                type = QString(type.data()).toLower().toStdString();

                WiseDataType datatype;

                if(tag == "SCALARS" || tag == "scalars"){
                    if(type == "float"){
                        datatype = FLOAT;
                    } else if(type == "double"){
                        datatype = DOUBLE;
                    } else if(type == "int"){
                        datatype = INT;
                    } else if(type == "string"){
                        datatype = STRING;
                    } else {
                        datatype = DOUBLE;
                    }
                } else if(tag == "VECTORS"){
                    datatype = VECTOR;
                } else if(tag == "NORMALS"){
                    datatype = VECTOR;

                } else if(tag == "TENSORS"){
                    datatype = TENSOR;
                    return 0; // TENSORS WILL PROVOKE AN ERROR IN LOADING <<HAS TO READ 3 LINES>>
                } else {
                    datatype = UNKNOWN;
                }

                if(datatype == UNKNOWN){
                    delete obj.release();
                    return 0;
                }

                obj->add_point_info(name,datatype);

                //LOOKUP TABLE IGNORED
                if(!igufile::jump_read_line(line,file)){
                    delete obj.release();
                    return 0;
                }

                //DATA
                for(int i = 0; i < nPoints; i++){
                    if(!igufile::jump_read_line(line,file)){
                        delete obj.release();
                        return 0;
                    }

                    string data(line);
                    obj->add_point_data(data,name);
                }

                //JUMPS END LINE
                if(!igufile::jump_read_line(line,file))
                    break;
            }
        }
        /**
          * Cell Data
          *********************/
        if(strstr(line,"CELL_DATA") != NULL){
            int aux;
            sscanf (line, "%*s %d", &aux);

            if((aux != nCells && aux != nLines) || !igufile::jump_read_line(line,file)){
                delete obj.release();
                return 0;
            }

            while(true){
                string tag = "";
                string name = "";
                string type = "";

                stringstream s(line);

                s >> tag >> name >> type;

                tag = QString(tag.data()).toLower().toStdString();
                type = QString(type.data()).toLower().toStdString();

                WiseDataType datatype;

                if(tag == "SCALARS" || tag == "scalars"){
                    if(type == "float"){
                        datatype = DOUBLE;
                    } else if(type == "double"){
                        datatype = DOUBLE;
                    } else if(type == "int"){
                        datatype = INT;
                    } else if(type == "string"){
                        datatype = STRING;
                    } else {
                        datatype = UNKNOWN;
                    }
                } else if(tag == "VECTORS"){
                    datatype = VECTOR;
                } else if(tag == "NORMALS"){
                    datatype = VECTOR;

                } else if(tag == "TENSORS"){
                    datatype = TENSOR;
                    return 0; // TENSORS WILL PROVOKE AN ERROR IN LOADING <<HAS TO READ 3 LINES>>
                } else {
                    datatype = UNKNOWN;
                }

                if(datatype == UNKNOWN){
                    delete obj.release();
                    return 0;
                }


                obj->add_cell_info(name,datatype);

                //LOOKUP TABLE IGNORED
                if(!igufile::jump_read_line(line,file)){
                    delete obj.release();
                    return NULL;
                }

                //DATA
                for(int i = 0; i < aux; i++){
                    if(!igufile::jump_read_line(line,file)){
                        delete obj.release();
                        return NULL;
                    }

                    string data(line);
                    obj->add_cell_data(data,name);
                }

                //JUMPS END LINE
                if(!igufile::jump_read_line(line,file))
                    break;
            }
        }
    }

    fclose(file);

    obj = recreate(move(obj));

    if(obj == 0)
        return 0;

    obj->update_structure();

    return obj;
}

unique_ptr<WiseElement> WiseElementFactory::scale_through(WiseElement* obj, vector<pair<string,double>> points_scale, vector<pair<string,double>> lines_scale,
                                                        vector<pair<string,double>> cells_scale, vector<pair<string,double>> fields_scale){
    unique_ptr<WiseElement> r = clone(obj->get_name(),obj, wise::HOT);

    obj->scale_through(points_scale,lines_scale,cells_scale,fields_scale);

    return r;
}

void WiseElementFactory::scale_through(WiseElement* obj,vector<tuple<WiseCellType,string,double>> scaling){
    obj->scale_through(scaling);
}


unique_ptr<WiseElement> WiseElementFactory::clone(string name, WiseElement* obj, WiseElementStatus clone_type){
    if(!obj)
        return NULL;

    if(obj->get_params()->type != wise_type)
        return NULL;

    if(obj->get_params()->status == wise::COLD){
        return move(xml_load(name,(path+obj->get_params()->filename_xml)));
    }

    unique_ptr<WiseElement> r = move(make(name,obj->dna())); //creates empty vessel

    r->get_params()->element_key = obj->dna();
    r->get_params()->instance ++;

    r->get_params()->wise_structure->operator=(obj->get_params()->wise_structure.get());

    r->get_params()->set_filename(r->get_params()->instance);

    switch (clone_type) {
    case wise::COLD:
        r->update_xml();

        save_vtk((r.get()));

        save_xml((r.get()));

        r->clear(true);

        r->set_status(wise::COLD);

        break;
    case wise::HOT:
        r = recreate(move(r));

        r->update_xml();

        r->set_status(wise::HOT);
        break;

    default:
        r->set_status(clone_type);
        break;
    }

    return r;
}

unique_ptr<WiseElementFactory> WiseElementFactory::clone_factory(){
    return clone_factory_aux();
}

void WiseElementFactory::save_vtk(WiseElement* obj){
    FILE *file;

    string f = obj->get_params()->filename_vtk;
    QString fileName = QString::asprintf("%s%s",path.data(),f.data());
    file = fopen(fileName.toStdString().data(),"wr");

    if(file == NULL){
        delete obj;
        return;
    }

    fprintf(file,"%s/n",obj->to_string(false).data());

    fclose(file);
}

void WiseElementFactory::save_xml(WiseElement* obj){
    FILE *file;

    string f = obj->get_params()->filename_xml;
    QString fileName = QString::asprintf("%s%s",path.data(),f.data());
    file = fopen(fileName.toStdString().data(),"wr");

    if(file == NULL){
        delete obj;
        return;
    }

    fprintf(file,"%s/n",obj->to_string(true).data());

    fclose(file);
}


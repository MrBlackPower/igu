#include "graphicobject.h"

unsigned int GraphicObject::CURR_GID = 0;
GraphicObject* GraphicObject::first_graphic = NULL;
GraphicObject* GraphicObject::last_graphic = NULL;
SmartObject* GraphicObject::default_raw = NULL;

double GraphicObject::angles_per_pixel = 1;
double GraphicObject::scalar_step_per_pixel = 0.01;
double GraphicObject::move_per_pixel = 10;
double GraphicObject::z_offset = 0.0;

GraphicObject::GraphicObject(QString name, SmartObject* parent) : SmartObject(name,parent) , GID(CURR_GID ++){
    mouse_last_pos = point(0,0,0,0);
    mouse_being_used = false;
    colorBar = Brasil();
    status = Fresh;
    for(int i = 0; i < 9; i++)
        guide_lines_status.push_back(true);

    QPainterSet = false;
    CID = -1;

    next_graphic = NULL;
    previous_graphic= NULL;
    master = NULL;

    SmartLogMessage aux(getSID(),"");

    if(first_graphic == NULL && last_graphic == NULL){
        first_graphic = this;
        last_graphic = this;
    } else {
        if(last_graphic == NULL){
            aux = name.asprintf("ERROR IN GRAPHIC OBJECT LINKAGE");
            aux.setType(SmartLogType::LOG_CRASH);

            emit emit_log(aux);
            status = Crashed;
            last_graphic = this;
        } else {
            previous_graphic = last_graphic;
            last_graphic->next_graphic = this;
            last_graphic = this;
        }
    }

    if(default_raw){
        QObject::connect(this,SIGNAL(print_raw(vector<QString>,QString)),default_raw,SLOT(addToBuffer(vector<QString>,QString)));
    }

    aux = name.asprintf("<GRAPHIC OBJECT #GID=%d CREATED >", GID);
    aux.setType(SmartLogType::LOG_CREATE);

    emit emit_log(aux);

    classStack->addClass("GRAPHIC OBJECT", GID);
}

GraphicObject::~GraphicObject(){
    emit emit_log(SmartLogMessage(getSID(),QString::asprintf("<GRAPHIC OBJECT #GID=%d DELETED >", GID),SmartLogType::LOG_DELETE));
    //GRAPHIC OBJECT LIST
    if(next_graphic != NULL){
        if(first_graphic == this){
            first_graphic = next_graphic;
            next_graphic->previous_graphic = NULL;
        } else if(previous_graphic != NULL){
            next_graphic->previous_graphic = previous_graphic;
            previous_graphic->next_graphic = next_graphic;
        } else {
            next_graphic->previous_graphic = NULL;
        }
    } else if(previous_graphic != NULL){
        previous_graphic->next_graphic = NULL;

        if(last_graphic == this)
            last_graphic = previous_graphic;
    } else if(last_graphic == this && first_graphic == this){
        first_graphic = NULL;
        last_graphic = NULL;
    }

    //WARNING! THE DRAW GRAPH & ITERATION TREE DOES NOT UPDATE AS GRAPHIC OBJECTS ARE DELETED
}

void GraphicObject::setDefaultRawPool(SmartObject* default_raw){
    if(GraphicObject::default_raw != NULL){
        purgeDefaultLogPool();
    }

    if(default_raw ==NULL)
        return;

    GraphicObject::default_raw = default_raw;
    GraphicObject* it = first_graphic;

    while(it != NULL){
        QObject::connect(it,SIGNAL(emit_log(vector<QString>)),default_raw,SLOT(addToBuffer(vector<QString>)));

        it = it->next_graphic;
    }
}

void GraphicObject::purgeDefaultRawPool(){
    GraphicObject* it = first_graphic;

    while(it != NULL){
        QObject::disconnect(it,SIGNAL(emit_log(vector<QString>)),default_raw,SLOT(addToBuffer(vector<QString>)));

        it = it->next_graphic;
    }

    default_raw = NULL;
}

QString GraphicObject::statusToString(GraphicObjectStatus status){
    switch (status) {
    case Fresh:
        return "FRESH";
    case Initialized:
        return "INITIALIZED";
    case Iterated:
        return "ITERATED";
    case Finished:
        return "FINISHED";
    case Crashed:
        return "CRASHED";
    default:
        return "NONE";
    }
}

void GraphicObject::setLimit(int max_x, int max_y, int max_z, int  min_x, int  min_y, int  min_z){
    point max = point(0, max_x,max_y,max_z);
    point min = point(0,min_x,min_y,min_z);

    setLimit(max,min);
}

void GraphicObject::setLimit(point limit_max, point limit_min){
    vector<point> aux_vec;

    if(limit_max.X() < limit_min.X()){
        double aux = limit_max.X();
        limit_max.X(limit_min.X());
        limit_min.X(aux);
    }
    if(limit_max.Y() < limit_min.Y()){
        double aux = limit_max.Y();
        limit_max.Y(limit_min.Y());
        limit_min.Y(aux);
    }
    if(limit_max.Z() < limit_min.Z()){
        double aux = limit_max.Z();
        limit_max.Z(limit_min.Z());
        limit_min.Z(aux);
    }

    aux_vec.push_back(limit_max);
    aux_vec.push_back(limit_min);

    resetLimit();

    updateLimit(aux_vec);
}

void GraphicObject::mouse_press(int width, int height, point eye, QMouseEvent *event, GraphicTools tool){
    if(event->x() < 0 || event->x() > width)
        return;

    if(event->y() < 0 || event->y() > height)
        return;


    switch (tool) {
    case LIMIT:
        if (event->buttons() & Qt::LeftButton) {
            setLimitDialog();
        } else if (event->buttons() & Qt::RightButton) {
            resizeTransform();
        } else {

        }
        break;
    case ROTATE:
        if (event->buttons() & Qt::LeftButton) {

        } else if (event->buttons() & Qt::RightButton) {
            point r = point();
            setRotation(r);
        } else {

        }
        break;
    case SCALE:
        if (event->buttons() & Qt::LeftButton) {

        } else if (event->buttons() & Qt::RightButton) {
            resizeTransform();
        } else {

        }
        break;
    case MOVE:
        if (event->buttons() & Qt::LeftButton) {

        } else if (event->buttons() & Qt::RightButton) {
            resizeTransform();
        } else {

        }
        break;
    default:
        mouse_press_event(width,height,eye,event,tool);
        break;
    }

    mouse_last_pos = point(0,event->pos().x(),event->pos().y());
    mouse_last_pos = point(1,mouse_last_pos.X(), height - mouse_last_pos.Y(), mouse_last_pos.Z());

}

void GraphicObject::mouse_move(int width, int height, point eye, QMouseEvent *event, GraphicTools tool){
    double dx = (int)(event->x() - mouse_last_pos.X());
    double dy = (int)((height - event->y()) - mouse_last_pos.Y());

    switch (tool) {
    case LIMIT:
        break;
    case ROTATE:
        if (event->buttons() & Qt::LeftButton) {
            point r = getRotation();
            r = r + point(1,dy,dx,ZERO);
            setRotation(r);
        } else if (event->buttons() & Qt::RightButton) {
            point r = getRotation();
            r = r + point(1,dy,ZERO,dx);
            setRotation(r);
        } else {

        }
        break;
    case SCALE:
        if (event->buttons() & Qt::LeftButton) {
            trnsf.scale += dx * scalar_step_per_pixel;
        } else if (event->buttons() & Qt::RightButton) {

        } else {

        }
        break;
    case MOVE:
        if (event->buttons() & Qt::LeftButton) {
            move(dx * move_per_pixel,dy * move_per_pixel);
        } else if (event->buttons() & Qt::RightButton) {
            move(ZERO,ZERO,dx * move_per_pixel);
        } else {

        }
        break;
    default:
        mouse_move_event(width,height,eye,event,tool);
        break;
    }

    mouse_last_pos = point(0,event->pos().x(),event->pos().y());
    mouse_last_pos = point(1,mouse_last_pos.X(), height - mouse_last_pos.Y(), mouse_last_pos.Z());
}

vector<QString> GraphicObject::extrapolations(){
    vector<QString> aux = getExtrapolations();
    QString ex;

    //ADDS EXTRAPOLATIONS EVERY GRAPHIC OBJECT HAS
    ex = "LIVE_PARAM_GRAPHIC";
    aux.push_back(ex);

    return aux;
}

QString GraphicObject::liveParameterList(){
    QString r;

    r += "ITERATIONS MODEL_TIME DELTA_T ";
    r += getLiveParameterList();

    return r;
}

QString GraphicObject::liveParameter(QString name){
    QString r;

    if(name == "ITERATIONS"){
        r = QString::asprintf("%d",time.iterations);
    } else if (name == "MODEL_TIME"){
        r = QString::asprintf("%f",time.modelTime);
    } else if (name == "DELTA_T"){
        r = QString::asprintf("%f",time.deltaT);
    } else {
        r = getLiveParameter(name);
    }

    return r;
}

DataType GraphicObject::liveParameterDataType(QString name){
    DataType r = undefined;

    if(name == "ITERATIONS"){
        r = integer;
    } else if (name == "MODEL_TIME"){
        r = doublep;
    } else if (name == "DELTA_T"){
        r = doublep;
    } else {
        r = getLiveParameterDataType(name);
    }

    return r;
}

point GraphicObject::getTransformation(){
    return trnsf.translate;
}

point GraphicObject::getRotation(){
    return trnsf.rotation;
}

double GraphicObject::getScale(){
    return trnsf.scale;
}

GraphicObjectStatus GraphicObject::getStatus(){
    return status;
}

void GraphicObject::resetLimit(){
    limit.left = 1.0;
    limit.right = -1.0;
    limit.bottom = 1.0;
    limit.top = -1.0;
    limit._near = 1.0;
    limit._far = -1.0;

    GraphicSpace::Transform t;
    trnsf  = t;
}

void GraphicObject::resetGuide(){
    guide_lines_status.clear();

    for(int i = 0; i < 9; i++)
        guide_lines_status.push_back(true);
}

void GraphicObject::updateLimit(point* p){
    point aux = point(ZERO,p->X(),p->Y(),p->Z());
    updateLimit(aux);
}

void GraphicObject::updateLimit(vector<point> points){
    for(int i = 0; i < points.size(); i++)
        updateLimit(points[i]);
}

void GraphicObject::updateLimit(vector<point*> points){
    for(int i = 0; i < points.size(); i++)
        updateLimit(points[i]);
}

void GraphicObject::updateLimit(point p){
    //IF LIMITS HAVE BEEN INITIALIZED
    bool aux = true;
    if(limit.left > limit.right || limit.bottom > limit.top || limit._near > limit._far)
        aux = false;

    if(!aux){
        limit.left = p.X();
        limit.right = p.X();
        limit.bottom = p.Y();
        limit.top = p.Y();
        limit._near = p.Z();
        limit._far = p.Z();
    } else {
        if(p.X() < limit.left)
            limit.left = p.X();
        else if(p.X() > limit.right)
            limit.right = p.X();

        if(p.Y() < limit.bottom)
            limit.bottom = p.Y();
        else if(p.Y() > limit.top)
            limit.top = p.Y();

        if(p.Z() < limit._near)
            limit._near = p.Z();
        else if(p.Z() > limit._far)
            limit._far = p.Z();
    }


    updateTransform();
}


void GraphicObject::updateTransform(){
    float w = abs(limit.right - limit.left);
    float h = abs(limit.top - limit.bottom);
    float d = abs(limit._near - limit._far);

    point* dist = new point(1,-(limit.left + w/2),-(limit.bottom + h/2),-(limit._near + d/2));
    trnsf.translate = *dist;

    //biggest scaling fator
    vector<float> scales;

    //WIDTH SCALE
    if(w != 0)
        scales.push_back((PLOT_SIZE/w) * 0.6);
    else
        scales.push_back(1e+20);

    //HEIGHT SCALE
    if(h != 0)
        scales.push_back((PLOT_SIZE/h) * 0.6);
    else
        scales.push_back(1e+20);

    //DEPTH SCALE
    if(d != 0)
        scales.push_back((PLOT_SIZE/d) * 0.6);
    else
        scales.push_back(1e+20);

    trnsf.scale = MathHelper::vector_min(scales);
}

bool GraphicObject::compatible(VTKFile* file){
    if(!compatibleFields(file))
        return false;

    return compatibility(file);
}

bool GraphicObject::compatibleFields(VTKFile* file){
    vector<QString> cellDataList = file->getCellDataList();
    vector<Field> cellReadList = getCellDataList();
    vector<QString> pointDataList = file->getPointDataList();
    vector<Field> pointReadList = getPointDataList();

    //CELLS
    for(int i= 0; i < cellReadList.size(); i++){
        Field f = cellReadList[i];
        if(f.required && f.stream == FIELD_IN){
            bool found = false;

            for(int j = 0; j < cellDataList.size() && !found; j++)
                if(f.names.contains(cellDataList[j]))
                    found = true;

            if(!found)
                return false;
        }
    }

    //POINTS
    for(int i= 0; i < pointReadList.size(); i++){
        Field f = pointReadList[i];
        if(f.required && f.stream == FIELD_IN){
            bool found = false;

            for(int j = 0; j < pointDataList.size() && !found; j++)
                if(f.names.contains(pointDataList[j]))
                    found = true;

            if(!found)
                return false;
        }
    }

    return true;
}
void GraphicObject::updateGuide(bool status, int id){
    if(id < 0 || id >= guide_lines_status.size())
        return;

    guide_lines_status[id] = status;
}

bool GraphicObject::setGraphicComposerDialog(GraphicObjectComposer* component){
    GraphicObjectComposer aux = *component;
    GraphicComponentDialog* dialog = new GraphicComponentDialog(aux);
    QEventLoop loop;
    QTimer timeout;

    connect(dialog,SIGNAL(componentSet()),&loop,SLOT(quit()));
    connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

    timeout.start(TIMEOUT);
    dialog->show();
    loop.exec();

    if(dialog->Set()){
        component->operator=(dialog->getComponent());

        delete dialog;

        return true;
    }

    delete dialog;

    return false;
}

bool GraphicObject::setLimitDialog(){
    point max = point (0,limit.right,limit.top,limit._far);
    point min = point (0,limit.left,limit.bottom,limit._near);
    GraphicLimitDialog* dialog = new GraphicLimitDialog(max,min);
    QEventLoop loop;
    QTimer timeout;

    connect(dialog,SIGNAL(limitSet()),&loop,SLOT(quit()));
    connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

    timeout.start(TIMEOUT);
    dialog->show();
    loop.exec();

    if(dialog->Set()){
        dialog->getLimit(&max,&min);

        setLimit(max,min);

        delete dialog;

        return true;
    }

    delete dialog;

    return false;
}

QColor GraphicObject::interpolateColor(double value){
    return colorBar.interpolate(value);
}

point GraphicObject::getLimitMax(){
    return point(1,limit.right,limit.top,limit._far);
}

point GraphicObject::getLimitMin(){
    return point(1,limit.left,limit.bottom,limit._near);
}

void GraphicObject::drawCilynder(point *start, point *end, double radius){
    glPushMatrix();
    glTranslatef(start->X()  , start->Y()  , start->Z());
    point aux = point(1,(end->X() - start->X()),(end->Y() - start->Y()),(end->Z() - start->Z()));
    glRotatef(aux.get_theta(),ZERO,ZERO,ONE);
    glRotatef(aux.get_phi(),ZERO,-ONE,ZERO);
    //DRAWS THE CILYNDER ITSELF
    for(int i = 0; i <= visualParams.cilynder_slices; i++){
        double theta = i * (360 / visualParams.cilynder_slices);
        double next_theta = (i + 1) * (360 / visualParams.cilynder_slices);
        glBegin(GL_QUAD_STRIP);
        for (int j = 0; j <= visualParams.cilynder_stacks; j++){
            glNormal3f(ZERO, MathHelper::cosine(theta), MathHelper::sine(theta));
            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(theta), radius * MathHelper::sine(theta));
            glNormal3f(ZERO, MathHelper::cosine(next_theta), MathHelper::sine(next_theta));
            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(next_theta), radius * MathHelper::sine(next_theta));
        }
        glEnd();
    }
    glPopMatrix();
}

void GraphicObject::drawCilynder(point *start, point *end, double radius, vector<double> value){
    if(value.size() != visualParams.cilynder_stacks + 1)
        return;

    glPushMatrix();
    glTranslatef(start->X()  , start->Y()  , start->Z());
    point aux = point(1,(end->X() - start->X()),(end->Y() - start->Y()),(end->Z() - start->Z()));
    glRotatef(aux.get_theta(),ZERO,ZERO,ONE);
    glRotatef(aux.get_phi(),ZERO,-ONE,ZERO);
    //DRAWS THE CILYNDER ITSELF
    for(int i = 0; i <= visualParams.cilynder_slices; i++){
        double theta = i * (360 / visualParams.cilynder_slices);
        double next_theta = (i + 1) * (360 / visualParams.cilynder_slices);
        glBegin(GL_QUAD_STRIP);
        for (int j = 0; j <= visualParams.cilynder_stacks; j++){
            setMaterialColor(colorBar.interpolate(value[j]));
            glNormal3f(ZERO, MathHelper::cosine(theta), MathHelper::sine(theta));
            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(theta), radius * MathHelper::sine(theta));
            glNormal3f(ZERO, MathHelper::cosine(next_theta), MathHelper::sine(next_theta));
            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(next_theta), radius * MathHelper::sine(next_theta));
        }
        glEnd();
    }
    glPopMatrix();
}

void GraphicObject::drawCilynder(point* start, point* end, double radius, vector<double> value, QColor color){
    if(value.size() != visualParams.cilynder_stacks + 1)
        return;

    glPushMatrix();
    glTranslatef(start->X()  , start->Y()  , start->Z());
    point aux = point(1,end->X() - start->X(),end->Y() - start->Y(),end->Z() - start->Z());
    glRotatef(aux.get_theta(),ZERO,ZERO,ONE);
    glRotatef(aux.get_phi(),ZERO,-ONE,ZERO);
    //DRAWS THE CILYNDER ITSELF
    for(int i = 0; i <= visualParams.cilynder_slices; i++){
        double theta = i * (360 / visualParams.cilynder_slices);
        double next_theta = (i + 1) * (360 / visualParams.cilynder_slices);
        bool odd_1 = (i % 2 == 1);
        glBegin(GL_QUADS);
        for (int j = 0; j < visualParams.cilynder_stacks; j++){
            bool odd_2 = (j%2 == 1);

            if((odd_1 && odd_2))
                setMaterialColor(color);
            else
                setMaterialColor(colorBar.interpolate(value[j]));

            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(theta), radius * MathHelper::sine(theta));
            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(next_theta), radius * MathHelper::sine(next_theta));
            glVertex3f((j+1) * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(next_theta), radius * MathHelper::sine(next_theta));
            glVertex3f((j+1) * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(theta), radius * MathHelper::sine(theta));

        }
        glEnd();
    }
    glPopMatrix();
}


void GraphicObject::drawCilynder(point* start, point* end, double startRadius, double endRadius){
    glPushMatrix();
    glTranslatef(start->X()  , start->Y()  , start->Z());
    point aux = point(1,end->X() - start->X(),end->Y() - start->Y(),end->Z() - start->Z());
    glRotatef(aux.get_theta(),ZERO,ZERO,ONE);
    glRotatef(aux.get_phi(),ZERO,-ONE,ZERO);
    //DRAWS THE CILYNDER ITSELF
    for(int i = 0; i <= visualParams.cilynder_slices; i++){
        double theta = i * (360 / visualParams.cilynder_slices);
        double next_theta = (i + 1) * (360 / visualParams.cilynder_slices);
        glBegin(GL_QUAD_STRIP);
        for (int j = 0; j <= visualParams.cilynder_stacks; j++){
            double deltaR = (endRadius - startRadius);
            double radius = ((j * 1.0 / visualParams.cilynder_stacks) * deltaR) + startRadius;
            glNormal3f(ZERO, MathHelper::cosine(theta), MathHelper::sine(theta));
            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(theta), radius * MathHelper::sine(theta));
            glNormal3f(ZERO, MathHelper::cosine(next_theta), MathHelper::sine(next_theta));
            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(next_theta), radius * MathHelper::sine(next_theta));
        }
        glEnd();
    }
    glPopMatrix();
}

void GraphicObject::drawCilynder(point* start, point* end, double startRadius, double endRadius, vector<double> value){
    if(value.size() != visualParams.cilynder_stacks + 1)
        return;

    glPushMatrix();
    glTranslatef(start->X()  , start->Y()  , start->Z());
    point aux = point(1,end->X() - start->X(),end->Y() - start->Y(),end->Z() - start->Z());
    glRotatef(aux.get_theta(),ZERO,ZERO,ONE);
    glRotatef(aux.get_phi(),ZERO,-ONE,ZERO);
    //DRAWS THE CILYNDER ITSELF
    for(int i = 0; i <= visualParams.cilynder_slices; i++){
        double theta = i * (360 / visualParams.cilynder_slices);
        double next_theta = (i + 1) * (360 / visualParams.cilynder_slices);
        glBegin(GL_QUAD_STRIP);
        for (int j = 0; j <= visualParams.cilynder_stacks; j++){
            setMaterialColor(colorBar.interpolate(value[j]));
            double deltaR = (endRadius - startRadius);
            double radius = ((j * 1.0 / visualParams.cilynder_stacks) * deltaR) + startRadius;
            glNormal3f(ZERO, MathHelper::cosine(theta), MathHelper::sine(theta));
            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(theta), radius * MathHelper::sine(theta));
            glNormal3f(ZERO, MathHelper::cosine(next_theta), MathHelper::sine(next_theta));
            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(next_theta), radius * MathHelper::sine(next_theta));
        }
        glEnd();
    }
    glPopMatrix();
}

void GraphicObject::drawCilynder(point* start, point* end, double startRadius, double endRadius, vector<double> value, QColor color){
    if(value.size() != visualParams.cilynder_stacks + 1)
        return;

    glPushMatrix();
    glTranslatef(start->X()  , start->Y()  , start->Z());
    point aux = point(1,end->X() - start->X(),end->Y() - start->Y(),end->Z() - start->Z());
    glRotatef(aux.get_theta(),ZERO,ZERO,ONE);
    glRotatef(aux.get_phi(),ZERO,-ONE,ZERO);
    //DRAWS THE CILYNDER ITSELF
    for(int i = 0; i <= visualParams.cilynder_slices; i++){
        double theta = i * (360 / visualParams.cilynder_slices);
        double next_theta = (i + 1) * (360 / visualParams.cilynder_slices);
        bool odd_1 = (i % 2 == 1);
        glBegin(GL_QUADS);
        for (int j = 0; j < visualParams.cilynder_stacks; j++){
            bool odd_2 = (j%2 == 1);

            if((odd_1 && odd_2))
                setMaterialColor(color);
            else
                setMaterialColor(colorBar.interpolate(value[j]));

            double deltaR = (endRadius - startRadius);
            double radius = ((j * 1.0 / visualParams.cilynder_stacks) * deltaR) + startRadius;

            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(theta), radius * MathHelper::sine(theta));
            glVertex3f(j * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(next_theta), radius * MathHelper::sine(next_theta));

            radius = (((j+1) * 1.0 / visualParams.cilynder_stacks) * deltaR) + startRadius;

            glVertex3f((j+1) * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(next_theta), radius * MathHelper::sine(next_theta));
            glVertex3f((j+1) * (aux.abs()/visualParams.cilynder_stacks), radius * MathHelper::cosine(theta), radius * MathHelper::sine(theta));

        }
        glEnd();
    }
    glPopMatrix();
}

void GraphicObject::drawSphere(point *p, double radius, QColor color){
    setMaterialColor(color);
    glPushMatrix();
    glTranslatef(p->X()  , p->Y()  , p->Z());
    GLUquadric* q = gluNewQuadric();
    gluSphere(q,radius,visualParams.sphere_slices,visualParams.sphere_stacks);
    glPopMatrix();
}

point GraphicObject::getScreenPoint(point p, point eye, float aspect){
    point aux = point();
    aux = p;
    aux = aux + trnsf.translate;
    aux = aux * trnsf.scale;
    aux = MathHelper::rotateX(aux,trnsf.rotation.X());
    aux = MathHelper::rotateY(aux,trnsf.rotation.Y());
    aux = MathHelper::rotateZ(aux,trnsf.rotation.Z());
    float x = aux.X();
    float y = aux.Y();
    float z = eye.Z() - aux.Z();

    if(z > NEAR && z < FAR){
        float tan = MathHelper::tangent(FOVY/2);
        float h = tan * z;
        float w = h * aspect;

        //IF IS IN THE FRUSTRUM
        if((x <= w && x >= -w) && (y <= h && y >= -h)){
            x = x/w;
            y = y/h;
            point rp = point(2,x,y);

            return rp;
        }
    }

    return point(1,-1,-1,-1);
}

point GraphicObject::getCanvasPoint(point p, point eye, float width, float height){
    return ((getScreenPoint(p, eye, width/height) + 1.0) / 2) * point(1,width,height,0.0);
}

point GraphicObject::getModelPoint(point p, point eye, float width, float height, float z_offset, bool canvas_point){
    if(canvas_point){
        p.X((p.X() / (width/2)) - ONE);
        p.Y((p.Y() / (height/2)) - ONE);
    }

    p.X((p.X() + ONE));
    p.Y((p.Y() + ONE));

    double model_depth = abs(limit._far - limit._near);
    double model_height = abs(limit.top - limit.bottom);
    double model_width = abs(limit.right - limit.left);

    p.X(limit.left + (model_width * p.X()) - (model_width / 2));
    p.Y(limit.bottom + (model_height * p.Y()) - (model_height / 2));
    p.Z(limit._near + (model_depth * 0.5));

    return p;
}

int GraphicObject::getGID(){
    return GID;
}


double GraphicObject::getMax(){
    return range.max;
}

void GraphicObject::setMax(double max){
    if(range.max < range.min){
        range.max = max;
        range.min = max;

        colorBar.setMax(range.min);

        return;
    }

    if(max > range.max){
        range.max = max;

        colorBar.setMax(range.max);
    }
}

double GraphicObject::getMin(){
    return range.min;
}

void GraphicObject::setMin(double min){
    if(range.max < range.min){
        range.max = min;
        range.min = min;

        colorBar.setMin(range.min);

        return;
    }

    if(min < range.min){
        range.min = min;

        colorBar.setMin(range.min);
    }
}

void GraphicObject::setMinMax(double val){
    setMin(val);
    setMax(val);
}

QString GraphicObject::getColorBar(){
    return colorBar.getName();
}

void GraphicObject::setRotation(point r){
    trnsf.rotation = r;
}

vector<QString> GraphicObject::print(){
    vector<QString> emp;
    return emp;
}

vector<QString> GraphicObject::raw(){
    vector<QString> emp;
    return emp;
}

void GraphicObject::drawObject(){
    if(time.frames == 0){
        //SELECTS FIRST RANGE TO BE DEFAULT SHOWN
        vector<QString> ranges = getRanges();
        if(!ranges.empty())
            setContextRange(ranges[ZERO]);
    }

    double elapsed;
    QElapsedTimer timer;
    timer.start();

    //DRAWS OBJECT
    glPushMatrix();
    glRotatef(trnsf.rotation.Z(), 0.0, 0.0, 1.0);
    glRotatef(trnsf.rotation.Y(), 0.0, 1.0, 0.0);
    glRotatef(trnsf.rotation.X(), 1.0, 0.0, 0.0);
    glScalef(trnsf.scale,trnsf.scale,trnsf.scale);
    glTranslatef(trnsf.translate.X(),trnsf.translate.Y(),trnsf.translate.Z());
        drawGuideLines();
        draw();
        if(concurrent.size() > 0){
            for(unsigned int i = 0; i < concurrent.size(); i++){
                GraphicObject* g = concurrent[i];
                g->draw();
            }
        }
    glPopMatrix();

    if(guide_lines_status[6]){
        QColor c;
        switch (status) {
        case Fresh:
            setMaterialColor(c.fromRgb(113,247,159));
            break;
        case Crashed:
            setMaterialColor(c.fromRgb(200,29,37));
            break;
        case Initialized:
            setMaterialColor(c.fromRgb(0,166,237));
            break;
        case Iterated:
            setMaterialColor(c.fromRgb(255,180,0));
            break;
        case Finished:
            setMaterialColor(c.fromRgb(127,184,0));
            break;
        default:
            setMaterialColor(c.fromRgb(0.0,0.0,0.0));
            break;
        }
        //SETS BORDER WIDTH
        glLineWidth(5.0);

        glBegin(GL_LINES);
        glVertex3f(-0.8,-0.8,8.0);
        glVertex3f(0.8,-0.8,8.0);

        glVertex3f(0.8,-0.8,8.0);
        glVertex3f(0.8,0.8,8.0);

        glVertex3f(0.8,0.8,8.0);
        glVertex3f(-0.8,0.8,8.0);

        glVertex3f(-0.8,0.8,8.0);
        glVertex3f(-0.8,-0.8,8.0);
        glEnd();
        //RETURNS WIDTH TO DEFAULT VALUE
        glLineWidth(1.0);
    }
    if(guide_lines_status[8]){
        point start = point(1,0.75,0.4,8.0);
        point end = point(1,0.7,-0.4,8.0);
        QColor c;
        setMaterialColor(c.fromRgb(0.0,0.0,0.0));
        for(int i = 0; i < 100; i++){
            double pct = (double)i/100.0;
            double pct_1 = (double)(i + 1)/100.0;
            double size = (colorBar.getMax() - colorBar.getMin() > 0) ? colorBar.getMax() - colorBar.getMin() : 1.0;
            if(size != 1.0){
                setMaterialColor(colorBar.interpolate(colorBar.getMin() + (pct * size)));
            } else {
                setMaterialColor(colorBar.interpolate(colorBar.getMin() + (pct * (colorBar.getMax() - colorBar.getMin()))));
            }
            glBegin(GL_QUADS);
                point start_i = point(1,start.X(),((start.Y() * pct) + (end.Y() * (1-pct))),start.Z());
                point end_i = point(1,end.X(),((start.Y() * pct_1) + (end.Y() * (1-pct_1))),start.Z());
                glVertex3f(start_i.X(),start_i.Y(),start.Z());
                glVertex3f(start_i.X(),end_i.Y(),start.Z());
                glVertex3f(end_i.X(),end_i.Y(),start.Z());
                glVertex3f(end_i.X(),start_i.Y(),start.Z());
            glEnd();
        }
        glBegin(GL_LINES);

        glVertex3f(start.X(),start.Y(),start.Z());
        glVertex3f(start.X(),end.Y(),start.Z());

        glVertex3f(start.X(),end.Y(),start.Z());
        glVertex3f(end.X(),end.Y(),start.Z());

        glVertex3f(end.X(),end.Y(),start.Z());
        glVertex3f(end.X(),start.Y(),start.Z());

        glVertex3f(end.X(),start.Y(),start.Z());
        glVertex3f(start.X(),start.Y(),start.Z());

        glEnd();
    }

    //DRAWS TEXT INFO
    if(QPainterSet){
        //DRAW TITLE
        point p = point(1,-0.9625,0.925);
        drawText(&p,getName());

        //DRAW LIMITS
        QString x;
        QString y;
        QString z;
        x = x.asprintf("x in [%.4f,%.4f]",limit.left,limit.right);
        y = y.asprintf("y in [%.4f,%.4f]",limit.bottom,limit.top);
        z = z.asprintf("z in [%.4f,%.4f]",limit._near,limit._far);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,x);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,y);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,z);

        //DRAW CLOCK
        QString aux;
        aux = aux.asprintf("FPS = %.4f - [%.4f,%.4f,%.4f,%.4f]",fps(),time.frames_time[0],time.frames_time[1],time.frames_time[2],time.frames_time[3],time.frames_time[4]);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,aux);
        aux = aux.asprintf("IPS = %.4f - [%.4f,%.4f,%.4f,%.4f]",time.ips,time.iterations_times[0],time.iterations_times[1],time.iterations_times[2],time.iterations_times[3],time.iterations_times[4]);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,aux);
        aux = aux.asprintf("Elapsed Time = %.4f s",time.elapsedTime);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,aux);
        aux = aux.asprintf("Model Time = %.4f s",time.modelTime);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,aux);
        aux = aux.asprintf("Frames = %d",time.frames);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,aux);
        aux = aux.asprintf("Iterations = %d",time.iterations);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,aux);
        aux = aux.asprintf("Time Step = %.4f s",time.deltaT);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,aux);

        //DRAW RANGE
        p = point(1,0.85,0.55);
        drawText(&p,range.parameter);
        p.X(0.84);
        p.Y(p.Y() - LINE_SPACING);
        drawText(&p,aux.asprintf("%.4f",range.max));
        p = point(1,0.84,-0.52);
        drawText(&p,aux.asprintf("%.4f",range.min));
    }

    elapsed = (double)timer.elapsed()/1000;
    int id = time.frames % 5;
    time.frames_time[id] = elapsed;
    time.frames ++;
}

void GraphicObject::drawGuideLines(){
    if(guide_lines_status[0] || guide_lines_status[1] || guide_lines_status[2] ||
            guide_lines_status[3] || guide_lines_status[4] || guide_lines_status[5]){
        vector<double> aux;
        aux.push_back((limit.right - limit.left));
        aux.push_back((limit.top - limit.bottom));
        aux.push_back((limit._far - limit._near));
        double max_limit = MathHelper::vector_max(aux);
        int safety_repeat = 10;

        //FINE GRID
        if(guide_lines_status[0] || guide_lines_status[1] || guide_lines_status[2]){
            double step = pow(10,(((int)log10(max_limit))))/4;
            QColor c = QColor("Gray");
            if(guide_lines_status[0]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    if(i % 4 != 0){
                        drawGuideLineY(i*step,i*step,c);
                        drawGuideLineY(-i*step,-i*step,c);
                    }
                }
            }
            if(guide_lines_status[1]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    if(i % 4 != 0){
                        drawGuideLineZ(i*step,i*step,c);
                        drawGuideLineZ(-i*step,-i*step,c);
                    }
                }
            }
            if(guide_lines_status[2]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    if(i % 4 != 0){
                        drawGuideLineX(i*step,i*step,c);
                        drawGuideLineX(-i*step,-i*step,c);
                    }
                }
            }
        }

        //COARSE GRID
        if(guide_lines_status[0] || guide_lines_status[1] || guide_lines_status[2]){
            double step = pow(10,(((int)log10(max_limit))));
            if(guide_lines_status[0]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    drawGuideLineY(i*step,i*step);
                    drawGuideLineY(-i*step,-i*step);
                }
            }
            if(guide_lines_status[1]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    drawGuideLineZ(i*step,i*step);
                    drawGuideLineZ(-i*step,-i*step);
                }
            }
            if(guide_lines_status[2]){
                for(int i = 1; i * step < max_limit * safety_repeat; i++){
                    drawGuideLineX(i*step,i*step);
                    drawGuideLineX(-i*step,-i*step);
                }
            }
        }

        drawGuideLineX(ZERO,ZERO,QColor("Blue"));
        drawGuideLineY(ZERO,ZERO,QColor("Red"));
        drawGuideLineZ(ZERO,ZERO,QColor("Green"));
    }
}

vector<Field> GraphicObject::getVisualParameters(){
    vector<Field> params;

    Field aux;
    aux.names = "ROTATION_X";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",trnsf.rotation.X());

    params.push_back(aux);

    aux.names = "ROTATION_Y";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",trnsf.rotation.Y());

    params.push_back(aux);

    aux.names = "ROTATION_Z";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",trnsf.rotation.Z());

    params.push_back(aux);

    aux.names = "SCALE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",trnsf.scale);

    params.push_back(aux);

    aux.names = "TRANSLATE_X";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",trnsf.translate.X());

    params.push_back(aux);

    aux.names = "TRANSLATE_Y";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",trnsf.translate.Y());

    params.push_back(aux);

    aux.names = "TRANSLATE_Z";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",trnsf.translate.Z());

    params.push_back(aux);

    aux.names = "CILYNDER_STACKS";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = integer;
    aux.current_value = aux.names.asprintf("%d",visualParams.cilynder_stacks);

    params.push_back(aux);

    aux.names = "CILYNDER_SLICES";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = integer;
    aux.current_value = aux.names.asprintf("%d",visualParams.cilynder_slices);

    params.push_back(aux);

    aux.names = "SPHERE_STACKS";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = integer;
    aux.current_value = aux.names.asprintf("%d",visualParams.sphere_stacks);

    params.push_back(aux);

    aux.names = "SPHERE_SLICES";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = integer;
    aux.current_value = aux.names.asprintf("%d",visualParams.sphere_slices);

    params.push_back(aux);

    aux.names = "FRAME_DELAY";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",visualParams.frame_delay);

    params.push_back(aux);

    aux.names = "PRINT_RAW_EVERY_ITERATION";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((visualParams.printRawEveryIteration)?"true":"false"));

    params.push_back(aux);


    //GETS START PARAMETERS FROM SUBCLASS
    vector<Field> context = getContextVisualParameters();
    for(int i = 0; i < context.size(); i++)
        params.push_back(context[i]);

    return params;
}

bool GraphicObject::updateVisualParameter(QString field, QString value){
    if(field == "ROTATION_X"){
        trnsf.rotation.X(value.toDouble());
        return true;
    } else if(field == "ROTATION_Y"){
        trnsf.rotation.Y(value.toDouble());
        return true;
    } else if(field == "ROTATION_Z"){
        trnsf.rotation.Z(value.toDouble());
        return true;
    } else if(field == "SCALE"){
        trnsf.scale = value.toDouble();
        return true;
    } else if(field == "TRANSLATE_X"){
        trnsf.translate.X(value.toDouble());
        return true;
    } else if(field == "TRANSLATE_Y"){
        trnsf.translate.Y(value.toDouble());
        return true;
    } else if(field == "TRANSLATE_Z"){
        trnsf.translate.Z(value.toDouble());
        return true;
    } else if(field == "CILYNDER_SLICES"){
        visualParams.cilynder_slices = value.toInt();
        return true;
    } else if(field == "CILYNDER_STACKS"){
        visualParams.cilynder_stacks = value.toInt();
        return true;
    } else if(field == "SPHERE_SLICES"){
        visualParams.sphere_slices = value.toInt();
        return true;
    } else if(field == "SPHERE_STACKS"){
        visualParams.sphere_stacks = value.toInt();
        return true;
    } else if(field == "FRAME_DELAY"){
        visualParams.frame_delay = value.toDouble();
        return true;
    } else if(field == "PRINT_RAW_EVERY_ITERATION"){
        visualParams.printRawEveryIteration = (value.toInt() == 1)? true : false;
        return true;
    }

    return updateContextVisualParameter(field,value);
}

bool GraphicObject::setRange(QString field){
    if(status ==  Iterated || status == Initialized || status == Finished){
        return setContextRange(field);
    }
}

QString GraphicObject::getCurrentRange(){
    return range.parameter;
}

bool GraphicObject::operator ==(GraphicObject* gObj){
    return (GID == gObj->getGID());
}

bool GraphicObject::operator ==(int gid){
    return gid == GID;
}

bool GraphicObject::hasMaster(){
    if(master == NULL)
        return false;

    return true;
}

GraphicObject* GraphicObject::getMaster(){
    return master;
}

void GraphicObject::setMaster(GraphicObject* gObj){
    if(gObj == NULL)
        return;

    master = gObj;
}

bool GraphicObject::hasSlaves(){
    return !slaves.empty();
}

void GraphicObject::purgeMaster(){
    master = NULL;
}

vector<GraphicObject*> GraphicObject::getSlaves(){
    return slaves;
}

void GraphicObject::addSlave(GraphicObject* gObj){
    if(gObj == NULL)
        return;

    slaves.push_back(gObj);
    gObj->setMaster(this);
}

void GraphicObject::removeSlave(int gid){
    for(int i = 0; i < slaves.size(); i ++){
        GraphicObject* g = slaves[i];
        if(g->operator ==(gid)){
            slaves.erase(slaves.begin() + i);
            return;
        }
    }
}

bool GraphicObject::isInIterationTree(GraphicObject* gObj){
    bool ans = false;

    //ITERATION TO ROOT
    GraphicObject* it = this;

    while(it->hasMaster())
        it = it->getMaster();

    isInIterationTree(it,gObj);

    return ans;
}

bool GraphicObject::isInIterationTree(GraphicObject* root, GraphicObject* gObj){
    if(root->hasMaster())
        return false;

    if(root->operator ==(gObj))
        return true;

    bool ans = false;

    vector<GraphicObject*> slaves = root->getSlaves();
    for(int i = 0; i < slaves.size() && !ans; i++){
        GraphicObject* slave = slaves[i];
        isInIterationTreeAux(slave,gObj);
    }

    return ans;
}

bool GraphicObject::isInIterationTreeAux(GraphicObject* it, GraphicObject* gObj){
    if(it->operator ==(gObj))
        return true;

    bool ans = false;

    vector<GraphicObject*> slaves = it->getSlaves();
    for(int i = 0; i < slaves.size() && !ans; i++){
        GraphicObject* slave = slaves[i];
        isInIterationTreeAux(slave,gObj);
    }

    return ans;
}

void GraphicObject::move(double dx, double dy, double dz){
    trnsf.translate = trnsf.translate + point(0,dx,dy,dz);
}

vector<GraphicObject*> GraphicObject::getConcurrents(){
    return concurrent;
}

void GraphicObject::addConcurrent(GraphicObject* gObj){
    if(gObj == NULL)
        return;

    if(gObj->operator ==(this))
        return;

    for(int i = 0; i < concurrent.size(); i++)
        if(gObj->operator ==(concurrent[i]))
            return;

    concurrent.push_back(gObj);
}

void GraphicObject::removeConcurrent(int gid){
    for(int i = 0; i < concurrent.size(); i ++){
        GraphicObject* g = concurrent[i];
        if(g->operator ==(gid)){
            concurrent.erase(concurrent.begin() + i);
            return;
        }
    }
}

vector<GraphicObject*> GraphicObject::getGraphics(){
    GraphicObject* graphic = first_graphic;
    vector<GraphicObject*> res;

    while(graphic != NULL){
        res.push_back(graphic);
        graphic = graphic->next_graphic;
    }

    return res;
}

GraphicObject* GraphicObject::getGraphic(int gid){
    GraphicObject* graphic = first_graphic;

    while(graphic != NULL){
        if(graphic->GID == gid)
            return graphic;

        graphic = graphic->next_graphic;
    }

    return NULL;
}

vector<ColorBar> GraphicObject::getPossibleColorbars(){
    vector<ColorBar> aux;

    Red r;
    Green g;
    Blue b;
    Black bl;
    Cyan c;
    Magenta m;
    Yellow y;
    Aero a;
    Brasil br;
    Summer sm;
    Winter wn;
    ColorCube cc;

    aux.push_back(r);
    aux.push_back(g);
    aux.push_back(b);
    aux.push_back(bl);
    aux.push_back(c);
    aux.push_back(m);
    aux.push_back(y);
    aux.push_back(a);
    aux.push_back(br);
    aux.push_back(sm);
    aux.push_back(wn);
    aux.push_back(cc);

    return aux;
}

bool GraphicObject::isIterating(){
    if(status == Finished)
        return false;

    return time.iterating;
}

bool GraphicObject::hasIterated(){
    if(status == Initialized || status == Iterated || status == Finished)
        return true;

    return false;
}

void GraphicObject::setColorBar(ColorBar c){
    this->colorBar = c;

    if(range.max != 0.0 || range.min != 0.0){
        colorBar.setMax(range.max);
        colorBar.setMin(range.min);
    }
}

void GraphicObject::setColorBar(QString name){
    if(name.contains("AERO")){
        Aero a;
        setColorBar(a);
    } else if(name.contains("BRASIL")){
        Brasil a;
        setColorBar(a);
    } else if(name.contains("SUMMER")){
        Summer a;
        setColorBar(a);
    } else if(name.contains("WINTER")){
        Winter a;
        setColorBar(a);
    } else if(name.contains("COLORCUBE")){
        ColorCube a;
        setColorBar(a);
    } else if(name.contains("BLACK")){
        Black a;
        setColorBar(a);
    } else if(name.contains("RED")){
        Red a;
        setColorBar(a);
    } else if(name.contains("GREEN")){
        Green a;
        setColorBar(a);
    } else if(name.contains("BLUE")){
        Blue a;
        setColorBar(a);
    } else if(name.contains("CYAN")){
        Cyan a;
        setColorBar(a);
    } else if(name.contains("MAGENTA")){
        Magenta a;
        setColorBar(a);
    } else if(name.contains("YELLOW")){
        Yellow a;
        setColorBar(a);
    } else {
        Aero a;
        setColorBar(a);
    }
}

void GraphicObject::setMaterialColor(QColor c){
    qreal r, g, b, a;
    c.getRgbF(&r,&g,&b,&a);
    vector<float> matAmbient = { (float)r, (float)g, (float)b, (float)a };
    vector<float> matDiffuse = { (float)r * (float) 0.1, (float) g * (float) 0.1, (float) b * (float) 0.1, (float)a };
    glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient.data());
    glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse.data());
}

int GraphicObject::getCilynderStacks(){
    return visualParams.cilynder_stacks;
}

int GraphicObject::getCilynderSlices(){
    return visualParams.cilynder_slices;
}

int GraphicObject::getSphereStacks(){
    return visualParams.sphere_stacks;
}

int GraphicObject::getSphereSlices(){
    return visualParams.sphere_slices;
}

double GraphicObject::getDeltaT(){
    return time.deltaT;
}

void GraphicObject::setPainterMethod(std::function<QPainter*(int)> f, int CID){
    getQPainter = f;
    this->CID = CID;

    if(getQPainter == NULL)
        return;

    QPainterSet = true;
}

void GraphicObject::purgePainterMethod(){
    getQPainter = NULL;
    CID = -1;

    QPainterSet = false;
}

void GraphicObject::drawText(point* position, QString text, QColor color, const QFont &font){
    if(!MathHelper::isBetween(position->X(),ONE,-ONE))
        return;

    if(!MathHelper::isBetween(position->Y(),ONE,-ONE))
        return;

    point* pos = new point();
    pos->operator=(position);

    if(QPainterSet){
        QPainter* painter = getQPainter(CID);
        if(painter != NULL){
            //SETS POSITION
            pos->X((int)(((pos->X() + ONE)/2) * painter->window().width()));
            pos->Y((int)(((pos->Y() + ONE)/2) * painter->window().height()));
            pos->Y((int)(painter->window().height() - pos->Y()));

            painter->setCompositionMode(QPainter::CompositionMode_Source);
            painter->setFont(font);
            painter->setPen(color);
            painter->drawText(pos->X(),pos->Y(),text);
        }

        delete painter;
    }

    delete pos;
}

double GraphicObject::getModeltime(){
    return time.modelTime;
}

void GraphicObject::crash(){
    status = Crashed;
}

bool GraphicObject::start(){
    if(status != Fresh && status != Initialized)
        return -1.0;

    status = Initialized;

    return initialize();
}

double GraphicObject::delay(){
    return visualParams.frame_delay;
}

double GraphicObject::iterate(bool iterateSlaves){
    if(status != Initialized && status != Iterated)
        return -1.0;

    status = Iterated;

    double elapsed;

    QElapsedTimer timer;
    timer.start();

    iteration();

    elapsed = ((double)timer.elapsed()/1000);

    if(!slaves.empty() && iterateSlaves){
        for(int i = 0; i < slaves.size(); i++){
            GraphicObject* slave = slaves[i];

            double aux = slave->iterate();

            if(aux > 0.0)
                elapsed += aux;
        }
    }

    if(visualParams.printRawEveryIteration){
        emit print_raw(raw());
    }

    int id = time.iterations % 5;
    time.iterations_times[id] = elapsed;
    time.iterations ++;
    time.elapsedTime += elapsed;
    time.modelTime += time.deltaT;
    emit emit_log(SmartLogMessage(getSID(),QString::asprintf("ITERATION [%d] #GID[%d] = %fs", time.iterations, GID, elapsed),SmartLogType::LOG_ITERATION));

    return elapsed;
}

double GraphicObject::iterate(int n){
    if(n < 0)
        return 0.0;

    time.iterating = true;

    if(n != 0)
        time.iterator_count = n;
    else
        time.iterator_count = INT_MAX;

    if(hasSlaves()){
        for(int i = 0; i < slaves.size(); i++){
            GraphicObject* it = slaves[i];
            it->iterate(n);
        }
    }

    return 0.0;
}

double GraphicObject::iterate(double time){
    int n = time / this->time.deltaT;

    if(this->time.modelTime + (this->time.deltaT * n) < time)
        n++;

    return iterate(n);
}

bool GraphicObject::restart(){
    status = Fresh;

    time.modelTime = 0.0;
    time.elapsedTime = 0.0;
    time.fps = 0.0;
    time.ips = 0.0;
    time.frames = 0;
    time.iterations = 0;

    return initialize();
}

void GraphicObject::pause(){
    time.iterating = false;
    time.iterator_count = 0;
    time.iterations = 0;

    if(hasSlaves()){
        for(int i = 0; i < slaves.size(); i++){
            GraphicObject* it = slaves[i];
            it->pause();
        }
    }
}

void GraphicObject::end(){
    if(status != Iterated)
        return;

    status = Finished;

    finish();
}

void GraphicObject::drawArrow(point* start, point* end, double arrow_end_offset, double triangle_size, QColor color){
    point h = (*end - *start);
    bool drawTriangle = ((arrow_end_offset + arrow_end_offset) < h.abs())? true : false ;
    point triangle_start = *start + ( h * ((h.abs() - (arrow_end_offset + triangle_size))/h.abs()));
    point triangle_end = *start + ( h * ((h.abs()- arrow_end_offset)/h.abs()));

    setMaterialColor(color);
    glBegin(GL_LINES);
        glVertex3f(start->X(),start->Y(), start->Z());
        glVertex3f(end->X(), end->Y(), end->Z());
    glEnd();


    if(drawTriangle){
        this->drawTriangle(&triangle_start,&triangle_end,color);
    }
}

//DRAWS A TRINGLE FROM TWO POINTS
//END IS WHERE THE TRIANGLE IS POINTING TO
void GraphicObject::drawTriangle(point* start, point* end, QColor color){
    point left;
    point right;
    point up;

    point h = (*start - *end)/2;

    up.operator=(*end);
    right.operator=((*start) + MathHelper::rotateZ(h,90));
    left.operator=((*start) + MathHelper::rotateZ(h,-90));


    setMaterialColor(color);
    glBegin(GL_TRIANGLES);
        glVertex3f(up.X(),up.Y(), up.Z());
        glVertex3f(right.X(), right.Y(), right.Z());
        glVertex3f(left.X(), left.Y(), left.Z());

        glVertex3f(left.X(), left.Y(), left.Z());
        glVertex3f(right.X(), right.Y(), right.Z());
        glVertex3f(up.X(),up.Y(), up.Z());
    glEnd();
}

void GraphicObject::drawLine(point* start, point* end, QColor color){
    setMaterialColor(color);
    glBegin(GL_LINES);
        glVertex3f(start->X(),start->Y(), start->Z());
        glVertex3f(end->X(), end->Y(), end->Z());
    glEnd();
}

void GraphicObject::drawGuideLineX(double y, double z, QColor color){
    setMaterialColor(color);
    glBegin(GL_LINES);
        point a = point(INFINITY_PLUS,y,z);
        point b = point(INFINITY_MINUS,y,z);
        glVertex3f(b.X(), b.Y(), b.Z());
        glVertex3f(a.X(), a.Y(), a.Z());
    glEnd();
}

void GraphicObject::drawGuideLineY(double x, double z, QColor color){
    setMaterialColor(color);
    glBegin(GL_LINES);
        point a = point(x,INFINITY_PLUS,z);
        point b = point(x,INFINITY_MINUS,z);

        glVertex3f(a.X(), a.Y(), a.Z());
        glVertex3f(b.X(), b.Y(), b.Z());
    glEnd();
}

void GraphicObject::drawGuideLineZ(double x, double y, QColor color){
    setMaterialColor(color);
    glBegin(GL_LINES);
        point a = point(x,y,INFINITY_PLUS);
        point b = point(x,y,INFINITY_MINUS);

        glVertex3f(a.X(), a.Y(), a.Z());
        glVertex3f(b.X(), b.Y(), b.Z());
    glEnd();
}

double GraphicObject::fps(){
    double sum = 0.0;
    double sum_i = 0.0;

    for(int i = 0; i < 5; i++){
        sum += time.frames_time[i];
        sum_i += time.iterations_times[i];
    }

    time.fps = 1/(sum / 5);
    time.ips = 1/(sum_i / 5);

    return time.fps;
}

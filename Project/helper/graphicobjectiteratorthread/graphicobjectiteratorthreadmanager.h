#ifndef GRAPHICOBJECTITERATORTHREADMANAGER_H
#define GRAPHICOBJECTITERATORTHREADMANAGER_H

#include "../../model/smartobject.h"
#include "../../model/graphicobject.h"

class GraphicObjectIteratorThreadManager : public SmartObject
{
Q_OBJECT
public:
    GraphicObjectIteratorThreadManager(GraphicObject* gObj, QString name, SmartObject* parent = NULL);

    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();

signals:
    void finished();

public slots:
    void process();

    void kill_me();

private:
    GraphicObject* subject;

    bool kill;
};

#endif // GRAPHICOBJECTITERATORTHREADMANAGER_H

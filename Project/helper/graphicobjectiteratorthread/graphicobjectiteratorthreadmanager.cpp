#include "graphicobjectiteratorthreadmanager.h"

GraphicObjectIteratorThreadManager::GraphicObjectIteratorThreadManager(GraphicObject* gObj, QString name, SmartObject* parent) : SmartObject (name,parent)
{
    subject = gObj;
    kill = false;
}

void GraphicObjectIteratorThreadManager::process(){
    if( subject!= NULL){
        while(subject != NULL && !kill){
            if(subject->isIterating())
                subject->iterate();
            else
                break;
        }
    }
}

void GraphicObjectIteratorThreadManager::kill_me(){
    subject = NULL;
    kill = true;
}

vector<QString> GraphicObjectIteratorThreadManager::print(){
    vector<QString> aux;

    return aux;
}

vector<QString> GraphicObjectIteratorThreadManager::data(){
    vector<QString> aux;

    return aux;
}

vector<QString> GraphicObjectIteratorThreadManager::raw(){
    vector<QString> aux;

    return aux;
}

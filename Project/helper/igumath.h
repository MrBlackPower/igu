#ifndef IGUMATH_H
#define IGUMATH_H

#include <vector>
#include <chrono>
#include <random>

//#include "wisestructure.h"

#include "model/wise/point.h"

#include "../boost/date_time/gregorian/gregorian.hpp"
#include "../boost/date_time/posix_time/posix_time.hpp"

#define ESSENTIALY_ZERO 0.0000001
#define ZERO 0
#define ONE 1

#define PI 3.14159265359

using namespace std;

using namespace wisepoint;

using namespace boost::posix_time;
using namespace boost::gregorian;

class igumath
{
public:
    igumath();

    static void thomas_algorithm(const std::vector<double>& a, const std::vector<double>& b, const std::vector<double>& c, const std::vector<double>& d, std::vector<double>& f);

    static double cosine(double val);

    static double sine(double val);

    static double degree_to_radian(double degree);

    static double euclidean_distance(point* a, point* b);

    static double vector_max(vector<double> vect);

    static double vector_min(vector<double> vect);

    static double vector_mean(vector<double> values);

    static vector<double> matrix_max(vector<vector<double>> vect);

    static vector<double> matrix_min(vector<vector<double>> vect);

    static vector<double> matrix_mean(vector<vector<double>> values);

    static date today();

    static ptime now();

    static string date_time_now();

    static string random_key(unsigned int size, string alphabet);

    static bool is_between(float val, float max, float min);

    static int chose_from_cdf(vector<float> cdf);

    static int chose_from_cdf(vector<double> cdf);

    static int chose_from_cdf(vector<float>* cdf);

    static int chose_from_cdf(vector<double>* cdf);

    static vector<double> string_to_double(vector<string> vec);

    static vector<vector<double>> string_to_vector(vector<string> vec);

    static vector<vector<double>> string_to_tensor(vector<string> vec);

    static vector<int> string_to_int(vector<string> vec);
};

#endif // IGUMATH_H

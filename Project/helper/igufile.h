#ifndef IGUFILE_H
#define IGUFILE_H

#include "model/wise/point.h"

#include <vector>
#include <string>

#include <stdio.h>

using namespace std;

using namespace wisepoint;

class igufile
{
public:
    igufile();

    static string os_name();

    static bool save_points(vector<point*> points, char *fileName);

    static bool file_exists(string filename);

    static bool remove(string filename);

    static bool save_raw(vector<string> raw, char *fileName, bool skip_line = true);

    static bool save_raw(vector<string> raw, const char *fileName, bool skip_line = true);

    static bool save_raw(string raw, const char *fileName, bool skip_line = false);

    static bool save_raw(const char* raw, const char *fileName, bool skip_line = false);

    static bool move(const char *fileA, const char *fileB);

    static bool jump_read_line(char *line, FILE *file);

};

#endif // IGUFILE_H

#include "schedule.h"

unsigned int Schedule::CURR_ID = 0;

Schedule::Schedule(GraphicObject* gobj, QString filename, QString log_filename, int times) : SCHEDULE_ID(CURR_ID++)
{
    this->filename = filename;
    this->log_filename = log_filename;
    this->times = times;

    QString line = "";
    vector<Field> start = gobj->getStartParameters();
    vector<Field> visual = gobj->getVisualParameters();

    for (int i = 0; i < start.size(); i++) {
        Field f = start[i];

        line = line.asprintf("%s %s", f.names.toStdString().data(), f.current_value.toStdString().data());
        startParams.push_back(line);
    }

    for (int i = 0; i < visual.size(); i++) {
        Field f = visual[i];

        line = line.asprintf("%s %s", f.names.toStdString().data(), f.current_value.toStdString().data());
        visualParams.push_back(line);
    }

}

Schedule::Schedule(QString filename, QString log_filename,int times,vector<QString> startParams,vector<QString> visualParams) : SCHEDULE_ID(CURR_ID++)
{
    this->filename = filename;
    this->log_filename = log_filename;
    this->startParams = startParams;
    this->visualParams = visualParams;
    this->times = times;
}

QString Schedule::getFileName(){
    return filename;
}

QString Schedule::getLogFileName(){
    return log_filename;
}

int Schedule::getTimes(){
    return times;
}

vector<QString> Schedule::getStartParams(){
    return startParams;
}

vector<QString> Schedule::getVisualParams(){
    return visualParams;
}

void Schedule::operator=(Schedule s){
    this->filename = s.getFileName();
    this->log_filename = s.getLogFileName();
    this->times = s.getTimes();

    startParams.clear();
    visualParams.clear();

    startParams.operator=(s.getStartParams());
    visualParams.operator=(s.getVisualParams());
}

void Schedule::operator=(Schedule* s){
    this->filename = s->getFileName();
    this->log_filename = s->getLogFileName();

    startParams.clear();
    visualParams.clear();

    startParams.operator=(s->getStartParams());
    visualParams.operator=(s->getVisualParams());
}

int Schedule::id(){
    return SCHEDULE_ID;
}

void Schedule::xml(Schedule job, QString filename){
    vector<Schedule> jobs;

    jobs.push_back(job);

    xml(jobs,filename);
}

void Schedule::xml(vector<Schedule> jobs, QString filename){
    QDomDocument document;

    QDomElement root = document.createElement("jobs");

    // Adding the root element to the docuemnt
    document.appendChild(root);

    //adds jobs
    for (int i = 0; i < jobs.size(); i++) {
        Schedule s = jobs[i];
        QDomElement schedule = document.createElement("schedule");
        schedule.setAttribute("filename",s.getFileName());
        schedule.setAttribute("log_filename",s.getLogFileName());
        schedule.setAttribute("times",s.getTimes());

        root.appendChild(schedule);

        vector<QString> visual = s.getVisualParams();
        vector<QString> start = s.getStartParams();

        QDomElement visual_element = document.createElement("visual_parameters");
        QDomElement start_element = document.createElement("start_parameters");

        schedule.appendChild(visual_element);
        schedule.appendChild(start_element);

        for (int j = 0; j < visual.size(); j++) {
            QDomElement visual_ = document.createElement("visual_parameter");
            QTextStream s(&visual[j]);
            QString name, value;

            s >> name >> value;

            visual_.setAttribute("name",name);
            visual_.setAttribute("value",value);

            visual_element.appendChild(visual_);
        }

        for (int j = 0; j < start.size(); j++) {
            QDomElement start_ = document.createElement("start_parameter");
            QTextStream s(&start[j]);
            QString name, value;

            s >> name >> value;

            start_.setAttribute("name",name);
            start_.setAttribute("value",value);

            start_element.appendChild(start_);
        }
    }

    QFile f(filename);
    f.open(QFile::WriteOnly);

    if(f.isOpen()){
        QTextStream stream(&f);
        stream << document.toString();
        f.close();
    }
}

vector<Schedule> Schedule::fromXml(QString filename){
    vector<Schedule> r;

    fromXml(&r,filename);

    return r;
}
void Schedule::fromXml(vector<Schedule>* jobs, QString filename){
    if(jobs == NULL)
        return;

    QDomDocument doc;
    QFile f(filename);
    f.open(QFile::ReadOnly);

    if(!f.isOpen())
        return;

    if (!doc.setContent(&f)) {
        f.close();
        return;
    }

    f.close();



    QDomNode n = doc.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement();
        if(!e.isNull() && e.tagName() == "jobs") { // JOBS
            QDomNodeList schedules = e.childNodes();
            for(int k = 0; k < schedules.size(); k++){
                QDomElement schedule = schedules.at(k).toElement(); // try to convert the node to an element.
                if(!schedule.isNull() && schedule.tagName() == "schedule") {//SCHEDULE
                    QString filename = schedule.attribute("filename");
                    QString log_filename = schedule.attribute("log_filename");
                    int times = schedule.attribute("times").toInt();
                    vector<QString> visual;
                    vector<QString> start;

                    QDomNodeList it = schedule.childNodes();
                    for (int i = 0; i < it.size(); i++) {
                        QDomElement e_1 = it.at(i).toElement();

                        if(!e_1.isNull()) {//SCHEDULE
                            if(e_1.tagName() == "visual_parameters"){//VISUAL PARAMETERS
                                QDomNodeList it_2 = e_1.childNodes();

                                for (int j = 0; j < it_2.size(); j++) {
                                    QDomElement e_2 = it_2.at(j).toElement();

                                    if(!e_2.isNull() && e_2.tagName() == "visual_parameter") {//VISUAL PARAMETER
                                        QString name = e_2.attribute("name");
                                        QString value = e_2.attribute("value");

                                        visual.push_back(QString::asprintf("%s %s",name.toStdString().data(),value.toStdString().data()));
                                    }
                                }

                            } else if (e_1.tagName() == "start_parameters") {
                                QDomNodeList it_2 = e_1.childNodes();

                                for (int j = 0; j < it_2.size(); j++) {
                                    QDomElement e_2 = it_2.at(j).toElement();

                                    if(!e_2.isNull() && e_2.tagName() == "start_parameter") {//VISUAL PARAMETER
                                        QString name = e_2.attribute("name");
                                        QString value = e_2.attribute("value");

                                        start.push_back(QString::asprintf("%s %s",name.toStdString().data(),value.toStdString().data()));
                                    }
                                }

                            }
                        }
                    }

                    Schedule s(filename,log_filename,times,start,visual);

                    jobs->push_back(s);
                }
            }
        }


        n = n.nextSibling();
    }
}

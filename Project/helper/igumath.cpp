#include "igumath.h"

igumath::igumath()
{

}

void igumath::thomas_algorithm(const std::vector<double>& a,
                      const std::vector<double>& b,
                      const std::vector<double>& c,
                      const std::vector<double>& d,
                      std::vector<double>& f) {
  size_t N = d.size();

  // Create the temporary vectors
  // Note that this is inefficient as it is possible to call
  // this function many times. A better implementation would
  // pass these temporary matrices by non-const reference to
  // save excess allocation and deallocation
  std::vector<double> c_star(N, 0.0);
  std::vector<double> d_star(N, 0.0);

  // This updates the coefficients in the first row
  // Note that we should be checking for division by zero here
  c_star[0] = c[0] / b[0];
  d_star[0] = d[0] / b[0];

  // Create the c_star and d_star coefficients in the forward sweep
  for (int i=1; i<N; i++) {
    double m = 1.0 / (b[i] - a[i] * c_star[i-1]);
    c_star[i] = c[i] * m;
    d_star[i] = (d[i] - a[i] * d_star[i-1]) * m;
  }

  // This is the reverse sweep, used to update the solution vector f
  for (int i=N-1; i-- > 0; ) {
    f[i] = d_star[i] - c_star[i] * d[i+1];
  }
}

double igumath::cosine(double val){
    return cos(val * PI / 180);
}

double igumath::sine(double val){
    return sin(val * PI / 180);
}

double igumath::degree_to_radian(double degree){
    return (degree * PI) / 180;
}

double igumath::euclidean_distance(point* a, point* b){
    return (double) sqrt(pow(a->X() - b->X(),2)  + pow(a->Y() - b->Y(),2) + pow(a->Z() - b->Z(),2));
}

double igumath::vector_max(vector<double> vect){
    if(vect.empty())
        return -1;

    double max = vect.operator[](ZERO);

    for(int i = 1; i < vect.size(); i++)
        if(max < vect.operator[](i))
            max = vect.operator[](i);

    return max;
}

double igumath::vector_min(vector<double> vect){
    if(vect.empty())
        return -1;

    double min = vect.operator[](ZERO);

    for(int i = 1; i < vect.size(); i++)
        if(min > vect.operator[](i))
            min = vect.operator[](i);

    return min;
}

double igumath::vector_mean(vector<double> values){
    if(values.empty())
        return -1;

    double v = (ZERO);

    for(int i = 1; i < values.size(); i++)
        v+= values[i];

    return v/values.size();
}

vector<double> igumath::matrix_max(vector<vector<double>> vect){
    vector<double> r;

    for(auto v : vect)
        r.push_back(vector_max(v));

    return r;
}

vector<double> igumath::matrix_min(vector<vector<double>> vect){
    vector<double> r;

    for(auto v : vect)
        r.push_back(vector_min(v));

    return r;

}

vector<double> igumath::matrix_mean(vector<vector<double>> values){
    vector<double> r;

    for(auto v : values)
        r.push_back(vector_mean(v));

    return r;

}

date igumath::today(){
    return igumath::now().date();
}

ptime igumath::now(){
    return second_clock::local_time();
}

string igumath::date_time_now(){
    to_simple_string(igumath::now());
}

int igumath::chose_from_cdf(vector<float> cdf){
    vector<double> aux;

    for (int i = 0; i < cdf.size(); i++) {
        if(cdf[i] < ESSENTIALY_ZERO)
            aux.push_back(ZERO);
        else
            aux.push_back(cdf[i]);
    }

    double sum = 0.0;
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);

    for (int i = 0; i < aux.size(); i++) {
        if(aux[i] < 0.0)
            return 0.0;

        sum += aux[i];
    }

    if(sum == 0.0)
        return 0.0;

    discrete_distribution<> d(aux.begin(),aux.end());

    return d(generator);
}

int igumath::chose_from_cdf(vector<double> cdf){
    vector<double> aux;
    double sum = 0.0;
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);

    for (int i = 0; i < cdf.size(); i++) {
        if(cdf[i] < 0.0)
            return 0.0;

        if(cdf[i] < ESSENTIALY_ZERO)
            aux.push_back(ZERO);
        else
            aux.push_back(cdf[i]);

        sum = sum + cdf[i];
    }

    if(sum == 0.0)
        return 0.0;

    discrete_distribution<> d(aux.begin(),aux.end());

    return d(generator);
}

int igumath::chose_from_cdf(vector<float>* cdf){
    vector<double> aux;

    for (int i = 0; i < cdf->size(); i++) {
        if(cdf->operator[](i) < ESSENTIALY_ZERO)
            aux.push_back(ZERO);
        else
            aux.push_back(cdf->operator[](i));
    }

    double sum = 0.0;
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);

    for (int i = 0; i < aux.size(); i++) {
        if(aux[i] < 0.0)
            return 0.0;

        sum += aux[i];
    }

    if(sum == 0.0)
        return 0.0;

    discrete_distribution<> d(aux.begin(),aux.end());

    return d(generator);
}

int igumath::chose_from_cdf(vector<double>* cdf){
    vector<double> aux;

    for (int i = 0; i < cdf->size(); i++) {
        if(cdf->operator[](i) < ESSENTIALY_ZERO)
            aux.push_back(ZERO);
        else
            aux.push_back(cdf->operator[](i));
    }

    double sum = 0.0;
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);

    for (int i = 0; i < aux.size(); i++) {
        if(aux[i] < 0.0)
            return 0.0;

        sum += aux[i];
    }

    if(sum == 0.0)
        return 0.0;

    discrete_distribution<> d(aux.begin(),aux.end());

    return d(generator);
}



vector<double> igumath::string_to_double(vector<string> vec){
    vector<double> aux;

    for(auto v : vec){
        aux.push_back(stod(v));
    }

    return aux;
}

vector<vector<double>> igumath::string_to_vector(vector<string> vec){
    vector<vector<double>> r;

    for(auto v : vec){
        stringstream ss(v);
        vector<double> vec;

        while (ss.tellg() != ss.end) {
            double aux;
            ss >> aux;
            vec.push_back(aux);
        }

        r.push_back(vec);
    }
}

vector<vector<double>> igumath::string_to_tensor(vector<string> vec){
    vector<vector<double>> r;

    for(auto v : vec){
        stringstream ss(v);
        vector<double> vec;

        for (int i = 0; i < 9; i++) {
            double aux;
            ss >> aux;
            vec.push_back(aux);
        }

        r.push_back(vec);
    }
}

vector<int> igumath::string_to_int(vector<string> vec){
    vector<int> aux;

    for(auto v : vec){
        aux.push_back(stoi(v));
    }

    return aux;
}

string igumath::random_key(unsigned int size, string alphabet){
    string key = "";
    vector<float> cdf;

    for (int i = 0; i < size; i++)
        cdf.push_back(ONE);


    for (int i = 0; i < size; i++) {
        char c = alphabet.operator[](chose_from_cdf(cdf));

        key.push_back(c);
    }

    return key;
}

bool igumath::is_between(float val, float max, float min){
    if(val <= max)
        if(val >= min)
            return true;

    return false;
}

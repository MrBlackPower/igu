#include "igufile.h"

igufile::igufile()
{

}

string igufile::os_name(){
    return "";
}

bool igufile::save_points(vector<point*> points, char *fileName){
    FILE *file;
    file = fopen(fileName,"w+");

    if(file == NULL)
        return false;

    for(int i = 0; i < points.size(); i++){
        fprintf(file,"%f %f\n", points[i]->X(), points[i]->Y());
    }

    fclose(file);

    return true;
}

bool igufile::file_exists(string filename){
    FILE *file;
    file = fopen(filename.data(),"r");

    if(file == NULL)
        return false;

    fclose(file);

    return true;
}

bool igufile::remove(string filename){
    if(std::remove(filename.data()) !=0)
        return false;

    return true;
}

bool igufile::save_raw(vector<string> raw, char *fileName, bool skip_line){
    FILE *file;
    file = fopen(fileName,"w+");

    if(file == NULL)
        return false;

    for(int i = 0; i < raw.size(); i++){
        string line = raw[i];
        if(skip_line){
            fprintf(file,"%s\n", line.data());
        } else {
            fprintf(file,"%s", line.data());
        }
    }

    fclose(file);

    return true;
}

bool igufile::save_raw(vector<string> raw, const char *fileName, bool skip_line){
    FILE *file;
    file = fopen(fileName,"w+");

    if(file == NULL)
        return false;

    for(int i = 0; i < raw.size(); i++){
        string line = raw[i];
        if(skip_line){
            fprintf(file,"%s\n", line.data());
        } else {
            fprintf(file,"%s", line.data());
        }
    }

    fclose(file);

    return true;
}

bool igufile::save_raw(string raw, const char *fileName, bool skip_line){
    FILE *file;
    file = fopen(fileName,"w+");

    if(file == NULL)
        return false;

    if(skip_line){
        fprintf(file,"%s\n", raw.data());
    } else {
        fprintf(file,"%s", raw.data());
    }

    fclose(file);

    return true;
}

bool igufile::save_raw(const char* raw, const char *fileName, bool skip_line){
    FILE *file;
    file = fopen(fileName,"w+");

    if(file == NULL)
        return false;

    if(skip_line){
        fprintf(file,"%s\n", raw);
    } else {
        fprintf(file,"%s", raw);
    }

    fclose(file);

    return true;
}

bool igufile::move(const char *fileA, const char *fileB){
    FILE *flA;
    FILE *flB;
    flA = fopen(fileA,"r");
    flB = fopen(fileB,"w+");

    if(flA == NULL){
        if(flB != NULL)
            fclose(flB);

        return false;
    }

    if(flB == NULL){
        if(flA != NULL)
            fclose(flA);

        return false;
    }

    char line[100];

    while(fgets(line, 100, flA) != NULL){
        fprintf(flB,line);
    }

    fclose(flA);
    fclose(flB);

    return true;
}

bool igufile::jump_read_line(char *line, FILE *file){
    if(fgets(line, 80, file) != NULL)
        return true;

    return false;
}

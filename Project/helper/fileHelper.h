#ifndef FILEHELPER_H
#define FILEHELPER_H
#define _CRT_NONSTDC_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE

/************************************************************************************
  Name:        fileHelper.h
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Header class of a static class to help manage files.
************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <QString>
#include <QTextStream>
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <stack>
#include "model/wise/point.h"

using namespace std;
using namespace wisepoint;

class FileHelper
{
    public:
/************************************************************************************
    Identifies the current OS

    @return String value of current OS.
************************************************************************************/
        static string getOsName();

        static bool savePoints(vector<point*> points, char *fileName);

        static bool saveRaw(vector<QString> raw, char *fileName);

        static bool saveRaw(vector<QString> raw, const char *fileName);

        static bool move(const char *fileA, const char *fileB);

        static bool jumpReadLine(char *line, FILE *file);

    protected:

    private:
};

#endif // FILEHELPER_H

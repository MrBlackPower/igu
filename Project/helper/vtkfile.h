#ifndef VTKFILE_H
#define VTKFILE_H

#include <QFile>
#include <QString>
#include <QProgressBar>
#include <vector>
#include <fstream>
#include <string>
#include "model/wise/point.h"
#include "../helper/fileHelper.h"


using namespace wisepoint;
namespace vtk {
    enum VTKType{
        Polydata,
        StructuredGrid,
        StructuredPoints,
        UnstructuredGrid
    };

    enum DataType{
        _boolean,
        integer,
        floatp,
        doublep,
        stringp,
        stringlist,
        normals,
        vectors,
        tensor,
        undefined
    };

    struct Data{
        int ID;
        DataType type;
        QString name;
        QString values = "";
    };

    struct Line{
        int n;
        vector<int> points;
    };

    struct Cell{
        int n;
        int type = 8;
        vector<int> points;
    };
}

using namespace std;
using namespace vtk;

class VTKFile
{
public:
    VTKFile(VTKType type = Polydata);

    VTKType getType();
    QString getFileName();

    bool addPoint(point p);
    bool addLine(vector<int> points);
    bool addCell(vector<int> points);

    bool addPointInfo(DataType type, QString name);
    bool addCellInfo(DataType type, QString name);

    bool addPointData(QString name, QString data);
    bool addCellData(QString name, QString data);

    bool load(QString filename);
    bool loadMWCDSInstance(QString filename);
    bool loadAutomatusInstance(QString filename);
    point generate_point(int i);
    bool checkIntegrity();
    void clear();
    void setProgressBar(QProgressBar* p);

    QString getType(DataType type);

    int getN();
    vector<point> getPoints();

    int getLineN();
    vector<Line> getLines();

    int getCellN();
    vector<Cell> getCells();

    vector<QString> print();

    vector<QString> getPointDataList();
    vector<QString> getPointDataInfo();
    vector<DataType> getPointDataType();
    DataType getPointDataType(QString names);

    vector<QString> getCellDataList();
    vector<QString> getCellDataInfo();
    vector<DataType> getCellDataType();
    DataType getCellDataType(QString names);

    vector<QString> getPointData(QString names);
    vector<QString> getCellData(QString names);

    VTKFile scaleThrough(vector<QString> pointConversions, vector<QString> cellConversions);

    void operator =(VTKFile b);
    void operator =(VTKFile* b);

    static void* getValue(QString value, DataType type);

    vector<QString> extra_data;
signals:
    void percent(int value);

private:
    vector<QString> findAllBetween(QChar c, QString s);

    //PRIVATE METHODS
    QString multiply(QString data, DataType type, double scalar);

    VTKType type;

    //PROGRESS BAR
    bool hasProgressBar;
    QProgressBar* progressBar;

    //POINTS
    int points_n;
    vector<point> points;
    vector<Data> points_info;
    vector<vector<QString>> points_data;

    //LINES OR CELLS
    // -1 -> not set 0-> lines 1 -> polygon
    int isPolygon;

    //LINES
    int lines_n;
    vector<Line> lines;

    //CELLS
    int cells_n;
    vector<Cell> cells;
    vector<Data> cells_info;
    vector<vector<QString>> cells_data;

    //FILE
    QString filename;
};

#endif // VTKFILE_H

#include "smartthreadmanager.h"

unsigned int SmartThreadManager::CURR_ID = 0;

SmartThreadManager::SmartThreadManager(QString name, SmartObject* parent) : SmartObject(name,parent), STID (CURR_ID++)
{
    kill = false;
}

int SmartThreadManager::getSTID(){
    return  STID;
}

vector<QString> SmartThreadManager::print(){
    vector<QString> aux;

    return aux;
}

vector<QString> SmartThreadManager::data(){
    vector<QString> aux;

    return aux;
}

vector<QString> SmartThreadManager::raw(){
    vector<QString> aux;

    return aux;
}

void SmartThreadManager::process(){
    while (!kill) {
        if(!jobs.empty()){
            Job j = jobs.front();

            switch (j.type) {
            case ORDER_VECTOR_INT:{
                for(int i = j.i_min; i < j.i_max; i += 2){
                    int phase = j.phase;
                    bool crescent = j.crescent;
                    int tmp;

                    if(crescent){
                        if(phase % 2 == 0){
                            if(j.vector_int->operator[](i-1) > j.vector_int->operator[](i)){
                                tmp = j.vector_int->operator[](i-1);
                                j.vector_int->at(i-1) = (j.vector_int->operator[](i));
                                j.vector_int->at(i) = (tmp);
                            }
                        } else {
                            if(j.vector_int->operator[](i) > j.vector_int->operator[](i+1)){
                                tmp = j.vector_int->operator[](i+1);
                                j.vector_int->at(i+1) = (j.vector_int->operator[](i));
                                j.vector_int->at(i) = (tmp);
                            }
                        }
                    } else {
                        if(phase % 2 == 0){
                            if(j.vector_int->operator[](i-1) > j.vector_int->operator[](i)){
                                tmp = j.vector_int->operator[](i-1);
                                j.vector_int->at(i-1) = (j.vector_int->operator[](i));
                                j.vector_int->at(i) = (tmp);
                            }
                        } else {
                            if(j.vector_int->operator[](i) > j.vector_int->operator[](i+1)){
                                tmp = j.vector_int->operator[](i+1);
                                j.vector_int->at(i+1) = (j.vector_int->operator[](i));
                                j.vector_int->at(i) = (tmp);
                            }
                        }
                    }
                }

                emit finished_step(STID,j.id);

                break;
            }
            case ORDER_VECTOR_SOLUTION:{
                for(int i = j.i_min; i < j.i_max; i += 2){
                    int phase = j.phase;
                    bool crescent = j.crescent;
                    Solution tmp;
                    bool useScore = j.useScore;

                    if(crescent){
                        if(phase % 2 == 0){
                            if(useScore){
                                if(j.vector_solution->operator[](i-1).score > j.vector_solution->operator[](i).score){
                                    tmp = j.vector_solution->operator[](i-1);
                                    j.vector_solution->at(i-1).operator=(j.vector_solution->operator[](i));
                                    j.vector_solution->at(i).operator=(tmp);
                                }
                            } else {
                                if(j.vector_solution->operator[](i-1).cost > j.vector_solution->operator[](i).cost){
                                    tmp = j.vector_solution->operator[](i-1);
                                    j.vector_solution->at(i-1).operator=(j.vector_solution->operator[](i));
                                    j.vector_solution->at(i).operator=(tmp);
                                }
                            }
                        } else {
                            if(useScore){
                                if(j.vector_solution->operator[](i).score > j.vector_solution->operator[](i+1).score){
                                    tmp = j.vector_solution->operator[](i+1);
                                    j.vector_solution->at(i+1).operator=(j.vector_solution->operator[](i));
                                    j.vector_solution->at(i).operator=(tmp);
                                }
                            } else {
                                if(j.vector_solution->operator[](i).cost > j.vector_solution->operator[](i+1).cost){
                                    tmp = j.vector_solution->operator[](i+1);
                                    j.vector_solution->at(i+1).operator=(j.vector_solution->operator[](i));
                                    j.vector_solution->at(i).operator=(tmp);
                                }
                            }
                        }
                    } else {
                        if(phase % 2 == 0){
                            if(useScore){
                                if(j.vector_solution->operator[](i-1).score > j.vector_solution->operator[](i).score){
                                    tmp = j.vector_solution->operator[](i-1);
                                    j.vector_solution->at(i-1).operator=(j.vector_solution->operator[](i));
                                    j.vector_solution->at(i).operator=(tmp);
                                }
                            } else {
                                if(j.vector_solution->operator[](i-1).cost > j.vector_solution->operator[](i).cost){
                                    tmp = j.vector_solution->operator[](i-1);
                                    j.vector_solution->at(i-1).operator=(j.vector_solution->operator[](i));
                                    j.vector_solution->at(i).operator=(tmp);
                                }
                            }
                        } else {
                            if(useScore){
                                if(j.vector_solution->operator[](i).score > j.vector_solution->operator[](i+1).score){
                                    tmp = j.vector_solution->operator[](i+1);
                                    j.vector_solution->at(i+1).operator=(j.vector_solution->operator[](i));
                                    j.vector_solution->at(i).operator=(tmp);
                                }
                            } else {
                                if(j.vector_solution->operator[](i).cost > j.vector_solution->operator[](i+1).cost){
                                    tmp = j.vector_solution->operator[](i+1);
                                    j.vector_solution->at(i+1).operator=(j.vector_solution->operator[](i));
                                    j.vector_solution->at(i).operator=(tmp);
                                }
                            }
                        }
                    }

                }
                emit finished_step(STID,j.id);

                break;
            }
            case ORDER_VECTOR_COST:{
                for(int i = j.i_min; i < j.i_max; i += 2){
                    int phase = j.phase;
                    bool crescent = j.crescent;
                    Cost tmp;

                    if(crescent){
                        if(phase % 2 == 0){
                            if(j.vector_cost->operator[](i-1) > j.vector_cost->operator[](i)){
                                tmp = j.vector_cost->operator[](i-1);
                                j.vector_cost->at(i-1).operator=(j.vector_cost->operator[](i));
                                j.vector_cost->at(i).operator=(tmp);
                            }
                        } else {
                            if(j.vector_cost->operator[](i) > j.vector_cost->operator[](i+1)){
                                tmp = j.vector_cost->operator[](i+1);
                                j.vector_cost->at(i+1).operator=(j.vector_cost->operator[](i));
                                j.vector_cost->at(i).operator=(tmp);
                            }
                        }
                    } else {
                        if(phase % 2 == 0){
                            if(j.vector_cost->operator[](i-1) > j.vector_cost->operator[](i)){
                                tmp = j.vector_cost->operator[](i-1);
                                j.vector_cost->at(i-1).operator=(j.vector_cost->operator[](i));
                                j.vector_cost->at(i).operator=(tmp);
                            }
                        } else {
                            if(j.vector_cost->operator[](i) > j.vector_cost->operator[](i+1)){
                                tmp = j.vector_cost->operator[](i+1);
                                j.vector_cost->at(i+1).operator=(j.vector_cost->operator[](i));
                                j.vector_cost->at(i).operator=(tmp);
                            }
                        }
                    }
                }

                emit finished_step(STID,j.id);

                break;
            }
            default:{
                break;
            }
            }

            jobs.erase(jobs.begin());
        }
    }
}

void SmartThreadManager::addToBuffer(vector<int>* vector_int, int thread_id, int job_id,int phase, int i_min, int i_max, bool crescent){
    if(thread_id != STID)
        return;

    Job j;

    j.id = job_id;
    j.phase = phase;
    j.i_min = i_min;
    j.i_max = i_max;
    j.crescent = crescent;
    j.vector_int = vector_int;

    jobs.push_back(j);
}

void SmartThreadManager::addToBuffer(vector<Cost>* vector_cost, int thread_id, int job_id,int phase, int i_min, int i_max, bool crescent){
    if(thread_id != STID)
        return;

    Job j;

    j.id = job_id;
    j.phase = phase;
    j.i_min = i_min;
    j.i_max = i_max;
    j.crescent = crescent;
    j.type = ORDER_VECTOR_COST;
    j.vector_cost = vector_cost;

    jobs.push_back(j);
}

void SmartThreadManager::addToBuffer(vector<Solution>* vector_solution, int thread_id, int job_id,int phase, int i_min, int i_max,bool useScore, bool crescent){
    if(thread_id != STID)
        return;

    Job j;

    j.id = job_id;
    j.phase = phase;
    j.i_min = i_min;
    j.i_max = i_max;
    j.crescent = crescent;
    j.type = ORDER_VECTOR_SOLUTION;
    j.vector_solution = vector_solution;
    j.useScore = useScore;

    jobs.push_back(j);
}

//void SmartThreadManager::addToBuffer(QSharedPointer<Solution> neighboor_solution, QSharedPointer<WeightedGraph> neighboor_WeightedGraph, int thread_id, int job_id, WeightedGraphSolutionMovement movement){
//    if(thread_id != STID)
//        return;

//    Job j;

//    j.id = job_id;
//    j.type = GENERATE_NEIGHBOOR_SOLUTION;
//    j.neighboor_solution = neighboor_solution;
//    j.neighboor_solution = neighboor_WeightedGraph;
//    j.movement = movement;

//    jobs.push_back(j);
//}


void SmartThreadManager::finish(){
    kill = true;
}

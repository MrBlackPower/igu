#ifndef SMARTTHREADPOOL_H
#define SMARTTHREADPOOL_H

#include <QThread>
#include <QEventLoop>
#include <QTimer>
#include <QMetaType>

#include "../../model/smartobject.h"
#include "../../model/graph/cost.h"
#include "../../model/graph/solution.h"
#include "smartthreadmanager.h"

struct JobReceipt{
    int thread_id;
    int job_id;
};

class SmartThreadPool : public SmartObject
{
Q_OBJECT
public:
    SmartThreadPool(int n, QString name, SmartObject* parent = NULL);
    ~SmartThreadPool();

    void order(vector<int>* costs,  bool crescent = true);
    void order(vector<Cost>* costs,  bool crescent = true);
    void order(vector<Solution>* solutions,bool useScore = false,  bool crescent = true);

//    void neighboorSolution(Solution sol, WeightedGraph* graph, WeightedGraphSolutionMovement movement,  bool crescent = true);


    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();

public slots:
    void thread_finished(int thread_id, int job_id);

signals:
    void post_job(vector<int>* vector_int, int thread_id, int job_id,int phase, int i_min, int i_max, bool crescent);

    void post_job(vector<Cost>* vector_cost, int thread_id, int job_id,int phase, int i_min, int i_max, bool crescent);

    void post_job(vector<Solution>* vector_solution, int thread_id, int job_id,int phase, int i_min, int i_max, bool useScore, bool crescent);

    void all_jobs_done();

private:
    vector<JobReceipt> posted_jobs;

    unsigned int jobs_posted;

    unsigned int N;

    vector<QThread*> threads;
    vector<QTimer*> timers;
    vector<SmartThreadManager*> thread_managers;

};

#endif // SMARTTHREADPOOL_H

DEPENDPATH += flann/lib/Linux\
                /usr/include/GL
INCLUDEPATH += flann/lib/Linux\
               /usr/include/GL

greaterThan(QT_MAJOR_VERSION, 4):
QT += widgets
QT += core gui
QT += opengl
QT += xml
QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp
LIBS += -lglut -lGLU -lGL -fopenmp

CONFIG += c++11

TARGET = IGU
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp \
    model/artery.cpp \
    model/arteryTree.cpp \
    model/point.cpp \
    helper/fileHelper.cpp \
    helper/mathHelper.cpp \
    canvas/oglmaincanvas.cpp \
    window/messagewindow.cpp \
    window/mainwindow.cpp \
    window/consolewindow.cpp \
    model/smartobject.cpp \
    model/graphicobject.cpp \
    model/colorbar.cpp \
    model/colorbar/brasil.cpp \
    model/colorbar/aero.cpp \
    model/colorbar/red.cpp \
    canvas/oglcanvas.cpp \
    model/graphic2d.cpp \
    model/graphic3d.cpp \
    model/graphic2d/ktgraphic.cpp \
    model/graphic2d/staticgraphic.cpp \
    model/graphic2d/lfgraphic.cpp \
    model/colorbar/blue.cpp \
    model/graphic3d/staticgraphic3d.cpp \
    model/graphic3d/lines3d.cpp \
    model/colorbar/black.cpp \
    window/contourwindow.cpp \
    model/graphic3d/ktgraphic3d.cpp \
    helper/vtkfile.cpp \
    window/loadwizard.cpp \
    canvas/oglsidecanvas.cpp \
    window/widget/listgraphiccompatibilitywidget.cpp \
    window/dialog/inputscalardialog.cpp \
    window/widget/celldatalistwidget.cpp \
    window/dialog/viewdatadialog.cpp \
    window/widget/pointdatalistwidget.cpp \
    window/widget/stringitemlistwidget.cpp \
    window/graphicobjectswindow.cpp \
    window/widget/graphicobjecttreewidget.cpp \
    window/graphicobjectdetailswindow.cpp \
    window/dialog/inputbooleandialog.cpp \
    model/graphic2d/nsgraphic.cpp \
    model/graphic2d/livearteryparamgraphic.cpp \
    window/generateccotreewindow.cpp \
    helper/arterialtreecoreHelper.cpp \
    model/smartlog.cpp \
    model/smartlogmessage.cpp \
    model/colorbar/yellow.cpp \
    model/colorbar/magenta.cpp \
    model/colorbar/green.cpp \
    model/colorbar/cyan.cpp \
    model/colorbar/summer.cpp \
    model/colorbar/colorcube.cpp \
    model/colorbar/winter.cpp \
    model/graph.cpp \
    model/graph/node.cpp \
    model/graph/nodegraph.cpp \
    model/graph/weightedgraph.cpp \
    window/generateweightedgraphwindow.cpp \
    model/smartclassstack.cpp \
    model/graph/solution.cpp \
    window/dialog/selectfromstringlistdialog.cpp \
    model/graph/cost.cpp \
    model/liveparameter.cpp \
    model/graphic2d/liveparamgraphic.cpp \
    helper/systemsettingshelper.cpp \
    model/graph/automatusgraph.cpp \
    model/graph/automatusgraph/formalautomatadefinitegraph.cpp \
    window/dialog/igusettingsdialog.cpp \
    window/dialog/graphiccomponentdialog.cpp \
    window/dialog/graphiclimitdialog.cpp \
    window/dialog/inputstringdialog.cpp \
    helper/smartthread/smartthreadpool.cpp \
    helper/smartthread/smartthreadmanager.cpp \
    helper/schedule.cpp \
    window/schedulerdialog.cpp \
    window/widget/schedulestreewidget.cpp \
    helper/schedulerthread/schedulerthreadmanager.cpp \
    helper/graphicobjectiteratorthread/graphicobjectiteratorthreadmanager.cpp

HEADERS += \
    model/artery.h \
    model/arteryTree.h \
    model/point.h \
    helper/fileHelper.h \
    helper/mathHelper.h \
    canvas/oglmaincanvas.h \
    window/messagewindow.h \
    window/mainwindow.h \
    window/consolewindow.h \
    model/smartobject.h \
    model/graphicobject.h \
    model/colorbar.h \
    model/colorbar/brasil.h \
    model/colorbar/aero.h \
    model/colorbar/red.h \
    canvas/oglcanvas.h \
    model/graphic2d.h \
    model/graphic3d.h \
    model/graphic2d/ktgraphic.h \
    model/graphic2d/staticgraphic.h \
    model/graphic2d/lfgraphic.h \
    model/colorbar/blue.h \
    model/graphic3d/staticgraphic3d.h \
    model/graphic3d/lines3d.h \
    model/colorbar/black.h \
    window/contourwindow.h \
    model/graphic3d/ktgraphic3d.h \
    helper/vtkfile.h \
    window/loadwizard.h \
    canvas/oglsidecanvas.h \
    window/widget/listgraphiccompatibilitywidget.h \
    window/dialog/inputscalardialog.h \
    window/widget/pointdatalistwidget.h \
    window/widget/celldatalistwidget.h \
    window/dialog/viewdatadialog.h \
    window/widget/stringitemlistwidget.h \
    window/graphicobjectswindow.h \
    window/widget/graphicobjecttreewidget.h \
    window/graphicobjectdetailswindow.h \
    window/dialog/inputbooleandialog.h \
    model/graphic2d/nsgraphic.h \
    model/graphic2d/livearteryparamgraphic.h \
    window/generateccotreewindow.h \
    helper/arterialtreecoreHelper.h \
    model/smartlog.h \
    model/smartlogmessage.h \
    model/colorbar/yellow.h \
    model/colorbar/magenta.h \
    model/colorbar/green.h \
    model/colorbar/cyan.h \
    model/colorbar/colorcube.h \
    model/colorbar/winter.h \
    model/colorbar/summer.h \
    model/graph.h \
    model/graph/node.h \
    model/graph/nodegraph.h \
    model/graph/weightedgraph.h \
    window/generateweightedgraphwindow.h \
    model/smartclassstack.h \
    model/graph/solution.h \
    window/dialog/selectfromstringlistdialog.h \
    model/graph/cost.h \
    model/liveparameter.h \
    model/graphic2d/liveparamgraphic.h \
    helper/systemsettingshelper.h \
    model/graph/automatusgraph.h \
    model/graph/automatusgraph/formalautomatadefinitegraph.h \
    window/dialog/igusettingsdialog.h \
    window/dialog/graphiccomponentdialog.h \
    window/dialog/graphiclimitdialog.h \
    window/dialog/inputstringdialog.h \
    helper/smartthread/smartthreadpool.h \
    helper/smartthread/smartthreadmanager.h \
    helper/schedule.h \
    window/schedulerdialog.h \
    window/widget/schedulestreewidget.h \
    helper/schedulerthread/schedulerthreadmanager.h \
    helper/graphicobjectiteratorthread/graphicobjectiteratorthreadmanager.h

DISTFILES += \
    config.igu \
    log.igu \
    freeglut.dll \
    glew32.dll \
    TODO \
    images/icons8-trash-can-50.png \
    images/icons8-stop-50.png \
    images/icons8-skip-to-start-50.png \
    images/icons8-save-close-50.png \
    images/icons8-save-as-50.png \
    images/icons8-save-all-50.png \
    images/icons8-save-50.png \
    images/icons8-rewind-50.png \
    images/icons8-resume-button-50.png \
    images/icons8-record-50.png \
    images/icons8-play-50.png \
    images/icons8-pause-50.png \
    images/icons8-loader-50.png \
    images/icons8-info-50.png \
    images/icons8-fast-forward-50.png \
    images/icons8-end-50.png \
    images/icons8-delete-bin-64.png

FORMS += \
    window/messagewindow.ui \
    window/mainwindow.ui \
    window/consolewindow.ui \
    window/contourwindow.ui \
    window/loadwizard.ui \
    window/dialog/inputscalardialog.ui \
    window/dialog/viewdatadialog.ui \
    window/graphicobjectswindow.ui \
    window/graphicobjectdetailswindow.ui \
    window/dialog/inputbooleandialog.ui \
    window/generateccotreewindow.ui \
    window/generateweightedgraphwindow.ui \
    window/dialog/selectfromstringlistdialog.ui \
    window/dialog/igusettingsdialog.ui \
    window/dialog/graphiccomponentdialog.ui \
    window/dialog/graphiclimitdialog.ui \
    window/dialog/inputstringdialog.ui \
    window/schedulerdialog.ui

STATECHARTS +=

RESOURCES += \
    images.qrc

mac {
    RC_FILE = misc/Icon.icns
    TARGET = IGU
}

unix {
    isEmpty(PREFIX) {
        PREFIX = /usr/local
    }

    target.path = $$PREFIX/bin

    shortcutfiles.files = misc/IGU.desktop
    shortcutfiles.path = $$PREFIX/share/applications/
    data.files += misc/feedthemonkey.xpm
    data.path = $$PREFIX/share/pixmaps/

    INSTALLS += shortcutfiles
    INSTALLS += data
}

INSTALLS += target

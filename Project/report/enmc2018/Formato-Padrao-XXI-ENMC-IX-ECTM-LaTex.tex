\documentclass[12pt,fleqn]{article}
\usepackage{xiiiemc}
\usepackage{natbib}
\usepackage{fancyhdr}
\usepackage{color}
\usepackage{wallpaper} 
\usepackage{titlesec}   %% Define space between paragraph e section
\usepackage{float} 	%% Use to fix Figure or Table: ex: \begin{table}[H]
%%%%Don't edit this block. It reduces the spacing between the lines of the references
\let\OLDthebibliography\thebibliography
\renewcommand\thebibliography[1]{\OLDthebibliography{#1} \setlength{\parskip}{0pt}\setlength{\itemsep}{0pt plus 0.3ex}}

%%-----------------------------------------------EDIT-----------------------------------------------
\title{ANÁLISE DO ESCOAMENTO PULSÁTIL EM ÁRVORES ARTERIAIS}

%%-----------------------------------------------EDIT----------------------------------------------
\author
    {\rm \begin{tabular}{l} 
    \textbf{Igor Pires dos Santos}$^{1}$ - {\textnormal igor675@hotmail.com}\\%
    \textbf{Rafael Alves Bonfim de Queiroz}$^{1}$ - {\textnormal bonfimraf@gmail.com}\\
    {\fontsize{11}{0}\selectfont $^{1}$Universidade Federal de Juiz de Fora - Juiz de Fora, MG, Brazil}\vspace*{-0.05cm} 
  \end{tabular}}
%%----------------------------------------------------------------------------------------------

\fancypagestyle{firspagetstyle}
{
	\lhead{}
	\fancyhead[C]{%
		\includegraphics[width=0.75\linewidth]{logo}\\%
		{\scriptsize \fontfamily{phv}\fontseries{b}\selectfont \color[rgb]{0.45,0.45,0.45}
		08 a 11 de Outubro de 2018\\
		Instituto Federal Fluminense\\
		Búzios - RJ\\
	    }
	}
	\renewcommand{\headrulewidth}{0.0pt}
	\fancyfoot[C]{\footnotesize \parbox{15cm} {\centering  \fontsize{7.5}{0}\selectfont \it Anais do XXI ENMC – Encontro Nacional de Modelagem Computacional e IX ECTM – Encontro de Ciências e Tecnologia de Materiais,  Búzios, RJ – 08 a 11 Outubro 2018}} % \ttfamil
	\rhead{}
}


\begin{document}
\maketitle

\thispagestyle{firspagetstyle}

\fancyhead[L]{\footnotesize{\fontsize{7.5}{0}\selectfont \it XXI ENMC e IX ECTM\\
	08 a 11 de Outubro de 2018\\
	Instituto Federal Fluminense – Búzios - RJ\\}}
\renewcommand{\headrulewidth}{0.0pt}
\fancyfoot[C]{\footnotesize \parbox{15cm} {\centering  \fontsize{7.5}{0}\selectfont \it  Anais do XXI ENMC – Encontro Nacional de Modelagem Computacional e IX ECTM – Encontro de Ciências e Tecnologia de Materiais,  Búzios, RJ – 08 a 11 Outubro 2018}} % \ttfamil
\rhead{}

\begin{abstract}
Um esquema iterativo analítico é apresentado para computar as características locais das ondas de fluxo e pressão enquanto elas passam através de uma estrutura de árvore e são modificadas por reflexões de onda. Resultados são obtidos para ilustrar o fenômeno de \textit{picos de pressão} sobre duas circunstâncias. É considerada a propagação de uma onda harmônica simples ao longo de árvore simples, aonde as reflexões de onda modificam a amplitude da onda de pressão enquanto ela progride. Os resultados comprovam que a causa para esse fenômeno é a reflexão de onda causada pelos degraus na queda da admitância, como previamente sugerido, ao invés de interações não-lineares como também previamente sugerido. É apresentado também um ambiente que permite a visualização do modelo arterial, e, posteriormente, utilizar o esquema iterativo.
\end{abstract}

\keywords{\em{Árvores arteriais, Fluxo pulsátil, Picos de pressão}}

\pagestyle{fancy}

\section{INTRODUÇÃO}
A incidência maior de \textit{picos} na onda de pressão ao viajar pela aorta já foi documentado como evidência para os efeitos da reflexão em árvores vasculares \cite{Kouchoukos}\cite{Lighthill}\cite{McDonald}. Enquanto as áreas de reflexão não podem sem completamente conhecidas ou localizadas, é geralmente aceito que a forma da onda de pressão é modificada significativamente enquanto progride pela aorta, de uma forma que só pode ser explicada por reflexões de onda. Um entendimento mais claro da relação entre modificações e fatores de modificação ainda há de ser alcançado. Para fazer tal, é necessário um método de análise ou mensuramento que determine a forma de onda que o pulso de pressão toma em cada ponto ao viajar por uma árvore arterial e isso permitirá a ramificação da arquitetura arterial para que esta seja modificada para observar a mudança correspondente aos efeitos da reflexão de onda.
Trabalhos anteriores prepararam os fundamentos para estudo experimental e teórico de reflexões de onda em um tubo simples\cite{Barnard}\cite{Taylor1}\cite{Taylor2}, e em uma árvore ramificada randomicamente \cite{Taylor3}\cite{Taylor4}. Uma interpretação desses resultados no contexto do que realmente pode ser observado  ou medido no sistema cardiovascular é dado por McDonald\cite{McDonald}. Mais recentemente, Avolio\cite{Avolio} propôs um modelo multi-ramificado da árvore arterial humana e computou a impedância e as distribuições de pressão e fluxo em artérias principais, e \cite{Helal} \textit{et al.}  propôs um esquema para computar as propriedades das redes vasculares que possivelmente incluiriam circuitos fechados. O esquema baseia-se no método de redução de rede que é particularmente adequado para tratar propriedades globais de redes vasculares como um todo ao invés de com propriedades locais.
O objetivo deste relatório é apresentar um esquema analítico que possa lidar com uma estrutura de árvore arterial numa base local de forma que cada segmento de vaso sanguíneo possa ser identificado individualmente e o formato de onda da pressão possa ser determinado em cada ponto ao longo deste vaso. E ainda apresentar um ambiente desenvolvido para tal finalidade que auxilia na análise dos mais diferentes formatos de árvore. Um esquema com esse grau de detalhe é necessário já que o fenômeno \textit{picos de pressão} e outros efeitos da reflexão de onda precisam ser estudados num contexto específico de estruturas de árvore em quem a arquitetura de ramificação é determinada por dados morfológicos em que cada segmento de vaso pode ter propriedades individuais.

\section{METODOLOGIA}

A propagação de ondas em um tubo é governada pela equação de onda para a pressão $p(x,t)$ e volume de fluxo $q(x,t)$, ambas sendo funções no tempo $t$ e coordenada axial $x$ ao longo do tubo,

\begin{equation}
\frac{\partial q}{\partial t} = -cY \frac{\partial p}{\partial x}  
\label{01_p}
\end{equation}

\begin{equation}
\frac{\partial p}{\partial t} = -\frac{c}{Y} \frac{\partial p}{\partial x}  
\label{02_q}
\end{equation}

aonde $c$ é a velocidade de onda, $Y = A/\rho c$ é a admitância e $A$ é a área da seção transversal, e $\rho$ é a densidade do fluido. Essa equações são baseadas na linearização das equações de movimento do fluido \cite{Lighthill}\cite{Fung}. Resultados baseados nessas equações são análogas àquelas da teoria da transmissão clássica \cite{Taylor1}, mas aqui elas estão moldadas de forma que possam ser iteradas por toda uma estrutura de árvore.
Para uma onda harmônica simples Eq.\ref{01_p} e Eq.\ref{02_q} ficam na forma:

\begin{equation}
p = \bar{p}_0 \exp{i\omega(t - x/c)} + R  \bar{p}_0 \exp{i\omega(t - 2L/c + x/c)}
\label{03_p}
\end{equation}

\begin{equation}
q = Y(\bar{p}_0 \exp{i\omega(t - x/c)} -  R  \bar{p}_0 \exp{i\omega(t - 2L/c + x/c)})
\label{04_1}
\end{equation}

aonde $\omega$ é a frequência angular, $L$ é o comprimento do tubo, $\bar{p}_0$ a amplitude da onda incidente e $R$ é o coeficiente de reflexão, definido pela razão das ondas refletidas pelas ondas que chegam à área de reflexão \cite{Fung} \cite{Karreman}.
Em árvores arteriais, são aplicadas as expressões acima para fluxo e pressão em cada segmento de vaso, tomando $x = 0$ para o nó adjunto e $x = L$ para o nó distal do segmento. Um segmento de vaso é definido pela intervalo vascular entre duas áreas de ramificação\cite{Zamir3}. Para o propósito do atual estudo, consideraremos um modelo geral de um árvore binária completamente desenvolvida como na Fig.\ref{fig:arterial-tree}. Essa estrutura é particularmente apropriada para o nosso propósito já que incorpora muitas estrutura de árvores incompletas como casos especiais, já que bifurcações arteriais são as mais comuns ramificações em árvores arteriais\cite{Zamir1}.
É proposto em \cite{Duan} uma matriz dimensional $(k,j)$ para identificar cada vaso, aonde o primeiro elemento representa a geração e o segundo representa um número sequencial, como mostrado na Fig.\ref{fig:arterial-tree} .

\begin{figure}[!htbp] %h or !htbp
	\vspace{-2pt}
	\begin{center}
		\includegraphics[height=8.7cm,width=4.5cm]{Images/ArterialTree.png}%
		\caption{Notação usada para identificar cada vaso (k,j) aonde $k$ é a geração/nível do vaso e j é um número sequencial dentro daquela geração. O nó adjunto e distal do vaso são denotados por A e B, respectivamente. O coeficiente de reflexão $R(k,j)$ do segmento $(k,j)$ está associado ao nó distal $B$.}
		\label{fig:arterial-tree}%
	\end{center}
\end{figure}

A matriz fica completa quando a árvore que ela descreve é completa, e incompleta quando a árvore não possui algumas ramificações. Aqui propomos tratar cada vaso desses como um objeto \textit{artéria} denominado $V$, que possui um ponteiro para a artéria superior $V|_f$, e uma para cada uma de suas ramificações $V|_r$ e $V|l$ como podemos ver na Fig.\ref{fig:my-method}.

\begin{figure}[!htbp] %h or !htbp
	\vspace{-2pt}
	\begin{center}
		\includegraphics[height=4.35cm,width=5.5cm]{Images/method.png}%
		\caption{Notação usada para identificar cada vaso $V$, aonde $V|_f = (k-1,s)$, $V|_l = (k+1,2j-1)$ e $V|r = (k+1,2j$).}
		\label{fig:my-method}%
	\end{center}
\end{figure}

Usando a notação citada em \cite{Duan}, a pressão e o fluxo ao longo de um segmento de vaso geral $(k,j)$ em uma estrutura de árvore é dada por:

\begin{equation}
p(k,j) = \bar{p}(k,j) \exp{[i\omega(t - x(k,j)/c(k,j)]} + R(k,j)  \bar{p}(k,j) \exp{[i\omega(t - 2L(k,j)/c(k,j) + x(k,j)/c(k,j)]}
\label{05_p}
\end{equation}

\begin{equation}
q = Y(\bar{p}(k,j) \exp{[i\omega(t - x(k,j)/c(k,j)]} - R(k,j)  \bar{p}(k,j) \exp{[i\omega(t - 2L(k,j)/c(k,j) + x(k,j)/c(k,j)]})
\label{06_q}
\end{equation}

Aonde $\bar{p}(k,j)$ é a amplitude combinada do grupo de ondas progressivas no vaso (k,j) e $R(k,k)$ é o coeficiente de reflexão do vaso, como é chamado a razão das ondas progressivas pelas retrógradas avaliadas no nó distal aonde $x(k,j) = L(k,j)$. O grupo de ondas progressivas viaja no sentido positivo de $x(k,j)$, estas são compostas de ondas progressivas vindo de vasos acima deste bem como ondas refletidas onde $x(k,j) = 0$. O grupo de ondas retrógradas viaja no sentido oposto e é composto por ondas vindas de vasos inferiores vem como ondas refletidas onde $X(k,j) = L(k,j)$.
Portanto as equações Eq.\ref{05_p} e Eq.\ref{06_q} descreve o fluxo e a pressão localmente no vaso (k,j) ao longo de uma estrutura de árvore numa posição geral $(k,j)$, e localmente na posição $x$ dentro deste segmento de vaso.As duas variáveis desconhecidas são a amplitude das ondas progressivas $\bar{p}$ e o coeficiente de reflexão $R$.

Usando a notação apresentada pela Fig.\ref{fig:my-method}, Eq.\ref{05_p} e Eq.\ref{06_q} viram, respectivamente:

\begin{equation}
p(k,j) = \bar{p} \exp{[i\omega(t - x/c]} + R  \bar{p}(k,j) \exp{[i\omega(t - 2L/c + x/c]}
\label{07_p}
\end{equation}

\begin{equation}
q = Y(\bar{p} \exp{[i\omega(t - x/c)]} - R  \bar{p} \exp{[i\omega(t - 2L/c + x/c]})
\label{08_q}
\end{equation}


Para determinar a pressão $\bar{p}$ num certo vaso $V$ aplicamos a condição de continuidade no nó adjunto $A$ (Fig.\ref{fig:arterial-tree}). Escrevendo os componentes progressivos e retrógrados da onda como $p_f$ e $p_b$ respectivamente, a pressão no começo do segmento onde $x = 0$ é dado por:

\begin{equation}
\left[ p \right]_A = \left[ p_f \right]_a + \left[ p_b \right]_a
\label{09_p}
\end{equation}

aonde

\begin{equation}
\left[ p_f \right]_A = \bar{p}\exp{\left[ i\omega t\right] }
\label{10_p_f}
\end{equation}

\begin{equation}
\left[ p_b \right]_A = R\bar{p}\exp{\left[ i\omega \left( t - 2L/c\right)\right] }
\label{11_p_b}
\end{equation}

Similarmente, a pressão no vaso superior $V|f$ pode ser escrita como:

\begin{equation}
\left[ p|_f \right]_A = \left[ p_f|_f \right]_A + \left[ p_b|_f \right]_A
\label{12_p|_f}
\end{equation}

aonde

\begin{equation}
\left[ p_f|_f \right]_A = \bar{p|_f}\exp{\left[ i\omega|_f \left(t - x|_f/c|_f\right)\right] }
\label{13_p_f}
\end{equation}

\begin{equation}
\left[ p_b|_f \right]_A = R|_f\bar{p}\exp{\left[ i\omega \left( t - 2L|_f/c|_f + x|_f/c|_f\right)\right] }
\label{14_p_b}
\end{equation}

No nó distal do vaso superior aonde $x|_f = L|_f$, a pressão é

\begin{equation}
\left[ p_f|_f \right]_A = \bar{p|_f}\exp{\left[ i\omega|_f \left(t - L|_f/c|_f\right)\right] }
\label{15_p_f}
\end{equation}

\begin{equation}
\left[ p_b|_f \right]_A = R|_f\bar{p}\exp{\left[ i\omega \left( t - L|_f/c|_f \right)\right] }
\label{16_p_b}
\end{equation}

A condição de continuidade da pressão é que na junção ela assuma um único valor, portanto:

\begin{equation}
\left[ p_f \right]_A + \left[ p_b \right]_A = \left[ p_f|_f \right]_A + \left[ p_b|_f \right]_A
\label{17_p_cont}
\end{equation}

Substituindo as equações Eq.\ref{10_p_f},Eq.\ref{15_p_f} e Eq.\ref{16_p_b} na Eq.\ref{17_p_cont} obtemos:

\begin{equation}
\bar{p} = \bar{p|_f} \times \frac{\left( 1 + R|_f\right) \exp{\left[  -i \omega L|_f/c|_f \right] }}{1 + R\exp{\left[ -i2\omega L|_f/c|_f\right] }}
\label{18_barp}
\end{equation}

Para meios de cálculo as pressões são \textit{não-dimensionalizadas}, colocando a pressão de entrada em Eq.\ref{18_barp} como:
\begin{equation}
    p_0 = \bar{p}_0\exp{\left[ i\omega t\right] }
	\label{18_barp0}
\end{equation}


Escrevendo:


\begin{equation}
P = p/p_0
\label{19_P}
\end{equation}

\begin{equation}
\bar{P} = \bar{p}/\bar{p_0}
\label{20_barP}
\end{equation}

Podemos obter da Eq.\ref{05_p}:

\begin{equation}
P = \bar{P}\left\lbrace \exp{\left[ -i\beta X \right] } + R \exp{\left[ -i2\beta \right] } \exp{\left[ i\beta X \right] } \right\rbrace 
\label{21_P}
\end{equation}

Aonde

\begin{equation}
\beta = \omega \frac{L}{c}
\label{22_beta}
\end{equation}

\begin{equation}
X = x/L
\label{22_X}
\end{equation}

Similarmente da Eq.\ref{06_q} para o fluxo $q$ temos a forma \textit{não-dimensional}:

\begin{equation}
Q = M\bar{P}\left\lbrace \exp{\left[ -i\beta X \right] } + R \exp{\left[ -i2\beta \right] } \exp{\left[ i\beta X \right] } \right\rbrace 
\label{23_Q}
\end{equation}

Aonde,

\begin{equation}
Q = q/q_0
\label{24_Q}
\end{equation}

\begin{equation}
M = Y/Y|_r
\label{25_M}
\end{equation}

Onde $Y|_r$ é a admitância encontrada na raiz.

\begin{equation}
q_0 = Y|_rp_0
\label{26_q_0}
\end{equation}

Portanto, a forma não-dimensional da pressão $P$ e do fluxo $Q$ dependem da amplitude da onda de pressão $\bar{P}$ da onda progressiva e do coeficiente de reflexão $R$. O primeiro destes são determinados pela amplitude da onda de pressão e reflexão dos vasos superiores. Equação Eq.\ref{18_barp} pode ser usada como uma fórmula iterativa para calcular a distribuição de pressão e fluxo, começando em um ponto da árvore até o outro, dependendo de condições de contorno.

Para determinar os coeficientes de reflexão nas junções devemos primeiro considerar as duas junções $A$ e $B$ das duas extremidades do vaso $V$ numa estrutura como a apresentada na Figura \ref{fig:my-method}. No nó distal $B$ o coeficiente de reflexão pela fórmula padrão para uma bifurcação \cite{Fung} \cite{Lighthill}, que com a notação adotada aqui toma a forma:

\begin{equation}
R = \frac{Y - [Y_e|_l + Y_e|_r]}{Y + [Y_e|_l + Y_e|_r]}
\label{27_R}
\end{equation}

Aqui, $Ye|_r$ e $Ye|_l$ são admitâncias efetivas no nó adjunto dos vasos inferiores à junção $B$, determinada pela razão de fluxo pela pressão no local, que é dada por:

\begin{equation}
Y_e|_f = Y|_f\frac{1 - R|_f\exp{[-i2\beta|_f]}}{1 + R\exp{[-i2\beta|_f]}}
\label{28_Ye}
\end{equation}

aonde $R|_f$ é o coeficiente de reflexão ao final de cada vaso. Similarmente, $Ye$, a admitância no nó adjunto $A$, pode ser escrita como:

\begin{equation}
Y_e = Y\frac{1 - R\exp{[-i2\beta]}}{1 + R\exp{[-i2\beta]}}
\label{29_Ye}
\end{equation}

Substituindo o $R$ da equação Eq.\ref{27_R}, isso se torna uma fórmula iterativa para admitâncias efetivas pela árvore:

\begin{equation}
Y_e = Y\frac{Y_e|_r + Y_e|_l+ i Y\tan{\beta}}{Y + i(Y_e|_r + Y_e|_l)\tan{\beta}}
\label{30_Ye}
\end{equation}

Em vasos terminais(ou "\textit{folhas}"), pode ser assumido que não ocorrem mais reflexões vindas de vasos inferiores, portanto a admitância efetiva desses vasos é igual à suas admitâncias características. Dada a equação Eq.\ref{30_Ye}, todas as admitâncias efetivas podem ser determinadas "\textit{marchando}" das folhas para a raiz da árvore.

\section{RESULTADOS}

Resultados foram obtidos para ilustrar o fenômeno de \textit{picos de pressão} sob dois tipos diferentes de circunstâncias. No primeiro caso, a propagação de uma onda harmônica simples ao longo de uma árvore é considerado, aonde reflexões de onda modificam a amplitude da onda de pressão enquanto ela avança. A escolha de uma harmônica simples nesse caso torna possível examinar os efeitos da frequências, fluído viscoso e viscoelasticidade da parede do vaso. No segundo caso, a propagação é de uma onda composta através de uma árvore com diversas ramificações é considerado, aonde as reflexões de onda modificam a forma da onda enquanto esta viaja. A forma inicial de onda é escolhida para ser similar ao pulso da pressão cardíaca como se fosse medido na raiz da artéria. Quatro diferentes estruturas são consideradas para ilustrar os efeitos da ramificação e o gradiente de ramificação, particularmente em relação à \textit{picos} da forma de onda.

É suficiente considerar um estrutura de árvore simples neste caso, e como em \cite{Duan}, é escolhido um modelo com base na árvore arterial canina. Para atribuir a legenda correta para os diferentes elementos, a árvore é "mapeada" numa bifurcação geral como mostrado em \ref{fig:my-method}. 

\begin{table}[H]
	\caption{Parâmetros de entrada}
	\vspace{12pt}
	\centering{}
	\begin{tabular}{|c||c|c|c|c|}
		\hline 
		& Comprimento ($m$) & Densidade ($Kg/m^3$) & Viscosidade ($Kg/m s$) & Diâmetro ($m$) \\ 
		\hline
		\hline 
		Aorta Descendente & 0.25 & $0.960 x 10^{-3}$ & 0.00385 & 0.013 \\ 
		\hline 
		Aorta Abdominal & 0.11 & $1.134 x 10^{-3}$ & 0.00449 & 0.009 \\ 
		\hline 
		Artéria Ilíaca & 0.12 & $1.172 x 10^{-3}$ & 0.00472 & 0.006 \\ 
		\hline 
		Artéria Femoral & 0.10 & $1.235 x 10^{-3}$ & 0.00494 & 0.004 \\ 
		\hline 
	\end{tabular} 
\end{table}


E a grossura das paredes é dada por $h = 0.05 d$. O módulo de Young foi tomado como $4.8 \times 10^5 \text{Pa}$ para o primeiro segmento e $1.0 \times 10^6 \text{Pa}$ para os segmentos subsequentes.

Das equações \ref{21_P} e \ref{23_Q} observa-se que as propriedades do fluxo depende da admitância $Y$ e o parâmetro não-dimensional $\beta$, que por sua vez depende da velocidade $c$. A admitância característica $Y$ para cada segmento é calculado com:

\begin{equation}
Y = \frac{A}{\rho c}
\label{31_Y}
\end{equation}

aonde $A$ é área da seção transversal do segmento, $\rho$ é a densidade do fluido dentro do vaso e $c$ é a velocidade de onda correspondente.

Assumindo uma parede final e elástica a velocidade de onda é dada pela fórmula de Moens-Korteweg \cite{Fung}.

\begin{equation}
c = \sqrt{\frac{Eh}{\rho d}}
\label{32_c}
\end{equation}

Aonde $E$ é o módulo de Young e $h$ a espessura da parede do vaso. Pela teoria linear, é assumido que o diâmetro de um vaso não varia ao longo do vaso, portanto a velocidade também não.

Os efeitos da viscosidade do fluido podem ser incorporados nos cálculos iterativos substituindo a velocidade de onda $c$ por uma velocidade de onda complexa $c_v =  c \sqrt{\epsilon}$, aonde $\epsilon$ é o fator viscoso que corresponde à um tubo elástico com fortes limites, como proposto por Duan \& Zamir \cite{Duan}. Viscoelasticidade do vaso é incorporada substituindo o módulo de Young por um módulo de Young complexo $E_c = |E| \exp{i \phi}$, aonde $\phi$ é o ângulo de fase entre a pressão e o deslocamento da parede do vaso, como proposto por \cite{Taylor3}. Como neste caso de estudo temos um valor grande de $\alpha$, foi utilizada a seguinte proposta, como em \cite{Taylor3}

\begin{equation}
\epsilon = 1 - \frac{2.0}{\alpha \sqrt{i}}
\label{33_phi}
\end{equation}

Aonde $\alpha$ representa o número não-dimensional de Womersley, dado pela seguinte equação:

\begin{equation}
\alpha = r \sqrt{\frac{w \rho}{\mu}}
\label{34_alpha}
\end{equation}

Baseado nisso, foi calculada a amplitude da pressão ao longo desta simples árvore arterial da Fig.\ref{fig:arterial-tree}. Resultados foram obtidos para quatro diferentes frequências e três diferentes fluxo/vaso configurações (variando o angulo da fase $\phi$: (i) fluxo viscoso num tubo puramente elástico (ii) fluxo invíscido num tubo viscoelástico e (iii) fluxo viscoso num tubo viscoelástico. Os resultados são exibidos nas figuras abaixo. 

Na falta de reflexões de onda, a distribuição de onda seria $|P| = 1.0$ para toda a árvore no caso invíscido. No caso com amortecimento viscoso, causado tanto pela viscosidade do fluido quanto pela viscoelasticidade da parede do vaso, ou ambos, a distribuição seria $|P| = 1.0$ na raiz(aonde $X = 0.0$) e $|P| < 1.0$ para todo o resto da árvore devido ao amortecimento. Em sua maioria há um crescimento na pressão no sentido do fluxo, porém isto pode variar localmente dependendo da frequência. Esse comportamento é a base da causa dos "\textit{picos}" de pressão como é demonstrado abaixo.

\begin{figure}[!htbp]
	\resizebox{\columnwidth}{!}{%
		\begin{tabular}{cc}
			\includegraphics[width=1.0\linewidth]{Images/varying_visc_f3_65} & \includegraphics[width=1.0\linewidth]{Images/varying_phi_f3_65} \\
			
			(a) Amplitude da pressão $|P|$ ao longo da árvore arterial variando o $\mu_x$ em ($0.0,0.5,1.0,1.5$); Com frequência de $3.65$Hz. & (b) Amplitude da pressão $|P|$ ao longo da árvore arterial variando o $\phi_0$ em ($0^\circ,4^\circ,8^\circ,12^\circ$); Com frequência de $3.65$Hz. \\
			
			\includegraphics[width=1.0\linewidth]{Images/varying_visc_f7_3} & \includegraphics[width=1.0\linewidth]{Images/varying_phi_f7_3} \\
			
			(c) Amplitude da pressão $|P|$ ao longo da árvore arterial variando o $\mu_x$ em ($0.0,0.5,1.0,1.5$); Com frequência de $7.30$Hz. & (d) Amplitude da pressão $|P|$ ao longo da árvore arterial variando o $\phi_0$ em ($0^\circ,4^\circ,8^\circ,12^\circ$); Com frequência de $7.30$Hz. \\
			
			\includegraphics[width=1.0\linewidth]{Images/varying_visc_f10_95} & \includegraphics[width=1.0\linewidth]{Images/varying_phi_f10_95}\\
			
			(e) Amplitude da pressão $|P|$ ao longo da árvore arterial variando o $\mu_x$ em ($0.0,0.5,1.0,1.5$); Com frequência de $10.95$Hz. & (f) Amplitude da pressão $|P|$ ao longo da árvore arterial variando o $\phi_0$ em {$0^\circ,4^\circ,8^\circ,12^\circ$}; Com frequência de $10.95$Hz.\\
			
			\includegraphics[width=1.0\linewidth]{Images/varying_visc_f14_6} & \includegraphics[width=1.0\linewidth]{Images/varying_phi_f14_6}\\
			
			(g) Amplitude da pressão $|P|$ ao longo da árvore arterial variando o $\mu_x$ em ($0.0,0.5,1.0,1.5$); Com frequência de $14.60$Hz. & (h) Amplitude da pressão $|P|$ ao longo da árvore arterial variando o $\phi_0$ em $0^\circ,4^\circ,8^\circ,12^\circ$; Com frequência de $14.60$Hz.\\
			
		\end{tabular}%
	}
	\label{fig:varyingviscphi}
	\caption{Amplitude da pressão $|P|$ ao longo da árvore arterial em diversos casos de estudo.}
\end{figure}

\section{CONCLUSÃO}

Observando a Fig.\ref{fig:varyingviscphi}, na primeira coluna, o efeito de viscosidade do fluido pode ser examinado, considerando um tubo puramente elástico com quarto diferentes valores de viscosidade do fluído $\mu = \mu \mu_x$, com $\mu_x$ variando entre ($0.0,0.5,1.0,1.5$) e $\mu$ sendo o valor da viscosidade dado na seção anterior.

Na segunda coluna da Fig.\ref{fig:varyingviscphi} o efeito da viscoelasticidade da parede do vaso pode ser examinado considerando um fluido invíscido e tomando quatro diferentes valores de viscoelasticidade. Percebe-se que o efeito da viscoelasticidade, como todo fluido viscoso, é amortecer o crescimento geral na amplitude da onda de pressão causada pelas pressões de onda enquanto a onda viaja no sentido para baixo, assim como suavizar os picos locais na distribuição de pressão. O efeito de amortecimento é mais dramático no caso, entretanto, particular de altas frequências.

Portanto, os resultados produzidos no ambiente computacional são compatíveis com \cite{Duan}. O ambiente é possibilita que novos modelos árvores arteriais sejam visualizados e, então, analisados. Este foi projetado para abrir qualquer 

\subsection*{\textit{Reconhecimentos}}
This section should be positioned between the end of the text and the reference list. Type \textbf{\textit{Acknowledgements}} in boldface italics, skip one line of space and type the text in regular type.


% ------------------------------------------------------------------------
\begin{thebibliography}{99}
\fontsize{11}{0}\selectfont

\bibitem[Kouchoukos, 1970]{Kouchoukos}
Kouchoukos, N. T., L. C. Sheppard, and D. A. McDonald.(1970) , "Estimation of stroke volume in the dog by a pulse contour method. Circ. Res. 26:611-23".

\bibitem[Lighthill, 1975]{Lighthill}
Lighthill, M. (1975), "Mathematical Biofluidmechanics. Philadelphia: Society for Industrial \& Applied Mathematics".

\bibitem[McDonald, 1974]{McDonald}
McDonald, D.A. (1974), "Blood Flow in Arteries. Baltimore: Williams \& Wilkins".

\bibitem[Barnard \& Hunt \& Varley, 1966]{Barnard}
Barnard, A. C. L., W. A. Hunt, W. P. Timlake, and E. Varley. (1966), "Peaking of pressure in fluid-filled tubes od spatially varying compliance, \textit{Biophys}. J. 6:735-746".

\bibitem[Taylor, 1957]{Taylor1}
Taylor, M. G. (1957), "An approach to an analysis of the arterial pulse wave I. Oscillations in an attenuating line. Phys. Med. Biol. 1:258-269".

\bibitem[Taylor, 1957]{Taylor2}
Taylor, M. G. (1957), "An approach to an analysis of the arterial pulse wave II. Fluid oscillations in an elastic pipe. Phys. Med. Biol. 1:321-329".

\bibitem[Taylor, 1966]{Taylor3}
Taylor, M. G. (1966), "The input impedance of an assembly of randomly branching elastic tubes. Biophys. J. 6:29-51".

\bibitem[Taylor, 1966]{Taylor4}
Taylor, M. G. (1966), "Wave transmission through an assembly of randomly branching elastic tubes. Biophys. J. 6:697-716".

\bibitem[Avolio, 1980]{Avolio}
Avolio, A. P. (1980), "Multi-branched model of the human arterial system. Med. Biol. Eng. Comput. 18:709-718".

\bibitem[Helal, 1990]{Helal}
Helal, M. A., K. C. Watts, A. E. Marble, and S. N. Sarwal. (1980), "Theoretical model for assessing haemodynamics in arterial networks which include bypass grafts. Med. Biol. Eng. Comput. 28:465-473".

\bibitem[Fung, 1984]{Fung}
Fung, Y. C. (1984), "Biodynamics: Circulation. New York: Springer-Verlag".

\bibitem[Karreman, 1952]{Karreman}
Karreman, G. (1952), "Some contributions to the mathematical biology of blood circulation. Reflection of pressure waves in the arterial system. Bull. Math. Biophys. 14:327-350".

\bibitem[Zamir, 1976]{Zamir1}
Zamir, M. (1976), "Optimality principles in arterial branching. J. Theor. Biol. 62:227-251".

\bibitem[Zamir, 1988]{Zamir2}
Zamir, M. (1988), "The branching structure of arterial trees. Comments Theor. Biol. 1:15-37".

\bibitem[Zamir, 1988]{Zamir3}
Zamir, M., and S. Phipps. (1988) "Network analysis of an arterial tree. J. Biomech. 21:25-34".

\bibitem[Zamir, 1992]{Zamir4}
Zamir, M., P. Sinclair, and T. H. Wonnacott. (1992) "Relation between diameter and flow rate in blood vessels. J. Biomech. 25:1303-1310".

\bibitem[Duan \& Zamir, 1995]{Duan}
Duan, B. Zamir, M. (1995), "Pressure Peaking in Pulsatile Flow through Arterial Tree Structures" em Annals of BiomeDical Engineering, Vol 23. pp. 794-803.

\end{thebibliography}
\vspace*{-0.1cm}
\newpage
\section*{APPENDIX A}

\vspace{0.5cm} % Example spaces
% ------------------------------------------------------------------------

%For papers written in Portuguese or Spanish.

\begin{center}
  ANALYSIS OF PULSATILE FLOW THROUGH AN ARTERIAL TREE
\end{center}

\def\abstractname{Abstract}%

\begin{abstract}
	An analytic iterative scheme is presented to compute local characteristics of pressure and flow waves as they pass through tree structure and are modified by wave reflections. Results are obtained to illustrate the phenomena of \textit{pressure peaking} under two circumstances. It's considered a single harmonic wave propagating along the tree, where reflection waves modify the amplitude of the pressure wave as it goes down the tree. The results show that the cause to this is the wave reflection, as previously suggested, instead of non-linear interations as also previously suggested. It's also presented a computational ambient in which those results are computed.
\end{abstract}

\keywords{\em{Arterial trees, Pulsatile Flow, Pressure Peaking}}

\end{document}



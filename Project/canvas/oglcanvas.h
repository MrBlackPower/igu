﻿#ifndef OGLCANVAS_H
#define OGLCANVAS_H

#define EYE_X 0.00
#define EYE_Y 0.00
#define EYE_Z 10.0

#define LOOKAT_X 0.00
#define LOOKAT_Y 0.00
#define LOOKAT_Z 0.0

#define REFRESH_RATE 166.66666667

#include <QOpenGLWidget>
#include <QTimer>
#include <vector>
#include <functional>
#include "model/graphicobject.h"

using namespace std;

class OglCanvas : public QOpenGLWidget//, public SmartObject
{
public:
    OglCanvas(QString name, QWidget *parent = NULL);
    ~OglCanvas();

    void purgeGraphicObject();
    void setGraphicObject(GraphicObject* gObj);
    GraphicObject* getGraphicObject();
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

    void changeTool(GraphicSpace::GraphicTools tool);

    bool hasGraphicObject();

    OglCanvas* next_canvas;
    OglCanvas* previous_canvas;
    const unsigned int CID;

    static QPainter* getQPainter(int cid);
    static vector<OglCanvas*> getCanvases();
    static OglCanvas* getCanvas(int cid);

public slots:
    void repaint();

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    bool objLoaded;

private:
    QTimer* sync;
    GraphicObject* gObj;
    point eye;
    point lookAt;
    vector<float> lightPosition;
    vector<float> lightAmbient;
    vector<float> lightDiffuse;
    int height;
    int width;

    GraphicSpace::GraphicTools current_tool;

    //CANVAS ID
    static unsigned int CURR_CID;
    static OglCanvas* first_canvas;
    static OglCanvas* last_canvas;
};

#endif // OGLCANVAS_H

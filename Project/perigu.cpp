#include <QCoreApplication>

#include <QString>
#include <QProcess>
#include <QDir>

#include "helper/fileHelper.h"
#include "qprocessreader.h"

#include <vector>

using namespace std;

int             args_size;
vector<QString> buffer;
vector<QString> args;

QProcess* main_process;
QProcessReader main_reader;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc,argv);
    args_size = argc;

    cout << "[PERIGU] PARAMETERS READ " << args_size << endl;
    for(int i = 0; i < args_size; i++){

        args.push_back(argv[i]);

        cout << i << ":" << argv[i] << endl;
    }

    QString igu = QDir::currentPath() + QDir::separator() + "IGU";
    QString ingu = QDir::currentPath() + QDir::separator() + "/INGU";

    QString os = FileHelper::getOsName().data();

    cout << "[PERIGU] Is running on os(" << os.toStdString() << ")" << endl;

    if(os.contains("Windows")){
        //.EXE
        igu += ".exe";
        ingu += ".exe";
    }

    main_process = new QProcess();
    main_reader.add_process(main_process);

    QStringList aux;

    for(QString s : args)
        aux.append(s);

    if(args_size <= 1){
        cout << "[PERIGU] Opening IGU..." << endl;
        main_process->start(igu,aux);
    } else {
        if(args_size == 2 && (args[1] == "graphic" || args[1] == "GRAPHIC")){
            cout << "[PERIGU] Opening IGU..." << endl;
            main_process->start(igu,aux);
        }else{
            cout << "[PERIGU] Opening InGU..." << endl;
            main_process->start(ingu,aux);
        }
    }

    cout << "[PERIGU] Waiting for Started..." << endl;
    main_process->waitForStarted();

    cout << "[PERIGU] Waiting for Finished..." << endl;
    while (main_process->isOpen()){
        main_process->waitForFinished();
    }

    cout << "[PERIGU] Killing in the name of..." << endl;
    main_process->kill();

    QString output(main_process->readAllStandardOutput());

    cout << "[PERIGU] REMAINING OUTPUT: " << endl;

    cout << output.toStdString() << endl;

    QString error(main_process->readAllStandardError());

    cout << "[PERIGU] REMAINING ERROR: " << endl;

    cout << error.toStdString() << endl;


    return 0;
}

#include "generateweightedgraphwindow.h"
#include "ui_generateweightedgraphwindow.h"

GenerateWeightedGraphWindow::GenerateWeightedGraphWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GenerateWeightedGraphWindow)
{
    ui->setupUi(this);

    ui->ln_name->setText("defaull_graph");
    ui->chk_2D->setChecked(true);
    ui->spn_Nodes->setValue(100);
    ui->spn_Edges->setValue(100);
    ui->spn_x->setValue(10);
    ui->spn_y->setValue(10);
    ui->spn_z->setValue(10);
}

GenerateWeightedGraphWindow::~GenerateWeightedGraphWindow()
{
    delete ui;
}

void GenerateWeightedGraphWindow::on_chk_2D_clicked(bool checked)
{
    params.treeD = !checked;
    ui->chk_3D->setChecked(!checked);
}

void GenerateWeightedGraphWindow::on_chk_3D_clicked(bool checked)
{
    params.treeD = checked;
    ui->chk_2D->setChecked(!checked);
}

void GenerateWeightedGraphWindow::on_spn_Edges_editingFinished()
{
    params.m = ui->spn_Edges->value();
}

void GenerateWeightedGraphWindow::on_spn_Nodes_editingFinished()
{
    params.n = ui->spn_Nodes->value();
}

void GenerateWeightedGraphWindow::on_spn_x_editingFinished()
{
    params.m = ui->spn_x->value();
}

void GenerateWeightedGraphWindow::on_spn_y_editingFinished()
{
    params.m = ui->spn_y->value();
}

void GenerateWeightedGraphWindow::on_spn_z_editingFinished()
{
    params.m = ui->spn_z->value();
}

void GenerateWeightedGraphWindow::on_buttonBox_accepted()
{
    emit generateGraph(params);
    this->close();
}

void GenerateWeightedGraphWindow::on_buttonBox_rejected()
{
    this->close();
}

void GenerateWeightedGraphWindow::on_ln_name_textChanged(const QString &arg1)
{
    params.name = ui->ln_name->text();
}

void GenerateWeightedGraphWindow::on_chk_load_clicked(bool checked)
{
    params.load = checked;
    ui->chk_export->setChecked(!checked);
}

void GenerateWeightedGraphWindow::on_chk_export_clicked(bool checked)
{
    params.load = !checked;
    ui->chk_load->setChecked(!checked);
}

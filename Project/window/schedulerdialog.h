#ifndef SCHEDULERDIALOG_H
#define SCHEDULERDIALOG_H

#include "../helper/schedule.h"
#include "../helper/schedulerthread/schedulerthreadmanager.h"
#include <QDialog>
#include <QFileDialog>

enum SchedulerDialogPhase{
    LOAD_SCHEDULES,
    VIEW_SCHEDULES,
    RUN_SCHEDULES,
    FINISHED
};

namespace Ui {
class SchedulerDialog;
}

class SchedulerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SchedulerDialog(QWidget *parent = nullptr);
    ~SchedulerDialog();

    void setUi(SchedulerDialogPhase phase);

public slots:

    void setCanvas(GraphicObject* g);

    void iterate_object();

    void start_object();

    void pause_object();

    void purgeCanvas();

    void progress(unsigned int pct);

    void thread_finished();

signals:

    void setMainCanvas(GraphicObject* gObj);
    void purgeMainCanvas();

    void iterateMainCanvas();
    void startMainCanvas();
    void pauseMainCanvas();

    void setRightCanvas(GraphicObject* gObj);
    void purgeRightCanvas();

    void iterateRightCanvas();
    void startRightCanvas();
    void pauseRightCanvas();

private slots:
    void on_btn_load_clicked();

    void on_btn_filesearch_clicked();

    void on_ln_filename_textChanged(const QString &arg1);

    void on_btn_start_clicked();

    void on_chk_main_canvas_clicked();

    void on_chk_right_canvas_clicked();

    void on_btn_stop_clicked();

private:
    bool running;

    bool mainCanvas;

    QString schedule_filename;

    Ui::SchedulerDialog *ui;

    vector<Schedule> schedules;

    SchedulerThreadManager* scheduler_thread_manager;

    QThread* scheduler_thread;

    SchedulerDialogPhase current_phase;
};

#endif // SCHEDULERDIALOG_H

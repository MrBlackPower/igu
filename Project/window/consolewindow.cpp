#include "consolewindow.h"
#include "ui_consolewindow.h"

using namespace window;

ConsoleWindow::ConsoleWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConsoleWindow)
{
    ui->setupUi(this);
    text = "";
}

void ConsoleWindow::appendText(QString text){
    this->text.append(". ");
    this->text.append(text.append("\n"));
    ui->txtConsole->setText(this->text);
}

ConsoleWindow::~ConsoleWindow()
{
    delete ui;
}

void ConsoleWindow::on_btnRun_clicked()
{
    QString buffer = ui->lnComand->text();
    appendText(buffer);

    emit emitCommand(buffer.toStdString());

    ui->lnComand->clear();
}

#include "generateccotreewindow.h"
#include "ui_generateccotreewindow.h"

GenerateCCOTreeWindow::GenerateCCOTreeWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GenerateCCOTreeWindow)
{
    ui->setupUi(this);
    params.threeD = false;
    params.LegacyMode = false;
    params.ConstantFlow = true;
    params.FLANNEnabled = true;
    params.FLANNEnableFactor = 0.0;
    params.FLANNParam = 30;
    params.MaxThreads = 8;
    params.ThreadEnabled = true;
    params.TerminalCount = 100;
}

GenerateCCOTreeWindow::~GenerateCCOTreeWindow()
{
    delete ui;
}

void GenerateCCOTreeWindow::on_chk_2D_clicked()
{
    bool aux = ui->chk_2D->isChecked();

    params.threeD = !aux;
    ui->chk_3D->setChecked(!aux);
}

void GenerateCCOTreeWindow::on_chk_3D_clicked()
{
    bool aux = ui->chk_3D->isChecked();

    params.threeD = aux;
    ui->chk_2D->setChecked(!aux);
}

void GenerateCCOTreeWindow::on_spn_TerminalCount_editingFinished()
{
    params.TerminalCount = ui->spn_TerminalCount->value();
}

void GenerateCCOTreeWindow::on_chk_LegacyMode_clicked()
{
    params.LegacyMode = ui->chk_LegacyMode->isChecked();
}

void GenerateCCOTreeWindow::on_chk_ConstantFlow_clicked()
{
    params.ConstantFlow = ui->chk_ConstantFlow->isChecked();
}

void GenerateCCOTreeWindow::on_chk_ThreadEnabled_clicked()
{
    params.ConstantFlow = ui->chk_ConstantFlow->isChecked();
}

void GenerateCCOTreeWindow::on_chk_FLANNEnabled_clicked()
{
    params.ConstantFlow = ui->chk_FLANNEnabled->isChecked();
}

void GenerateCCOTreeWindow::on_spn_FLANN_editingFinished()
{
    params.FLANNParam = ui->spn_FLANN->value();
}

void GenerateCCOTreeWindow::on_sldr_FLANNEnableFactor_actionTriggered(int action)
{
    double pct = (double)ui->sldr_FLANNEnableFactor->value() / 100.0;
    params.FLANNEnableFactor = pct;
}

void GenerateCCOTreeWindow::on_spn_MaxThreads_editingFinished()
{
    params.MaxThreads = ui->spn_MaxThreads->value();
}

void GenerateCCOTreeWindow::on_buttonBox_accepted()
{
    emit generateTree(params);
}

void GenerateCCOTreeWindow::on_buttonBox_rejected()
{
    params.threeD = false;
    params.LegacyMode = false;
    params.ConstantFlow = true;
    params.FLANNEnabled = true;
    params.FLANNEnableFactor = 0.0;
    params.FLANNParam = 30;
    params.MaxThreads = 8;
    params.ThreadEnabled = true;
    params.TerminalCount = 100;
    params.load = false;
}

void GenerateCCOTreeWindow::on_chk_Load_clicked(bool checked)
{
    ui->chk_Export->setChecked(!checked);
    params.load = checked;
}

void GenerateCCOTreeWindow::on_chk_Export_clicked(bool checked)
{
    ui->chk_Load->setChecked(!checked);
    params.load = !checked;
}

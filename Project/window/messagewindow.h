#ifndef MESSAGEWINDOW_H
#define MESSAGEWINDOW_H

#include <QDialog>
#include <QString>
#include <iostream>
#include <string>

using namespace std;

namespace Ui {
class MessageWindow;
}

namespace window {
    class MessageWindow : public QDialog
    {
        Q_OBJECT

    public:
        explicit MessageWindow(QWidget *parent = 0);
        ~MessageWindow();

    public slots:
        void setMessage(string msg);

    private slots:
        void on_btnOK_clicked();

    private:
        Ui::MessageWindow *ui;
        string msg;
    };
}

#endif // MESSAGEWINDOW_H

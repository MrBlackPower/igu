#ifndef LOADWIZARD_H
#define LOADWIZARD_H

#include <QWizard>
#include <QFileDialog>
#include <QTimer>
#include <QEventLoop>
#include <QListWidgetItem>
#include "../helper/vtkfile.h"
#include "dialog/inputscalardialog.h"
#include "dialog/viewdatadialog.h"
#include "../model/arteryTree.h"
#include "../model/graphicobject.h"
#include "../model/graphic2d.h"
#include "../model/graphic2d/ktgraphic.h"
#include "../model/graphic2d/lfgraphic.h"
#include "../model/graphic2d/staticgraphic.h"
#include "../model/graphic3d.h"
#include "../model/graphic3d/ktgraphic3d.h"
#include "../model/graphic3d/staticgraphic3d.h"
#include "../model/graphic3d/lines3d.h"

#define STEP1 1
#define STEP2 2
#define STEP3 3
#define STEP4 4
#define STEP5 5

#define TIMEOUT 30000

namespace LoadWizardParams {
    enum Status{
        Initialized,
        Loaded,
        Morphed,
        Finished
    };

    struct Params{
        int last_page = 0;
        Status stat = Initialized;
        QString name = "";
        QString fileName = "";
        bool loaded = false;
        VTKFile file;
        VTKFile original;
    };
}

namespace Ui {
class LoadWizard;
}

using namespace std;
using namespace LoadWizardParams;

class LoadWizard : public QWizard
{
    Q_OBJECT

public:
    explicit LoadWizard(QWidget *parent = 0);
    ~LoadWizard();

signals:
    void registerMainCanvas(GraphicObject* obj);
    void registerSideCanvas(GraphicObject* obj);

private slots:
    void on_btn_browse_clicked();

    void on_ln_name_editingFinished();

    void on_ln_file_editingFinished();

    void on_LoadWizard_currentIdChanged(int id);

    void on_LoadWizard_finished(int result);

    void on_btn_load_clicked();

    void on_PointDataList_itemDoubleClicked(QListWidgetItem *item);

    void on_CellDataList_itemDoubleClicked(QListWidgetItem *item);

    void on_lstPointDataWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_lstCellDataListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_graphicCompatibilityListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_chkMainCanvas_clicked(bool checked);

    void on_chkSideCanvas_clicked(bool checked);

    void on_PointConversionList_itemDoubleClicked(QListWidgetItem *item);

    void on_CellConversionList_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::LoadWizard *ui;

    ViewDataDialog* viewDataDialog;

    GraphicObject* result;

    Params params;

    bool mainCanvas;

    bool sideCanvas;

    void clear();
};

#endif // LOADWIZARD_H

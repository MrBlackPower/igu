#include "graphicobjectswindow.h"
#include "ui_graphicobjectswindow.h"

GraphicObjectsWindow::GraphicObjectsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GraphicObjectsWindow)
{
    ui->setupUi(this);

    selected = false;
    selection = NULL;

    details = NULL;
}

GraphicObjectsWindow::~GraphicObjectsWindow()
{
    delete ui;
}

void GraphicObjectsWindow::on_btnSide_clicked()
{
    if(selected)
        emit setSideCanvas(selection);
}

void GraphicObjectsWindow::on_btnMain_clicked()
{
    if(selected)
        emit setMainCanvas(selection);
}

void GraphicObjectsWindow::on_btnSet_clicked()
{
    int id = 0;
    id = ui->spnCID->text().toInt();

    switch (id) {
    case 0://MAIN CANVAS
        if(selected)
            emit setMainCanvas(selection);
        break;
    case 1://SIDE CANVAS
        if(selected)
            emit setSideCanvas(selection);
        break;
    default:
        if(selected){
            OglCanvas* canvas = OglCanvas::getCanvas(id);
            if(canvas != NULL){
                canvas->setGraphicObject(selection);
            }
        }

        break;
    }
}

void GraphicObjectsWindow::on_btnOK_clicked()
{
    destroy();
}

void GraphicObjectsWindow::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    selection = GraphicObject::getGraphic(item->text(0).toInt());

    if(selection != NULL)
        selected = true;
}

void GraphicObjectsWindow::on_btnDeatils_clicked()
{
    if(selected){
        details = new GraphicObjectDetailsWindow(selection,this);
        details->show();
    }
}

void GraphicObjectsWindow::on_btnUpdate_clicked()
{
    ui->treeWidget->populate();
}

void GraphicObjectsWindow::on_btnDelete_clicked()
{
    if(selected){
        emit purgeGraphicObject(selection);

        selection->deleteLater();
        on_btnUpdate_clicked();
    }
}

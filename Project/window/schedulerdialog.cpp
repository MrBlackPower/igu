#include "schedulerdialog.h"
#include "ui_schedulerdialog.h"

SchedulerDialog::SchedulerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SchedulerDialog)
{
    ui->setupUi(this);

    scheduler_thread_manager = NULL;
    scheduler_thread = NULL;


    current_phase = LOAD_SCHEDULES;
    running = false;
    mainCanvas = true;

    setUi(current_phase);
}

void SchedulerDialog::setUi(SchedulerDialogPhase phase){
    switch (phase) {
    case LOAD_SCHEDULES:
        ui->ln_filename->setEnabled(true);
        ui->btn_filesearch->setEnabled(true);
        ui->btn_load->setEnabled(true);
        ui->btn_stop->setEnabled(false);
        ui->chk_main_canvas->setEnabled(false);
        ui->chk_right_canvas->setEnabled(false);
        ui->schedules_widget->setEnabled(false);
        ui->btn_start->setEnabled(false);
        ui->progress_bar->setEnabled(false);
        break;
    case VIEW_SCHEDULES:
        ui->ln_filename->setEnabled(false);
        ui->btn_filesearch->setEnabled(false);
        ui->btn_load->setEnabled(false);
        ui->btn_stop->setEnabled(false);
        ui->chk_main_canvas->setEnabled(true);
        ui->chk_right_canvas->setEnabled(true);
        ui->schedules_widget->setEnabled(true);
        ui->btn_start->setEnabled(true);
        ui->progress_bar->setEnabled(false);
        break;
    case RUN_SCHEDULES:
        ui->ln_filename->setEnabled(false);
        ui->btn_filesearch->setEnabled(false);
        ui->btn_load->setEnabled(false);
        ui->btn_stop->setEnabled(true);
        ui->chk_main_canvas->setEnabled(false);
        ui->chk_right_canvas->setEnabled(false);
        ui->schedules_widget->setEnabled(false);
        ui->btn_start->setEnabled(false);
        ui->progress_bar->setEnabled(true);
        break;
    case FINISHED:
        ui->ln_filename->setEnabled(false);
        ui->btn_filesearch->setEnabled(false);
        ui->btn_load->setEnabled(false);
        ui->btn_stop->setEnabled(false);
        ui->chk_main_canvas->setEnabled(false);
        ui->chk_right_canvas->setEnabled(false);
        ui->schedules_widget->setEnabled(false);
        ui->btn_start->setEnabled(false);
        ui->progress_bar->setEnabled(true);
        break;
    }
}
void SchedulerDialog::setCanvas(GraphicObject* g){
    if(mainCanvas){
        emit setMainCanvas(g);
    } else {
        emit setRightCanvas(g);
    }
}

void SchedulerDialog::iterate_object(){
    if(mainCanvas){
        emit iterateMainCanvas();
    } else {
        emit iterateRightCanvas();
    }
}

void SchedulerDialog::start_object(){
    if(mainCanvas){
        emit startMainCanvas();
    } else {
        emit startRightCanvas();
    }
}

void SchedulerDialog::pause_object(){
    if(mainCanvas){
        emit pauseMainCanvas();
    } else {
        emit pauseRightCanvas();
    }
}

void SchedulerDialog::purgeCanvas(){
    if(mainCanvas){
        emit purgeMainCanvas();
    } else {
        emit purgeRightCanvas();
    }
}


SchedulerDialog::~SchedulerDialog()
{
    delete ui;
}

void SchedulerDialog::on_btn_load_clicked()
{
    Schedule::fromXml(&schedules,schedule_filename);

    if(schedules.empty())
        return;

    current_phase = VIEW_SCHEDULES;

    setUi(current_phase);

    ui->schedules_widget->populate(&schedules);
}

void SchedulerDialog::on_btn_filesearch_clicked()
{
    schedule_filename = QFileDialog::getOpenFileName(this, tr("Open Schedule XML File"),"/home/");
    ui->ln_filename->clear();
    ui->ln_filename->insert(schedule_filename);
}

void SchedulerDialog::on_ln_filename_textChanged(const QString &arg1)
{
    schedule_filename = ui->ln_filename->text();
}

void SchedulerDialog::on_btn_start_clicked()
{

    if(!running){
        running = true;
        scheduler_thread = new QThread(this);
        scheduler_thread_manager = new SchedulerThreadManager(schedules,"SMART SCHEDULE MANAGER");
        scheduler_thread_manager->moveToThread(scheduler_thread);

        connect(scheduler_thread, &QThread::finished, scheduler_thread_manager, &QObject::deleteLater);
        connect(scheduler_thread, &QThread::started, scheduler_thread_manager, &SchedulerThreadManager::process);

        connect(scheduler_thread_manager, &SchedulerThreadManager::purgeCanvas, this, &SchedulerDialog::purgeCanvas);
        connect(scheduler_thread_manager, &SchedulerThreadManager::start_object, this, &SchedulerDialog::start_object);
        connect(scheduler_thread_manager, &SchedulerThreadManager::pause_object, this, &SchedulerDialog::pause_object);
        connect(scheduler_thread_manager, &SchedulerThreadManager::iterate_object, this, &SchedulerDialog::iterate_object);
        connect(scheduler_thread_manager, &SchedulerThreadManager::setCanvas, this, &SchedulerDialog::setCanvas);
        connect(scheduler_thread_manager, &SchedulerThreadManager::pct, this, &SchedulerDialog::progress);


        scheduler_thread->start();

        current_phase = RUN_SCHEDULES;
        setUi(current_phase);

    }
}

void SchedulerDialog::on_chk_main_canvas_clicked()
{
    mainCanvas = ui->chk_main_canvas->isChecked();

    ui->chk_right_canvas->setChecked(!mainCanvas);
}

void SchedulerDialog::on_chk_right_canvas_clicked()
{
    mainCanvas = !ui->chk_right_canvas->isChecked();

    ui->chk_main_canvas->setChecked(mainCanvas);
}

void SchedulerDialog::progress(unsigned int pct){
    if(pct >= 100)
        pct = 100;

    ui->progress_bar->setValue(pct);
}

void SchedulerDialog::thread_finished(){
    running = false;
    current_phase = FINISHED;
    setUi(current_phase);

    scheduler_thread->deleteLater();
    scheduler_thread_manager->deleteLater();

    scheduler_thread = NULL;
    scheduler_thread_manager = NULL;
}

void SchedulerDialog::on_btn_stop_clicked()
{
    scheduler_thread->quit();

    scheduler_thread->deleteLater();
    scheduler_thread_manager->deleteLater();

    scheduler_thread = NULL;
    scheduler_thread_manager = NULL;
}

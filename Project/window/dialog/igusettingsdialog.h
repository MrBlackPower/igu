#ifndef IGUSETTINGSDIALOG_H
#define IGUSETTINGSDIALOG_H

#include <QDialog>

#include <vector>

#include "../../helper/systemsettingshelper.h"
#include "../../helper/mathHelper.h"
#include "../../model/graphicobject.h"
#include "../../model/smartobject.h"

#define DEFAULT_CONFIG_FILE "config.igu"

namespace Ui {
class IGUSettingsDialog;
}

using namespace std;

class IGUSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit IGUSettingsDialog(QWidget *parent = nullptr);
    ~IGUSettingsDialog();

    vector<QString> print();

    void print(vector<QString>* aux);

    static void update_config(SystemSettingsHelper config);

signals:
    void setSettings();

    void pushSettings(SystemSettingsHelper config);

    void printConfig(vector<QString> raw, QString filename = DEFAULT_CONFIG_FILE);

private slots:
    void on_btn_refresh_clicked();

    void on_btnBox_accepted();

    void on_chk_parallelizing_stateChanged(int arg1);

    void on_cbox_paradigma_currentTextChanged(const QString &arg1);

    void on_spn_number_of_threads_editingFinished();

    void on_spn_thread_blocks_editingFinished();

    void on_spn_threads_in_blocks_editingFinished();

    void on_spn_angles_per_pixel_editingFinished();

    void on_spn_scalar_step_per_pixel_editingFinished();

    void on_spn_move_per_pixel_editingFinished();

    void on_spn_z_offset_valueChanged(double arg1);

private:
    void updateDialogs();

    SystemSettingsHelper config;

    vector<QString> raw;

    Ui::IGUSettingsDialog *ui;
};

#endif // IGUSETTINGSDIALOG_H

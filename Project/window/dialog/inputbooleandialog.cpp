#include "inputbooleandialog.h"
#include "ui_inputbooleandialog.h"

InputBooleanDialog::InputBooleanDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InputBooleanDialog)
{
    ui->setupUi(this);
    set = true;
}

InputBooleanDialog::~InputBooleanDialog()
{
    delete ui;
}

bool InputBooleanDialog::getBoolean(){
    return value;
}

void InputBooleanDialog::setBoolean(QString name){
    ui->chkBoolean->setText(name);
}


void InputBooleanDialog::on_buttonBox_accepted()
{
    value = ui->chkBoolean->isChecked();
    set = true;

    emit pushBoolean(name, value);
    emit booleanSet();
}

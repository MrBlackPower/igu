#include "viewdatadialog.h"
#include "ui_viewdatadialog.h"

ViewDataDialog::ViewDataDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ViewDataDialog)
{
    ui->setupUi(this);
}

ViewDataDialog::~ViewDataDialog()
{
    delete ui;
}

void ViewDataDialog::fillCellData(VTKFile file, QString field){
    ui->dataListWidget->clear();
    ui->lblName->setText(field);
    vector<QString> data = file.getCellData(field);

    for(int i = 0; i < data.size(); i++){
        ui->dataListWidget->addItem(data[i]);
    }
}

void ViewDataDialog::fillPointData(VTKFile file, QString field){
    if(field == "POINT_COORDINATES")
        return;

    ui->dataListWidget->clear();
    ui->lblName->setText(field);
    vector<QString> data = file.getPointData(field);

    for(int i = 0; i < data.size(); i++){
        ui->dataListWidget->addItem(data[i]);
    }
}

void ViewDataDialog::on_btnOk_clicked()
{
    hide();
}

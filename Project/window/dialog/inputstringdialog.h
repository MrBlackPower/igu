#ifndef INPUTSTRINGDIALOG_H
#define INPUTSTRINGDIALOG_H

#include <QDialog>
#include <QString>

namespace Ui {
class InputStringDialog;
}

class InputStringDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InputStringDialog(QString name, QString current_value, QWidget *parent = nullptr);
    ~InputStringDialog();

    QString getString();

    bool Set();

    void updateInterface();

signals:
    void stringSet();

    void pushString(QString string);

    void cancelled();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_ln_string_textChanged(const QString &arg1);

private:
    Ui::InputStringDialog *ui;

    QString name;

    QString string;

    bool set;
};

#endif // INPUTSTRINGDIALOG_H

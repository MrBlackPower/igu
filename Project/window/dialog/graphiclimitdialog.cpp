#include "graphiclimitdialog.h"
#include "ui_graphiclimitdialog.h"

GraphicLimitDialog::GraphicLimitDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GraphicLimitDialog)
{
    set = false;
    max = point();
    min = point();

    ui->setupUi(this);
    updateInterface();
}

GraphicLimitDialog::GraphicLimitDialog(point max, point min, QWidget *parent):
    QDialog(parent),
    ui(new Ui::GraphicLimitDialog)
{
    set = false;
    this->max = max;
    this->min = min;

    ui->setupUi(this);
    updateInterface();
}

GraphicLimitDialog::~GraphicLimitDialog()
{
    delete ui;
}

void GraphicLimitDialog::getLimit(point* max, point* min){
    max->operator=(this->max);
    min->operator=(this->min);
}

bool GraphicLimitDialog::Set(){
    return set;
}

void GraphicLimitDialog::updateInterface(){
    ui->spn_max_x->setValue(max.X());
    ui->spn_max_y->setValue(max.Y());
    ui->spn_max_z->setValue(max.Z());

    ui->spn_min_x->setValue(min.X());
    ui->spn_min_y->setValue(min.Y());
    ui->spn_min_z->setValue(min.Z());
}

void GraphicLimitDialog::on_spn_max_x_valueChanged(double arg1)
{
    max.X(arg1);
}

void GraphicLimitDialog::on_spn_max_y_valueChanged(double arg1)
{
    max.Y(arg1);
}

void GraphicLimitDialog::on_spn_max_z_valueChanged(double arg1)
{
    max.Z(arg1);
}

void GraphicLimitDialog::on_spn_min_x_valueChanged(double arg1)
{
    min.X(arg1);
}

void GraphicLimitDialog::on_spn_min_y_valueChanged(double arg1)
{
    min.Y(arg1);
}

void GraphicLimitDialog::on_spn_min_z_valueChanged(double arg1)
{
    min.Z(arg1);
}

void GraphicLimitDialog::on_buttonBox_accepted()
{
    set = true;

    emit limitSet();
    emit pushLimit(max,min);
}

void GraphicLimitDialog::on_buttonBox_rejected()
{
    emit cancelled();
}

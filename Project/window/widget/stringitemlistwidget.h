#ifndef POINTCONVERSIONLISTWIDGET_H
#define POINTCONVERSIONLISTWIDGET_H

#include <QListWidget>
#include <QString>
#include <vector>

using namespace std;

class StringItemListWidget : public QListWidget
{
public:
    StringItemListWidget(QWidget *parent);

    vector<QString> getItems();

public slots:

    void addItem(QString conv);

    void addItem(vector<QString> conv);

    void flush();

    void remove(int row);

private:
    vector<QString> list;
};

#endif // POINTCONVERSIONLISTWIDGET_H

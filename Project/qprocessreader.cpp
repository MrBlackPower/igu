#include "qprocessreader.h"

QProcessReader::QProcessReader()
{
    n = 0;
}

bool QProcessReader::add_process(QProcess* p){
    process.push_back(QPointer<QProcess>(p));

    QObject::connect(p,SIGNAL(readyReadStandardOutput()),this,SLOT(read_message()));
    QObject::connect(p,SIGNAL(readyReadStandardError()),this,SLOT(read_error()));

    n++;

    return true;
}

int QProcessReader::size(){
    return n;
}

void QProcessReader::read_message(){
    for(QPointer<QProcess> p : process){
        QByteArray array = p->readAllStandardOutput();

        if(!array.isEmpty())
            cout << array.data() << endl;
    }
}

void QProcessReader::read_error(){
    for(QPointer<QProcess> p : process){
        QByteArray array = p->readAllStandardError();

        if(!array.isEmpty())
            cout << array.data() << endl;
    }
}

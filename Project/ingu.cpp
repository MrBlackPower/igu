#include <QCoreApplication>

#include <QString>
#include <QProcess>
#include <QDir>
#include <QDateTime>

#include <vector>

#include "helper/fileHelper.h"

#include "model/wise/wiseconsole.h"

#define VERSION "1.1"

using namespace std;


int             args_size;
vector<QString> buffer;
vector<QString> args;

int main(int argc, char *argv[])
{

    WiseConsole main_console;
    QCoreApplication a(argc,argv);
    args_size = argc;
    QString sum;

    cout << "[InGU] STARTED InGU" << endl;


    if(args_size > 1){
        sum += QString::asprintf("%s",argv[1]);
        for(int i = 2; i < args_size; i++){
            args.push_back(argv[i]);

            sum += QString::asprintf(" %s",argv[i]);
        }
    }

    if(!sum.isEmpty())
    {
        main_console.run_cmd(sum);
        cout << sum.toStdString() << endl;
    }
    else
    {
        main_console.run_cmd(QString::asprintf("element list"));
    }


    cout << "[InGU] FINISHED InGU" << endl<< endl;

    return 0;
}

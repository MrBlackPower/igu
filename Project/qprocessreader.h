#ifndef QPROCESSREADER_H
#define QPROCESSREADER_H

#include <QObject>
#include <QPointer>
#include <QProcess>
#include <vector>
#include <iostream>

using namespace std;

class QProcessReader : QObject
{
Q_OBJECT
public:
    QProcessReader();

    bool add_process(QProcess* p);

    int size();

public slots:

    void read_message();
    void read_error();

private:
    int n;

    vector<QPointer<QProcess>> process;
};

#endif // QPROCESSREADER_H
